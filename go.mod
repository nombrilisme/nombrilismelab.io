module gitlab.com/nombrilisme/hugo

go 1.20

require (
	gitlab.com/nombrilisme/linux v0.0.0-20230517153308-0f7657072e2e // indirect
	gitlab.com/nombrilisme/mathjax v0.0.0-20230517153435-6bc8ad4571ab // indirect
	gitlab.com/nombrilisme/physics v0.0.0-20230518175832-511c2115f922 // indirect
	gitlab.com/nombrilisme/zw v0.0.0-20230517183528-f0b3ef5ad508 // indirect
)
