---
title: notes
type: domain
cascade:
  #colour: giraffe
---

- {{< domain physics >}}
- {{< domain linux >}}
- <li class="domain"><a href="https://stats103.com">statistics</a></li>
- {{< domain math >}}
- {{< domain 中文 >}}
{.domainlist}

- {{< domain old >}}
{.domainlist}
