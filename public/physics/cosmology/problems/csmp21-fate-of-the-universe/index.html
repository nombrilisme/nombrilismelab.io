<html>
<head>
<meta content='text/html; charset=UTF-8' name='Content-Type'>
<meta name='viewport' content='width=device-width'>
<link rel='stylesheet' href='../../../../css/main.css'>
</head><p>I&rsquo;ll start with a little background: in an isotropic, homogeneous universe, the
<em>friedmann equation</em> describes the evolution of the scale factor:</p>


	<div class="eq">
	££
	\begin{equation}
	  H⁽2⁾ = (÷{˙a}a)⁽2⁾ = ÷13 8πGρ - ÷{k⁽2⁾ c⁽2⁾}{a⁽2⁾} 
	\end{equation}
	££
	</div>


<p>(I&rsquo;ve absorbed the Λ term into ρ).</p>
<p>The density ˝ρ˝ in ˝(1)˝ is really a <em>sum</em> of the densities of known
constituents, multiplied by their (potentially power law) dependencies on scale
factor ˝a˝. In the most general sense, you can express ˝ρ˝ as an expansion in
inverse powers of ˝a˝:</p>


	<div class="eq">
	££
	\begin{equation}
	  ρ = ∑{n=-∞…∞} ρ₍n₎ a⁽-n⁾ 
	\end{equation}
	££
	</div>


<p>In practice, though, this sum only has a finite number of terms because the
universe is pretty much always dominated by a few forms of energy.˝⁽[1]⁾˝ Matter
density corresponds to the case ˝n=3˝, since ˝ρ₍m₎˝ scales as ˝a⁽-3⁾˝. Radiation
density ˝ρ₍r₎˝ scales as ˝a⁽-4⁾˝.</p>
<p>You can derive a more specific relation for the dependency of ˝ρ˝ on ˝a˝ by
using the fact that in a perfect fluid, the relation between ˝P˝ and ˝ρ˝ is
given by</p>


	<div class="eq">
	££
	\begin{equation}
	  w = ÷Pρ 
	\end{equation}
	££
	</div>


<p>where ˝w˝ is an equation of state parameter. [units where ˝c = 1˝. if not this
equation should be ˝w = P / ρc⁽2⁾˝]. Now, if you take the fluid conservation law
for an expanding universe</p>


	<div class="eq">
	££
	\begin{equation}
	  𝝏₍t₎ρ &#43; (˙a/a) (3ρ &#43; 3P) = 0 
	\end{equation}
	££
	</div>


<p>and substitute ˝w = P/ρ˝, and then integrate the equation, you get the following
relation for the scaling of ˝ρ₍i₎˝ with ˝a˝:</p>


	<div class="eq">
	££
	\begin{equation}
	  ρ₍i₎(a) ∝ exp(-3 ∫₍a₎ da&#39;ͺ÷1{a&#39;} (1 &#43; w₍i₎(a&#39;))) 
	\end{equation}
	££
	</div>


<p>If the e.o.s parameter ˝w₍i₎˝ is time-independent (curious &ndash; when would it not
be?), then this simplifies to</p>


	<div class="eq">
	££
	\begin{equation}
	  ρ₍i₎(a) ∝ a⁽-3(1&#43;w₍i₎)⁾ 
	\end{equation}
	££
	</div>


<p>For matter ˝w = 0˝ and for radiation ˝w = 1/3˝.</p>
<p>Anyway, the first thing you need to do for this problem is rewrite the friedmann
equation in a dimensionless form, in terms of only the density parameters of the
different constituents ˝Ω₍i₎˝ and their scaling relationships with ˝a˝. The
density parameter ˝Ω₍i₎˝ is defined as the ratio of the observed density of that
constituent, ˝ρ₍i₎˝, and the critical density, ˝ρ₍c₎ = 3H₍0₎⁽2⁾/8πG˝, so</p>


	<div class="eq">
	££
	\begin{equation}
	  Ω₍i₎ = ÷{ρ₍i₎}{ρ₍c₎} = ÷{8πGρ₍i₎}{3H₍0₎⁽2⁾} 
	\end{equation}
	££
	</div>


<p>Next, divide the friedmann equation by ˝H₍0₎⁽2⁾˝</p>


	<div class="eq">
	££
	\begin{equation}
	  ÷{H⁽2⁾}{H₍0₎⁽2⁾} = ÷{8πGρ}{3H₍0₎⁽2⁾} - ÷{kc⁽2⁾}{a⁽2⁾H₍0₎⁽2⁾} 
	\end{equation}
	££
	</div>


<p>and note that the first term on the RHS is</p>


	<div class="eq">
	££
	\begin{equation}
	  Ω₍total₎ = ∑{i} Ω₍i₎ a⁽-3(1&#43;w₍i₎)⁾ 
	\end{equation}
	££
	</div>


<p>and that the second term can be written ˝(1-Ω₍total₎)a⁽-2⁾˝, or just
˝Ω₍k₎a⁽-2⁾˝, where ˝Ω₍k₎˝ is a density parameter for curvature. When you put it
all together,</p>


	<div class="eq">
	££
	\begin{equation}
	  ÷{H⁽2⁾}{H₍0₎⁽2⁾} = ∑{i} Ω₍i₎ a⁽-3(1&#43;w₍i₎)⁾ &#43; Ω₍k₎a⁽-2⁾ 
	\end{equation}
	££
	</div>


<p>or, using ˝H = ˙a/a˝,</p>


	<div class="eq">
	££
	\begin{equation}
	  d₍t₎a = a H₍0₎ ( ∑{i} Ω₍i₎ a⁽-3(1&#43;w₍i₎)⁾ &#43; Ω₍k₎a⁽-2⁾ )⁽1/2⁾ 
	\end{equation}
	££
	</div>


<p>This is a 1st order differential equation for ˝a(t)˝.</p>
<p>The problem first asks you to consider a universe with matter and dark energy,
with different values of ˝w₍de₎ = [-1.2, -1, -0.8]˝. The relevant equation is</p>


	<div class="eq">
	££
	\begin{equation}
	  d₍t₎a = a H₍0₎ ( Ω₍m₎a⁽-3⁾ &#43; Ω₍de₎a⁽-3(1&#43;w₍de₎)⁾ )⁽1/2⁾ 
	\end{equation}
	££
	</div>


<p>with values ˝Ω₍m₎ = 0.25˝ and ˝Ω₍de₎ = 0.75˝. I wrote a quick ODE integrator for
this problem using the RK2 algorithm with time step dt = 0.001 and a starting
value of a = 0.01 (I learned the hard way that the starting value of ˝a˝ needs
to be bigger than the time step &ndash; if not the integrator will diverge pretty
much immediately).</p>
<p>Here are the results:</p>



<div class="fig">
<img src="../figs/fate1.png" width="420">
</div>

<ul class="caption">
<li>FIGURE 211. plot of ˝a(t)˝ for universes with ˝Ω₍m₎ = 0.25˝, ˝Ω₍de₎ = 0.75˝,
and ˝w ∈ [-1.2, -1.0, -0.8]˝.</li>
</ul>
<p>It&rsquo;s clear that all three cases, the universe is expanding˝^❄˝, ne&rsquo;er to return,
and also the expansion is accelerating.</p>
<p>Case ˝w = -1.2˝: the BIG RIP. In this (hypothetical) scenario, the expansion of
the universe is accelerating, and the universe will become dominated by dark
energy (specifically: phantom energy). The fact that ˝w &lt; -1˝ will cause the
dark energy density to <em>increase</em>, and continue increasing, as the universe
expands. As time progresses, the distances of interactions between objects will
tend to decrease, until eventually the horizon shrinks below the size of
structures themselves, at which point they become &ldquo;ripped apart&rdquo; as they won&rsquo;t
any longer be able to interact via fundamental forces. At the final singularity,
all distances will be infinite.˝⁽[2]⁾˝</p>
<p>Case ˝-1 &lt; w &lt; 0˝: in this scenario the expansion of the universe is
accelerating, but the dark energy density will <em>decrease</em> with time, so there
won&rsquo;t be a big rip. This is still a dark energy dominated universe though, since
matter scales as ˝a⁽-3⁾˝ but dark energy scales as ˝a⁽-6⁾˝.</p>
<p>Case ˝w = -1˝: in this scenario the expansion of the universe is accelerating
with a cosmological constant as used in the einstein equations. Will become
dark energy dominated.</p>
<p>Predicted age of the universe:</p>
<ul>
<li>for w = -1.2: ∼14.60 billion years</li>
<li>for w = -1.0: ∼14.14 billion years</li>
<li>for w = -0.8: ∼13.56 billion years</li>
</ul>
<p>Here is a more zoomed in version of figure 211, focusing on the region up to ˝a =
1˝:</p>



<div class="fig">
<img src="../figs/fate2.png" width="420">
</div>

<ul class="caption">
<li>FIGURE 212. zoomed in plot of ˝a(t)˝ for universes with ˝Ω₍m₎ = 0.25˝, ˝Ω₍de₎
= 0.75˝, and ˝w ∈ [-1.2, -1.0, -0.8]˝.</li>
</ul>
<p>Next: the closed universe scenario:</p>



<div class="fig">
<img src="../figs/fate3.png" width="480">
</div>

<ul class="caption">
<li>FIGURE 213. plot of ˝a(t)˝ for a closed universe with ˝Ω₍m₎ = 1.25˝, ˝Ω₍de₎ =
0˝, and ˝Ω₍k₎ = -0.25˝.</li>
</ul>
<p>In this scenario the matter density is strong enough that gravity will
ultimately overcome the force of expansion from the big bang. This will cause
the expansion to eventually stop and reverse direction (i.e.  contract). The
scale factor will start decreasing back to zero and universe will end up
collapsing to a point with infinite temperature. The &ldquo;big crunch&rdquo;, as they say.</p>
<p>(I couldn&rsquo;t get the integrator to demonstrate the collapse of ˝a(t)˝ back down
to zero, but supposedly this is because of numerical divergences/infinities. The
maximum I could get this program to run for before getting a div-by-zero error
was ∼80b years, at which point the curve was still increasing (though at a
decreasing rate). I&rsquo;m sure that some rescaling of variables could fix this).</p>
<p>According to this model, the age of the universe is 8.87 billion years old.</p>
<p>REFERENCES:</p>
<p>(1) R. Nemiroff, B. Patla, Adventures in Friedmann cosmology: a detailed
expansion of the cosmological Friedmann equations, Michigan Technological
University, 2007.</p>
<p>(2) <em>Big Rip</em>, wikipedia <a href="https://en.wikipedia.org/wiki/Big_Rip">page</a>.</p>
<p>˝^*˝ although there is a strong rebuttal to this notion in one of the opening
scenes of Annie Hall (1977): &ldquo;Brooklyn is not expanding!&rdquo;</p>
<p>Code: <a href="https://github.com/knbe/fate">https://github.com/knbe/fate</a></p>
</html>
