---
title: manjaro
type: term
domain: linux
back: "../"
---

# CLI commands

- change directory:

		cd <dirname>

- go backwards a directory:


		cd ../


- make directory:


		mkdir <dirname>


- print all files and directories: 


		ls


- delete file:


		rm <filename>


- delete a directory:


		rm -r <dirname>


- delete a write-protected directory:


		rm -rf <dirname>


- move a file/directory to a different location:


		mv <current path to file> <desired path>


- here's a [CLI cheatsheet](https://cheatography.com/davechild/cheat-sheets/linux-command-line/pdf/)

# file explorer

- install `ranger`
- [archwiki page][13] 
- open ranger in current directory: `ranger`
- open file with specific application: hit r then type app name
- show hidden files: hit backspace
- another file explorer: `pcmanfm`

# configuring i3

- i3 config file located in .i3 dir in the home dir

# packages

- packages from the [official archlinux package repository][6] can be downloaded with the [pacman][7] package manager
- to install a package from the official arch repository:


		sudo pacman -S <packagename>


- delete a package:


		sudo pacman -Rns <packagename>


- search for a package (to see if it's installed):


		sudo pacman -Qs <packagename>


- for most packages typing `packagename -h` or `packagename --help` will print its CLI options

- another common package repository for arch is the AUR (arch user repository); packages from here can be installed with `yay`:


		yay -S <packagename>


- delete an AUR package:


		yay -Rns <packagename>


# Xresources 

- .Xresources file: used to set terminal colours, fonts, cursor style
- [archwiki page][8]
- make an .Xresources file:


		touch ~/.Xresources


- to enact a change in the .Xresources file:


		xrdb ~/.Xresources


- should link colors in the i3 config to those in .Xresources

# key mapping with xmodmap

- [archwiki page][1] (found this pretty cumbersome)
- install `xorg-xmodmap` 
- create a file with a list of the current keymaps:

 
		xmodmap -pke ~/.Xmodmap 


- change key mappings as you please
- to find out the keycode of a particular key, install `xorg-xev`, then run
  `xev` in a terminal. this should open a new window and every time you press 
  a key it should print information about that event, including the keycode
- then you can change the entry for that keycode in `~/.Xmodmap` 
- to enact changes to the .Xmodmap file:

 
		xmodmap ~/.Xmodmap 


# extracting a tar file

 
		tar xvf [package] 


# installing a package from a tarball

 
		sudo pacman -U package.tar.gz 


# mounting an external drive

- run `sudo fdisk -l` to see what the hard drive is called, should be
  `/dev/sdXY` or something
- make a directory for it (called a "mountpoint"?) e.g. `mkdir /mnt/my_ssd`
- mount the drive:

 
		sudo mount /dev/sdXY /mnt/my_ssd 


- to unmount:

 
		sudo umount /dev/sdXY 


- if it says "target is busy", you can use `fuser` to kill all processes that
  are currently using the mounted file system:

 
		fuser -cuk /mnt/my_ssd 


# system upgrades

- do this every so often: 

 
		sudo pacman -Syu 


- sometimes if you haven't upgraded in a while Syu fails, citing some issue
  with keyrings or something. update the keyring (below)
- sometimes there is a glibc error, e.g. version `GLIBC_2.33' not found, this
  was fixed by upgrading the system

# update the keyring(s)

 
		sudo pacman -Sy archlinux-keyring 



# backlight

- install `xorg-xbacklight`

 
		xbacklight -set <number between 0 and 100> 


# timezones

- check current status:

 
		timedatectl status 


- print list of timezones:

 
		timedatectl list-timezones 


- connect to a timezone:

 
		timedatectl set-timezone <Zone/SubZone> 


# GTK config 

- this is a pain
- use `lxappearance` to control GTK themes:

 
		sudo pacman -S lxappearance 


- lots of themes for download at official repositories
- edit GTK themes in `/usr/share/themes/<themedirectory>/gtk.css`
- an annoying warning that keeps appearing: `Gtk-WARNING: unable to locate
  theme engine ... "murrine"`. this fixes it:

 
		sudo pacman -S gtk-engine-murrine 


# setxkbmap: setting a compose key for special characters  

- can define a compose key with `setxkbmap` which allows typing nonstandard
  characters (diacritics, the £ sign, etc)
- see [the wikipedia page][2] for a table of mappings
- the file `/usr/share/X11/xkb/rules/base.lst` has a list of accepted mappings
- e.g. set the right ctrl key as the compose key:

 
		setxkbmap -option compose:rctrl 


- put this command in the i3 config to make permanent

# fcitx: input for languages with non-roman characters

- install `fcitx`
- install an input method engine: the best one is `fcitx-libpinyin` (supports
  both pīnyīn and 注意)
- install the `fcitx-im` package (an input method module), supposedly gives a
  better experience in Qt programs
- install fonts. for Pan-CJK fonts the package is
  `adobe-source-han-sans-otc-fonts` (alternatively serif)
- in `.xinitrc` add the following before the exec i3 line:

 
		export GTK_IM_MODULE=fcitx 
		export QT_IM_MODULE=fcitx 
		export XMODIFIERS=@im=fcitx 


- add `fcitx &` to the list of autostart applications in the i3 config file
- install a configuration tool. for a GTK UI the package is `fcitx-configtool`
- then run `fcitx-config-gtk3` to open the UI
- configure the input methods
- restart the session. it should work

# LaTeX

- install `texlive-most`
- for chinese fonts get `texlive-langextra` (use `xelatex` as the pdf engine)

# pdf viewers

- [evince][9] and [zathura][10] are two good pdf viewers, can be installed with pacman

# word processor

- [libreoffice][11]

# image editing

- [GIMP][12]

# fonts

- to see font list: `fc-list`

# GLFW 

- download source [here][3]
- open with xarchiver. click Action -> Extract
- then:

 
		cd <glfw-root-dir> 
		cmake .  
		sudo make install 


# vulkan

- packages to install:
	+ `vulkan-icd-loader`
	+ `vulkan-intel` (depends on graphics card)
	+ `vulkan-headers`
	+ `vulkan-validation-layers`
	+ `vulkan-tools`

[1]: https://wiki.archlinux.org/title/Xmodmap
[2]: https://en.wikipedia.org/wiki/Compose_key
[3]: https://www.glfw.org/download.html
[4]: https://unity3d.com/get-unity/download
[5]: https://rodrigoribeiro.site/2020/08/09/mirroring-ipad-iphone-screen-on-linux/
[6]: https://wiki.archlinux.org/title/Official_repositories
[7]: https://wiki.archlinux.org/title/Pacman
[8]: https://wiki.archlinux.org/title/X_resources
[9]: https://archlinux.org/packages/extra/x86_64/evince/
[10]: https://wiki.archlinux.org/title/Zathura
[11]: https://wiki.archlinux.org/title/LibreOffice
[12]: https://wiki.archlinux.org/title/GIMP
[13]: https://wiki.archlinux.org/title/Ranger
