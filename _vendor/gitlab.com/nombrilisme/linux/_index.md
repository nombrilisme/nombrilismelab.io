---
title: "linux"
type: term
exit: https://eff.org
---

## [../](../)
## [notes/](./)
- {{< note archlinux >}}
- {{< note manjaro >}}
{.notelist2}

## [installs/](./)
- {{< subdir/installs install archlinux 2 >}}
- {{< subdir/installs kali >}}
- {{< subdir/installs install archlinux >}}
- {{< subdir/installs install manjaro >}}
{.notelist2}
