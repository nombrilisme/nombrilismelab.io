---
title: archlinux
type: term
domain: linux
back: "../"
---

archlinux: notes

# wifi
--------------------------------------------------------------------------------

install nmcli.

print available networks and BSSIDs:

    nmcli d wifi list 

connect:

    nmcli d wifi connect <BSSID> password <password> 

show saved networks:

    nmcli con show 

delete a saved network:

    nmcli con del <ssid> 

# connecting to an unencrypted wifi network
--------------------------------------------------------------------------------

e.g. hotel wifi
same as above but omit the password: 

    nmcli d wifi connect <BSSID> 


then open the browser, it should redirect to the login page
or if not go to 192.168.0.1

# setting up wifi for wpa2 networks
--------------------------------------------------------------------------------

this worked to connect to nyu:
 
    nmcli connection add \ 
    type wifi con-name <ssid> ifname wlp1s0 ssid <ssid> -- \ 
    wifi-sec.key-mgmt wpa-eap 802-1x.eap ttls \ 
    802-1x.phase2-auth mschapv2 802-1x.identity <username> 
 
where "ssid" is nyu and "username" is username
then a popup appears asking for the passsword

# extracting a tar file
--------------------------------------------------------------------------------
 
    tar -xvf [package] 
 

# installing a package from a tarball
--------------------------------------------------------------------------------
 
    sudo pacman -U package.tar.gz 
 

# mounting an external drive
--------------------------------------------------------------------------------

run `sudo fdisk -l` to see what the hard drive is called, should be /dev/sdXY or 
something.

make a directory for it (called a "mountpoint"?) e.g. mkdir /mnt/my_ssd

mount the drive:
 
    sudo mount /dev/sdXY /mnt/my_ssd 
 
to unmount:
 
    sudo umount /dev/sdXY 
 
if it says "target is busy", you can use `fuser` to kill all processes that
are currently using the mounted file system:
 
    fuser -cuk /mnt/my_ssd 
 
# system upgrade
--------------------------------------------------------------------------------
 
    sudo pacman -Syu 
 
sometimes if you haven't upgraded in a while Syu fails, citing some issue
with keyrings or something. update the keyring (below)

sometimes there is a glibc error, e.g. version `GLIBC_2.33' not found, this
was fixed by upgrading the system

# update the keyring(s)
--------------------------------------------------------------------------------
 
    sudo pacman -Sy archlinux-keyring 
 
# i3 window manager
--------------------------------------------------------------------------------

packages:
 
    sudo pacman -S i3-gaps i3blocks i3lock i3status 
 
create copy of default xinitrc in home directory:
 
    cp /etc/X11/xinit/xinitrc ~/.xinitrc
 
put `exec i3` at the end of the .xinitrc file

run `startx` to start an i3 session

# Xresources: configure colours, fonts, etc
--------------------------------------------------------------------------------

make an .Xresources file:
 
  touch ~/.Xresources
 
to load changes in the .Xresources file:
 
  xrdb ~/.Xresources
 
you can link colors in the i3 config to those in .Xresources

# battery level 
--------------------------------------------------------------------------------

install `acpi` 
to check current battery status: `acpi -k` 

# key mapping with xmodmap
--------------------------------------------------------------------------------

[archwiki][1] (found this pretty cumbersome)
 
    install `xorg-xmodmap` 
 
create a file with a list of the current keymaps:
 
    xmodmap -pke > ~/.Xmodmap 
 
change key mappings as you please

to find out the keycode of a particular key, install `xorg-xev`, then run
`xev` in a terminal. this should open a new window and every time you press 
a key it should print information about that event, including the keycode

then you can change the entry for that keycode in `~/.Xmodmap` 

to enact changes to the .Xmodmap file:
 
    xmodmap ~/.Xmodmap 
 
list of keysyms recognised by xmodmap: 
https://wiki.linuxquestions.org/wiki/List_of_Keysyms_Recognised_by_Xmodmap

# backlight
--------------------------------------------------------------------------------
 
    xbacklight -set <number between 0 and 100> 
 
# timezones
--------------------------------------------------------------------------------

check current status:
 
    timedatectl status 
 
print list of timezones:
 
    timedatectl list-timezones 
 
connect to a timezone:
 
    timedatectl set-timezone <Zone/SubZone> 

# GTK config 
--------------------------------------------------------------------------------

this is a pain
use `lxappearance` to control GTK themes:
 
  sudo pacman -S lxappearance 
 
lots of themes for download at official repositories

edit GTK themes in `/usr/share/themes/<themedirectory>/gtk.css`

an annoying warning that keeps appearing: `Gtk-WARNING: unable to locate
theme engine ... "murrine"`. this fixes it:
 
  sudo pacman -S gtk-engine-murrine 
 
# key mapping with setxkbmap
--------------------------------------------------------------------------------

e.g. map caps lock to escape: 
 
  setxkbmap -option caps:escape
 
put permanent mappings in .xinitrc, before the line usermodmap=$HOME/.Xmodmap 

using xmodmap to remap the capslock key works, but there doesn't seem to be a 
way to disable the light. it worked with setxkbmap. 

# setting a compose key for special characters  
--------------------------------------------------------------------------------

can define a compose key with `setxkbmap` which allows typing nonstandard
characters (diacritics, the £ sign, etc)

see [the wikipedia page][2] for a table of mappings

the file `/usr/share/X11/xkb/rules/base.lst` has a list of accepted mappings

e.g. set the right ctrl key as the compose key:
 
  setxkbmap -option compose:rctrl 
 
put this command in the i3 config to make permanent

# fcitx: input for languages with non-roman characters
--------------------------------------------------------------------------------

install `fcitx`

install an input method engine: the best one is `fcitx-libpinyin` (supports
both pīnyīn and 注意)

install the `fcitx-im` package (an input method module), supposedly gives a
better experience in Qt programs

install fonts. for Pan-CJK fonts the package is
`adobe-source-han-sans-otc-fonts` (alternatively serif)

in `.xinitrc` add the following before the exec i3 line:
 
    export GTK_IM_MODULE=fcitx 
    export QT_IM_MODULE=fcitx 
    export XMODIFIERS=@im=fcitx 
 
add `fcitx &` to the list of autostart applications in the i3 config file

install a configuration tool. for a GTK UI the package is `fcitx-configtool`

then run `fcitx-config-gtk3` to open the UI

configure the input methods

restart the session. it should work

# LaTeX
--------------------------------------------------------------------------------

install `texlive-most`

for chinese fonts get `texlive-langextra` (use `xelatex` as the pdf engine)

# fonts
--------------------------------------------------------------------------------

to see font list: `fc-list`

# GLFW 
--------------------------------------------------------------------------------

download source [here][3]

open with xarchiver. click Action -> Extract

then:

    cd <glfw-root-dir> 
    cmake .  
    sudo make install 

# unityhub
--------------------------------------------------------------------------------

haven't been able to make this work again

trying to download the unityhub AUR package with yay but keep running into a 
gconf build error

go to the [unity download page][4] and download the app image

run `chmod +x` on the app image

run the app image: 
  
    ./UnityHub.AppImage --appimage-extract 
 
cd into `squashfs-root` run the AppRun executable

can't get it to work (my incompetence)

# vulkan
--------------------------------------------------------------------------------

install:
- `vulkan-icd-loader`
- `vulkan-intel` (depends on graphics card)
- `vulkan-headers`	
- `vulkan-validation-layers`
- `vulkan-tools`

# gstreamer/antimof: ipad mirroring
--------------------------------------------------------------------------------

read [this][5]

install:
- `gstreamer` 
- `gst-libav` 
- `gst-plugins-good` 
- `gstreamer-vaapi` 
- `avahi`

doesn't work

# protonvpn-cli
--------------------------------------------------------------------------------

first install kwallet: pacman -S kwallet (archlinux keyring isn't supported)
I first tried this with the gnome-keyring and it didn't work

update: just kidding. uninstall kwallet. install gnome-keyring first, add the 
following lines to xinitrc:

    eval $(gnome-keyring-daemon --start)
    export SSH_AUTH_SOCK
 
then install protonvpn-cli with yay

should work

# python
--------------------------------------------------------------------------------

when python does a big update and all the python-related shit doesn't work 
anymore

get a list of python packages

    pacman -Qoq /usr/lib/python3.XX

where XX is the old version

rebuild them all with yay

    yay -S $(pacman -Qoq /usr/lib/python3.XX) --answerclean All

# check disk usage
--------------------------------------------------------------------------------
 
  df -h
 
# root partition too full
--------------------------------------------------------------------------------

to clear the cache
 
    pacman -Sc
 

[1]: https://wiki.archlinux.org/title/Xmodmap
[2]: https://en.wikipedia.org/wiki/Compose_key
[3]: https://www.glfw.org/download.html
[4]: https://unity3d.com/get-unity/download
[5]: https://rodrigoribeiro.site/2020/08/09/mirroring-ipad-iphone-screen-on-linux/

