---
title: kali
type: term
domain: linux
back: "../../"
---

install kali 

on macbook air 2020 (parallels)

- install default kali linux option on the parallels installation window
- follow the prompt to install parallels tools and restart
- open parallels desktop preferences (on mac)
	+ go to shortcuts
	+ go to macOS system shortcuts
	+ change send mac system shortcuts to "Always"
	+ go to virtual machines (click on the VM) and uncheck all the commands
- restart the VM, now all the shortcuts will go to the VM
