---
title: install archlinux (ii)
type: term
domain: linux
back: "../../"
---

ARCH INSTALL

COMPUTER 1 AMD RYZEN 5700X

# PRELIMS
--------------------------------------------------------------------------------

flash archlinux iso file to a usb

plug in usb and boot computer

hit F11 

in the grub menu hit "arch linux installation medium"

verify boot mode

    ls /sys/firmware/efi/efivars

should show directory contents

# INTERNET
--------------------------------------------------------------------------------

check if network interface is enabled

    ip link

use iwctl menu

    iwctl

get device name
	
    device list

  should be "wlan0" 

scan for networks

    station wlan0 scan

  (no output)

list networks

    station wlan0 get-networks

connect

    station wlan0 connect SSID

exit iwctl and ping to check

    ping archlinux.org

# TIME
--------------------------------------------------------------------------------

    timedatectl status
    timedatectl set-timezone America/New_York

# DISK PARTITIONING
--------------------------------------------------------------------------------

list disks

    fdisk -l

to start partitioning on the drive

    fdisk /dev/nvme0n1

efi partition:

    n
    first sector: default
    last sector: +512M
    type: EFI

swap:
	
    n
    first sector: default
    last sector: +1024M
    type: Linux swap

root:
	
    n
    first: default
    last: enter
    type: Linux

format the partitions

    mkfs.fat -F 32 /dev/nvme0n1p1
    mkswap /dev/nvme0n1p2
    mkfs.ext4 /dev/nvme0n1p3

# MOUNT FILESYSTEMS
--------------------------------------------------------------------------------

mount root

    mount /dev/nvme0n1p3 /mnt

mount efi:

    mount --mkdir /dev/nvme0n1p1 /mnt/boot

mount swap:
	
    swapon /dev/nvme0n1p2

# CHECK MIRRORLIST
--------------------------------------------------------------------------------

    vim /etc/pacman.d/mirrorlist
    reflector

# INSTALL PACKAGES
--------------------------------------------------------------------------------

    pacstrap /mnt base linux linux-firmware

# FSTAB
--------------------------------------------------------------------------------

    genfstab -U /mnt >> /mnt/etc/fstab

# CHROOT
--------------------------------------------------------------------------------

    arch-chroot /mnt

set timezone

    ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime
    hwclock --systohc

localisation

    locale-gen

install packages
	
    pacman -S
    vim 
    networkmanager
    man-db man-pages texinfo
    grub
    mesa xf86-video-amdgpu vulkan-radeon
    nvtop
    efibootmgr
    dosfstools mtools openssh wireless_tools
    sudo

enable networkmanager

    systemctl enable NetworkManager

create locale.conf

    vim /etc/locale.conf
    write lang=en.US.UTF-8

hostname

    vim /etc/hostname

  enter the hostname

initramfs

    mkinitcpio -P

install grub

    grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub_uefi --recheck
	
generate grub config file

    grub-mkconfig -o /boot/grub/grub.cfg

create user account

    useradd -m -g users -G wheel <uname>
    passwd <uname>

allow users in wheel group to use sudo

    visudo
    
  uncomment line '%wheel ALL=(ALL) ALL'

# EXIT CHROOT
--------------------------------------------------------------------------------

    exit
    umount -R /mnt
    reboot		# quickly remove the usb

REBOOT
############################################################

wifi

    nmcli d wifi list
    nmcli d wifi connect <SSID> password <password>

edit pacman mirrorlist

    vim /etc/pacman.d/mirrorlist

install basedevel and git

    sudo pacman -S --needed base-devel git

install yay

    git clone https://aur.archlinux.org/yay.git
    cd yay
    makepkg -si

# X WINDOW SYSTEM
--------------------------------------------------------------------------------

arch packages:

    xf86-input-libinput
    xorg-server
    xorg-xinit
    xorg-apps
    mesa
    xf86-video-amdgpu
    xorg-twm xterm xorg-xrdb

run

    startx

# I3 WM
--------------------------------------------------------------------------------

packages:

    i3-gaps i3blocks i3lock i3status

make copy of default xinitrc in home directory

    cp /etc/X11/xinit/xinitrc >/.xinitrc

put 'exec i3' at the end of the .xinitrc file

make .Xresources file
