---
title: install manjaro
type: term
domain: linux
back: "../../"
---

manjaro install/setup

2018 macbook pro (parallels vm)

# prelims

- download the manjaro iso file
- make sure to get the slightly older one from the archive (version 18.0.3?). 
  parallels doesn't work with the new ones (ugh)
- http://osdn.net/projects/manjaro-archive/storage/i3/
- a [useful article][1] regarding installing parallels tools
- open parallels and choose "install windows or another os from a dvd or 
  image file"
	+ choose the manjaro iso file
	+ click "create"
- on the welcome screen click "Boot: Manjaro..."

# after booting into the new manjaro environment

- on the manjaro hello screen click "Launch installer"
- configure the installation settings
	+ can leave most settings as default
	+ under *Partitions*, choose "Erase disk" and "No Swap"
- after installing click "Restart now"

# after rebooting 

- on the manjaro welcome window, uncheck the slider in the bottom right for 
  "Launch at start"
- now install parallels tools (below)

# mac settings for parallels virtualisation app

- under *Shortcuts* -> *macOS System Shortcuts*, set 
  *Send macOS sysstem shortcuts:* Always

# installing parallels tools

- read [this post][2]. scroll to the bit on installing parallels tools in arch
- this didn't work for me the first time. I can't remember why. I think you have 
  to do a full system upgrade (pacman -Syu) before (or after?) installing 
  parallels tools
- ugh

[1]: https://kb.parallels.com/en/124124
[2]: https://kb.parallels.com/en/124124

