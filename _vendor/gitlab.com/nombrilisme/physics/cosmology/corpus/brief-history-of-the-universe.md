---
title: "brief history of the universe"
type: long 
---

# THE UNIVERSE IS EXPANDING
--------------------------------------------------------------------------------

there's evidence the universe is expanding.
  - bloody hell. I guess I should stop doing my homework. what's the point?
  - also see: annie hall
  - {{< 𝖗 DOSC p2 >}}

SCALE FACTOR, a
  - a measure of the current state of the universe's expansion. = 1 at the 
    current time. < 1 in the past. is a function of time, a = a(t).

COMOVING DISTANCE
  - define a coordinate grid in space. take two arbitrary points, x₍1₎ and x₍2₎.
    the COMOVING DISTANCE between them is the distance *as measured in the 
    coordinate system*.
  - the comoving distance is fixed in time (remains constant as the universe 
    expands)
  - the PHYSICAL DISTANCE is what increases with time. physical distance = 
    comoving distance × scale factor

# REDSHIFT
--------------------------------------------------------------------------------

as the universe expands, the "physical" wavelength of light emitted from a 
distant object gets stretched by a factor proportional to the scale factor 
  - ∴ the observed wavelength is bigger than the original wavelength when the 
    light was emitted
  - the stretching factor is called the REDSHIFT, z 
  ```
    1 + z = ÷{λ₍observed₎}{λ₍emitted₎} 
          = ÷{a₍observed₎}{a₍emitted₎} 
          = ÷{1}{a₍emitted₎}
  ```
  - note a₍observed₎ means the scale factor now, which is just 1

# GEOMETRY
--------------------------------------------------------------------------------

geometry is another "parameter" that characterises the universe. apparently 
there are three possible geometries: euclidean, open, closed.

EUCLIDEAN UNIVERSE, aka FLAT UNIVERSE
  - given two free particles, their trajectories remain parallel 

CLOSED UNIVERSE
  - given two free particles, their trajectories eventually converge
  - analogy: think of the surface of a sphere, how from the equator all the 
    lines converge to a pole
  - a closed universe (and a sphere) is a space of positive curvature, in 3 
    spatial dimensions. (on a sphere it's 2 dimensions)

OPEN UNIVERSE
  - given two free particles, their trajectories eventually diverge
  - like balls rolling off a sphere 

# ENERGY & GEOMETRY
--------------------------------------------------------------------------------

the total energy density in the universe determines its geometry (apparently 
this comes from general relativity)
  - if density > ρ₍critical₎ = 10⁽-29⁾ g cm⁽-3⁾, CLOSED
  - if density < ρ₍critical₎, OPEN
  - if density = ρ₍critical₎, EUCLIDEAN

observations suggest the universe is euclidean. this is explained by INFLATION.

# EVOLUTION OF SCALE FACTOR WITH COSMIC TIME
--------------------------------------------------------------------------------

GR gives full details on the evolution of a with t. today: a = 1 and T = 2.73 K.

in the early days of the universe, radiation was dominant, and a ∝ t⁽1/2⁾. 
eventually the universe transitioned to a state where matter was dominant, and a 
∝ t⁽2/3⁾.
  - {{< 𝖗 dodelson fig 1.2 p3 >}}

very recently a has been growing faster than t⁽2/3⁾. is a sign there's a new 
form of energy that's beginning to dominate the cosmos.
  - >> so how tf do we know this? <<
    {.question}

HUBBLE RATE
  - measure of how quickly the scale factor changes
  ```
    H(t) = ÷1a Ɗ₍a₎t
  ```
  - e.g. if the universe is euclidean and matter dominated, then a ∝ t⁽2/3⁾, and 
    H = (2/3)·t⁽-1⁾.
  - let t₍0₎ denote the time today (the 0 subscript implies the value of a 
    quantity today)

HUBBLE'S CONSTANT
  ```
    H₍0₎ = H(t = t₍0₎)
  ```
  - the hubble rate at present time
  - in a euclidean matter dominated universe (unlike ours) H₍0₎·t₍0₎ = 2/3

FRIEDMANN EQUATION
  - GR predicts the evolution of the scale factor is determined by the friedmann 
    equation:
    ```
      H⁽2⁾(t) = ÷13 8πG(ρ(t) + ÷{ρ₍cr₎ - ρ(t₍0₎)}{a⁽2⁾(t)})
    ```
  - if the universe is euclidean, the last term vanishes (the sum of all the 
    energy densities equals the critical density)
  - if the universe is not euclidean, the curvature contributes 1/a⁽2⁾
  - G is newton's constant
  - ρ(t) is the energy density in the universe as a function of time, ρ(t₍0₎) is 
    the energy density today
  - ρ₍cr₎ is the CRITICAL DENSITY
    ```
      ρ₍cr₎ = ÷{3H₍0₎⁽2⁾}{8πG}
    ```

to use the friedmann equation, you need to know how the energy density evolves 
with time. 
  - ρ is the sum of several components that scale differently with time.

the temperature of radiation as a function of time is given by
  ```
    T(t) = T₍0₎ / a(t)
  ```

at early times, the temperature was higher than it is today.

the energy density of blackbody radiation scales as T⁽4⁾ ∝ a⁽-4⁾. ∴ at early 
times the hubble parameter evolves as H ∝ T⁽2⁾

early on, RADIATION was the dominant constituent of the universe (because of the 
a⁽-4⁾ scaling), but today MATTER and DARK ENERGY dominate.
  
# THE EXPANSION RATE
--------------------------------------------------------------------------------

the expansion rate is a measure of how fast the universe is expanding.
  - determine by measuring the velocities of distant galaxies and dividing by 
    distance from us.
  - ∴ units of expansion are velocity per distance.

the HUBBLE CONSTANT: defined by a dimensionless number, h, defined
  ```
    H₍0₎ = 100 h  km s⁽-1⁾ Mpc⁽-1⁾
    = h / (0.98×10⁽10⁾  years)
    = 2.13×10⁽-33⁾  eVh / ħ
  ```
  - h is unrelated to planck's constant ħ
  - the astronomical length scale of a megaparsec (Mpc) is 3.0856×10⁽24⁾ cm.
  - current measurements indicate h ≃ 0.7

for some reason it's customary to use h⁽-1⁾Mpc as the unit of length in 
cosmology (p5).

predicted age for euclidean, matter dominated universe, (2/3)·H₍0₎⁽-1⁾, is 
6.5h⁽-1⁾ Gyr.
  - the age of a universe with a cosmological constant is larger.

numerical estimate of critical density:
  ```
    ρ₍critical₎ = 1.88 h⁽2⁾ × 10⁽-29⁾ gcm⁽-3⁾
  ```

in the past the density and temperature was higher.
  - the rates for particles to interact scale as density squared. also used to 
    be higher.

if a particle scatters with rate much higher than the expansion rate, the 
particle stays in equilibrium. 
  - else it falls out of equilibrium and freezes out.
