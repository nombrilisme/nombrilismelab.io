---
time: 202303031843
title: "big bang nucleosynthesis"
type: note
---

# NUCLEAR STATISTICAL EQUILIBRIUM
--------------------------------------------------------------------------------

  the NUMBER DENSITY of a VERY NONRELATIVISTIC nuclear species A(Z), in KINETIC 
  EQUILIBRIUM, with mass number A and charge Z
  ```
    n₍A₎ = g₍A₎·(÷{m₍A₎T}{2π})⁽3/2⁾ exp(÷{μ₍A₎-m₍A₎}{T})
  ```

  pretty much all the neutrons in the universe got incorporated into ⁽4⁾He. so 
  the evolution of the neutron-proton ratio is a big "part" of nucleosynthesis.

  the proton-neutron balance is maintained by these weak interactions:

# GENERAL
--------------------------------------------------------------------------------

  key observable: baryon/photon number ratio, or the BARYON DENSITY. it allows 
  to calculate the "effects" of BBN.
  - this obbl controls the rate that nucleons collide and react
  - can use it to calculate the element abundances after nucleosynthesis ends.

  another key observable: the neutron-proton ratio.

  the EXACT value of the b/p number ratio is not actually that important. unless 
  the theory of the BB is very wrong, BBN generally results in mass abundances 
  of:
  - 75% ⁽1⁾H, 
  - 25% ⁽4⁾He, 
  - 0.01% D and ⁽3⁾He, 
  - trace amounts of lithium and heavier elements

  the observed abundances in the universe of these elements are generally 
  consistent with these predicted abundance numbers (predicted by BBN), so this 
  is "evidence" for the BBT.

  it's customary to quote the ⁽4⁾He fraction by mass, symbol Y. 
  - ∴ 25% ⁽4⁾He accounts for 25% of the mass. but <8% of the actual nuclei are 
    ⁽4⁾He.
  
  normally you express other nuclei as number ratios to hydrogen.

# NEUTRON-PROTON RATIO
--------------------------------------------------------------------------------

  is a key parameter of BBN, used to model creation of the light elements.

  the np ratio was set before nucleosynthesis, within the first 1 s after the 
  BB.

  neutrons react with positrons or electron neutrinos to create protons (and 
  other things). these are the reactions that maintian the proton-neutron 
  balance.
  ```m
    n ⇌ p + e⁽-⁾ + v₍e-₎
    ν₍e₎ + n ⇌ p + e⁽-⁾
    e⁽+⁾ + n ⇌ p + v₍e-₎
  ```

  at t « 1⦃s⦄, these reactions were fast. they maintained n:p close to 1:1. 

  as the T dropped, the equilibrium shifted in favour of protons because they 
  have slightly lower mass. the n:p ratio smoothly decreased.

  the reactions continued until the T and ρ decreased too much, causing the 
  reactions to become too slow. this happened at about T = 0.7⦃MeV⦄, when t ∼ 
  1⦃s⦄. this iis FREEZE OUT.

  at freeze out, n:p is ∼ 1:6. 

  but free neutrons are unstable and have a decay time τ = 880⦃s⦄. some neutrons 
  decayed int he next few minutes before fusing into any nucleus. so the total 
  n:p after nucleosynthesis ends is about 1:7.

  among the neutrons that fused (rather than decayed), almost all ended up 
  incorporated into ⁽4⁾He nuclei, since it has the highest binding energy per 
  nucleon among the light elements. 
  - this predicts that about 8% of all atoms should be ⁽4⁾He, or mass fraction 
    25%. in line with observations.

# BARYON-PHOTON RATIO, η
--------------------------------------------------------------------------------

  the baryon:photon ratio, η. determines the abundances of light elements after 
  nucleosynthesis ends.

  baryons and light elements fuse in these reactions:
  ```m
    p + n → ⁽2⁾H + γ
    p + ⁽2⁾H → ⁽3⁾He + γ
    ⁽2⁾H + ⁽2⁾H → ⁽3⁾H + n
    ⁽2⁾H + ⁽2⁾H → ⁽3⁾H + p
    ⁽3⁾He + ⁽2⁾H → ⁽4⁾He + p
    ⁽3⁾H + ⁽2⁾H → ⁽4⁾He + n
  ```

  most fusion chains during BBN terminate in ⁽4⁾He. incomplete reaction chains 
  lead to small amounts of leftover ⁽2⁾H or ⁽3⁾He. the amt of these decreases as 
  η increases. 

  so, the larger the η, the more reactions there'll be, and the more efficiently 
  D will be transformed into ⁽4⁾He.

  so D is pretty useful for measuring η.

# TIMELINE
--------------------------------------------------------------------------------

  - THE BIG BANG, t = 0


  - INITIAL CONDITIONS, t ∼ 1⦃s⦄, T » 1⦃MeV⦄ 
    - the neutron-proton ratio is set.
    - the universe is pretty much homogeneous. radiation dominated.


  - FUSION OF NUCLEI, t = 10⦃s⦄::20⦃min⦄
    - the temperature here is cool enough for D to survive, but hot and dense 
      enough for many fusion reactions to occur. 

  forming ⁽4⁾He requires the intermediate step of forming D. before BBN, the 
  temperature was high enough for photons to have energy > the binding energy of 
  D, so any D created was immediately destroyed. the DEUTERIUM BOTTLENECK.

  the formation of ⁽4⁾He is delayed until the universe is cool enough for D to 
  survive. about T = 0.1⦃MeV⦄. at this point there's a "burst" of element 
  formation. 

  but, then, at about 20 mins after the BB, the ] ˝T˝ and ρ ()become too low for 
  significant fusion to happen. at this point, the elemental abundances are 
  pretty much fixed. the only changes are due to radioactive decay of the two 
  unstable products of BBN, tritium and ⁽7⁾beryllium.

# HELIUM
--------------------------------------------------------------------------------

  BBN predicts a primordial abundance of 25%. this is indep of the ICs of the 
  universe. as long as it was hot enough for protons and neutrons to transform 
  into each other easily, their ratio is determined by their relative masses, 
  and should be about 1:7. once the universe was cool enough, the neutrons then 
  quickly bound with an equal number of protons first into deuterium, then into 
  helium 4.

  analogy: think of ⁽4⁾He as ash. the amount of ash that forms when you 
  completely burn a piece of wood is independent of HOW you burn it.

  the helium abundance is an important test for the BBT. if the observed helium 
  abundance is > 25%, then questions the validity of BBT. especially if the 
  abundance was <25%, becaues it's hard to destroy ⁽4⁾He.

# DEUTERIUM
--------------------------------------------------------------------------------

  D is the opposite of ⁽4⁾He. the latter is stable, and difficult to destroy. D 
  is marginally stable, easy to destroy.

  the temperatures, time, and densities (of the universe) were enough to combine 
  a big fraction of the D nuclei to form ⁽4⁾He. but they weren't enough to carry 
  the process further, i.e. use ⁽4⁾He in the "next" fusion step.

  BBN didn't convert ALL of the D in the universe to ⁽4⁾He because the expansion 
  that cooled the universe also reduced the density. this stopped conversion 
  before it could proceed further. 

  thus, unlike ⁽4⁾He, the amount of D is very sensitive to initial conditions. 
  the denser the initial universe, the more D would be converted to ⁽4⁾He before 
  time ran out, so less D would ramin.

  there are no (known) post-BB process that produce significant amounts of D.

# NUCLEAR STATISTICAL EQUILIBRIUM
--------------------------------------------------------------------------------

  the NUMBER DENSITY of a VERY NONRELATIVISTIC nuclear species A(Z), in KINETIC 
  EQUILIBRIUM, with mass number A and charge Z
  ```
    n₍A₎ = g₍A₎·(÷{m₍A₎T}{2π})⁽3/2⁾ exp(÷{μ₍A₎-m₍A₎}{T})
  ```

  chemical equilibrium
  --------------------
  if the nuclear reactions that produce nucleus A out of Z protons and (A-Z) 
  neutrons occur rapidly compared to the expansion rate, there is chemical 
  equilibrium. in chemical equilibrium,
  ```
    μ₍A₎ = Z μ₍p₎ + (A - Z)μ₍n₎
  ```

  in NSE the mass fraction of species A(Z) is
  ```
    X₍A₎ = g₍A₎...
  ```
  KT eq 4.7

  the baryon:photon ratio
  --------------------
  ```
    η = ÷{n₍n₎}{n₍γ₎} = 2.68·10⁽-8⁾ (Ω₍b₎h⁽2⁾)
  ```

# INITIAL CONDITIONS
--------------------------------------------------------------------------------

  epoch: T » 1⦃MeV⦄, t » 1⦃s⦄

  three reactions that maintain the balance between neutrons and protons (via 
  the weak interaction).
  ```m
    n ⇌ p  + e⁽-⁾ + ν₍e-₎ 
    ν₍e₎ + n ⇌ p + e⁽-⁾
    e⁽+⁾ + n ⇌ p + ν₍e-₎
  ```

  when the rate for these interactions is large compared to the expansion rate 
  H, there is chemical equilibrium
  ```
    μ₍n₎ + μ₍ν₎ = μ₍p₎ + μ₍e₎
  ```
  so in chemical equilibrium
  ```
    ÷np = ÷{n₍n₎}{n₍p₎} = ÷{X₍n₎}{X₍p₎} = e⁽-Q/T + (μ₍e₎-μ₍ν₎)/T⁾
  ```

  from the charge neutrality of the universe, ˝μ₍e₎/T ∼ n₍e₎/n₍γ₎ = n₍p₎/n₍γ₎ ∼ 
  η˝, so ˝μ₍e₎/T ∼ 10⁽-10⁾˝.

  but the relic neutrino background hasn't been detected, so we don't know the 
  neutrino lepton numbers (e, μ, τ). we assume that the lepton numbers are small 
  (like the baryon number), « 1, so μ₍ν₎/T « 1. under this assumption, the 
  equilibrium value of the n:p ratio is
  ```
    ÷np = e⁽-Q/T⁾
  ```

  the RATE for the weak interactions that convert neutrons and protons, use 
  symbol Γ. e.g. the rate for pe → νn is Γ₍pe→νn₎. KT p91. 

  the high T and low T limits:
  ```
    Γ₍pe→νn₎ = ...
  ```
  [KT eq 4.18]

  then compare Γ to the expansion rate of the universe H ≃ 
  1.66g₍*₎⁽1/2⁾T⁽2⁾/m₍Pl₎ ≃ 5.5T⁽2⁾/m₍Pl₎, you find
  ```
    Γ/H ∼ (T/0.8⦃MeV⦄)⁽3⁾
  ```
  so at Ts higher than about 0.8⦃MeV⦄ you expect the n:p ratio to be equal to 
  its equilibrium value. 

  entropy of the universe is very high. this favours free nucleons. 

  rough estimate of when a certain nuclear species A becomes thermodynamically 
  favoured, solve for the temp T₍nuc₎ when X₍A₎ ∼ 1, 
  - deuterium: T₍nuc₎ ≃ 0.07⦃MeV⦄
  - etc
  - [KT p92]

  so, abundances of the light elements didn't start to build up until 
  temperatures much less than 1 MeV. this is because of the small binding energy 
  of the deuteron. the DEUTERIUM BOTTLENECK.

  

# PRODUCTION OF LIGHT ELEMENTS 1-2-3
--------------------------------------------------------------------------------

  epoch: t = 0.1⦃s⦄, T = 10⦃MeV⦄.
  --------------------

  at this epoch the energy density of the universe is dominated by radiation.

  the relativistic degrees of freedom: e⁽±⁾, γ, 3 neutrino species. so g₍*₎ = 
  10.75.
  - these are the light (≤ MeV) particle species known to exist. the electron 
    and muon neutrinos are clearly light. also assume the tau neutrino is light. 
    use g₍*₎ = 10.75 as the standard scenario. but you can also consider the 
    possibility of additional hypothetical light species. if the tau neutrino is 
    heavy (» MeV) then g₍*₎ = 9.

  g is the effective degrees of freedom. 
  ```
    g₍*₎ = ∑{i=bosons} g₍i₎(÷{T₍i₎}T)⁽4⁾ + ÷78 ∑{i=fermions} g₍i₎ (÷{T₍i₎}T)⁽4⁾
  ```

  epoch: t ∼ 1⦃s⦄, T = T₍f₎ ∼ 1⦃MeV⦄
  --------------------

  just before this epoch, the 3 neutrino species decouple from the plasma. the 
  weak interactions that convert neutrons and protons freeze out (Γ becomes 
  smaller than H) and the n:p ratio is given by its equilibrium value.

  after freezeout the n:p ratio doesn't remain constant. there are occasional 
  weak interactions and free neutron decays.

  the n/p ratio is about 1/6.

  the deviation of n/p from the equilibrium value is significant by the time 
  nucleosynthesis starts.

  epoch: t ∼ 1-3⦃minutes⦄, T = 0.3-0.1⦃MeV⦄
  --------------------

  g₍*₎ has decreased to its value today, 3.36. because the e⁽±⁾ pairs have 
  disappeared and transferred their entropy to the photons.

  the neutron proton ratio has decreased from 1/6 to 1/7.


