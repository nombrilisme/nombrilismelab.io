---
time: 202305131352
title: "arcade 2 results"
type: note
domain: cosmology
---

the findings of ARCADE 2 were unexpected and significant. it observed
found a residual background signal of radio emission in the frequency range [3 
GHz], substantially much stronger than predicted by theoretical models. this 
signal was given the name "space roar".
- specifically, it's different from the ΛCDM given the sources of emission
- radio wave frequencies are in the range 30 Hz to 300 GHz

the signal is consistent with a thermal spectrum (i.e. it very likely originates 
in a physical process, and not the result of instrumental noise or 
interference). the observation was independently detected and verified by a 
combination of low frequency data and FIRAS.

preliminary conclusions about the unexpected emission at 3 GHz: that it's due 
either to a diffuse extragalactic background of emission from discrete radio 
sources that have different properties than the [faint end] of the distribution 
of known sources, OR it's due to unmodelled residual emission from our galaxy. 
- they believe the former is more likely
