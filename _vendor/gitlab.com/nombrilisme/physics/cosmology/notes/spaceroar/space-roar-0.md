---
time: 202305090143
title: "Current Theories on the Origins of the Space Roar"
domain: cosmology
type: paper
abstract: |
  In 2006 the ARCADE 2 experiment's high-altitude balloon instrument discovered 
  that the CMB appeared to be ∼6 times brighter than expected by the theory at 
  radio frequencies in the range 10-20 GHz. This _space roar_ is still not fully 
  understood and has led to a number of new theories about the early universe and 
  the physical processes that might have taken place. This paper is intended to 
  chronicle the findings of the ARCADE 2 experiment, the questions they raise, and 
  subsequent developments in the theories on the sources and implications of this 
  space roar.
---

# INTRODUCTION
--------------------------------------------------------------------------------

{{< fig externalcalibrator.png 8  >}}

# BRIEF SUMMARY OF THE HISTORY OF CMB MEASUREMENTS
--------------------------------------------------------------------------------

{{< fig externalcalibrator.png 8  >}}

# THE ARCADE 2 EXPERIMENT
--------------------------------------------------------------------------------

{{< fig cmb-spectrum.png 10 >}}


{{< fig ARCADE-1.png 6 >}}

{{< fig blockdiagram.png 8 >}}
- Circuit diagram of the ARCADE radiometers.
{.caption}

{{< fig externalcalibrator.png 8  >}}
- Schematic diagram of the external calibrator.
{.caption}




# RELATIONSHIP TO THE PRODUCTION OF THE HIGGS BOSON
--------------------------------------------------------------------------------

{{< fig externalcalibrator.png 8  >}}


# POSSIBLE EMISSIONS FROM UNOBSERVED STARS AND GALAXIES
--------------------------------------------------------------------------------

{{< fig externalcalibrator.png 8  >}}
{{< fig externalcalibrator.png 8  >}}
{{< fig externalcalibrator.png 8  >}}
{{< fig externalcalibrator.png 8  >}}

# NONEQUILIBRIUM ECHO OF THE EARLY UNIVERSE
--------------------------------------------------------------------------------

{{< fig externalcalibrator.png 8  >}}

# FURTHER THEORIES
--------------------------------------------------------------------------------

{{< fig externalcalibrator.png 8  >}}


# FUTURE OF ARCADE 2 AND PROSPECTIVE MEASUREMENTS
--------------------------------------------------------------------------------

{{< fig externalcalibrator.png 8  >}}
{{< fig externalcalibrator.png 8  >}}


# CONCLUSION
--------------------------------------------------------------------------------

{{< fig externalcalibrator.png 8  >}}

# REFERENCES
--------------------------------------------------------------------------------

{{< src >}}
  D.J. Fixsen et al. ,,
  ARCADE 2 Measurement of the Absolute Sky Brightness at 3-90 GHz. ,,
  The Astrophysical Journal. ,,
  734:5. ,,
  2011. ,,
{{< /src >}}

{{< src >}}
  S. Kornowski. ,,
  The Origin of the Space Roar. ,,
  ,,
  2016. ,,
{{< /src >}}

{{< src >}}
  M. Baiesi, C. Burigana, et al. ,,
  On a possible nonequilibrium imprint in the cosmic background at low 
  frequencies. ,,
  ,,
  2020. ,,
{{< /src >}}

{{< src >}}
  M. Seiffert, D.J. Fixsen, et al.,,
  Interpretation of the ARCADE 2 Absolute Sky Brightness Measurement,,
  The Astrophysical Journal.,,
  734:6.,,
  2011.,,
{{< /src >}}

{{< src >}}
  S.M. Ulam.,,
  On some statistical properties of dynamical systems.,,
  Proc. 4th Berkeley Symposium on Math, Statistics, and Probability.,, 
  pp 315-323.,,
  1961.,,
{{< /src >}}

{{< src >}}
  A. Kogut, D. Fixsen, et al.,,
  ARCADE: Absolute Radiometer for Cosmology, Astrophysics, and Diffuse Emission.,,
  New Astronomy.,,
  2018.,,
{{< /src >}}

{{< src >}}
  A. Lewis, A. Challinor.,,
  Weak gravitational lensing of the CMB.,,
  Physics Reports 349.,, 
  125-238.,,
  2001.,,
{{< /src >}}

{{< src >}}
  A. Lewis, A. Challinor.,,
  Weak gravitational lensing of the CMB.,,
  Physics Reports 349.,, 
  125-238.,,
  2001.,,
{{< /src >}}

{{< src >}}
  R. Barkana.,,
  Possible interaction between baryons and dark matter particles revealed by the 
  first stars.,,
  Nature (London). ,, 
  555, 71-74.,,
  2018.,,
{{< /src >}}

{{< src >}}
  S. Brodd, D.J. Fixsen, et al.,,
  COBE Far Infrared Absolute Spectrophotometer (FIRAS) Explanatory Supplement.,,
  COBE Ref. Pub. No. 97-C, NASA-GSFC.,,
  1997.,,
{{< /src >}}
