---
time: 202305121603
title: "space roar 2"
type: paper
domain: cosmology
---

# INTRODUCTION
--------------------------------------------------------------------------------

In 2006 the {{< 词典 ARCADE 2 >}} experiment's high-altitude balloon instrument 
discovered that the CMB appeared to be ∼6 times brighter than expected by the 
theory at radio frequencies in the range 10-20 GHz. This unexpected brightness, 
known as the _space roar_ is still not fully understood and has led to a number 
of new theories about the early universe and the physical processes that might 
have taken place. 

Possible explanations for the origins of the space roar include emission from 
stars and galaxies that are too faint to be seen directly, or emission from hot 
gas in the early universe. Other theories propose that the signal is related to 
dark matter or to exotic physics beyond the standard model of particle physics.

This project is intended to chronicle the nature and findings of the ARCADE 2, 
the questions they raise, and subsequent developments in the theories on the 
sources and implications of this space roar.


# BRIEF SUMMARY OF THE HISTORY OF CMB MEASUREMENTS
--------------------------------------------------------------------------------

The CMB {{< 词典 cosmic microwave background >}} (CMB) is currently the best 
source we have to study the early universe. Even though the CMB is relatively 
uniform {{< 词典 blackbody spectrum >}}, there are processes that can cause 
deviations from this. Experiments to measure the frequency spectrum of the CMB 
give clues about what these processes may/not be {{< 𝖗 "Seiffert et al. 2011" 
>}}.

In 1990, 1996, 1999, and 2002 the frequency spectrum of the CMB was observed 
with the FIRAS (Far Infrared Absolute Spectrophotometer) instrument on the 
Cosmic Background Explorer at wavelengths between 1 cm and 100 μm. These 
measurements consistently found the temperature of the CMB to be 2.725 ± 
0.001 K. {{< 𝖗 "Fixsen, Mather 2002" >}}.

For measurements at even lower frequencies than FIRAS, there have been a number 
of ground experiments and balloon experiments, amongst which the most precise 
are: {{< 𝖗 "Johnson, Wilkinson, 1987" >}} {{< 𝖗 "Levin et al. 1992" >}} {{< 𝖗 
"Zannoni et al. 2008" >}}.

One balloon-based experiment in particular, ARCADE 2, launched in 2006, 
specifically set out to measure the CMB at frequencies in the range 3-10 GHz.




# THE ARCADE 2 EXPERIMENT
--------------------------------------------------------------------------------

The Absolute Radiometer for Cosmology, Astrophysics, and Diffuse Emission 
(ARCADE 2) experiment was a balloon-borne instrument.

was an experiment in 2006 whose goal was to measure the temperature of 
the cosmic microwave background radiation (CMB), at wavelengths on the order of 
∼1 cm. 

{{< figure cmb-spectrum.png >}}


## Technical details of the ARCADE measuring instrument
--------------------------------------------------------------------------------

The ARCADE balloon comprises seven cryogenic radiometers with frequencies 
3.3, 5.5, 8.1, 10, 30, 90 GHz. The radiometers measured the temperature of the 
CMB in different frequency bands by observing the difference in power between a 
beam-defining antenna and a temperature controlled reference load. 

There is a separate blackbody calibrator to allow each antenna to view either 
the sky or a known blackbody. The temperature of the calibrator can be adjusted 
to zero the difference between the sky and that of the antenna, which allows the 
measurement of small spectral shifts of a blackbody.

{{< figure ARCADE-1.png 6 >}}
- Schematic diagram of the measuring instrument on ARCADE.
{.caption}

The radiometers each have a cryogenic chamber with a high electron mobility 
transistor direct-gain receiver, set at 75 Hz for gain stability between a 
wavelength-scaled corrugated conical horn antenna and a temperature-controlled 
internal load.

{{< figure blockdiagram.png 8 >}}
- Circuit diagram of the ARCADE radiometers.
{.caption}

## The external calibrator
--------------------------------------------------------------------------------

The external calibrator replaces the sky signal with the emission from a 
blackbody source at known temperature. It comprises 298 cones mounted on a 
thermal diffusion plate weakly coupled to a liquid helium reservoir. There are 
pumps that transport the superfluid liquid helium from the main dewar to the 
calibrator, and thermal control is provided by heaters on the back of the 
thermal diffusion plate.

{{< figure externalcalibrator.png 8  >}}
- Schematic diagram of the external calibrator.
{.caption}

## Error minimization
--------------------------------------------------------------------------------

The design of the instrument naturally minimizes sources of systematic error.

# ARCADE 2, 2011
--------------------------------------------------------------------------------

A second generation of the balloon experiment took place in 2011 (ARCADE 2). It 
had the goal of improving constraints on spectral distortions in the CMB in the 
3-10 GHz range.

The main findings of ARCADE 2 were unexpected and significant. The experiment 
found a pervasive and uniform background signal of radio emission in the 
frequency range it observed, which was much stronger than what had been 
predicted by theoretical models. This signal was dubbed the "ARCADE 2 Excess" 
and it was found to be consistent with a thermal spectrum, indicating that it 
had a physical origin and was not simply the result of instrumental noise or 
interference.

They detected residual radio emission at 3 GHz. They found that this observation 
was independently detected by a combination of low frequency data and {{< 词典 
FIRAS >}}.

Conclusion about the unexpected emission at 3 GHz: that it's due either to a 
diffuse extragalactic background of emission from discrete radio sources that 
have different properties than the *faint end* of the distribution of known 
sources, OR it's due to unmodelled residual emission from our galaxy. They 
believe the former to be more likely.

Radio wave frequencies are in the range 30 Hz to 300 GHz. The ARCADE 2 results 
suggest the faint end of the emission distribution of known sources is very 
different from the expected value predicted by the ΛCDM model, _given_ the known 
sources of emission.


# RELATIONSHIP TO THE PRODUCTION OF THE HIGGS BOSON
--------------------------------------------------------------------------------

In 2016 Kornowski suggested that the space roar is due to the relative motion of 
the center of mass in the very early universe compared to the ground state of 
the luminal Einstein spacetime. 


# NONEQUILIBRIUM IMPRINT
--------------------------------------------------------------------------------

On the nonequilibrium imprint: the space roar we observe today is a 
nonequilibrium echo of the early universe, and of nonequilibrium conditions in 
the primordial plasma more specifically.


# REFERENCES
--------------------------------------------------------------------------------

{{< src >}}
  D.J. Fixsen et al. ,,
  ARCADE 2 Measurement of the Absolute Sky Brightness at 3-90 GHz. ,,
  The Astrophysical Journal. ,,
  734:5. ,,
  2011. ,,
{{< /src >}}

{{< src >}}
  S. Kornowski. ,,
  The Origin of the Space Roar. ,,
  ,,
  2016. ,,
{{< /src >}}

{{< src >}}
  M. Baiesi, C. Burigana, et al. ,,
  On a possible nonequilibrium imprint in the cosmic background at low 
  frequencies. ,,
  ,,
  2020. ,,
{{< /src >}}

{{< src >}}
  M. Seiffert, D.J. Fixsen, et al.,,
  Interpretation of the ARCADE 2 Absolute Sky Brightness Measurement,,
  The Astrophysical Journal.,,
  734:6.,,
  2011.,,
{{< /src >}}

{{< src >}}
  A. Kogut, D. Fixsen, et al.,,
  ARCADE: Absolute Radiometer for Cosmology, Astrophysics, and Diffuse Emission.,,
  New Astronomy.,,
  2018.,,
{{< /src >}}
