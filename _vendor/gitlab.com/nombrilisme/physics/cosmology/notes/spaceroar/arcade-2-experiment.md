---
time: 202305131130
title: "arcade 2 experiment"
type: note
domain: cosmology
---

ARCADE 2  is the second generation of the ARCADE (Absolute Radiometer for 
Cosmology, Astrophysics, and Diffuse Emission) balloon instrument. for measuring 
the CMB temperature. 

## technical details of the measuring instrument

the balloon has seven [cryogenic radiometers] with frequencies of
3.3, 5.5, 8.1, 10, 30, 90 GHz. these radiometers measure the temperature of the 
CMB in different frequency bands. by observing the difference in power between a 
beam-defining antenna and a temperature controlled reference load.  [KF2006]

{{< fig ARCADE-1.png 6 >}}
- schematic diagram of the measuring instrument on ARCADE [KF2006]
{.caption}

each of the [cryogenic chamber] have a [high electron mobility transistor 
direct-gain receiver]. set at 75 Hz for gain stability between a 
wavelength-scaled corrugated conical horn antenna and a temperature-controlled 
internal load.

{{< fig blockdiagram.png 8 >}}
- circuit diagram of the ARCADE radiometers [KF2006]
{.caption}

## the external calibrator

there is a blackbody calibrator that allows each of the antennas to view either 
the sky or a known blackbody. the temperature of the calibrator can be adjusted 
to zero the difference between the sky and that of the antenna, which allows the 
measurement of small spectral shifts of a blackbody.

the calibrator replaces the sky signal with the emission from a 
blackbody source at known temperature. it has 298 cones mounted on a 
thermal diffusion plate weakly coupled to a liquid helium reservoir. There are 
pumps that transport the superfluid liquid helium from the main dewar to the 
calibrator, and thermal control is provided by heaters on the back of the 
thermal diffusion plate.

{{< fig externalcalibrator.png 8  >}}
- schematic diagram of the external calibrator [KF2006]
{.caption}

## error minimization

the instrument has some important features that naturally attempt to minimize 
sources of systematic error. 

most of the instrument operates at the temperature of the CMB. minimizes 
contributions from emissive optics.
