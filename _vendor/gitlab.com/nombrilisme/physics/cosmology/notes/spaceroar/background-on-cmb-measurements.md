---
time: 202305131302
title: "background on cmb measurements"
type: note
domain: cosmology
---

the cosmic microwave background (CMB) is relatively uniform blackbody spectrum 
with an observed temperature of 2.725 K. it's currently the best probe we have 
to glean information about early universe. {{< 𝖗 KF2006 >}}

but most cosmological models expect there to be long wavelength distortions in 
the CMB spectrum. {{< 𝖗 KF2006 >}}
- expand on what these are and why
{.methinks}

so over the past couple of decades [a bunch of] experiments have been conducted 
to measure the temperature and frequency spectrum of the CMB. to give clues 
about what early universe [processes] may/not have viably occured. [Seiffert et 
al 2011]

FIRAS: in 1990, 1996, 1999, and 2002 the frequency spectrum of the CMB was observed 
with the FIRAS (Far Infrared Absolute Spectrophotometer) instrument on the 
Cosmic Background Explorer. at wavelengths between 1 cm and 100 μm. these 
measurements consistently found the temperature of the CMB to be 2.725 ± 
0.001 K. [FM2002]

for measurements at lower frequencies than FIRAS: most of the experiments have 
been ground and balloon-based. some important experiments [todo: check & 
summarize results]:
- [Johnson Wilkinson 1987]
- [Levin et al 1992]
- [Zannoni et al 2008]
- []

in 2006: ARCADE 2, also a balloon experiment. specifically set out to measure 
the CMB at frequencies in the range of 3-10 GHz. it was launched to find 
deviations in the CMB from a perfect blackbody spectrum.
