---
time: 202302180732
title: "components of the universe"
type: note
---

# SETUP
--------------------------------------------------------------------------------

we want to model the universe as a PERFECT FLUID that's completely characterised 
by rest-frame mass density and isotropic pressure.
  - recall, we started with the einstein equation ˝G₍μν₎ = 8πG·T₍μν₎˝ and 
    derived the friedmann equations
    ```
      ÷{ä}{a} = -÷13·4πG·(ρ + 3p)
    ```
  - we want to know what are: ˝ρ,p,T₍μν₎˝

conservation of stress-energy:
```
  ÷{dT⁽μ⁾₍ν₎}{dx⁽μ⁾} = 0
```
  - this is frame dependent. ie, it only applies in a local inertial frame.
  - we need a more general form since we're dealing with an expanding universe.
  - in the generalised form, the covariant derivative should vanish.

general form of stress-energy conservation:
```mm
  ÷{dT⁽μ⁾₍ν₎}{dx⁽μ⁾} 
      &→ ÷{dT⁽μ⁾₍ν₎}{dx⁽μ⁾} + Γ⁽μ⁾₍αm₎·T⁽α⁾₍ν₎ - Γ⁽α⁾₍νμ₎·T⁽μ⁾₍α₎
      &= T⁽μ⁾₍ν;μ₎
```
  - > ah, here we go. this notation again. pfft. <
    {.methinks}
  - {{< 𝖗 JPL l4 p2 >}}

the FLUID EQUATION:
```
  𝝏{t}ρ + ÷{3˙a}{a}·(ρ + p) = 0
```
  - if you can relate ˝ρ˝ and ˝p˝ then you can solve for time evolution of 
    energy density (or scale factor evolution)

barotropic equation of state:
```
  p = ωρ
```
  - ω is an equation of state parameter.
  - >> where does this  come from? <<
    {.question}

# RADIATION
--------------------------------------------------------------------------------

radiation: relativistic particles like photons, neutrinos, etc.

for an isotropic perfect fluid
```
  T⁽μ⁾₍μ₎ = -ρ + 3p
```
  - trace of stress energy tensor

means
```m
  p = ÷13 ρ

  ω = ÷13
```

radiation redshifts as ˝a⁽4⁾˝
```
  ρ₍rad₎(a) = ρ₍rad,0₎·a⁽-3(1 + ÷13)⁾ = ρ₍rad,0₎·a⁽4⁾
```
  - three powers of a come from volume dilation as space expands
  - the other power of a comes from the redshift itself (λ gets bigger, ρ 
    decreases)

measure of ˝Ω₍r,0₎˝ from the CMB:
```
  Ω₍r,0₎ = 9.23·10⁽-5⁾
```
  - so basically, it's not dynamically important.

# MATTER
--------------------------------------------------------------------------------

matter: "dust" and dark matter. anything with negligible pressure. 

there's no difference between baryonic and dark matter.

```m
  p = 0
  ω = 0
```
and
```
  ρ₍m₎(a) = ρ₍m,0₎·a⁽-3⁾
```
  - redshifts due to volume dilation.

from planck, 2015:
```
  Ω₍m₎ = 0.3089
  Ω₍b₎ = 0.0486
  Ω₍c₎ = 0.2589
```

baryons are approx ≤5% of the critical density. of the 5%, ∼25% is in stars and 
gas in galaxies, ∼50% is in the intergalactic medium, and the rest is "dark". 
not *dark matter*, but just "dark" baryons.

# DARK ENERGY
--------------------------------------------------------------------------------

JPL l4p5. from einstein's equation. if Λ ≠ 0 then the stress energy tensor has 
the form of a perfect fluid with
```
  ρ = -p = ÷Λ{8pG}
```

∴
```
  ω = -1
```

∴
```
  ρ₍Λ₎ = ρ₍Λ,0₎·a⁽0⁾ = ρ₍Λ,0₎
```
  - dark energy doesn't redshift. its energy density is constant as the universe 
    expands.

# CURVATURE
--------------------------------------------------------------------------------

for a nonflat universe, the friedmann equation is
```
  (÷{˙a}{a})⁽2⁾ + ÷K{a⁽2⁾} = ÷13·8πG·ρ
```

so
```
  Ω₍tot₎ = ÷{ρ₍tot₎}{ρ₍c₎}
```
```
  H⁽2⁾ + ÷K{a⁽2⁾} = Ω₍tot₎·H⁽2⁾
```
```
  1 = Ω₍tot₎ - ÷K{H₍0₎⁽2⁾·a⁽2⁾} = Ω₍tot₎ + Ω₍k₎
```

the sign of k is det'd by ˝1 - Ω₍tot₎˝
```m
  ρ₍tot₎ < ρ₍crit₎    || k = -1 || (open)
  ρ₍tot₎ = ρ₍crit₎    || k = 0  || (flat)
  ρ₍tot₎ > ρ₍crit₎    || k = 1  || (closed)
```

from the form of the F equation, can see that ˝Ω₍k₎˝ redshifts as ˝a⁽2⁾(t)˝.

planck: ˝Ω₍k₎ = 0.0023 ±0.0055˝. so we live in a nearly flat universe.

so the k subscript is for the curvature component.

rewrite friedmann equation ito ˝Ω˝:
```
  H⁽2⁾ = H₍0₎⁽2⁾(÷1{a⁽3⁾}·Ω₍m₎ + ÷1{a⁽4⁾}·Ω₍r₎ + ÷1{a⁽2⁾}·Ω₍k₎ + Ω₍Λ₎)
```

# KINDS OF UNIVERSES
--------------------------------------------------------------------------------

- radiation dominated
- matter dominated - einstein de sitter
- cosmological constant dominated - de sitter

benchmark: 
- Ω₍r₎ ∼ 10⁽-4⁾
- Ω₍Λ₎ ∼ 0.7
- Ω₍μ₎ ∼ 0.3

and 
```
  H⁽2⁾(t) = H₍0₎⁽2⁾(÷1{a⁽3⁾}·Ω₍m₎ + Ω₍Λ₎)  
```
  - a useful model for the universe, but don't go too early.

important epochs:
- matter radiation equality
- matter Λ equality
- change from decelerating to accelerating

einstein's static universe:
  - "the greatest blunder".
