---
time: 202302170349
title: "describing spacetime"
type: note
---

# THE METRIC
--------------------------------------------------------------------------------

HOW to describe spacetime? answer: THE METRIC. the metric instructs how to 
calculate distances in arbitrary spacetime. 

the metric is a 2nd order symmetric tensor:
```
  g₍μν₎ 
  = 
  %%<
  g⟦00⟧ & g⟦01⟧ & ,,
  g⟦10⟧ & g⟦11⟧ & ,,
        &       & \ddots ,,
        &       &     & g⟦33⟧
  >%%
```
  - symmetric so ˝g⟦μν⟧ = g⟦νμ⟧˝
  - there are 10 independent coordinates

the simplest metric is the MINKOWSKI METRIC:
```
  η⟦μν⟧
  =
  %%<
  -1 & 0 & 0 & 0 ,,
  0 & 1 & 0 & 0 ,,
  0 & 0 & 1 & 0 ,,
  0 & 0 & 0 & 1
  >%%
```
  - is for flat, euclidean space. special relativity.

# INVARIANT DISTANCES
--------------------------------------------------------------------------------

in 2d space you can define invariant distances that are independent of 
coordinate systems. 
```
  dl⁽2⁾ = g⟦ij⟧·dx⟦i⟧·dx⟦j⟧
```
in cartesian the metric is... 

in polar it's...

IN RELATIVITY we talk about a SPACETIME INTERVAL (aka invariant interval)
```
  ds⁽2⁾ = g₍μν₎·dx⟦μ⟧·dx⟦ν⟧
```

# GEODESIC EQUATION
--------------------------------------------------------------------------------

in minkowski space a pcl travels in a straight line unless there's a force 
(except gravity, right). straight lines minimize the interval. hamilton's 
principle, etc.

in general spacetime the EL EOMs are 
```
  Ɗ⁽2⁾₍λ₎x⁽μ⁾ = -Γ⁽μ⁾₍αβ₎ Ɗ₍λ₎x⁽α⁾·Ɗ₍λ₎x⁽β⁾
```
aka the GEODESIC EQUATION. 

GEODESIC: generalisation of a straight line. shortest path between two points in 
curved spacetime. when there aren't any forces, particles move along geodesics. 
NOTE, WE DON't THINK OF GRAVITY AS A FORCE ANYMORE.

also,
```
ε = E  
```
  - see ALPHAVILLE

# CHRISTOFFEL
--------------------------------------------------------------------------------

aka connection coefficient. something u can calculate given g₍μν₎.

# WHAT GR DOES
--------------------------------------------------------------------------------

1. expresses gravitation as curved spacetime. now there's no gravitational 
   force. particles move on geodesics thru curved spacetime.

2. tells us how matter and energy cause spacetime to curve.

# EINSTEIN EQUATIONS
--------------------------------------------------------------------------------

```
  G₍μν₎ = 8π·G·T₍μν₎ - Λ·g₍μν₎
```
  - Λ is the COSMOLOGICAL CONSTANT
  - T₍μν₎ is the STRESS ENERGY TENSOR
  - G is the gravitational constant
  - G₍μν₎ is the einstein tensor, = R₍μν₎ - ½·g₍μν₎R
  - R₍μν₎ is the ricci tensor, R is the ricci scalar

# THE FLRW METRIC
--------------------------------------------------------------------------------

there's only 1 metric for a homogeneous isotropic universe. the FLRW metric. for 
flat spacetime:
```
  ds⁽2⁾ = -dt⁽2⁾ + a⁽2⁾(t) ((dx⁽1⁾)⁽2⁾ + (dx⁽2⁾)⁽2⁾ + (dx⁽3⁾)⁽2⁾)
```
```
  g₍μν₎
  = 
  %%<
  -1 & 0 & 0 & 0 ,,
  0 & a⁽2⁾(t) & 0 & 0 ,,
  0 & 0 & a⁽2⁾(t) & 0 ,,
  0 & 0 & 0 & a⁽2⁾(t) 
  >%%
```
  - a(t) is the scale factor
  - a₍t₍0₎₎ = 1, a₍0₎ = 0. distance between points is a function of time.

there's evidence the universe is flat, but a more general expression that can 
also account for curved options: 

l2p4

# TIME EVOLUTION OF THE SCALE FACTOR
--------------------------------------------------------------------------------

friedmann equation, acceleration equation
l2p5
