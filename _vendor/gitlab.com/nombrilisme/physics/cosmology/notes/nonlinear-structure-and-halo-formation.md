---
time: 202304160534
title: "nonlinear structure and halo formation"
type: note
---

# LF CHAPTER 3
--------------------------------------------------------------------------------

last chapter - evol of structure in the _linear_ regime. here, perturbations are 
small. 

but most things you see in telescopes don't fall into this regime. their 
densities are usually ∼1000 × the cosmic mean. 

so, here: study the evol of perts in the _nonlinear_ regime. focus on analytic 
models.

# SPHERICAL COLLAPSE
--------------------------------------------------------------------------------

current csml data show: DARK MATTER IS COLD. 
- reason: its pressure is negligible during the "gravitational growth" of 
  galaxies.

as a result, nonlinear evolution is simplified. it depends only on the 
gravitational force.

consider: an isolated, spherically symmetric density OR velocity perturbation of 
the smooth cosmological background. and figure out: the dynamics of a test 
particle at radius r relative to the centre of symmetry.
- birkhoofs theorem: can ignore the mass outside the radius 
- the EOM reduces to the usual friedmann equation for the evol of the scale 
  factor of a homogeneous U, 
- BUT the density parameter Ω now accounts for the additional mass that's inside 
  the shell AND the modified expansion velocity.

the perturbation has arbitrary density and velocity profile. in spite of this, 
only the total mass inside the particle's radius AND the peculiar velocity at 
the particle's radius contribute to the effective value of Ω.

so you can find a sol to the pcl's motion that describes it's departure from the 
background hubble flow AND its subsequent collapse/expansion. this sol will hold 
until the pcl crosses paths with one from anothe radius (happens late for most 
ICs).

reform the problem in newtonian form. at an early epoch with a₍i₎ « 1, consider 
a spherical patch of uniform overdensity δ₍i₎, which makes a "top-hat 
perturbation". 
- if Ω₍m₎ is basically 1 at this time AND if the pert is a pure growing mode, 
  then the initial peculiar velocity is radially inward with magnitude
  ```
    ÷13 δ₍i₎ H(t₍i₎) r
  ```
  where H(t₍i₎) is the hubble constant at the initial time and r is the radius 
  from the centre of the sphere. you can easily derive this from mass 
  conservation (continuity equation in spherical symmetry)
- the collapse of a spherical tophat pert beginning at radius r₍i₎ is described 
  by
  ```
    d⁽2⁾₍t₎ r = H₍0₎⁽2⁾ Ω₍Λ₎ r - ÷1{r⁽2⁾} GM
  ```
  r is the radius in a fixed (not comoving) coordinate frame, H₍0₎ is the 
  current hubble constant, and dr/dt = H(t) r is the unperturbed hubble flow 
  velocity (we add the peculiar velocity to this)
- the total mass enclosed by the radius r is
  ```
    M = ÷{4π}3 r₍i₎⁽3⁾ ρ₍i₎ (1 + δ₍i₎)
  ```
  where ρ₍i₎ is the background density of the universe at time t₍i₎.
- now define the dimless radius x = a₍i₎ (r/r₍i₎). 
  ```
    x = a₍i₎ ÷r{r₍i₎}
  ```
- rewrite the collapse of spherical tophat pert eq as
  ```
    ÷1{H₍0₎⁽2⁾} d⁽2⁾₍t₎ x =  -÷1{2x⁽2⁾} Ω₍m₎ (1 + δ₍i₎) + Ω₍Λ₎ x
  ```
- assume a flat U with Ω₍Λ₎ = 1 - Ω₍m₎.
- ICs for the integration of this orbit:
  ```
    x(t₍i₎) = a₍i₎
  ```
  ```m
    d₍t₎x (t₍i₎) 
    = H(t₍i₎) x(t₍i₎) (1 - ÷13 δ₍i₎) 
    = H₍0₎a₍i₎(1 - ÷13 δ₍i₎) (÷1{a₍i₎⁽3⁾} Ω₍m₎ + Ω₍Λ₎)⁽1/2⁾
  ```
  where
  ```
    H(t₍i₎) = H₍0₎ (÷1{a₍i₎⁽3⁾} Ω₍m₎ + (1 - Ω₍m₎))⁽1/2⁾
  ```
  is the hubble parameter for a flat U at time t₍i₎.
- integrate the dimless spherical tophat collapse eq
  ```
    ÷1{H₍0₎⁽2⁾} (d₍t₎x)⁽2⁾ = ÷1x Ω₍m₎ (1 + δ₍i₎) + Ω₍Λ₎ x⁽2⁾ + K
  ```
  K is an integration constant.
- evaluate this expression at the initial time
  ```
    K = -÷53 ÷{δ₍i₎}{a₍i₎} Ω₍m₎
  ```
- if K is suff -ve, the pcl will turn around the the sphere will collapse to 
  zero size at time
  ```
    H₍0₎ t₍coll₎ = 2 ∫₍0₎⁽a₍max₎⁾ daͺ(Ω₍m₎ a⁽-1⁾ + K + Ω₍Λ₎ a⁽2⁾)⁽-1/2⁾
  ```
  where a₍max₎ is the value of a that sets the denominator of the integrand to 
  zero. used tft δ₍i₎ « 1. note: the integrand determines the expansion time. 
  the factor of 2 accounts for time from maximum expansion to collapse.
- it's easier to solve the eom analytically for the regime where the cosmo 
  constant is negg, Ω₍Λ₎ = 0 and Ω₍m₎ = 1 (redshifts 1 < z < 10⁽3⁾)
- 3 branches of solutions:
  - the pcl turns around and collapses
  - reaches an infinite radius with an asymptotically +ve velocity
  - intermediate case where it reaches an infinite radius but with velocity → 0.

although we cast this problem as a test pcl in an over/under-dense region, you 
can also do get the same eqs by "carving out" a spherical region from a truly 
uniform medium. in this case the 3 solutions correspond to
  - closed
    ```m
      r = A(1 - cos(η)) ͺͺͺ0 ≤ η ≤ 2π
      t = B(η - sin(η))
    ```
  - flat
    ```m
      r = ÷12 Aη⁽2⁾ ͺͺͺ0 ≤ η ≤ ∞
      t = ÷16 Bη⁽3⁾
    ```
  - open
    ```m
      r = A(cosh(η) - 1) ͺͺͺ0 ≤ η ≤ ∞
      t = B(sinh(η) - η)
    ```

in all 3 cases, A⁽3⁾ = GMB⁽2⁾

all 3 sols have r⁽3⁾ = 9GMt⁽2⁾ / 2 as t → 0
  - this matches the expectation from linear theory that the pert amplitude gets 
    smaller as you go back in time

closed case: shell turns around at time πB and radius 2A 
  - at this point, the density contrast relative to the background is 9π⁽2⁾/16 = 
    5.6, for a Ω₍m₎ = 1 u.

the collapse times are independent of the initial distance from the origin.
  - perturbations with fixed initial density contrast collapse homologously. all 
    shells turn around and collapse at the same time.

so: that's the "fully" nonlinear sol for the simplified problem of the collapse 
of a spherical tophat pert.
  - BUT: the real density distribuition of the U is more complicated. you can't 
    describe the full nonlinear evol of density perts. but you can fully 
    describe their linear evol. 
  - compromise: use linear evol to identify regions (eg galaxies) where 
    spherical nonlinear evolution is a decent approximation.

to do this, it's useful to determine the mapping between the LINEAR DENSITY 
FIELD described by pert theory and the NONLINEAR densities in the spherical 
model.
  - you have to relate the spherical collapse params A B M to the linear theory 
    density pert δ
  - case Ω₍Λ₎ = 0, Ω₍m₎ = 1: this is easy. 
  - substitute the closed sol equations for r and t into the dimless diffeq for 
    the spherical tophat collapse (7) AT the turnaround radius, solve for A and 
    B, you get

# PRESS SCHECHTER MASS FUNCTION
--------------------------------------------------------------------------------

alright, so basically spherical collapse is a _simple_ model for the formation 
of nonlinear structures.

you can't actually observe structure formation directly. you only see the end 
results: clusters of galaxies, for e.g. you can then look at these clusters and 
collect statistics, like count the number of objects and find the density.
  - e.g. the galaxy luminosity function. gives number density of galaxies of a 
    given luminosity.

the mass function ˝n(M)˝ of cosmic structures is defined
```
  dN = n(M) dM
```
˝dN˝ is the number of structures per unit volume with mass between ˝M˝ and ˝M + 
dM˝

to measure the mass function irl: choose a volume in space. count the number of 
structures in it, _that have a given mass_

P&S "gave us" an analytic expression for the mass function.
  - oft described as a "classic" paper
