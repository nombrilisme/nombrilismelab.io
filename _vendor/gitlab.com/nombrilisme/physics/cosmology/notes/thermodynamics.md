---
time: 202302180939
title: "thermodynamics"
type: note
---

# DISTRIBUTIONS
--------------------------------------------------------------------------------

f(p) for smooth, homogeneous universe. FD distribution for fermions, BE 
distribution for bosons.
```
  f(↗x,↗p) = ÷1{ e⁽(E-μ)/ƙT⁾ ± 1 }
```

μ is chemical potential. the response of a system to change in particle number.
  - reactions flow in the direction of lower μ
  - chemical equilibrium is got when the sum of μs on both sides are equal. then 
    the forward and reverse reactions occur at the same rate.

n = particle number (density?)
```
  n₍i₎ = g₍i₎ ∫ d⁽3⁾p ÷1{(2π)⁽3⁾}·f(p)
```
  - index i is the species index
  - g is the degeneracy or internal dofs.

```
  ρ₍i₎ = g₍i₎ ∫ d⁽3⁾p ÷1{(2π)⁽3⁾}·f(p)·E(p)
```

for nonrelativistic particles, m » T
```m
  n = g(mT/2π)⁽3/2⁾·e⁽-m/T⁾
  ρ = mn
  p = nT « ρ    || (ideal \; gas)
```
  - p = 0

# THERMODYNAMICS
--------------------------------------------------------------------------------

```
  T dS = d(ρV) + p dV
  T dS = V dρ + ρ dV + p dV
```

use the fluid equation, express a in terms of V
```
  𝝏{t}ρ = -÷1V Ɗ{t}V (ρ + p)
```

entropy density: s = S/V
```
  s = ÷1T·(ρ + p)
```

entropy is dominated by relativistic particles
```
  s = ÷43·÷ρT
```

for photons
```
  ρ₍r₎ = ÷{π⁽2⁾}{30}·g·T⁽4⁾
```

in general we need g for all relativistic species. g* is the effective degres of 
freedom. this expression accounts for difference between fermion and boson 
tatistics. fermions can't occupy the same state, so have fewer effective dofs.
```
  g₍*₎ = ∑{i=bosons} g₍i₎·(÷{T₍i₎}{T})⁽4⁾ 
         + ÷78 ∑{i=fermions} g₍i₎·(÷{T₍i₎}{T})⁽4⁾
```

effective dofs *in entropy*:
```
  g₍*s₎ = ∑{i=bosons} g₍i₎·(÷{T₍i₎}{T})⁽3⁾
          + ÷78 ∑{i=fermions} g₍i₎·(÷{T₍i₎}{T})⁽3⁾
```

g₍*₎ = g₍*s₎ if all the same species have the same T. this condition is true 
untli a few seconds after the BB. one exception to this tho.

```
  s = ÷{2π⁽2⁾}{45} g₍*s₎·T⁽3⁾ ≈ 7n₍γ₎ = 2889.2 cm⁽-3⁾
```
  - often use s to refer to n₍γ₎ but the two aren't interchangable when g₍*₎ ≠ 
    g₍*s₎.
