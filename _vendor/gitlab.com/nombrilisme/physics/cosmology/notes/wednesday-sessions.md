---
time: 202303221502
title: "wednesday sessions"
type: longue
---

THE HUBBLE DIAGRAM
================================================================================

the universe is expanding. we know this because galaxies are moving away from 
us. the speed they're moving away from us (recession velocity) increases with 
distance.

the physical distance between two galaxies:
```
  d = ax
```
x is the comoving distance.

if there's no comoving motion, i.e. d₍t₎x = 0, the relative velocity v is
```
  v = d₍t₎(ax) = ˙ax = H₍0₎dͺͺͺv « c
```
(HUBBLE-LEMAITRE LAW)

so the velocity should increase linearly with distance (at low redshift) with 
slope given by H₍0₎, the hubble constant.

the value of the constant: measure the slope of the line in the hubble diagram. 
and that's why the units of H₍0₎ are km/s/Mpc, you imbecile.

this equation breaks down. you need to generalise the distance-redshift relation 
for larger distances. 

the distance-redshift relation depends on the energy content of the universe.

current data: best fit scenario that the universe is euclidean, and about 70% of 
the energy is in the form of a cosmological constant or some other form of dark 
energy. this is the CONCORDANCE model of cosmology.

BIG BANG NUCLEOSYNTHESIS
================================================================================

when you know how the scale factor evolves, and when you know the densities of 
the constituents of the universe, you can back extrapolate to explore early 
phenomena.

when the universe was at T ∼ 1 MeV/ƙ (the universe was much hotter and denser) 
there were NO neutral atoms or bound nuclei. 

the were large amounts of high energy radiation in this hot environment. thus 
any atom or nucleus that got produced would be immediately destroyed by a high 
energy photon.

as the U cooled below the binding energies, the light elements began to form - 
process called BBN.

you can calculate the primordial abundances of the elements if you know the 
early conditions of the universe and the relevant nuclear cross sections.

DS fig 1.6: BBN predictions for the He and D abundances, as a function of the 
average baryon density (the density of ordinary matter), IN UNITS of critical 
density.

the predicted abundances (esp D) depend on the density of protons and neutrons 
at the time of nucleosynthesis. 

the combined proton + neutron density = the baryon density, because protons and 
neutrons both have baryon number 1 and these are the only baryons around at the 
time.

we measured the D abundance in the intergalactic medium at high redshifts by: 
looking for a subtle absorption feature in the spectrum of distant quasars. 
these measurements and BBN calculations allow us to measure the baryon density 
in the universe. what we found: ordinary matter can contribute AT MOST 5% of the 
critical density.

the total matter density today is much larger than this. thus nucleosynthesis 
gives a good argument that there exists matter that isn't made of protons or 
neutrons. we call this dark matter, because it doesn't emit light.

THE CMB
================================================================================

the origin of the CMB: a phenomenon that "falls out of energetics".

when the T of radiation was ∼ 10⁽4⁾ K (crsps2 energies of order eV), free 
electrons and protons combined to form neutral hydrogen.

before this time, the T was too high. any hydrogen produced was immediately 
ionised by energetic photons.

after that epoch, at z ≃ 1100, the photons that comprise the CMB stopped 
interacting with any particles that travelled freely through space. so when we 
observe them today, we're looking at messengers from an early moment in the 
universe's histoire.

so the CMB photons are the most powerful probes of the early universe. much of 
the book is dedicated to working thru the details of what happened to the 
photons before they last scattered off free electrons. this involves developing 
the math of freestreaming.

we'll see that the CMB constrains the baryon density independently of BBN, but 
also in agreement with BBN. so this is more evidence in support of the 
concordance model.

the main thing for now: the interactions of photons with electrons before the 
last scattering ENSURED that photons were in equilibrium. that means they should 
have a blackbody spectrum.
- if something is in equilibrium, it must have a black body spectrum. since 
  fucking when?

the specific intensity of a gas of photons with a blackbody spectrum is:
```
  I₍ν₎ = ÷{4πħν⁽3⁾/c⁽2⁾}{exp(2πħν/ƙT) - 1}
```

DS fig 1.7: the "remarkable agreement" between the prediction of big bang 
cosmology and the observations of the FIRAS instrument on the COBE satellite.

the CMB provides the best blackbody spectrum ever measured.

chapter 1 of partridge, 2007: history of the discovery of the CMB.

the detection of the 3K background by penzias and wilson in the mid 1960s: suff 
evidence to decide in favour over the big bang rather than the steady state 
universe (an alternative state with no expansion).

penzias and wilson: measured the radiation at just one wavelength. their one 
wavelength result was enough to top the scales.

most important fact learned from the first 25 years surveying the CMB: the early 
universe was very smooth.
- we didn't detect any anisotropies in the CMB
- more evidence for the view of a smooth big bang

the COBE satellite mission discovered anisotropies in the CMB in 1992, so the 
early universe is actually not smooth. there were small perturbations in the 
cosmic plasma. fractional temperature fluctuations: order 10⁽-5⁾.

by now we've mapped these small fluctuations with exquisite precision. 

state of the art: look for even more subtle effects like:
- CMB polarisation, 
- the effect of the intervening matter distribution through gravitational 
  lensing.

to understand these effects, we must go beyond the smooth background universe 
and look at deviations from smoothness, INHOMOGENEITIES. 

inhomogeneities in the universe: are called STRUCTURE.

STRUCTURE IN THE UNIVERSE
================================================================================

we know about the existence of structure in the universe well before we detected 
the CMB anisotropies.
- when we mapped out the distro of galaxies in the local universe, it's clear 
  they're not distributed homogeneously.

the numGalaxies and volume covered by our surveys have increased exponentially.
- the sloan digital sky survey: SDSS
- two degree field galaxy redshift survey (2dF)

they compiled the redshifts of (and thus distances to) > 1 million galaxies.

DS fig 1.8: slice of the distribution of the main galaxy sample in the northern 
part of the SDSS survey. 
- us, the observers, are at the bottom, at z = 0.
- each dot: position of a galaxy. 
- color represents the actual color of the galaxy (red corresponds to redder 
  galaxies).

it's clear from DS fig 1.8 that the galaxies are not distributed randomly. the 
universe has structure on large scales.

to understand the structure, need tools to study perturbations around the smooth 
background. this is simple in theory, ala the perturbations are small.

to compare theory with observations, must avoid regimes that can't be described 
by small perturbations.

extreme example: we can't understand cosmology by examining rock formations on 
the earth. 

the intermediate steps
- collapse of matter into a galaxy
- star formation
- planet formation
- geology
are too complicated to allow comparing between linear theory and observations.

perturbations to the matter on small scales (< 10 Mpc) have become large in the 
late universe.
- i.e. FRACTIONAL DENSITY FLUCTUATIONS on these scales are NOT small. they're 
  comparable to or bigger than 1.
- we say the SCALES have GROWN NONLINEAR.

but on the other hand large scale perturbations are still small (quasi linear). 
so they've been processed much less than the small scale structure.

anisotropies in the CMB are small because they originated at early times. the 
photons we observe from the CMB don't "clump" on their way to us.

this means the best way to learn about the evolution of structure and to compare 
theory with observations: 
- look at anisotropies in the CMB 
- and look at anisotropies at large scale structure (LSS), i.e. how galaxies and 
  matter are distributed at large scales.

but ch12-13: you can still get valuable cosmo info from smaller nonlinear 
scales if you chose your observables wisely.

what we need: statistics to help us compare maps (like fig 1.8) to theories 
while isolating large scales from small scales.

for this reason: take the F transform of the distribution. working in F space 
makes it easier to separate large from small scales.

the most important statistic in the CMB and LSS: the TWO POINT FUNCTION, short 
for TWO POINT CORRELATION FUNCTION.

when you measure the 2pcf in F space, it's called the POWER SPECTRUM. 

ok ok, so the POWER SPECTRUM is the 2 point correlation function in fourier 
space.

take the number density of galaxies in the SDSS survey: 
- let density of galaxies as a function of positions be ˝n₍g₎(⬀x)˝
- also define the mean over the whole survey, ˝⟨n₍g₎⟩˝

then you can characterise the inhomogeneities with 
```
  δ₍g₎(⬀x) = ÷{ n₍g₎(⬀x) - ⟨n₍g₎⟩ }{ ⟨n₍g₎⟩ }
```

OR the fourier transform, 
```
  ~δ₍g₎(⬀k)
```
(DS box 5.1)

obviously, the mean of the field ˝δ₍g₎(⬀x)˝ is zero.

consider the GALAXY POWER SPECTRUM ˝P₍g₎(k)˝, defined
```
  ⟨~δ₍g₎(⬀k) ~δ₍g₎⁽❄⁾(⬀k')⟩ = (2π)⁽3⁾ δ₍D₎(⬀k - ⬀k') P₍g₎(k)
```
- angle brackets mean an average over the whole ensemble.
- ˝δ₍D₎˝ is the dirac delta function that constrains ˝⬀k = ⬀k'˝
- details in ch11
- this equation implies the power spectrum is the spread, or variance, in the 
  distribution.

if there are lots of very under or overdense regions, the power spectrum will be 
large. it will be small if the distribution is smooth (the power spectrum 
vanishes in a homogeneous universe)

DS fig 1.9: shows the measured power spectrum of the galaxy distribution in the 
SDSS/BOSS survey.

the best measure of anisotropies in the CMB is also the 2pcf of the intensity on 
the sky.

the CMB T is a 2D field, measured everywhere in the sky. 2 angular coords but no 
third coord corresponding to distance.

instead of F transforming the CMB T, you expand it in spherical harmonics in a 
basis apt for a 2d field on the surface of the sphere.

∴ the power spectrum of the CMB is a function of multipole moment l, not wave 
number k.

big difference between the map of the CMB and the map of the structure in the 
universe: the contrast (amplitude) of structure.
- the very young universe was v smooth
- maps of the current  universe tell is it's very inhomogeneous.
- how did the U go from smooth to "clumpy"?

simple answer: gravity forced more and more matter into overdense regions. ∴ a 
region that started with only small fractional overdensity, 10⁽-4⁾, evolved over 
billions of years to become much denser than the homogeneous universe today. 
this is the site at which a galaxy formed. in the process, small scale 
perturbations grew nonlinear, then hierarchically assembled to form larger 
structures.

major goal of most cosmologists today: understanding the development of 
structure in the universe.

FOURIER SPACE
================================================================================

take a field ˝δ(⬀x,t)˝ that obeys a linear PDE. if you define the spatial F 
transforms, ˝δ(⬀x)˝ and ˝δ(⬀k)˝ you can basically transform the PDE into a set 
of decoupled ODES. 

so rather than solving an infinite number of coupled equations, you can solve 1 
k mode at a time.

this little trick always works at linear order.
