---
time: 202303041343
title: "saturday sessions"
type: longue
---

ENERGY EVOLUTION
================================================================================

  how to characterise the constituents (matter, radiation, etc) in the universe?

  in a smooth, isotropic, homogeneous universe, we're only really interested in 
  _average_ quantities. 

  since the universe is isotropic, there's no mean _net_ momentum or _net_ 
  velocity (that would contradict isotropy).

  so, the MEAN DENSITY and PRESSURE are the only properties of the constituents 
  (matter, radiation, etc) we have.

the energy-momentum tensor
--------------------

  when you combine the E and P of a particle you get a relativistic 4-momentum. 

  similarly, you can combine ENERGY DENSITY and PRESSURE into a relativistic 
  tensor, the energy-momentum tensor (stress-energy tensor?), ⬈T. 

  in the isotropic smooth universe, ⬈T:
  ```
    T⟦μ'ν⟧ = %%<
      -ρ ,,
      & P ,,
      & & P ,,
      & & & P
    >%%
  ```
  - this is what appears on the RHS of the einstein field equation.
  - the simple form is because of the symmetries of the FLRW metric.
  - it appears to be the E-P tensor of an ideal fluid at rest. BUT many 
    constituents of the universe don't behave like fluids. 

time evolution of T: the continuity equation in GR
--------------------

  want: to know the time evolution of the components of T.
  - e.g. for a fluid sans gravity, with negligible velocities, the pressure and 
    energy density evolve according to the continuity equation
    ```
      𝝏₍t₎ρ = 0
    ```
    and the euler equation
    ```
      𝝏₍x₍i₎₎P = 0
    ```
  - want a generalised form of this, a 4-component conservation equation for T.
  - note the coordinate derivatives of tensors don't have meaning in GR, since 
    they're fundamentally coordinate dependent. so you have to use the covariant 
    derivative
    ```
      ∇₍μ₎T⟦μ'ν⟧ = 𝝏₍x⟦μ'⟧₎T⟦μ'ν⟧ + Γ... = 0
    ```
  - this the GR version of the continuity and euler equations. the statement of 
    local energy and momentum conservation.

the fluid equation
--------------------

  after some manipulation, you get the conservation law in an expanding 
  universe:
  ```
    𝝏₍t₎ρ + ÷{˙a}{a}·(3ρ + 3P) = 0
  ```
  - aka the FLUID EQUATION (?)
  - you can use the conservation law to infer details about the scaling of 
    matter/radiation with the expansion.

  nonrelativistic matter has zero pressure so
  ```
    𝝏₍t₎(ρ₍m₎·a⁽3⁾) = 0
  ```
  - so energy density of matter follows ˝ρ₍m₎ ∝ a⁽-3⁾˝.

  apply to radiation, where P₍r₎ = ρ₍r₎/3, so from the fluid equation you get
  ```
    𝝏₍t₎ρ₍r₎ + ÷{˙a}{a}·4ρ₍r₎ = a⁽-4⁾·𝝏₍t₎(ρ₍r₎·a⁽4⁾) = 0
  ```
  - so the energy density of radiation follows ˝ρ₍r₎ ∝ a⁽-4⁾˝.

the equation of state parameter
--------------------

  can summarise the matter and radiation cases in one equation, generalise the 
  evolution to other constituents, by defining the equation of state parameter 
  ˝w₍s₎˝, 
  ```
    w₍s₎ = ÷{P₍s₎}{ρ₍s₎}
  ```
  where s is some constituent of the universe. 
  - matter: w = 0
  - radiation: w = 1/3
  - cosmological constant: w = -1

the distribution function
--------------------

  so far: _mean_ density and pressure. these are macroscopic. but 
  microscopically, in a volume centered on a given point, matter and radiation 
  are made up of many particles of different species, possibly interacting. you 
  can describe these statistically by their distribution functions.

  for an infinitesimal volume element d⁽3⁾x centred around point x and time t, 
  the distribution function f₍s₎(⬀x, ⬀p, t) counts the number of particles of 
  species s within an infinitesimal momentum-space volume d⁽3⁾p. then you get 
  the total energy density of the species by summing this energy over all phase 
  space elements, weighted by the number of particles:
  ```
    ∑ f₍s₎(⬀x, ⬀p, t)·E₍s₎(p)
  ```
  where
  ```
    E₍s₎(p) = √{p⁽2⁾ + m₍s₎⁽2⁾}
  ```

  the number  of phase space elements in a region of 6D phase volume:
  ```
    d⁽3⁾x·d⁽3⁾p/(2πħ)⁽3⁾
  ```
  - heisenberg's principle: no particle can be localised in a region of phase 
    space smaller than (2πħ)⁽3⁾. so this is the size of a fundamental element.

  divide this by the volume d⁽3⁾x, you get the energy density of species s, 
  ```
    ρ₍s₎(⬀x,t) = g₍s₎ ∫ d⁽3⁾pͺ÷1{(2π)⁽3⁾}ͺf₍s₎(⬀x,⬀p,t)ͺE₍s₎(p)
  ```
  - g₍s₎ is the degeneracy of the species. = 2 for photons, since it has two 
    spin states. also, assuming ħ = 1.

  an expression for pressure:
  ```
    P₍s₎(⬀x,t) = g₍s₎ ∫ d⁽3⁾pͺ÷1{(2π)⁽3⁾}ͺf₍s₎(⬀x,⬀p,t)ͺ÷{p⁽2⁾}{3E₍s₎(p)}
  ```

  during the early universe, reactions happened fast enough that particles were 
  in equilibrium. different species had the same common T. you want to express 
  the energy density and pressure ito this T. 

  in equilibrium at temp T, bosons (e.g. photons) have a BE distribution, so 
  f₍s₎(⬀x,⬀p,t) = f₍BE₎(E₍s₎(p)), 
  ```
    f₍BE₎(E) = ÷1{e⁽(E-μ)/T⁾ - 1}
  ```
  and fermions (e.g. electrons) have a FD distribution
  ```
    f₍FD₎(E) = ÷1{e⁽(E-μ)/T⁾ + 1}
  ```

BOLTZMANN EQUATION
================================================================================

  the number of particles in a small phase space volume element around the 
  specific point [⬀x,⬀p]:
  --------------------
  ```
    N(⬀x,⬀p,t) = f(⬀x,⬀p,t)ͺ(Δx)⁽3⁾ͺ÷{(Δp)⁽3⁾}{(2π)⁽3⁾}
  ```
  in the limit of big N (large number of particles in the volume element), f 
  approaches a continuous function that describes the state of the _collection_ 
  of particles.

  the appropriate integration measure is d⁽3⁾xͺd⁽3⁾p/(2π)⁽3⁾.

  want: an equation the governs (the evolution of?) this distribution function. 
  - this equation should follow from the eoms obeyed by the individual 
    particles.


  equation for the distribution function, sans interactions
  -------------------- 
  neglect particle-particle interactions. 

  the only forces acting on the particles are long range forces. can describe 
  via force field, i.e. acceleration field ⬀a(⬀x,⬀p,t). 
  - if the force field is gravity, then ⬀a = -∇Ψ(⬀x,t). the gravitational 
    potential Ψ is independent of the particle momenta.
  - could also be the lorentz force from electromagnetic fields.

  using the def of momentum, the eoms for nonrelativistic particles are
  ```m
    ˙x = ÷pm
    ˙p = m·a(x,p,t)
  ```

  the number of particles is conserved. so you can say the total time derivative 
  of f vanishes, 
  ```
    D₍t₎f(x,p,t) = 0
  ```
  where 
  ```
    D₍t₎ = 𝝏₍t₎ + ˙x⦁∇₍x₎ + ˙p⦁∇₍p₎
  ```
  - >> the advective derivative? <<
    {.question}
  - ∇₍x₎ and ∇₍p₎ are the gradients wrt x and p

  when you insert the eoms, this becomes
  ```mm
    𝝏₍t₎f(x,p,t) 
    &= -(˙x⦁∇₍x₎)ͺf(x,p,t) - (˙p⦁∇₍p₎)ͺf(x,p,t)
    &= -÷1m·(p⦁∇₍x₎)ͺf(x,p,t) - m·(a(x,p,t)⦁∇₍p₎)ͺf(x,p,t)
  ```

  so, the rate of change of the distribution function, 𝝏₍t₎f, is determined by 
  how many particles move in/out of the phase space volume element. 
  equivalently, the phase space volume occupied by a collection of particles is 
  conserved.

  equation for the distributions, with interactions
  --------------------
  if there are particle-particle interactions, modify the equation to include a 
  collision term on the rhs. describes how particles are moved from one phase 
  space element to another 
  ```
    D₍t₎f = C([f])
  ```

  expression for T given a distribution function f(x,p,t)
  --------------------
  ```
    T⟦μ'ν⟧(⬀x,t) 
    = ÷g{√{-det(g⟦αβ⟧)}} 
      ∫ dP₍1₎dP₍2₎dP₍3₎ͺ÷1{(2π)⁽3⁾}ͺ÷{P⟦μ'⟧·P⟦ν⟧}{P⟦0'⟧}ͺf(⬀x,⬀p,t)
  ```
  - g is the degeneracy factor, counts how many different particle states are 
    described by the distribution function f
  - P⟦μ'⟧ = dx⟦μ'⟧/dλ is the comoving momentum
  - P⟦μ⟧ = g⟦μν⟧·P⟦ν'⟧ 
  - the physical momentum ⬀p is related to P⟦i'⟧ via the equation p⟦i'⟧ = a·P⟦i'⟧

  the energy momentum tensor gives the current density of the 4-momentum carried 
  by the particles with distribution function f.

  boltzmann equation in the homogeneous expanding universe:
  --------------------
  ```
    𝝏₍t₎f + ÷pE·÷{⬀e₍p₎⟦i'⟧}{a}·𝝏₍x⟦i'⟧₎f - Hp·𝝏₍p₎f = C([f])
  ```
  - is valid for all particles, but this equation gets reduced in two important 
    limits.

  relativistic limit, p » m, E ∼ p and
  ```
    𝝏₍t₎f + ÷{⬀e₍p₎⟦i'⟧}{a}·𝝏₍x⟦i'⟧₎f - Hp·𝝏₍p₎f = C([f])
  ```

  nonrelativistic limit, p « m, and E ∼ m, so
  ```
    𝝏₍t₎f + ÷pm·÷{⬀e₍p₎⟦i'⟧}{a}·𝝏₍x⟦i'⟧₎f - Hp·𝝏₍p₎f = C([f])
  ```

  time evolution of the number density of a species using boltzmann
  --------------------
  the number density n(x,t) is the integral of f(x,p,t) over all momenta. so 
  integrate the boltzzmann equation above over p. use tft in the homogeneous 
  universe, 𝝏₍x⟦i'⟧₎f = 0, so
  ```
    ∫ d⁽3⁾p·÷1{(2π)⁽3⁾}·𝝏₍t₎f - H ∫ d⁽3⁾pͺ÷1{(2π)⁽3⁾}ͺpͺ𝝏₍p₎f
    =
    ∫ d⁽3⁾pͺ÷1{(2π)⁽3⁾}ͺC([f])
  ```
  which becomes
  ```
    D₍t₎n(t) + 3H·n(t) = ∫ d⁽3⁾pͺ÷1{(2π)⁽3⁾}ͺC([f])
  ```

EXPANSION OF THE UNIVERSE
================================================================================

  you integrate the friedmann equation to get the age of the universe ito 
  cosmological parameters.
  --------------------

  the energy density scales as ρ/ρ₍0₎ = (R/R₍0₎)⁽-3⁾ for a matter dominated 
  universe, and (R/R₍0₎)⁽-4⁾ for a radiation dominated universe. the friedmann 
  equation becomes
  ```m
    (˙R/R₍0₎) + k/R₍0₎⁽2⁾ = ÷13 8πG ρ₍0₎ (R₍0₎/R) ͺͺͺmatterͺdominated
    (˙R/R₍0₎) + k/R₍0₎⁽2⁾ = ÷13 8πG ρ₍0₎ (R₍0₎/R)⁽2⁾ ͺͺͺradiationͺdominated
  ```

  can use tft k/R₍0₎⁽2⁾ = H₍0₎⁽2⁾(Ω₍0₎-1), so the age of the universe as a 
  function of R₍0₎/R = 1 + z is given by
  ```
    t = ...
  ```

  the time scale for the age of the universe is set by the hubble time H₍0₎⁽-1⁾.


  one of the "routine" duties of an early universe cosmologist is to compute the 
  current mass density contributed by some massive stable particle species 
  hypothesised by a particle physicist.
  --------------------

  and to determine whether it's at odds with standard cosmology.

  you express the mass density contributed by the particle species S in terms of 
  ρ₍C₎:
  ```
    ρ₍X₎ = 1.88⋄10⁽-29⁾(Ω₍X₎h⁽2⁾)ͺgͺcm⁽-3⁾
  ```

DENSITY
================================================================================

  the density Ω is the ratio of the actual observed density ρ to the critical 
  density ρ₍c₎ of a friedmann universe. the ratio determines the overall 
  geometry of the universe. when they're equal, flat. 

  in earlier models that didn't have a cosmological constant, critical density 
  was defined as the watershed point between an expanding and contracting 
  universe.

  ρ₍c₎ is estimated to be 5 atoms of monatomic hydrogen per cubic metre today. 

  today, the aveg density of ordinary matter in the universe is 0.2-0.25 atoms 
  per cubic metre.

  much more density comes from unidentified dark matter. 

  largest part comes from dark energy. accounts for the cosmological constant.

  the total density = the critical density, exactly, with measurement error. 
  dark energy doesn't lead to contraction of universe. it accelerates the 
  expansion.

  expression for ρ₍c₎: assume Λ = 0 (as for all friedmann universes) and set 
  curvature k = 0. 

  apply substitutions to the first of the friedmann equations, get
  ```
    ρ₍c₎ = ÷{3H⁽2⁾}{8πG} = 1.8788⋄10⁽-26⁾ͺh⁽2⁾ͺkgͺm⁽-3⁾
  ```
  - h = H₍0₎/100 km/s/Mpc.
  
  for H₍0₎ = 67.4 km/s/Mpc, you get h = 0.674, and
  ```
    ρ₍c₎ = 8.5⋄10⁽-27⁾ kg/m⁽3⁾
  ```
