---
time: 202305090737
title: "SR: ARCADE"
type: note
---

ARCADE (Absolute Radiometer for Cosmology, Astrophysics, and Diffuse Emission) 
launched in 2006. 

A high altitude balloon radiometer, intended to measure the heating of the 
universe by the first stars and galaxies after the big bang.

Led by Alan Kogut of NASA Goddard Space Flight Center.

Launched from the Columbia Scientific Balloon Facility in Palestine Texas.

The balloon flew to an altitude of 37 km (120,000 ft) and viewed about 7% of the 
sky during its flight.

The balloon has 7 radiometers cooled to 2.7 K using liquid helium. The purpose 
is to detect temperature differences with sensitivity of 1/1000 of a degree 
against a background which is 3 K. The instrument detects radiation at 
centimeter wavelengths.

The optics in the instrument package are near the top of the {{< 词典 dewar 
flask >}}. The dewar flask cooled the optics to prevent the instruments seeing 
the walls of the container. The design requires superfluid pumps to drench the 
radiometers in liquid helium.
