---
type: blank
domain: cosmology
font: concrete
---

My paper, on the _Space Roar_, is posted here on my notes server:

[https://nombrilisme.gitlab.io/physics/cosmology/space-roar/][1]

[1]: https://nombrilisme.gitlab.io/physics/cosmology/space-roar/

Any questions, let me know.

KA
