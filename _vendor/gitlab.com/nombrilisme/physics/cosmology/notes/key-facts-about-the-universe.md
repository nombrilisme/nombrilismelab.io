---
title: "key facts about the universe"
type: note
draft: true
---

these statements are from pober lecture 1, 27.01.2023. they are all supposedly 
true at the time of writing. we will supposedly learn what exactly these mean 
over the course.

# HOMOGENEOUS UNIVERSE
--------------------------------------------------------------------------------

¶ the universe began with a HOT BIG BANG

¶ the universe has been EXPANDING since the big bang

¶ the LIGHT ELEMENTS were synthesised shortly after the big bang

¶ the universe is filled with RELIC RADIATION from the big bang

¶ the expansion of the universe is now ACCELERATING

# INHOMOGENEOUS UNIVERSE
--------------------------------------------------------------------------------

¶ the universe underwent a period of INFLATION (rapid expansion)

¶ inflation led to a FLAT UNIVERSE today

¶ inflation generated an almost scale invariant spectrum of ADIABATIC GAUSSIAN 
  FLUCTUATIONS

¶ structure in the universe grew from the fluctuations by GRAVITATIONAL 
  INSTABILITY dominated by DARK MATTER

# TENETS OF MODERN COSMOLOGY
--------------------------------------------------------------------------------

¶ the universe can be well described by math

¶ WE EXIST

¶ we are not privileged observers of the universe
