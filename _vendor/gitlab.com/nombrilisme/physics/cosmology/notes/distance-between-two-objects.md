---
time: 202302180312
title: "distance between two objects"
type: note
---

# COMOVING DISTANCE
--------------------------------------------------------------------------------

comoving distance:
```
  x = ∫{t₍0₎}{t} ɗ{t'} ÷{c}{a(t')}
```

light travels ˝dx = ÷1a c  dt˝.

comoving distance between two objets is not a function of time. it's what you'd 
hypothetically measure at a fixed time.

# PROPER DISTANCE
--------------------------------------------------------------------------------

proper distance:
```
  d(t) = a(t) x
```

and ˝x = d(t)˝ today.
