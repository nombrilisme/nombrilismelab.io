---
time: 202302170340
title: "geodesic equation"
type: note
domain: cosmology
---

in minkowski space particles travel in straight lines unless they're acted on by 
forces. in curved spacetime we use a generalised "notion" of a straight line: a 
GEODESIC.

a GEODESIC is the shortest path (extremal path) between two points. when there 
aren't any forces acting on a particle, EXCEPT GRAVITY, the geodesic is the path 
it follows (according to GR). 
- to express this sentiment, we generalise newton's law with no forces ˝Ɗ⁽2⁾₍t₎x 
  = 0˝. we want it to apply to more general coordinate systems (specifically, 
  spacetime).
