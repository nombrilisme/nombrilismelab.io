---
time: 202303011808
title: "nucleosynthesis"
type: note
---

# GENERAL TIMELINE
--------------------------------------------------------------------------------

t = 0: the big bang
--------------------

t ∼ 20 s: BBN begins
--------------------
  at this point the universe had cooled enough to allow 




# BINDING ENERGIES
--------------------------------------------------------------------------------

BINDING ENERGY of a nucleus, B
--------------------
  the energy needed to pull apart the nucleus into its constituent protons and 
  neutrons. OR, the energy released when you _fuse together_ a nucleus from 
  individual protons and neutrons. {{< 𝖗 RYD p241 >}}

  _e.g._ when you bind together a neutron and proton to form deuteron, D, [note D 
  = ⁽2⁾H, "heavy hydrogen"], this releases energy 2.22⦃MeV⦄.
  ```
    p + n ⇌ D + 2.22⦃MeV⦄
  ```


# IONISATION ENERGY
--------------------
  the energy needed to remove the most loosely bound outer electron in a neutral 
  atom.
  - >> OR, the energy released when you _fuse_ add electron to an atom? I mean, 
    I guess it can't be, because adding electrons isn't really the same game. <<
    {.question}


nuclei more massive than Fe/Ni release energy by FISSION (splitting into lighter 
nuclei). nuclei less massive can release energy by FUSION (merging into heavier 
nuclei).
--------------------
  - {{< 𝖗 RYD p241 >}}
  - >> why does the cutoff occur at Fe/Ni? and why is the cuttoff so vaguely 
    specified? shouldn't it be, idk, a _specific_ nucleon number? <<
    {.question}

electrons and protons combined form neutral hydrogen atoms when the T dropped 
suffifiently below the ionisation energy of hydrogen, Q = 13.6 eV.
--------------------
  - >> _sufficiently_ below? I mean, how sufficiently? <<
    {.question}


protons and neutrons "must have" combined to form deuterons when the T dropped 
sufficiently below the binding energy of deuterium, B₍D₎ = 2.22⦃MeV⦄.
--------------------


# EPOCH OF BIG BANG NUCLEOSYNTHESIS (BBN)
--------------------
  the epoch of RECOMBINATION was preceded by an epoch NUCLEAR FUSION. this 
  latter epoch is aka the epoch of big bang nucleosynthesis.


# EPOCH OF BIG BANG NUCLEOSYNTHESIS
================================================================================

in the EARLY UNIVERSE, nucleosynthesis starts when neutrons and protons form to 
form deuterons. _after_ this, successive "acts" of fusion form heavier nuclei 
(is this recombination?).
----


"you'd expect" deuterium synthesis to have occured at temp T₍nuc₎ ≈ 6·10⁽8⁾⦃K⦄, 
at time t₍nuc₎ ≈ 300⦃s⦄.
----
  this is because the binding energy of D is bigger than the ionisation energy 
  of H by a factor B₍D₎/Q = 1.6·10⁽5⁾. ∴ deuterium synthesis must logically at 
  temps 1.6·10⁽5⁾ higher than the recombination temperature, T₍rec₎ = 3760⦃K⦄.

  HOWEVER, this estimate is too low.


the BBN was extremely inefficient. the "preferred" universe would be one where 
baryonic matter consisted of a Fe/Ni alloy. alas, not so.
----
  >> you know, I've no idea why. what "preferred universe?" <<
  {.question}

  currently 3/4 of the baryonic part of the universe is (by mass) still in the 
  form of unbound protons, or ⁽1⁾H.

  if you look for nuclei heavier than ⁽1⁾H, you'll find mainly ⁽4⁾He, which is 
  also pretty lightweight.

  so Fe/Ni only provide 0.15% of the baryonic mass of the galaxy.


the primordial helium fraction of the universe:
----
  ```
    Y₍p₎ = ÷{ρ·(⁽4⁾He)}{ρ₍bary₎}
  ```
  Y₍p₎ is the mass ednsity of ⁽4⁾He divided by the mass density of all baryonic 
  matter.

  the CURRENT helium mass fraction is denoted Y. the outer regions of the sun 
  has Y = 0.27.

  baryonic objects like stars and gas clouds are at least 24% helium by mass.


Free neutrons are unstable. They decay by emitting an electron and an electron 
antineutrino:
--------------------
```
  n → p + e⁽-⁾ + ν₍e-₎
```


# INITIAL CONDITIONS, T » 1 MeV, t ≤ 1 s
================================================================================

the neutron-proton ratio is an important "part" of nucleosynthesis. pretty much 
all the neutrons in the universe become incorporated into ⁽4⁾He.

the neutron proton balance is maintained by these weak interactions
```
  n ⇌ p + e⁽-⁾ + ν₍e-₎
```
```
  ν₍e₎ + n ⇌ p + e⁽-⁾
```
```
  e⁽+⁾ + n ⇌ p + ν₍e-₎
```

when the rate of these interactions is large compared to the expansion rate H, 
there is chemical equilibrium, so μ₍n₎ + μ₍ν₎ = μ₍p₎ + μ₍e₎. thus in chemical 
equilibrium, 
```
  ÷np = ÷{n₍n₎}{n₍p₎} = ÷{X₍n₎}{X₍p₎} = e⁽-Q/T + (μ₍e₎-μ₍ν₎)/T⁾
```
where Q = m₍n₎ - m₍p₎ = 1.293 MeV.
