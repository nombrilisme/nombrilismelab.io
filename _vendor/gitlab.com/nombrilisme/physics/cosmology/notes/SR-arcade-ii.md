---
time: 202305090856
title: "SR: ARCADE II"
type: note
---

ARCADE 2 happened in 2011.

ARCADE 2 (Absolute Radiometer for Cosmology, Astrophysics, and Diffuse Emission) 
was a balloon-borne experiment designed to study the radio sky in the frequency 
range of 3 to 90 GHz. The experiment was launched in 2006 and its main goal was 
to search for faint signals of radiation from the early universe.

The main findings of ARCADE 2 were unexpected and significant. The experiment 
found a pervasive and uniform background signal of radio emission in the 
frequency range it observed, which was much stronger than what had been 
predicted by theoretical models. This signal was dubbed the "ARCADE 2 Excess" 
and it was found to be consistent with a thermal spectrum, indicating that it 
had a physical origin and was not simply the result of instrumental noise or 
interference.

The nature and origin of the ARCADE 2 Excess remains a subject of active 
research and debate among astrophysicists. Some possible explanations include 
emission from stars and galaxies that are too faint to be seen directly, or 
emission from hot gas in the early universe. Other theories propose that the 
signal is related to dark matter or to exotic physics beyond the standard model 
of particle physics.

They detected residual radio emission at 3 GHz. They found that this observation 
was independently detected by a combination of low frequency data and {{< 词典 
FIRAS >}}.

Conclusion about the unexpected emission at 3 GHz: that it's due either to a 
diffuse extragalactic background of emission from discrete radio sources that 
have different properties than the *faint end* of the distribution of known 
sources, OR it's due to unmodelled residual emission from our galaxy. They 
believe the former to be more likely.

Radio wave frequencies are in the range 30 Hz to 300 GHz. The ARCADE 2 results 
suggest the faint end of the emission distribution of known sources is very 
different from the expected value predicted by the ΛCDM model, _given_ the known 
sources of emission.


Overall, the findings of ARCADE 2 have led to a better understanding of the 
radio sky and the need for further research to understand the nature and origin 
of the ARCADE 2 Excess.
