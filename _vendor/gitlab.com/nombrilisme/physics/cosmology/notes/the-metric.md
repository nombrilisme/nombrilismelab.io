---
time: 2023
title: the metric
type: note
domain: cosmology
---

METRIC: something that returns the _physical_ distance between two 
infinitesimally close points in spacetime (which are defined in some arbitrary 
coordinate system).

note *physical distance* means the real, actual distance (as opposed to the 
distance as defined by vectors and a coordinate system). this is an important 
distinction.  given two points, and a coordinate system, you can define their 
separation using a vector. but the vector is just a mathematical entity. it's 
rooted in the coordinate basis. the *actual, physical* distance requires more 
information: the value of the scale factor a(t) at that time.

---

the metric in cartesian coordinates, (x,y): the physical distance between two 
points separated by dx and dy in 2D is £√{dx⁽2⁾ + dy⁽2⁾}£.

the metric in polar coordinates, (r,θ): the physical distance between two points 
separated by dr and dθ (if dr and dθ are small) is £√{dr⁽2⁾ + r⁽2⁾·dθ⁽2⁾}£.

the actual (physical) distance is INVARIANT. it doesn't matter whether you use 
cartesian, polar, or whatever. you'll get the same distance.

so, basically what a metric does is it turns observer-dependent coordinates into 
invariants.
  - > ok... so this sounds a bit like tensor algebra. coordinate independent 
    geometric functions, and all that shit...? <
    {.methinks}

---

let dl be the invariant distance. imagine we're in a 2d. in a 2d plane, the 
square of the invariant distance dl is:
```
  dl⁽2⁾ = ∑{i,j=1,2} g⟦ij⟧ dx⟦i⟧ dx⟦j⟧
```
˝g⟦ij⟧˝ is the metric. in 2D it's a 2x2 symmetric matrix. now, wtf does this 
equation actually mean? here are some examples:

  - in cartesian coordinates, x = [x,y] so x[1] = x and x[2] = y. when you 
    expand out the full equation,
    ```
      = g⟦11⟧ dx⟦1⟧ dx⟦1⟧ + g⟦12⟧ dx⟦1⟧ dx⟦2⟧ 
        + g⟦21⟧ dx⟦2⟧ dx⟦1⟧ + g⟦22⟧ dx⟦2⟧ dx⟦2⟧
    ```
    now obviously, you know that it should come out to ˝dl⁽2⁾ = dx⁽2⁾ + dy⁽2⁾˝. 
    so what this means is in 2D CCs the metric is just the identity matrix:
    ```
      g₍ij₎ =
      \begin{bmatrix}
        1 & 0 \\
        0 & 1
      \end{bmatrix}
      \hspace{1cm} (cartesian)
    ```

  - in polar, ˝x⟦1⟧ = r, x⟦2⟧ = θ˝, or ˝x⟦⟧ = r,θ˝. the metric is
    ```
      g₍ij₎ =
      \begin{bmatrix}
        1 & 0 \\
        0 & r⁽2⁾
      \end{bmatrix}
      \hspace{1cm} (polar)
    ```

---

metrics are useful in curved spaces. e.g. representing points on the earth's 
surface. there's no coordinate system that can accurately represent distances, 
areas, angles. the MERCATOR PROJECTION preserves angles but distorts distances 
and areas near the poles. the WINKEL-TRIPEL coordinates reduce the distortion of 
distances and areas, but distort angles. but this is only a problem for 
mapmaking. when we actually calculate distances, areas, angles, we use the 
metric. the metric will look different in different coordinate systems, but the 
result will be the same.

---

when you use the metric, you can incorporate gravity into the curved spacetime. 
this means you don't have to think about gravity as an external force, where 
particles move in a gravitational field. instead you can _include_ gravity in 
the metric. then particles move freely in a distorted (curved) spacetime. 

the principle underlying this is COVARIANCE. einstein's realisation: an 
observer in a gravitational field would make the same measurements as an 
observer in an accelerating reference frame that has gravity baked in.

---

spacetime has 4 dimensions. time is part of the invariant. let ds be the 
invariant distance. the metric is:
```
  ds⁽2⁾ = ∑{i=0:3, j=0:3} g⟦ij⟧ dx⟦i⟧ dx⟦j⟧
```
  - where ˝x = [t,x,y,z]˝, so ˝x⟦0⟧ = t˝ and ˝x⟦1:3⟧˝ are the three spatial 
    coordinates.
  - the metric ˝g⟦ij⟧˝ is symmetric. it has 4 diagonal components and 6 
    independent off diagonal components.
  - the time component of the metric has the opposite sign to the spatial 
    component.
  - >> why's this again? <<
    {.question}

---

the metric establishes the link between the values of coordinates and the 
physical value of the interval ds⁽2⁾. this is called the PROPER-TIME INTERVAL.

consider an observer with a watch. 
  - in coordinate system ˝[t, ↗x]˝.
  - she's at the origin.
  - the time equals the time on the watch.
  - define two spacetime events as: the points where her watch reads 12:00:00 
    and 12:00:01. 
  - she doesn't move wrt the spatial coordinates ˝x⟦i⟧˝, so ˝dx⟦i⟧ = 0˝.
  - the invariant interval is:
    ```
      ds⁽2⁾ = g⟦00⟧·dx⟦0⟧⁽2⁾ = g⟦00⟧·dt⁽2⁾ = -(1s)⁽2⁾.
    ```
    (1s = 1 second, because that's the time that elapses. the -ve sign comes 
    from the -ve sign of the 1st component of the metric)
  - ∴ the PROPER-TIME INTERVAL is the time elapsed according to the observer's 
    watch (excluding the -ve sign).
  - consider another observer who is moving wrt the first. she may have a 
    different ˝dt˝ and ˝d↗x˝. but will have an identical value of the proper time 
    interval ˝ds˝ for the same two events.

two events with a -ve proper time interval (PTI) means they're separated by a 
*time-like interval*. two events with a +ve PTI are separated by a *space-like 
interval*. two events with ds⁽2⁾ = 0 are connected by light rays.

---

indices, covariant vs contravariant. in 3d, vector ↗x has 3 components, x⁽i⁾, 
where i = 1,2,3. the dot product of two vectors is ↗x⦁↗y = ∑{i=1:3} A⁽i⁾B⁽i⁾.

matrices in component notation. product of matrices M and N
```
  (MN)⟦ij⟧ = M⟦ik⟧N⟦kj⟧
```

in relativity, every vector has 4 components. convention is to use zero 
indexing, so the time component is the 0th component.

another convention: use greek letters to represent all four components (pffft, 
WHY?), so A⁽μ⁾ = [A⁽0⁾, A⁽i⁾].

---

upper indices = CONTRAVARIANT. lower indices = COVARIANT.
```
  A₍μ₎ = g₍μν₎A⁽ν⁾
  A⁽μ⁾ = g⁽μν⁾A₍ν₎
```

a contravariant vector and a covariant vector can be CONTRACTED to produce an 
invariant (a scalar). e.g. the statement that the four-momentum squared of a 
massless particle must vanish:
```
  P⁽2⁾ = P₍μ₎P⁽μ⁾ = g₍μν₎P⁽μ⁾P⁽ν⁾
```

˝g⁽μν⁾·g₍να₎ = δ⁽μ⁾₍α₎˝. ∴ ˝g⁽μν⁾˝ is the inverse of ˝g₍μν₎˝.

---

special relativity is described by MINKOWSKI SPACETIME with metric g₍μν₎ = 
η₍μν₎,
```
  η₍μν₎ =
  \begin{bmatrix}
    -1 & 0 & 0 & 0 \\
    0 & 1 & 0 & 0 \\
    0 & 0 & 1 & 0 \\
    0 & 0 & 0 & 1
  \end{bmatrix}
```
maxwell implicitly used this metric when he derived the equations of e&m. it 
describes a non-curved spacetime.

---

now for the metric that describes the expanding universe. what we know is that 
two points on the grid move away from each other so their distance is 
proportional to the scale factor a(t).  if the coordinate (comoving) distance 
today is x₍0₎, the physical distance between the points at some earlier time t 
was a(t)·x₍0₎, where a today, a₍0₎, is 1.
  - > OH, I get it. I think. a₍0₎ is today, and a₍t₎ is some time ago. so the t 
    variable goes backwards. is that why the -ve sign? jesus. <
    {.methinks}

∴ in the euclidean (flat, as opposed to open or closed) universe, the metric 
is similar to the minkowski metric, but the spatial coordinates are multiplied 
by the scale factor. 

∴ the metric in an expanding, euclidean universe is
```
    g₍μν₎ = 
    \begin{bmatrix}
      -1 & 0 & 0 & 0 \\
      0 & a⁽2⁾(t) & 0 & 0 \\
      0 & 0 & a⁽2⁾(t) & 0 \\
      0 & 0 & 0 & a⁽2⁾(t) 
    \end{bmatrix}
```
aka the FRIEDMAN-LEMAITRE-ROBERTSON-WALKER (FLRW) METRIC for a euclidean 
universe.

to find out how a(t) evolves in time, need to know the composition of the 
homogenous constituents in the universe. for this you have to use einstein's 
equations. 

when you introduce perturbations, the metric becomes even more complicated. it 
will depend additionally on location in space. we will generalise the FLRW 
metric to include functions describing deviations from uniformity that depend 
on both time and space. these "perturbed" parts of the metric will be 
determined by the inhomogeneities in the matter and radiation.

next up: consider how matter and radiation behave in an expanding universe.  how 
to go from infinitesimal invariant intervals to actual finite distances.
