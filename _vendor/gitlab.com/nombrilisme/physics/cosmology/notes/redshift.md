---
time: 202302161749
title: "redshift"
type: note
---

# BASICS
--------------------------------------------------------------------------------

*define* {{< 词典 redshift >}}: an observed increase in the wavelength of 
electromagnetic radiation (e.g. light). and the accompanying decrease in 
frequency.

the opposite effect is called {{< 词典 blueshift >}}. the red/blue terminology 
comes from the visible light colour spectrum. increase in wavelength = more 
"red".

causes of redshift:
  1. RELATIVISTIC REDSHIFT: the EM radiation is travelling between objects that are 
  moving apart. 

  2. GRAVITATIONAL REDSHIFT: EM radiation is travelling towards an object in a 
  weaker gravitational potential. this implies the object is in a less strongly 
  curved (more flat) spacetime.

  3. COSMOLOGICAL REDSHIT: EM radiation is travelling through a space that is 
  expanding. since the universe is expanding, we've observed that all light 
  sources that are far enough away from us show redshift. this is the basis of 
  HUBBLE's LAW.

apparently some examples of redshift that "we know of" are gamma rays that we 
"perceive" as x-rays. or initially visible light that we perceive as x-rays.
  - > ok ok, so it definitely seems like there's passage of time / temporal 
    evolution involved here. talk of something "initially" being something, then 
    later "decaying" to something else. how can you tell by looking at an x-ray 
    that it used to be visible light? <

# GEODESIC DERIVATION OF REDSHIFT
--------------------------------------------------------------------------------

{{< 𝖗 JPN l3p1 >}}

you end up with an expression E ∝ ÷1a. for a photon, E = hc/λ. ths means λ ∝ a.  
so, in an expanding universe, the wavelengths of photons increase as they travel 
along geodesics. 

# DESCRIBING EXPANSION OF THE UNIVERSE
--------------------------------------------------------------------------------

redshift:
```
  1 + z = ÷{λ₍observed₎}{λ₍emitted₎} = ÷1a
```

redshift: ˝z = ÷1a - 1˝.
