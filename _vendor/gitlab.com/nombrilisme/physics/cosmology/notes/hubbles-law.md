---
time: 202302161748
title: "hubble's law"
type: note
domain: cosmology.
---

HUBBLE'S LAW states the linear relationship between the speed that galaxies are 
moving away "receding" from us, RECESSION SPEED, and their distance from us.  
```
  v = H₍0₎·d
```

in 1929 hubble noticed that the recession speed increases with distance 
linearly. since the universe is expanding, then the distance between two 
galaxies should be ˝d = a·x˝ where ˝x˝ is the comoving distance and ˝a˝ is the 
scale factor. if there's no comoving motion (no "peculiar velocity") then ˝Ɗ₍t₎x 
= 0˝ and the relative velocity is
```
  v = Ɗ₍t₎(a·x) = Ɗ₍t₎(a)·x = H₍0₎·d
```
  - so basically, the apparent velocity increases with time. the slope is 
    ˝H₍0₎˝.
  - ˝H₍0₎˝ is the HUBBLE CONSTANT.
  - is valid at low redshift, i.e. ˝v « c˝.

AS IT TURNS OUT THOUGH the hubble constant is actually not constant. it varies 
with time. the linearity only holds for galaxies that are relatively near 
(~ 100 Mpc). 

CURRENT VALUE of the hubble constant:
```
  H₍0₎ = 73.8 ± 2.4  km s⁽-1⁾ Mpc⁽-1⁾
```
  - {{< 𝖗 keeton p222 >}}
  - what this means is, a galaxy 100 Mpc away will have a recession velocity of 
    about 7380 km/s.

DIMENSIONS of the hubble constant: inverse time.

THE HUBBLE TIME: an (inaccurate) estimate of the time since the big bang:
```
  t₍0₎ = H₍0₎⁽-1⁾ = 4.18e17  s = 13.3  Gyr
```
this comes from tft the universe is expanding, so galaxies are moving apart. if 
you reverse time and watch them come back together, and if you assume the 
expansion occured at a constant rate (it didn't) then the age of the universe 
should be the inverse of the hubble constant. ˝t₍0₎ = H₍0₎⁽-1⁾˝. even though 
this isn't accurate, it still gives an idea of the time scale of the universe.  
