---
time: 202302191217
title: "units"
type: note
---

¶ parsec, pc. 3.26 light years = 206265 AU = 30.9 trillion km.
  - used for distances to objects outside the solar system.
  - parsec is a portmanteau of "parallax of one second"
