---
title: the universe is expanding
type: note
domain: cosmology
---

two important mathematical tools for describing an expanding universe:
- the METRIC. is the basis for GR.
- the DISTRIBUTION FUNCTION. basis for statistical mechanics.

we use these tools to derive these fundamental properties of a smooth expanding 
universe:
- redshifting of light
- the notion of distance
- the evolution of energy density with scale factor
- the epoch of equality ˝a₍eq₎˝.

performing a COSMIC INVENTORY: identifying the things that make up the universe, 
and which ones in particular dominate the energy budget at different epochs.

we ASSUME the universe is smooth, SPATIALLY HOMOGENEOUS. this means the 
densities of constituents (like matter and radiation) don't vary in space. we 
also assume the constituents have equilibrium distributions. these assumptions 
describe a "zeroth order universe". in reality there are deviations, obv.

THERE ARE two aspects to general relativity:
  1. general covariance. you can describe any physical phenomenon in any 
  reference frame and get the same result. this principle requires space and 
  time to be curved. to describe a spacetime that's curved, you use the metric.

  2. the relationship between spacetime and all the "stuff" in it, like matter 
  and radiation. this part of GR involves einstein's equations.
