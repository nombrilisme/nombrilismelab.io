---
time: 202302191227
title: "sunday sessions"
type: note
---

# THE (SUPPOSED) ISOTROPY AND HOMOGENEITY OF THE UNIVERSE
--------------------------------------------------------------------------------

*define* {{< ℑ COSMOLOGICAL PRINCIPLE >}}: the spatial distribution of matter in 
the universe is HOMOGENEOUS and ISOTROPIC, when viewed on a large enough scale. 
  - HOMOGENEOUS means it has the same properties at every point. that it's 
    uniform and doesn't have irregularities. e.g. a uniform electric field.
  - homogeneity implies there is some kind of invariance. all components of an 
    equation have the same _degree_ of a value, regardless of whether the 
    components are scaled up or down to different values. 

you can see about a trillion GALAXIES in the sky. their distribution "appears to 
be quite isotropic" [TB p1364]. why:
  - the galaxies are strongly clustered. there are clusters of about 1000 
    galaxies.
  - the strength of the clustering, |δN/N|, decreases with increasing linear 
    scale.  it decreases to |δN/N| ∼10⁽-5⁾ when the SCALE SIZE is similar to the 
    size of the universe (∼ 10⁽26⁾ m ∼3 Gpc).
  - IF luminous galaxies are a fair sample of all the material in the universe, 
    THEN on large scales we can assume the matter distribution today as 
    homogeneous.

you can also see this homogeneity from the CMB. 
  - it retains a blackbody spectrum.
  - the T fluctuations are only |δT/T| ≤ 3⋄10⁽-5⁾.
  - this is radiation that's come to us from an epoch when the universe was 
    1000x smaller than today and the T was a 1000x larger, ∼3000 K. 
  - if the universe were inhomogeneous at the time, we'd see much larger 
    fluctuations in temperature, spectrum, polarisation.
  - conclude the young universe was quite homogeneous. as the contemporary 
    universe appears to be on the largest scales.

>> ok, so what does HOMOGENEOUS actually mean? wtf actually is an inhomogeneous 
   universe?  <<
{.question}
  - see above for answer

the "only" alternative: that we live at the origin of a spherically symmetric, 
radially inhomogeneous universe. 
  - the premise that this _isn't_ true is the COPERNICAN PRINCIPLE.
  - the copernican principle has that the universe would look similarly 
    isotropic from all other vantage points at the same time. if this is true, 
    that is, there is isotropy about us _and_ from all other points at the same 
    time, this implies homogeneity.

# THE ZEROTH ORDER APPROXIMATION 
--------------------------------------------------------------------------------

the zeroth order approximation, that the universe is homogeneous and isotropic 
at a given time.

formal assumptions/conditions the 0th order approximation: 
  - there exists a family of slices of simultaneity (3d spacelike 
    hypersurfaces).
  - these slices completely cover spacetime.
  - on a given slice of simultaneity, 
    1. no two points are distinguishable from each other (homogeneity)
    2. at a given point no one spatial direction is distinguishable from any 
    other (isotropy)

to assign slices of simultaneity: use the universe's "clock", the temperature of 
the CMB.
  - ignore (for now) the tiny fluctuations in the CMB temp.
  - >>> ok... <<<
    {.methinks}
  - ENTER a set of FUNDAMENTAL OBSERVERS, FOs. (use ur imagination)
  - do some velocity correction shiz: [TB p1366]. give them a velocity that 
    removes "dipole anisotropy" in the T distro. then, when you say "observation 
    on earth", these crsp2 observations made by an FO _coincident_ with earth 
    today. then, to get the ACTUAL earth observations, add a small doppler shift 
    to the FO observations.
  - >>> yeah, read about this. and revise SR and GR please. <<<
    {.methinks}
  - the individual FOs move on world lines that keep the CMB isotropic.
  - the slices of simultaneity, the 3d hypersurfaces, are the spaces the FOs 
    live on. they're defined as spaces with the same CMB temperature. we 
    approximate as homogeneous.
  - >> why approximate? <<
    {.question}
  - isotropy implies the FO world lines are orthogonal to the slices of 
    simultaneity.
  - to focus on the hypersurface we live on today: freeze the action when 
    everyone measures the CMB temperature to be 2.725 K.
  - >>> simulate this. <<<
    {.methinks}

# METRIC TENSOR
--------------------------------------------------------------------------------

next object: we want to explore the geometry of the 3d space (slice of 
simultaneity) we just froze. task 1: figure out its metric tensor.

introduce a spherical coordinate system [χ,θ,φ] that's centred on us. 
  - χ is the radius from us to a distant point
  - θ and φ are spherical polar angles measured from the earth's north pole and 
    the direction of the sun at vernal equinox (for eg).

also introduce a cartesian coordinate system,
```mm
  ↗χ &= [χ⟦1'⟧, χ⟦2'⟧, χ⟦3'⟧]
     &= [χ·sinθ·cosφ, χ·sinθ·sinφ, χ·cosθ]
```

since space around us is isotropic, the angular part of the metric is dθ⁽2⁾ + 
sin⁽2⁾θ·dφ⁽2⁾ at each radius.

however, you CANNOT assume that the space is globally flat, and that the area of 
a sphere of constant radius χ is 4πχ⁽2⁾. 

this means you have to generalise the metric to become
```
  ds⁽2⁾ = dχ⁽2⁾ + Ξ⁽2⁾(dθ⁽2⁾ + sin⁽2⁾θ·dφ⁽2⁾)
```
  - Ξ(χ) is a fn tbd.
  - but, to satisfy the requirement that the space be LOCALLY flat, it must be 
    true that Ξ ≃ χ for small χ.
  - >>> OMG! <<<
    {.methinks}

derivation of the metric tensor...

let K = k/R⁽2⁾. R is the radius of curvature. three solutions, via parameter k = 
±1 or 0:
  1. Ξ = R·sin(χ/R),  for k = 1.
  - this describes a 2d sphere embedded in 3d eucl space. CLOSED.

  2. Ξ = χ, for k = 0.
  - flat euclidean space. FLAT UNIVERSE.

  3. Ξ = R·sinh(χ/R), for k = -1.
  - 2d space, but can't be embedded in 3d euclidean. OPEN.

# RW METRIC
--------------------------------------------------------------------------------

since relativistic cosmology happens in spacetime, not space, you need to 
generalise the space to include a time coordinate and allow for the expansion of 
the universe.

first thing: assume the 3-spaces in the past were also homogeneous and had the 
same geometry as today. the only thing you change is the radius of curvature. 
so, write
```
  R(t) = a(t)·R₍0₎
```
  - R₍0₎ is the value today, at t = t₍0₎ and a = a₍0₎ = 1.
  - a(t) is the SCALE FACTOR.
  - a FO moves from one hyperspace to the next, carrying the SAME spherical 
    coordinates [χ,θ,φ]. these are the COMOVING COORDINATES.

the components of the metric, ˝⬈g˝:
  - the world line of an FO is orthog to the hypersurface at time t. 
  - define the basis vector ˝𝝏{t} = 𝝏/𝝏t˝ that's tangent to the world line of 
    the FO 
  - >> this "basis" vector, it's in the ˝↗e₍t₎˝ direction right? hmm. <<
    {.question}
  - ∴ ˝g⟦ti⟧ = 𝝏{t}⦁𝝏{i} = 0˝.
  - for ˝g⟦tt⟧˝, set it to be -1, because that way you can add up al lthe 
    intervals to make the time coordinate ˝t˝ be the total age of the universe 
    since the big bang.

so the full metric is
```
  ds⁽2⁾ = -dt⁽2⁾ + a(t)⁽2⁾(dχ⁽2⁾ + Ξ⁽2⁾(dθ⁽2⁾ + sin⁽2⁾θ·dφ⁽2⁾))
```
  - the FLRW metric.
  - you can verify that the acceleration ˝∇₍↗u₎↗u˝ of an FO vanishes. so FOs 
    with fixed ˝↗χ˝ follow geodesics.

# EINSTEIN TENSOR
--------------------------------------------------------------------------------
