---
title: notes
type: note
---

- {{< note key facts about the universe >}}
- {{< note the universe is expanding >}}
- {{< note redshift >}}
- {{< note hubbles law >}}
- {{< note geodesic equation >}}
- {{< note the metric >}}
- {{< note distance between two objects >}}
- {{< note thermodynamics >}}
- {{< note nucleosynthesis >}}
- {{< note big bang nucleosynthesis >}}
- {{< note nonlinear structure and halo formation >}}
- {{< note spaceroar >}}
{.notelist}
