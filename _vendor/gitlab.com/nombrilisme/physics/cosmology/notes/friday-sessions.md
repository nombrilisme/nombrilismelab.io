---
time: 202304070703
title: "friday sessions"
type: long
---

# EINSTEIN & BOLTZMANN EQUATIONS AT EARLY TIMES
--------------------------------------------------------------------------------

to understand structure in the universe: equations governing perturbations 
around a smooth background. supposedly in ds ch5. 

to actually solve these equations, you need to know the initial conditions. this 
is a "quest" that will lead to a new realm of physics: inflation.

inflation was introduced to explain why regions that couldn't have been in 
causal contact with each other have the same T.

breakthru: the mechanism that explains the uniformity of the T in the U can also 
explain the origin of perturbations in the U.

so, to generate a set of ICs, enter INFLATION. the caveat is that we're not 
certain that inflation is the mechanism that generated the initial 
perturbations. you can't really test a theory based on energy scales beyond the 
reach of accelerators. but it's the most plausible explanation.

ch4-5: there are 9 1st order DEs for the 9 perturbation variables you need to 
track. in theory you need ICs for all of these variables. but we have some 
arguments that relate these variables, so you only need to det the IC for ONE. 
ch6.1: describes the way all the vars depend on the gravitational potential Φ 
at early times. rest of sections: work out the ICs for Φ.

first consider the boltzmann equations at very early times:
ds 4.100-4.107
in particular you consider times so early that for any k mode of interest, kη « 
1. this inequality leads to many simplifications.

next, look at einstein equations at early times: ds 5.27. simplifies to 6.4, and 
ultimately 6.7. 

the 2nd einstein equation 5.33 describes how higher moments of the photon and 
neutrino distributions cause Ψ+Φ to be nonzero. we neglect the higher order 
moments, 

the rest o 6.1: a bunch of derivations of initial conditions.

# MIDLIFE CRISIS
--------------------------------------------------------------------------------

ch6.2: "midlife crisis". up till now we:
- reviewd the standard BB cosmology
- expanded about this zero order smooth U, and got evol eqs for the perts to the 
  pcl distibs and the gravitational fields.
- we realised that these coupled DEs need ICs so in the sec 6.1 we set them up.
now we ask: what caused those initial perturbations? it's easy to say Φ = 2Θ₍0₎ 
initially. it's another thing to explain what caused Φ ≠ 0 in the first place.

recall physical meaning of conformal time η: the MAX comoving distance travelled 
by light, since the beginning of the U. objs separated by comoving distances 
larger than η today weren't ever in causal contact. there's no way information 
could have propagated over distances larger than η. so this is why we call η the 
COMOVING HORIZON.

revisit the condition kη « 1. this is the inequality that led to many 
simplifications. 
- k ≃ inverse wavelength of the mode in question (± factor of 2π)
- so kη is the ratio of the comoving horizon to the comoving wavelength of the 
  perturbation. if it's « 1, the mode in question has wavelength so big that no 
  causal physics could have affected it.
- pic worth remembering: fig 6.1
- the horizon grows as the scale factor increases. but comoving wavelengths are 
  constant.
- ∴ all modes of cosmological interest had wavelengths much larger than the 
  horizon early on. eventually these modes ENTER the horizon. after that, causal 
  physics starts to operate on them.

disturbing part of this realisation: look at the CMB today. on all scales 
observed, the CMB is close to isotropic. but how? 
- the largest scales observed have entered the horizon only recently, long after 
  decoupling.
- b4 decoupling, the wavelengths of these modes are so large that no causal 
  physics could force deviations from smoothness to go away.
- after decoupling, photons don't interact. they FREESTREAM.
- so even tho it's possible that photons reaching us today from opposite 
  directions had a change to communicate with each other and equilibriate to the 
  same T, practically this couldn't have happened.
- so the q remains: why is the CMB so uniform? 
- this is a big problem that we glossed over by assuming the T is uniform and 
  perturbations about the zero order T are small.

more intuitive pic of horizon problem: fig 6.2. region in the cone: at any time, 
causally connected to us. photons emitted from the last scattering surface 
(redshift z ∼ 1000) started outside this region. so at the last scattering 
surface, they weren't in causal contact with us or each other. yet, their Ts are 
identical.

# INFLATION

this sec: a "beautiful" sol to the horizon problem from the prev sec.
- first, a logical way out of the prev arg by realising that: 

  an early epoch of rapid expansion solves the horizon problem.

- then: consider the einstein equations to tell us what type of E is needed to 
  actually produce this rapid expansion. 
- the answer: a negative pressure is needed.
- finally: consider a scalar field theory, show that negative pressure is easy 
  to accommodate in such a theory.

couple o points/caveats/disclaimers:
- there's no known scalar field that can drive inflation. so it might be tru 
  that the idea of inflation is right, but it's still driven by something other 
  than a scalar field.

there are still good reasons to work with scalar fields:
- almost all fundamental physics theories contain scalar fields.
- pretty much all curr work on inflation is based on a scalar field or two.
- alternative, from the pp viewpoint: use a vector field, like the emag 
  potential or a set of fermions, to drive inflation. neither works very well, 
  and they both complicate things severely, so we'll stick to a scalar field.

# INHOMOGENEITIES
--------------------------------------------------------------------------------

ch6: we set up the system of equations that have to be solved. and we set up the 
ICs for the perturbations. thus, now, we can calculate the inhomogeneities and 
anisotropies in the U. 

ch7: is the "first" solutions chapter. we start with perturbations to the dark 
matter. 
- in principle, these are coupled to all other perturbations
- but in reality the perts to DM depend minimally on the radiation perts.
- by def, DM is affected by radiation only indirectly. thru gravitational 
  potentials. at late times when the U is dominated by matter, these potentials 
  are independent of radiation. at early times, the perts are determined by 
  radiation, but the radiation perts themselves are simple so all moments beyond 
  the monopole and dipole can be neglected.

goal of all this: compare theory with observations. 
- we'll solve for the evol of each F mode δ(k,η)
- given the solution and the init power spectrum generated by inflation, you can 
  construct the power spectrum of matter today
- on large scales this is the most important observable. 
- on small scales it's quite hard to compare with observations today, pq you 
  have to worry about nonlinearities and gas dynamics.

anyway: even on small scales, the LINEAR POWER SPECTRUM is the starting point 
for any quantitative statement about the distrib of matter. we will compute the 
linear power spectrum in this chapter.

# GRAVITATIONAL INSTABILITY
--------------------------------------------------------------------------------

gravitational instability is most likely responsible for the structure in the 
universe.

as time evolves, matter accumulates in initially overdense regions. doesn't 
matter how small the initial overdensity was (typically overdensity was of order 
1/10⁽5⁾). eventually enouch matter will be attracted to the region to form 
structure.

the F = ma of gravitational instability is: the equation governing overdensities 
δ. schematically, it is
```
  ¨δ + (pressureͺ-ͺgravity)δ = 0 
```
- this is a cartoon equation
- D fig 7.1: mass near an overdense region is attracted to the centre by gravity 
  but repelled by pressure
- if the region is dense enough, gravity wins and the overdensity grows with 
  time.

so basically, the basic forces (pressure and gravity) act in opposite 
directions. gravity acts to increase overdensities, "grabbing" more matter into 
the region. 

there are more particles in an overdense region, so random thermal motion causes 
a net loss of mass in an overdense region. so if pressure is strong, 
inhomogeneities don't grow.
- the cartoon equiation: if pressure is strong, δ grows exponentially; if large, 
  δ oscillates with time.

there are many versions of the gravitational instability in eq 7.1. 
- different "ambient" cosmological conditions affect the growth rate.
- in a matter dominated U, δ grows as a power of time, not exponentially
- in a radiation dominated U, the growth is (fuck's sake) logarithmic.

# THREE STAGES OF EVOLUTION
--------------------------------------------------------------------------------

there are 3 stages of the evolution of cosmological perts. to see this: fig 7.2 
shows the solutions for different modes.

fig 7.2: shows the gravitational potential as a function of scale factor for 
long, medium, short wavelength modes
- early on, all the modes are outside the horizon (kη « 1) and the potential is 
  constant
- at intermediate times (shaded) two things happen:
  - wavelengths fall within the horizon, and
  - the  universe evolves from radiation domination (a « a₍eq₎) to matter 
    domination (a » a₍eq₎). 
- the order of these epochs (a₍eq₎ and the epoch of horizon crossing) strongly 
  affects the potential.
- the large scale mode enters the horizon well after a₍eq₎ evolves very 
  differently than the small scale mode, which enters the horizon before 
  equality.
- at late times, all modes evolve identically again, remaining constant.
- we're assuming Ω₍m₎ = 1.

we can observe the distribution of matter mostly at late epochs (3rd stage of 
evolution). this is when all modes are evolving identically. 

to relate the potential during these late epochs to the primordial potential 
during inflation, schematically, 
```
  Φ(⬀k,a) = Φ₍p₎(⬀k) × (transferͺfn(k)) × (growthͺfn(a))
```
where ˝Φ₍p₎˝ is the primordial value of the potential set during inflation.

the TRANSFER FUNCTION: describes the evolution of perturbations thru the epochs 
of horizon crossing and radiation/matter transition (shaded region).

the GROWTH FACTOR: describes the wavelength independent growth at late times.

when it comes to actually defining the transfer function, there's a convention 
that you remove the slight decline in the largest wavelength pert as the U 
passes thru the epoch of equality. you remove the decline so the transfer 
function on large scales is equal to 1. so the transfer function is defined
```
  T(k) = ÷{Φ(k,a₍late₎)}{Φ₍largescale₎(k,a₍late₎)}
```
where a₍late₎ means an epoch well after the transfer function regime. the 
largescale solution is the primordial Φ decreased by a small amount.
- in sec 7.2 we derive that this factor is 9/10, when you neglect anisotropic 
  stress

another caveat: wrt the growth function. the ratio of the potential to its value 
after the transfer function regime is defined
```
  ÷{Φ(a)}{Φ(a₍late₎)} = ÷{D₍1₎(a)}{a} ͺͺͺa > a₍late₎
```
where D₍1₎ is the growth function.

in the flat matter dominated case from fig 7.2, the potential is constant so 
˝D₍1₎(a) = a˝.

so, with all the conventions, the potential is
```
  Φ(⬀k,a) = ÷9{10} Φ₍p₎(⬀k) T(k) ÷{D₍1₎(a)}{a} ͺͺͺa > a₍late₎
```

easiest way to probe the potential: measure the matter distribution. 

fig 7.3: evol of matter overdensity for 3 diff modes.
- at late times when the potential is constant and all modes are within the 
  horizon, the overdensity grows with scale factor, δ ∝ a.
- this explains the "odd" nomenclature: why's it called a "growth function" if 
  the potential remains constant?
- D₍1₎ describes the growth of matter perturbations at late times. 
- the growth is consistent with our intuition that as time evolves overdense 
  regions attract more and more matter. and become more overdense.

now we can express the power spectrum of the matter distribution ito the 
primordial power spectrum generated during inflation, and the transfer function, 
and the growth function.

to relate matter overdensity to the potential at late times: use poisson's 
equation - large k no radiation limit of eq 5.81
```
  Φ = ÷1{k⁽2⁾} 4πG ρ₍m₎ a⁽2⁾ δ ͺͺͺa > a₍late₎
```

the background density of matter is: ˝ρ₍m₎ = Ω₍m₎ ρ₍cr₎ / a₍3₎˝ and ˝4πGρ₍cr₎ = 
(3/2) H₍0₎⁽2⁾˝ so
```
  δ(⬀k,a) = ÷{k⁽2⁾ Φ(⬀k,a) a}{(3/2) Ω₍m₎ H₍0₎⁽2⁾} ͺͺͺa > a₍late₎
```

together with eq 7.5, we can relate the overdensity today to the primordial 
potential
```
  δ(⬀k,a) = ÷35 ÷{k⁽2⁾}{Ω₍m₎ H₍0₎⁽2⁾} Φ₍p₎(⬀k) T(k) D₍1₎(a) ͺͺͺa > a₍late₎
```
this holds regardless of how the initial perturbation Φ₍p₎ was generated.

in the "context" of inflation, ˝Φ₍p₎(⬀k)˝ is got from a gaussian distribution 
with mean zero and variance (eq 6.100)
```
  P₍Φ₎ = (50π⁽2⁾/9k⁽3⁾) (k/H₍0₎)⁽n-1⁾ δ₍H₎⁽2⁾ (Ω₍m₎ / D₍1₎(a = 1))⁽2⁾
```

so the power spectrum of matter at late times is
```
  P(k,a) = 2π⁽2⁾δ₍H₎⁽2⁾ ÷{k⁽n⁾}{H₍0₎⁽n+3⁾} T⁽2⁾(k) (÷{D₍1₎(a)}{D₍1₎(a = 1)})⁽2⁾ 
            ͺͺͺ a > a₍late₎ ͺͺͺ (7.9)
```

power spectrum has dimensions (length)⁽3⁾. so to express P as a dimless function 
, multiply by k⁽3⁾ and...
```
  Δ⁽2⁾(k) = ÷{k⁽3⁾}{2π⁽2⁾} P(k)
```
- small Δ means small inhomogeneities
- large Δ means nonlinear perturbations.

fig 7.4: the power spectrum today for two diff models. 
- in both models, P ∝ k on large scales, where the transfer function is 1 (this 
  behaviour is apparent from 7.9. corresponds to the simplest inflationary model 
  where n = 1. 
- on small scales the power spectrum turns over. from fig 7.2, the small scale 
  mode enters the horizon well before matter radiation equality. during the rad 
  epoch the potential decays, so the T fn is « 1. the effect of this on matter 
  perturbations can b seen in fig 7.3, where the growth of δ is stunted starting 
  at a ≃ 10⁽-5⁾ after the mode entered the horizon and ending at a ≃ 10⁽4⁾ when 
  the U becomes matter dominated. modes that enter the horizon earlier go thru 
  even more suppression. so the power spectrum is a decreasing fn of k on small 
  scales.

so, this leads to a realisation: there will be a turnover in the pwr spectrum at 
a scale that corresponds to the one that enters the horizon at matter radiation 
equality. 
- you can see this realisation in fig 7.4. 
- there are 2 models: flat matter dominated U (standard cold dark matter, aka 
  sCDM), and a U with a cosmological constant today (ΛCDM). 
- the main diff between the two models: sCDM has more matter (Ω₍m₎ = 1) and thus 
  an earlier a₍eq₎. 
- earlier a₍eq₎ means only the small scales enter the horizon during the rad 
  dominated epoch. 
- thus the turnover occurs on smaller scales. 

another important scale: the scale above which nonlinearities can't be ignored. 
- this is rougly set by Δ(k₍nl₎) ≃ 1. corresponds to k₍nl₎ ≃ 0.2 h Mpc⁽-1⁾ in 
  most models. 
- the power spectra in fig 7.4 are the linear power spectra today. on scales 
  smaller than k₍nl₎ you can't blindly compare the spectra from fig 7.4 with the 
  matter distribution today.
