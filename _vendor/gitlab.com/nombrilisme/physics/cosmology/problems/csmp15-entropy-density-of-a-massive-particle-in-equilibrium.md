---
time: 202302170704
title: "entropy density of a massive particle in equilibrium"
type: example
source: csmp15
draft: true

what: |
  Show that the entropy density, s, of a massive particle in equilibrium, T « m, 
  μ = 0, is exponentially small. The entropy density at any time is thus well 
  approximated as that due to the relativistic species in equilibrium.
---

Entropy density is defined:
```
  s = ÷1T (ρ + P)
```
where ˝ρ˝ is energy density, ˝p˝ is pressure. {{< 𝖗 KT eq3.71 >}}

The equilibrium distributions for ˝n, ρ, p˝ for a species with mass ˝m˝, 
chemical potential ˝μ˝ at temperature ˝T˝ are:
```m
  ρ = ÷g{2π⁽2⁾} ∫{m}{∞} ɗE ÷{(E⁽2⁾ - m⁽2⁾)⁽1/2⁾}{e⁽(E-μ)/T⁾ ± 1} E⁽2⁾
  n = ÷g{2π⁽2⁾} ∫{m}{∞} ɗE ÷{(E⁽2⁾ - m⁽2⁾)⁽1/2⁾}{e⁽(E-μ)/T⁾ ± 1} E
  p = ÷g{6π⁽2⁾} ∫{m}{∞} ɗE ÷{(E⁽2⁾ - m⁽2⁾)⁽1/2⁾}{e⁽(E-μ)/T⁾ ± 1}
```
[KT eq3.51].

For a massive particle in equilibrium, with ˝m » T˝ (the non-relativistic limit) 
and ˝μ = 0˝, these distribution equations become:
```m
  n = g(÷{mT}{2π})⁽3/2⁾·e⁽-m/T⁾
  ρ = mn
  p = nT
```
[KT eq3.55]. Also since ˝m » T˝, ˝p « ρ˝. Substituting these expressions into 
the equation for entropy density,
```
  s ≈ ÷mT g(÷{mT}{2π})⁽3/2⁾·e⁽-m/T⁾
```
Clearly, the exponential factor goes to zero when ˝m » T˝. The contribution of 
nonrelativistic particles to the entropy density is exponentially small. ∴ you 
can approximate ˝s˝ well as that due to relative species in equilibrium.
