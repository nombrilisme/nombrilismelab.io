---
time: 202304021828
title: "two points on the photosphere"
type: example2
source: csmp31
draft: true

what: |
  Consider two points, ˝A˝ and ˝B˝, at the same comoving distance ˝χ˝ from you 
  (the observer). At conformal time ˝η₍e₎˝, ˝A˝ and ˝B˝ come into causal contact 
  for the first time, i.e. a photon emitted by ˝A˝ at ˝η = 0˝ reaches ˝B˝ at ˝η 
  = n₍e₎˝ (and v.v.). At ˝η₍e₎˝, both ˝A˝ and ˝B˝ emit photons along radial 
  paths towards you, which you receive today at ˝η = η₍0₎ » η₍e₎˝.
  - If you live in an ˝Ω₍m₎ = 1˝ universe, what angular separation do you 
    measure between ˝A˝ and ˝B˝?
  - Evaluate the angle, in degrees, if ˝A˝ and ˝B˝ are situated on the cosmic 
    photosphere at recombination (˝z ≈ 1000˝). Without a period of inflation, 
    regions on the photosphere separated by more than this distance are causally 
    disconnected.
---

In a ˝Ω₍m₎ = 1˝ universe (einstein-de sitter), the friedmann equation is
```
  H⁽2⁾ = (÷{˙a}a)⁽2⁾ = ÷1a d₍t₎a = H₍0₎ a⁽-3/2⁾
```

Substitute in ˝H₍0₎ = ÷23 ÷1{t₍0₎}˝, separate variables, integrate, you get
```m
  ∫ da'ͺa⁽1/2⁾ = ∫ dt'ͺH₍0₎
  \Longrightarrow ÷23 a⁽3/2⁾ = H₍0₎t = ÷23 ÷t{t₍0₎}
  \Longrightarrow a = (÷t{t₍0₎})⁽2/3⁾
```
i.e. the usual result, ˝a ∝ t⁽2/3⁾˝.

Conformal time ˝η˝ is defined
```
  η = ÷1a dt
```

Substitute this into the friedmann equation ˝(3)˝, and follow the same steps, 
you get
```m
  ∫ da'ͺa⁽-1/2⁾ = ∫ dη' H₍0₎ 
  \Longrightarrow 2a⁽1/2⁾ = H₍0₎ η 
  \Longrightarrow a(η) = (÷{H₍0₎ η}{2})⁽2⁾
```
Substitute ˝H₍0₎ = 2/η₍0₎˝, you get 
```
  a(η) = (÷η{η₍0₎})⁽2⁾
```
so ˝a ∝ η⁽2⁾˝.

Invoke the definition of angular diameter distance
```
  d₍A₎ = ÷Lθ
```
where ˝d₍A₎˝ is the proper distance between you and ˝A˝ (or ˝B˝), ˝L˝ is the 
proper distance between ˝A˝ and ˝B˝, and ˝θ˝ is their angle of separation. In 
this problem you're solving for the angular separation ˝θ˝,
```
  θ = ÷L{d₍A₎}
```
so you need expressions for ˝L˝ and ˝d₍A₎˝ (which are both proper distances) 
i.t.o ˝η˝. You're told that a photon emitted by ˝A˝ at ˝η = 0˝ reaches ˝B˝ at ˝η 
= η₍e₎˝ so you can calculate the _comoving_ distance betwen ˝A˝ and ˝B˝ is
```
  χ₍A,B₎ = c ∫₍η=0₎⁽η=η₍e₎⁾ dη = cη₍e₎
```

The _proper_ distance between ˝A˝ and ˝B˝, which I've (annoyingly) called ˝L˝ is 
simply ˝aχ˝, where ˝a = a(η)˝, derived earlier. At conformal time ˝η₍e₎˝ the 
scale factor is ˝(η₍e₎/η₍0₎)⁽2⁾˝, so
```
  L = aχ = c(÷{η₍e₎}{η₍0₎})⁽2⁾ η₍e₎
```

Since a photon that leaves ˝A˝ at ˝η₍e₎˝ reaches you at ˝η₍0₎˝, the _comoving_ 
distance between you and ˝A˝ is
```
  χ₍u,A₎ = c ∫₍η = η₍e₎₎⁽η = η₍0₎⁾ dη = c(η₍0₎ - η₍e₎)
```
and the _proper_ distance between you and ˝A˝ is
```
  d₍A₎ = (÷{η₍e₎}{η₍0₎})⁽2⁾ c(η₍0₎ - η₍e₎)
```

So the angular separation between ˝A˝ and ˝B˝ is
```
  θ = ÷L{d₍A₎} = ÷{η₍e₎}{η₍0₎ - η₍e₎} = ÷{(÷{η₍e₎}{η₍0₎})}{1 - (÷{η₍e₎}{η₍0₎})}
```

__B.__ You're told that ˝A˝ and ˝B˝ are on a cosmic photosphere at recombination, 
with ˝z ≈ 1000˝. To calculate the angular separation, you need to express ˝(10)˝ 
in terms of ˝z˝. You can use the fact that
```
  a = ÷1{1+z}
```
and since ˝a(η) = (η/η₍0₎)⁽2⁾˝, 
```m
  ÷1{1+z} = (÷η{η₍0₎})⁽2⁾
  \Longrightarrow (÷η{η₍0₎}) = ÷1{(1+z)⁽1/2⁾}
```

Substitute this into ˝(10)˝, 
```
  θ = ÷{÷1{(1+z)⁽1/2⁾}}{1 - ÷1{(1+z)⁽1/2⁾}}
```
then substitute ˝z = 1000˝, you get
```
  θ ≈ 0.033ͺrad = 1.87ͺdeg
```
