---
time: 202302170713
title: "a different neutron decay time"
type: example
source: csmp17
draft: true

what: |
  Suppose that the neutron decay time were τ = 89 s rather than 890 s, all else 
  equal. What is the maximum value of Y₍p₎, assuming all available neutrons were 
  incorporated into ⁽4⁾He nuclei?
---

˝Y₍p₎˝ stands for the primordial helium (˝⁽4⁾He˝) mass fraction. It's defined as 
the ˝⁽4⁾He˝ mass density divided by the baryonic mass density,
```
  Y₍p₎ = ÷{ρ₍⁽4⁾He₎ }{ ρ₍baryons₎ }
```

˝⁽4⁾He˝ is formed during big bang nucleosynthesis, from intermediate reactions 
involving deuterium. The amount of ˝⁽4⁾He˝ that gets formed depends on the 
neutron-proton ratio at freezeout.

At freezeout, when ˝T₍f₎ = 9⋄10⁽9⁾⦃K⦄˝ and ˝t ∼1⦃s⦄˝, the neutrinos decouple 
from the neutrons and protons and the neutron-proton ratio gets temporarily 
frozen at the (approximate) value
```
  (÷{n₍n₎}{n₍p₎})₍f₎ = exp(-÷{Q₍n₎}{ƙT₍f₎}) ≈ exp(-÷{1.293⦃MeV⦄}{0.8⦃MeV⦄}) ≈ 
  0.18
```
[values are from DS p93]. But this ratio doesn't remain "frozen" for very long.
Free neutrons are unstable and decay with decay time ˝τ₍n₎˝. (Weak interactions 
also deplete the ratio). So the ratio is only approximately frozen for times 
˝t₍f₎ < t « τ₍n₎˝.

The "epoch" of big bang nucleosynthesis is where deuterium is formed (and 
consequently ˝⁽4⁾He˝ and other light elements). BBN doesn't start immediately 
after freezeout -- the energy is still too high for deuterium formation (the 
"deuterium bottleneck" -- it has a very small binding energy). Deuterium can 
only start being formed at ˝T₍nuc₎ ∼0.07⦃MeV⦄˝, at which point ˝t ∼ 270⦃s⦄˝ (you 
can compute this using the time-temperature relation, DS eq. 4.27, ˝t = 
132⦃s⦄×(0.1⦃MeV⦄/T)^2˝. Using the "regular" neutron decay time ˝τ = 880⦃s⦄˝, you 
can calculate that by the time BBN set in, the neutron-proton ratio would have 
decreased to about 1/6. But if you used ˝τ = 89⦃s⦄˝, then the neutron fraction 
that remains would have decayed to ˝e⁽-270/89⁾ ≈ 0.05˝ of its freezeout level.  
So the neutron-proton ratio would be about ˝0.18×0.05 = 0.009˝.

Using this value, and the following expression for the maximum possible value of 
˝Y₍p₎˝
```
  Y₍p,max₎ = ÷{2f}{1+f}
```
where ˝f˝ is the neutron-proton ratio at the onset of nucleosynthesis, ˝f = 
n₍n₎/n₍p₎˝ [Ryden eq. 9.21], you'd get

```
  Y₍p,max₎ ≈ 0.018
```
