---
time: 202302170703
title: "angular size vs redshift"
type: example
source: csmp13
draft: true
what: |
  In flat spacetime, objects of fixed physical size subtend smaller angles the 
  further away they are. This isn't necessarily so in an expanding universe. 
  Consider the angular size ˝θ(z)˝ of an object whose physical size is ˝L˝ at 
  redshift ˝z˝. Plot ˝θ(z)/L˝ as a function of redshift for:

  - an einstein-de-sitter universe, ˝Ω₍m₎ = 1, Ω₍Λ₎ = 0˝.
  - a flat universe with ˝Ω₍m₎ = 0.3, Ω₍Λ₎ = 0.7˝.

  Find the redshift at which ˝θ(z)/L˝ is minimized for these cosmologies. If all 
  galaxies are at least 10 kpc across (and always have been), what is the 
  minimum angular size of a galaxy in these two universes? Express your result 
  both in terms of ˝H₍0₎˝ and then plug in ˝H₍0₎ = 70ͺkm/s/Mpc˝.
---

Let ˝L˝ be the physical size of an object in the universe, and let ˝θ˝ be the 
angle it subtends. Since the object is very far away, ˝θ˝ is small. You can 
approximate the distance to the object as (using the small angle approximation)
```
  d₍A₎ = ÷Lθ
```
˝d₍A₎˝ is called the _angular diameter distance_. Note it's a "physical" 
distance. Expressed in terms of the comoving distance ˝χ˝, it's ˝d₍A₎ = aχ˝. 

The comoving distance to the object is:
```
  χ = ∫{t₍e₎}{t} ɗt' ÷c{a(t')}
  = ∫{a(t₍e₎)}{a(t)} da' ÷c{a'⁽2⁾·H(a')}
  = ∫{0}{z} dz' ÷c{H(z')}
```
(using t.f.t. ˝H = ÷1a (da/dt)˝, ∴ ˝da = aHͺdt˝). 

What you want is the angle ˝θ˝ as a function of redshift, ˝z˝
```
  θ[z] = ÷{L}{aχ[z]}
```
where ˝χ[z]˝ is the value of the comoving distance integral (I'm thinking of 
more as a functional here), evaluated at a particular value of redshift ˝z˝.

You can express ˝H(z)˝ as
```mm
  H(z) = H₍0₎·(Ω₍m₎(1+z)⁽-3⁾ + Ω₍k₎(1+z)⁽-2⁾ + Ω₍Λ₎)⁽1/2⁾ = H₍0₎·E(z)
```
where, note, I've introduced the function ˝E(z)˝ for brevity (my derivation of 
this expression is in Appendix 13A). With this you can express ˝χ˝ as
```
  χ[z] = ÷{c}{H₍0₎} ∫{0}{z} dz' ÷1{E(z')}
```

I evaluated this numerically using the Simpson algorithm (code linked in 
Appendix 13B).

Below are the results for ˝θ(z)˝ for an einstein de sitter universe with ˝Ω₍m₎ = 1, 
Ω₍Λ₎ = 0˝, and for a flat universe with ˝Ω₍m₎ = 0.3, Ω₍Λ₎ = 0.7˝.

{{< fig plot_θ_z.png 8 >}}

And ˝d₍A₎(z)˝:

{{< fig plot_dA_z.png 8 >}}

For the einstein de sitter universe, for ˝L = 1.0ͺMpc˝, I found that ˝θ₍min₎˝ 
(and ˝d₍A (max)₎˝) occur when redshift reaches around ˝z ≈ 1.25˝. ˝θ₍min₎ = 
÷{L(1+z)H₍0₎}{c·(I[z])₍max₎} ≈ 3.94⋄10⁽-4⁾° = 0.024ͺarcseconds˝.

For the flat universe, for ˝L = 1.0ͺMpc˝, I got ˝θ₍min₎˝ and ˝d₍A,max₎˝ falling 
around ˝z ≈ 1.61˝. ˝θ₍min₎ ≈ 2.86⋄10⁽-4⁾° = 0.017ͺarcseconds˝.

# APPENDIX 13A

[the blackboard]

{{< fig L-angularsize.jpg 10 >}}

# APPENDIX 13B

code: https://github.com/knbe/angularsize
