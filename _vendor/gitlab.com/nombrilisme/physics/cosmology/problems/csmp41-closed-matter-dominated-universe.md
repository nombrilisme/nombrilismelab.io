---
time: 202304021829
title: "closed, matter dominated universe"
title: "41"
type: example2
source: csmp41
draft: true
what: |
  For a closed, matter dominated universe, show that
  ```
    Ω(~η) = ÷2{1+cos(~η)}
  ```
  where ˝~η = √K η˝ is the development angle, running from ˝0˝ to ˝2π˝, and ˝K = 
  H₍0₎⁽2⁾(Ω₍m₎-1)˝ is the curvature. You will need to solve for both ˝a(η)˝ and 
  ˝t(η)˝. Provide analytic forms for these equations as well. Calculate the time 
  in years when the expansion reaches a maximum and the time when the "big 
  crunch" occurs if ˝H₍0₎ = 70ͺkm/s/Mpc˝ and ˝Ω₍0₎ = 1.5˝. Hint: you can reduce 
  the friedmann equation to that of a cycloid, ˝y(1+y'⁽2⁾) = const˝, which you 
  can solve parametrically.
---

The friedmann equation in a matter dominated universe:
```
  H² = (⌠˙a⧸a⌡)² = ÷13 8πG ρ₍m₎ - ⌠k⧸a²⌡
```
Divide by ˝H₍0₎⁽2⁾˝ to express in terms of density parameters ˝Ω₍m₎˝ and ˝Ω₍k₎˝:
```mm
  ⌠H²⧸H₀²⌡
  &= ⌠8πG⧸3H₀²⌡ρ₍m₎ - ⌠k⧸a²H₀²⌡
  &= Ω₍m₎ + Ω₍k₎ a⁻²
```
Rearrange,
```mm
  (d₍t₎a)⁽2⁾
  &= a⁽2⁾ H₍0₎⁽2⁾ (Ω₍m₎ a⁽-3⁾ + Ω₍k₎ a⁽-2⁾)
  &= H₍0₎⁽2⁾ Ω₍m₎ a⁽-1⁾ + H₍0₎⁽2⁾ Ω₍k₎
  &= H₍0₎⁽2⁾ Ω₍m₎ a⁽-1⁾ + H₍0₎⁽2⁾ (1 - Ω₍m₎)
  &= H₍0₎⁽2⁾ Ω₍m₎ a⁽-1⁾ - K
```
where ˝K = H₍0₎⁽2⁾(Ω₍m₎ - 1) = -H₍0₎⁽2⁾Ω₍k₎˝, and ˝Ω₍k₎ = -k / H₍0₎⁽2⁾˝.  In a 
closed universe, ˝k = 1˝, so ˝K = 1˝, and
```
  (d₍t₎a)⁽2⁾ = ÷1a H₍0₎⁽2⁾ Ω₍m₎ - 1
```

This differential equation for ˝a˝ has the same form as the equation of a 
cycloid:
```
  (d₍y₎x)⁽2⁾ = ÷1y C - 1 ͺͺͺ(cycloidͺinͺtheͺxyͺplane)
  𝓮𝓺{cycloid}
```
where ˝C˝ is a constant. The cycloid equation \eqref{cycloid} has the parametric 
solutions:
```m
  y = ÷12C (1 - cos(θ))
  x = ÷12C (θ - sin(θ))
```
where ˝θ˝ is the angle rotated by the cycloid.

([source][1])

[1]: https://en.wikipedia.org/wiki/Cycloid#Equations

Following this lead, you can construct the following parametric solutions for 
the closed matter dominated universe,
```
  a = ÷12 H₍0₎⁽2⁾ Ω₍m,0₎ (1 - cos(~η))
  𝓮𝓺{aEq}
```
```
  t = ÷12 H₍0₎⁽2⁾ Ω₍m,0₎ (~η - sin(~η))
  𝓮𝓺{tEq}
```

If you rearrange ˝(7)˝ for ˝Ω˝, and substitute ˝a = (η⁽2⁾/η₍0₎⁽2⁾)˝ and ˝H₍0₎ = 
2/η₍0₎˝ (since it's a matter dominated universe --- see problemset 3, question 1 
for more details on these expressions), you get
```
  Ω = ÷2{1 + cos(~η)}
```

To solve equations \eqref{aEq} and \eqref{tEq}, first solve the ˝a˝ equation for 
˝~η˝, then substitute it into the ˝t˝ equation:
```
  ~η = cos⁽-1⁾ (1 - ÷{2a}{H₍0₎⁽2⁾ Ω₍m,0₎})
```

and
```m
  t = (÷12 H₍0₎⁽2⁾ Ω₍m,0₎) cos⁽-1⁾(1 - ÷{2a}{H₍0₎⁽2⁾ Ω₍m,0₎}) 
      ͺͺͺ - (÷12 H₍0₎⁽2⁾ Ω₍m,0₎) sin( cos⁽-1⁾(1 - ÷{2a}{H₍0₎⁽2⁾ Ω₍m,0₎}) )
```
```
  = (÷12 H₍0₎⁽2⁾ Ω₍m,0₎) cos⁽-1⁾(1 - ÷{2a}{H₍0₎⁽2⁾ Ω₍m,0₎}) 
    - (a(H₍0₎⁽2⁾Ω₍m,0₎ - a))⁽1/2⁾
```
