---
time: 202304021829
title: "press-schechter theory"
title: "42"
type: example2
source: csmp42
draft: true
what: |
  Consider a universe with ˝Ω₍m₎ = 1˝ and a power-law linear theory power 
  spectrum
  ```
    Δ²(k) = ⌠k⧸k₍✶₎⌡
  ```
  Using press-schechter theory, compute the fraction of mass in the halos more 
  massive that ˝M₍✶₎˝, where ˝M₍✶₎˝ is implicitly defined by the relation ˝ν = 
  (δ₍c₎/σ(M)) = 1˝ at ˝M = M₍✶₎˝. How biased would the clustering of a ˝5M₍✶₎˝ 
  halo be compared to the mass clustering on large scales?
---

The enclosed mass in a comoving radius ˝R˝ is ˝M = ÷13 4π⟨ρ₍m₎⟩R³˝. 

Given the real space density field ˝δ(⬀x)˝, define the _smoothed density field_ 
for length scales ˝R˝ (mass scales ˝M˝) as
```
  δ₍M₎ = ∫ d³xͺδ(⬀x)W(⬀x')
```
where ˝W(⬀x)˝ is the window filter function which is normalized so ˝∫d³xͺW(⬀x) = 
1˝.

Press & Schechter: assume the density field is gaussian with mean zero and 
standard deviation ˝σ(M)˝.  The probability that ˝δ₍M₎ > δ₍c₎˝ is
```mm
  Pr(>δ₍c₎)
  &= ∫₍δ₍c₎₎⁽∞⁾ dδ₍M₎ͺ⌠1⧸√{2π}σ(M)⌡ exp(-⌠δ₍m₎²⧸2σ²(M)⌡)
  &= ÷12 erfc(⌠δ₍c₎⧸√2 σ(M)⌡)
  𝓮𝓺{pr}

```
where ˝δ₍c₎˝ is the linearized critical density for the collapse of a spherical 
top hat. Eq \eqref{pr} gives the probability that a point in the collapsed halo 
has mass ˝> M˝, i.e. the _mass fraction_ of matter ˝> M˝. 

To evaluate this, you need the relationship between variance ˝σ²(M)˝ and the 
linear power spectrum: 
```mm
  σ²(M) &= ⌠1⧸V²⌡ ∫ d³xͺd³x'ͺ W(⬀x) W(⬀x') ξ(|⬀x-⬀x'|)
  &= ∫ d³kͺ⌠1⧸(2π)³⌡ ⌠P(k)⧸V²⌡ W₍k₎²
```
where ˝W₍k₎˝ is the fourier transform of the window filter function. In terms of 
the dimensionless spectrum,
```
  σ²(M) = ∫₍0₎⁽∞⁾ dkͺ÷1k Δ²(k) W₍k₎²
```

Need to fourier transform the window function to fourier space. In real space 
it's defined (spherical top hat filter function)
```mm
  W(r)
  &= ⌠3⧸4πR³⌡ ͺͺͺ r < R
  &= 0 ͺͺͺͺͺͺ else
```
so
```mm
  W(⬀k)
  &= ∫d³xͺW(x)e⁽-i⬀k⦁⬀x⁾
  &= ⌠2π⧸V₍R₎⌡ ∫₍0₎⁽R⁾ dxͺ x⁽2⁾ ∫₍-1₎⁽1⁾ du e⁽ikxu⁾
```
and using ˝V₍R₎ = ÷13 4πR³˝,
```mm
  W(k)
  &= ⌠3⧸kR³⌡ ∫₍0₎⁽R⁾ dxͺxͺsin(kx)
  &= ⌠3⧸k³R³⌡ (sin(kR) - kRcos(kR))
```

This means
```
  σ(M)² = ∫₍0₎⁽∞⁾ dkͺ÷1k Δ² (⌠3⧸k³R³⌡ (sin(kR) - kRcos(kR)))²
```
and since ˝Δ² = k / k₍✶₎˝, 
```
  σ(M)² = ∫₍0₎⁽∞⁾ dkͺ k₍✶₎ (⌠3⧸k³R³⌡ (sin(kR) - kRcos(kR)))²
  𝓮𝓺{10}
```

Thus, to get the fraction of mass in the halos more massive than ˝M₍✶₎˝, 
substitute σ(M) into the probability equation
```
  f = ÷12 erfc(⌠δ₍c₎⧸√{2σ²(M)}⌡) = √{÷2π} νͺexp(-½ν²)
  𝓮𝓺{11}
```

_(ok, I didn't actually evaluate the integral for ˝σ²˝ to be able to get a 
numerical result for this. full disclosure: I took one look at that integral and 
thought, pffft. I leave it as an exercise for the reader, etc.)_

The bias is defined as
```
  b = -⌠1⧸σ⌡ ⌠d(ln(f(ν)))⧸dν⌡ \bigg|₍ν = δ₍c₎/σ₎
```
Plug in the mass fraction equation, \eqref{11}, you get
```
  b = ⌠ν²-1⧸δ₍c₎⌡ \bigg|₍ν=δ₍c₎/σ₎
```
which you can evaluate for ˝M = 5M₍✶₎˝.

_(ditto)_.
