---
time: 202303031646
title: "fate"
type: example
source: csmp21
draft: true

what: |
  Compute and plot the scale factor as a function of time for a universe with 
  ˝Ω₍m₎ = 0.25˝ and ˝Ω₍de₎ = 0.75˝ today (i.e. at a = 1) for three equation of 
  state parameters: ˝w = -1˝, ˝w = -0.8˝, ˝w = -1.2˝. Also plot the case of a 
  closed universe with ˝Ω₍m₎ = 1.25˝ and ˝Ω₍de₎ = 0˝. Plot both the past and 
  future expansion history. Use ˝H₍0₎ = 70⦃km/s/Mpc⦄˝. For each case, compute 
  the age of the universe today in years and describe the fate of the universe.
---

I'll start with a little background: in an isotropic, homogeneous universe, the 
_friedmann equation_ describes the evolution of the scale factor:
```
  H⁽2⁾ = (÷{˙a}a)⁽2⁾ = ÷13 8πGρ - ÷{k⁽2⁾ c⁽2⁾}{a⁽2⁾}
```
(I've absorbed the Λ term into ρ).

The density ˝ρ˝ in ˝(1)˝ is really a _sum_ of the densities of known 
constituents, multiplied by their (potentially power law) dependencies on scale 
factor ˝a˝. In the most general sense, you can express ˝ρ˝ as an expansion in 
inverse powers of ˝a˝:
```
  ρ = ∑{n=-∞…∞} ρ₍n₎ a⁽-n⁾
```
In practice, though, this sum only has a finite number of terms because the 
universe is pretty much always dominated by a few forms of energy.˝⁽[1]⁾˝ Matter 
density corresponds to the case ˝n=3˝, since ˝ρ₍m₎˝ scales as ˝a⁽-3⁾˝. Radiation 
density ˝ρ₍r₎˝ scales as ˝a⁽-4⁾˝.

You can derive a more specific relation for the dependency of ˝ρ˝ on ˝a˝ by 
using the fact that in a perfect fluid, the relation between ˝P˝ and ˝ρ˝ is 
given by
```
  w = ÷Pρ
```
where ˝w˝ is an equation of state parameter. [units where ˝c = 1˝. if not this 
equation should be ˝w = P / ρc⁽2⁾˝]. Now, if you take the fluid conservation law 
for an expanding universe
```
  𝝏₍t₎ρ + (˙a/a) (3ρ + 3P) = 0
```
and substitute ˝w = P/ρ˝, and then integrate the equation, you get the following 
relation for the scaling of ˝ρ₍i₎˝ with ˝a˝:
```
  ρ₍i₎(a) ∝ exp(-3 ∫₍a₎ da'ͺ÷1{a'} (1 + w₍i₎(a')))
```
If the e.o.s parameter ˝w₍i₎˝ is time-independent (curious -- when would it not 
be?), then this simplifies to 
```
  ρ₍i₎(a) ∝ a⁽-3(1+w₍i₎)⁾
```
For matter ˝w = 0˝ and for radiation ˝w = 1/3˝.

Anyway, the first thing you need to do for this problem is rewrite the friedmann 
equation in a dimensionless form, in terms of only the density parameters of the 
different constituents ˝Ω₍i₎˝ and their scaling relationships with ˝a˝. The 
density parameter ˝Ω₍i₎˝ is defined as the ratio of the observed density of that 
constituent, ˝ρ₍i₎˝, and the critical density, ˝ρ₍c₎ = 3H₍0₎⁽2⁾/8πG˝, so
```
  Ω₍i₎ = ÷{ρ₍i₎}{ρ₍c₎} = ÷{8πGρ₍i₎}{3H₍0₎⁽2⁾}
```
Next, divide the friedmann equation by ˝H₍0₎⁽2⁾˝
```
  ÷{H⁽2⁾}{H₍0₎⁽2⁾} = ÷{8πGρ}{3H₍0₎⁽2⁾} - ÷{kc⁽2⁾}{a⁽2⁾H₍0₎⁽2⁾}
```
and note that the first term on the RHS is
```
  Ω₍total₎ = ∑{i} Ω₍i₎ a⁽-3(1+w₍i₎)⁾
```
and that the second term can be written ˝(1-Ω₍total₎)a⁽-2⁾˝, or just 
˝Ω₍k₎a⁽-2⁾˝, where ˝Ω₍k₎˝ is a density parameter for curvature. When you put it 
all together,
```
  ÷{H⁽2⁾}{H₍0₎⁽2⁾} = ∑{i} Ω₍i₎ a⁽-3(1+w₍i₎)⁾ + Ω₍k₎a⁽-2⁾
```
or, using ˝H = ˙a/a˝,
```
  d₍t₎a = a H₍0₎ ( ∑{i} Ω₍i₎ a⁽-3(1+w₍i₎)⁾ + Ω₍k₎a⁽-2⁾ )⁽1/2⁾
```
This is a 1st order differential equation for ˝a(t)˝.

The problem first asks you to consider a universe with matter and dark energy, 
with different values of ˝w₍de₎ = [-1.2, -1, -0.8]˝. The relevant equation is
```
  d₍t₎a = a H₍0₎ ( Ω₍m₎a⁽-3⁾ + Ω₍de₎a⁽-3(1+w₍de₎)⁾ )⁽1/2⁾
```
with values ˝Ω₍m₎ = 0.25˝ and ˝Ω₍de₎ = 0.75˝. I wrote a quick ODE integrator for 
this problem using the RK2 algorithm with time step dt = 0.001 and a starting 
value of a = 0.01 (I learned the hard way that the starting value of ˝a˝ needs 
to be bigger than the time step -- if not the integrator will diverge pretty 
much immediately).

Here are the results:

{{< fig fate1.png 7 >}}
- FIGURE 211. plot of ˝a(t)˝ for universes with ˝Ω₍m₎ = 0.25˝, ˝Ω₍de₎ = 0.75˝, 
  and ˝w ∈ [-1.2, -1.0, -0.8]˝.
{.caption}

It's clear that all three cases, the universe is expanding˝^❄˝, ne'er to return, 
and also the expansion is accelerating.

Case ˝w = -1.2˝: the BIG RIP. In this (hypothetical) scenario, the expansion of 
the universe is accelerating, and the universe will become dominated by dark 
energy (specifically: phantom energy). The fact that ˝w < -1˝ will cause the 
dark energy density to _increase_, and continue increasing, as the universe 
expands. As time progresses, the distances of interactions between objects will 
tend to decrease, until eventually the horizon shrinks below the size of 
structures themselves, at which point they become "ripped apart" as they won't 
any longer be able to interact via fundamental forces. At the final singularity, 
all distances will be infinite.˝⁽[2]⁾˝

Case ˝-1 < w < 0˝: in this scenario the expansion of the universe is 
accelerating, but the dark energy density will _decrease_ with time, so there 
won't be a big rip. This is still a dark energy dominated universe though, since 
matter scales as ˝a⁽-3⁾˝ but dark energy scales as ˝a⁽-6⁾˝.

Case ˝w = -1˝: in this scenario the expansion of the universe is accelerating
with a cosmological constant as used in the einstein equations. Will become 
dark energy dominated.

Predicted age of the universe:
- for w = -1.2: ∼14.60 billion years
- for w = -1.0: ∼14.14 billion years
- for w = -0.8: ∼13.56 billion years

Here is a more zoomed in version of figure 211, focusing on the region up to ˝a = 
1˝:

{{< fig fate2.png 7 >}}
- FIGURE 212. zoomed in plot of ˝a(t)˝ for universes with ˝Ω₍m₎ = 0.25˝, ˝Ω₍de₎ 
  = 0.75˝, and ˝w ∈ [-1.2, -1.0, -0.8]˝.
{.caption}

Next: the closed universe scenario:

{{< fig fate3.png 8 >}}
- FIGURE 213. plot of ˝a(t)˝ for a closed universe with ˝Ω₍m₎ = 1.25˝, ˝Ω₍de₎ = 
  0˝, and ˝Ω₍k₎ = -0.25˝.
{.caption}

In this scenario the matter density is strong enough that gravity will 
ultimately overcome the force of expansion from the big bang. This will cause 
the expansion to eventually stop and reverse direction (i.e.  contract). The 
scale factor will start decreasing back to zero and universe will end up 
collapsing to a point with infinite temperature. The "big crunch", as they say.

(I couldn't get the integrator to demonstrate the collapse of ˝a(t)˝ back down 
to zero, but supposedly this is because of numerical divergences/infinities. The 
maximum I could get this program to run for before getting a div-by-zero error 
was ∼80b years, at which point the curve was still increasing (though at a 
decreasing rate). I'm sure that some rescaling of variables could fix this).

According to this model, the age of the universe is 8.87 billion years old.

REFERENCES:

(1) R. Nemiroff, B. Patla, Adventures in Friedmann cosmology: a detailed 
expansion of the cosmological Friedmann equations, Michigan Technological 
University, 2007.

(2) _Big Rip_, wikipedia [page](https://en.wikipedia.org/wiki/Big_Rip).

˝^*˝ although there is a strong rebuttal to this notion in one of the opening 
scenes of Annie Hall (1977): "Brooklyn is not expanding!"

Code: https://github.com/knbe/fate
