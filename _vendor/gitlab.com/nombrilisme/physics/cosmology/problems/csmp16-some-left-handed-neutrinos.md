---
time: 202302170713
title: "neutrino mass fraction today"
type: example
source: csmp16
draft: true

what: |
  The present temperature of the CMB is known to better than 1%. It's 2.73 K. If 
  there are three species of left handed neutrinos which froze out in 
  equilibrium at ∼1 MeV, what is Ω₍r₎ today?
---

During early epochs the universe was mostly made up of photons, electrons, 
positrons, and neutrinos, in thermal equilibrium. When ˝T ∼1ͺMeV˝ the three 
neutrino species (˝ν₍e₎, v₍μ₎, v₍τ₎˝) decouple from the plasma and start 
travelling freely through space. These neutrinos form the cosmic neutrino 
background, CνB, which is (currently) undetectable.

A short time after neutrino decoupling, when the temperature falls below the 
mass of the electron (when ˝T ∼ m₍e₎/3˝ according to KT) the electron/positron 
pairs become nonrelativistic and annihilate. Their entropy gets transferred to 
the photons but _not_ the neutrinos.

Thus, you can get an expression for the ratio of ˝T₍γ₎˝ to ˝T₍ν₎˝ by considering the 
conservation of entropy: ˝s₍e±₎ + s₍γ₎ = s₍γ,0₎˝.

_Before_ the ˝e⁽±⁾˝ annihilation (but after the neutrino decoupling), the 
particles in equilibrium are the photons (g = 2) and ˝e⁽±⁾˝ pairs (g = 2 each), 
and
```
  g₍❄₎ = ∑{i=bosons} g₍i₎ + ÷78 ∑{i=fermions} g₍i₎
  = 2 + (÷78) ⋄ (2 + 2) = ÷{11}2
```
_After_ the ˝e⁽±⁾˝ annihilation only the photons are in equilibrium, so ˝g₍❄₎ = 
2˝. But, the factor ˝g₍❄₎(aT)⁽3⁾˝ is constant for any particles in equilibrium 
with the photons.  This means that the value of ˝(aT₍γ₎)⁽3⁾˝ before the 
annihilation must be larger than that after the annihilation by a ratio of ˝g₍❄, 
before₎ / g₍❄, after₎ = 11/4˝. Thus, the ratio of ˝T₍γ₎˝ to ˝T₍ν₎˝ should be
```
  ÷{T₍ν₎}{T₍γ₎} = (÷4{11})⁽1/3⁾
```

Using the CMB temperature 2.73 K, you can work out that the CνB temperature is 
˝T₍ν₎ ≃ 1.95ͺK˝.

The number density of neutrinos ˝n₍ν₎ ∝ n₍b₎˝ satisfies the relation
```
  n₍ν₎ = ÷94 (÷{T₍ν₎}{T₍γ₎})⁽3⁾ n₍γ₎
```
Today the number density of photons is ˝n₍γ,0₎ = 4.1⋄10⁽8⁾ͺm⁽-3⁾˝ [TB p1380]. 

Using this value, and the relation above, the number density of neutrinos today 
is ˝n₍ν,0₎ ≃ 3.35⋄10⁽8⁾ͺm⁽-3⁾˝. And the energy density of neutrinos today is
```
  ρ₍ν,0₎ = 1.8⋄10⁽-12⁾ͺm₍ν₎ͺJͺm⁽-3⁾
```
where ˝m₍ν₎˝ is a factor representing the mass of the neutrinos which is nonzero 
(unlike for photons),
```
  m₍ν₎ = ÷{m₍ν₍e₎₎ + m₍ν₍μ₎₎ + m₍ν₍τ₎₎}{∼100ͺmeV}
```
The value in the denominator is approximate since we don't have any exact values 
for the masses of the neutrinos. We do have a lower and upper bound -- 60meV and 
230meV -- based on neutrino oscillation measurements (these values are from TB's 
textbook but apparently there have been more recent measurements of these bounds 
in 2016 [1]). 

So the neutrino energy density fraction today is ˝Ω₍ν,0₎ = ρ₍ν,0₎/ρ₍c₎˝
```
  Ω₍ν,0₎ = ÷{ρ₍ν,0₎}{ρ₍c₎} = ÷{1.8⋄10⁽-12⁾·m₍ν₎·Jm⁽-3⁾}{7.7⋄10⁽-10⁾ͺJm⁽-3⁾}
  = 0.0023m₍ν₎
```

The total energy density fraction due to radiation is
```
  Ω₍r,0₎ = Ω₍γ,0₎ + Ω₍ν,0₎
  = 5.4⋄10⁽-5⁾ + 0.0023m₍ν₎
```

which is very small indeed.

REFERENCES: 

[TB] Thorne, D. Blandford. Modern Classical Physics. Chapter 28.3, 
Cosmology: The Universe Today.


[1]: F. Capozzi et al, Neutrino masses and mixings: status of known and unknown 
3ν parameters, Nucl. Phys. B, 2016.

