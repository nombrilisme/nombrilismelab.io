---
time: 202302170702
title: "scaling of matter density with redshift"
type: example
source: csmp12
draft: true

what: |
  Consider a universe containing only matter and a cosmological constant (an 
  approximation to our universe for ˝10⁽3⁾ > z > 0˝). 
  - Show that the matter density ˝Ω₍m₎˝ scales with redshift ˝z˝ as
    ```
      Ω₍m₎(z) = ÷{Ω₍m,0₎}{Ω₍m,0₎ + Ω₍Λ,0₎·(1+z)⁽-3⁾}
    ```
    where ˝Ω₍m,0₎˝ and ˝Ω₍Λ,0₎˝ are the present day matter density and 
    cosmological constant in units of critical density. Hence show that ˝Ω₍m₎ ≈ 
    1˝ at early times.
  - At what redshift does ˝Ω₍m₎ = Ω₍Λ₎˝?
  - How does this redshift compare to when the universe goes from decelerated to 
    accelerated expansion?
---

At time ˝t˝, the matter density and cosmological constant (in units of 
critical density) are:
```
  Ω₍m,t₎ = (÷{a₍0₎}{a})⁽3⁾·(÷{H₍0₎}{H})⁽2⁾·Ω₍m,0₎
```
```
  Ω₍Λ,t₎ = (÷{H₍0₎}{H})⁽2⁾·Ω₍Λ,0₎
```

˝Ω₍m₎˝ should scale with ˝z˝ as a proportion of the total matter density,
```mm
  Ω₍m₎(z) &= ÷{Ω₍m,t₎}{Ω₍m,t₎ + Ω₍Λ,t₎}
          &= ÷{ (÷{a₍0₎}{a})⁽3⁾·(÷{H₍0₎}{H})⁽2⁾·Ω₍m,0₎ }{
            (÷{H₍0₎}{H})⁽2⁾·((÷{a₍0₎}{a})⁽3⁾ Ω₍m,0₎ + Ω₍Λ,0₎) }
```
Cancel the ˝(H₍0₎/H)˝ factors, and substitute ˝a₍0₎/a = 1/(1+z)˝,
```mm
  Ω₍m₎(z) &= ÷{ (1+z)⁽3⁾ Ω₍m,0₎ }{(1+z)⁽3⁾·Ω₍m,0₎ + Ω₍Λ,0₎}
          &= ÷{Ω₍m,0₎}{Ω₍m,0₎ + Ω₍Λ,0₎·(1+z)⁽-3⁾}
```

(qed). For early times, you can substitute ˝z ≈ 10⁽3⁾˝,
```
  Ω₍m₎(z = 10⁽3⁾) 
    = ÷{Ω₍m,0₎}{Ω₍m,0₎ + Ω₍Λ,0₎⋄10⁽-9⁾}
    ≈  ÷{Ω₍m,0₎}{Ω₍m,0₎} 
    = 1
```

(b) Set the equations for ˝Ω₍m₎˝ and ˝Ω₍Λ₎˝ equal to each other:
```
  Ω₍m₎ = Ω₍Λ₎
```
```
  \Longrightarrow (÷{a₍0₎}{a})⁽3⁾·(÷{H₍0₎}{H})⁽2⁾·Ω₍m,0₎ = 
  (÷{H₍0₎}{H})⁽2⁾·Ω₍Λ,0₎
```
Cancel the ˝H˝ factors and set ˝a₍0₎ = 1˝, you get
```
  (1+z)⁽3⁾·Ω₍m,0₎ = Ω₍Λ,0₎
```
rearranging for ˝z˝,
```
  z = (÷{Ω₍Λ,0₎}{Ω₍m,0₎})⁽1/3⁾ - 1
```

(c) Accelerated expansion means ˝\ddot a > 0˝. This corresponds to increasing 
˝˙a˝ or decreasing ˝˙a⁽-1⁾ = (aH)⁽-1⁾˝.

The hubble parameter for this universe is:
```
  H⁽2⁾ = H₍0₎⁽2⁾(Ω₍m₎ a⁽-3⁾ + Ω₍Λ₎)
```

Since ˝H(t) = ÷{˙a}{a}˝, then
```
  ˙a = aH = aH₍0₎ (Ω₍m₎a⁽-3⁾ + Ω₍Λ₎)⁽1/2⁾ = H₍0₎(Ω₍m₎a⁽-1⁾ + Ω₍Λ₎a⁽2⁾)⁽1/2⁾
```

If ˝˙a˝ is increasing, then ˝d˙a/dt > 0˝, so 
```
  ÷{d˙a}{dt} = ÷12·H₍0₎·(Ω₍m₎a⁽-1⁾ + Ω₍Λ₎a⁽2⁾)⁽-1/2⁾·(-Ω₍m₎a⁽-2⁾ + 2Ω₍Λ₎a) > 0
```
∴
```
  2Ω₍Λ₎·a > Ω₍m₎a⁽-2⁾
```
and
```
  2Ω₍Λ₎·a⁽3⁾ > Ω₍m₎
```
```
  a > (÷{Ω₍m₎}{2Ω₍Λ₎})⁽1/3⁾
```
```
  ÷1{1+z} > (÷{Ω₍m₎}{2Ω₍Λ₎})⁽1/3⁾
```
```
  z < (÷{2Ω₍Λ₎}{Ω₍m₎})⁽1/3⁾ - 1
```
