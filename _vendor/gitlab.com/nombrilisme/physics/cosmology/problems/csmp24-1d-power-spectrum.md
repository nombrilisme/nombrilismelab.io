---
time: 202303031656
title: "power spectrum of a cube"
type: example
source: csmp24
draft: true

what: |
  Plot the 1D (spherically averaged) power spectrum and the 1D dimensionless 
  power spectrum of the provided cosmological "data cube". The cube is 400 Mpc 
  on a side. The units are fractional overdensity.
---

Some theory: the _fractional overdensity_ ˝δ(⬀x)˝ describes the deviation of the 
density field, ˝ρ(⬀x)˝, from the average density, ˝⟨ρ⟩˝
```
  δ(⬀x) = ÷{ ρ(⬀x) - ⟨ρ⟩ }{ ⟨ρ⟩ }
```
where ˝ρ(⬀x)˝ is the density at point ˝⬀x˝ and ˝⟨ρ⟩˝ is the average density over 
(all) space. So ˝δ(⬀x)˝ is a dimensionless scalar field.

The data cube we're given in this problem contains values of ˝δ(⬀x)˝ at each 
point (or should I say voxel?) on a 200x200x200 3D grid (8e6 voxels in total).

By definition, the average value of ˝δ˝ across all space should be zero. From 
the data I calculated avg(δ) = -2.69e-9 ≃ 0.

The _two-point correlation function_ ˝ξ˝ can be defined in terms of the 
fractional overdensity as
```
  ξ(⬀x₍1₎, ⬀x₍2₎) = ⟨δ(⬀x₍1₎)δ(⬀x₍2₎)⟩ = ÷1V ∫ d⁽3⁾⬀xͺδ(⬀x₍1₎) δ(⬀x₍1₎-⬀x₍2₎)
```

And the _power spectrum_ ˝P(k)˝ can be defined in terms of ˝ξ˝ via the relation 
```
  ξ(⬀x₍1₎,⬀x₍2₎) = ∫ d⁽3⁾kͺ÷1{(2π)⁽3⁾} P(k) e⁽i⬀k⦁(⬀x₍1₎-⬀x₍2₎)⁾
```

So the power spectrum is basically the fourier transform of the correlation 
function. If you let ˝δ(⬀k)˝ be the fourier transform of the fractional 
overdensity ˝δ(⬀x)˝, then
```
  ⟨δ(⬀k₍1₎)δ⁽❄⁾(⬀k₍2₎)⟩ = (2π)⁽3⁾ͺδ⁽3⁾(⬀k₍1₎-⬀k₍2₎) P(k)
```

˝P(k)˝ has dimensions of length˝⁽3⁾˝, so you can also define the _dimensionless 
power spectrum,_
```
  Δ⁽2⁾(k) = ÷{k⁽3⁾}{2π⁽2⁾} P(k)
```

And that's it, really. To compute ˝P(k)˝ I took a fourier transform of the 
overdensity field (the _discrete fourier transform_ using the [AbstractFFTWs 
library][fftw]), then computed the correlation function and normalized. Here are 
my results:

[fftw]: https://juliamath.github.io/AbstractFFTs.jl/stable/

{{< fig spherical.png 7 >}}
{{< fig dimless.png 7 >}}

And here, also, is a quick little visual depiction of this so-called 
"cosmological cube":

{{< fig cube.png 12 >}}
- FIGURE 242. "the cube". (rather eerie, don't you think?)
{.caption}

The contrast in the image shows (somewhat) the local variations in the 
(over)density field. The power spectrum is essentially a measure of the _density 
contrast_ in space.

Code: https://github.com/knbe/cube
