---
time: 202304021828
title: "boltzmann code"
type: problem
source: csmp34
draft: true
what: |
  Use a boltzmann code to:

  (a) Calculate the cold dark matter transfer function and plot the 
  dimensionless power spectrum ˝Δ₍δ₎⁽2⁾ = k⁽3⁾ P(k) / (2π⁽2⁾)˝ for a fiducial 
  model with 
  - ˝Ω₍m₎ = 0.25˝
  - ˝Ω₍Λ₎ = 0.75˝
  - ˝Ω₍b₎ = 0.04˝
  - ˝H₍0₎ = 70ͺkm/s/Mpc˝
  - ˝n₍s₎ = 1˝
  Pay attention to the units and normalization. You should find ˝Δ₍δ₎⁽2⁾ ∼ 1˝ at 
  ˝k ∼ 0.1ͺMpc⁽-1⁾˝.

  (b) Vary each of the parameters ˝Ω₍m₎, H₍0₎, Ω₍b₎, n₍s₎˝ by 10% and in each 
  case compute the ratio of the power spectrum relative to the fiducial model.
  Qualitatively explain the change you see. Do the same for the sum of the 
  neutrino masses at ˝0.1ͺeV˝, ˝0.3ͺeV˝, and ˝1ͺeV˝ relative to the zero 
  neutrino mass.

---

For this problem I used [CAMB][1]. 

[1]: https://lambda.gsfc.nasa.gov/toolbox/camb_online.html

Relevant equations: 

- transfer function:
  ```
    T(k) = ÷{Φ(k,a₍l8₎)}{Φ₍ls₎(k,a₍l8₎)}
  ```
- for late time (˝a₍l8₎ = a₍late₎˝) evolution (when all modes are inside the 
  horizon and evolution no longer depends on scale) the evolution is 
  characterised by the growth function ˝D₍1₎(a)˝
  ```
    ÷{Φ(a)}{Φ(a₍l8₎)} = ÷{D₍1₎(a)}{a}
  ```
- potential:
  ```
    Φ(⬀k,a) = ÷9{10} Φ₍p₎(⬀k) T(k) ÷{D₍1₎(a)}{a}
  ```
- primordial power spectrum:
  ```
    P₍Φ₎(k) = ÷{50π⁽2⁾}{9k⁽3⁾} (÷{k}{H₍0₎})⁽n₍s₎-1⁾ δ₍H₎⁽2⁾ 
    (÷{Ω₍m₎}{D₍1₎(a=1)})⁽2⁾
  ```
- linear matter power spectrum:
  ```
    P(k,a) = 2π⁽2⁾ δ₍H₎⁽2⁾ ÷{k⁽2⁾}{H₍0₎⁽n+3⁾} T⁽2⁾(k) (÷{D₍1₎(a)}{D₍1₎(a=1)})⁽2⁾
  ```
- dimensionless power spectrum:
  ```
    Δ₍δ₎⁽2⁾ = ÷{k⁽3⁾}{2π⁽2⁾} P(k)
  ```

Below are graphs of the CDM transfer function and dimensionless power spectrum 
for a fiducial model with with ˝Ω₍m₎ = 0.25˝, ˝Ω₍Λ₎ = 0.75˝, ˝Ω₍b₎ = 0.04˝, 
˝H₍0₎ = 70ͺkm/s/Mpc˝, ˝n₍s₎ = 1˝. I'm using the units specified in the [CAMB 
notes][2]. 

[2]: https://cosmologist.info/notes/CAMB.pdf


{{< fig d1-ps.png 6 >}}
- __figure 341.__ CDM power spectrum for fiducial model. ˝Δ₍δ₎⁽2⁾ ∼ 1˝ at ˝k ∼ 
  0.1ͺMpc⁽-1⁾˝.
{.caption}

{{< fig d1-tf.png 6 >}}
- __figure 342.__ CDM transfer function for fiducial model. 
{.caption}



Varying ˝Ω₍m₎˝ by ∼10%:

{{< fig d2-ps.png 12 >}}

- __figure 343.__ Left: power spectrum of models with ˝Ω₍m₎ = 0.28˝ (teal) and 
  ˝Ω₍m₎ = 0.22˝ (orange). [Note: in varying ˝Ω₍m₎˝ I'm keeping ˝Ω₍b₎˝ the same 
  at 0.04 but adjusting ˝Ω₍Λ₎˝ to maintain a euclidean cosmology with ˝Ω₍m₎ + 
  Ω₍Λ₎ = 1˝.] The fiducial model with ˝Ω₍m₎ = 0.25˝ is indicated in grey. Right: 
  ratio of the power spectrum relative to the fiducial model.
{.caption}

Figure 343 shows that changing ˝Ω₍m₎˝ changes the epoch of matter-radiation 
equality. Increasing ˝Ω₍m₎˝ (teal) decreases mode suppression before the m-r 
equality, so ˝k₍eq₎˝ shifts to a higher value (and thus the m-r equality shifts 
to an earlier time). And vice versa: decreasing ˝Ω₍m₎˝ (orange) increases mode 
suppression before the m-r equality, so ˝k₍eq₎˝ shifts to a smaller value, and 
the m-r equality to shifts to a later time.

The shape of the matter power spectrum is given by
```
  P(k,a) = ÷{8π⁽2⁾}{25} ÷{A₍s₎}{Ω₍m₎⁽2⁾} D₍+₎⁽2⁾(a) T⁽2⁾(k) 
  ÷{k⁽n₍s₎⁾}{H₍0₎⁽4⁾k₍p₎⁽n₍s₎-1⁾}
```
[DS eq. 8.8]. 


{{< ppbreak >}}


Varying ˝Ω₍b₎˝ by ∼10%:

{{< fig d3a-ps.png 11 >}}
- __figure 344.__ Power spectrum and ratio with fiducial for models with (1) 
  ˝Ω₍b₎ = 0.06˝, ˝Ω₍cdm₎ = 0.19˝, ˝Ω₍m₎ = 0.25˝, ˝Ω₍Λ₎ = 0.75˝, and (2) ˝Ω₍b₎ = 
  0.02˝, ˝Ω₍cdm₎ = 0.23˝, ˝Ω₍m₎ = 0.25˝, ˝Ω₍Λ₎ = 0.75˝.
{.caption}

At early times, before matter-radiation equality, the baryons are tightly 
coupled to the photons, and their contribution to the gravitational potential is 
negligible. Radiation perturbations don't grow inside the horizon, so the baryon 
overdensities are suppressed compared to dark matter. However, after m-r 
equality, the baryons' contribution to the potential becomes significant.
Baryons lead to oscillations in the transfer function - "baryon acoustic 
oscillations".  These oscillations tend to have a small amplitude due to the 
tiny fraction of the total matter accounted for by baryons. Decreasing ˝Ω₍b₎˝ 
very slightly decreases mode suppression (orange), and v.v (teal).


{{< ppbreak >}}

Varying ˝H₍0₎˝:

{{< fig d4-ps.png 11 >}}
- __figure 345.__
{.caption}

Generally, the effects are comparable to those for ˝Ω₍m₎˝. Increasing ˝H₍0₎˝ 
reduces mode suppression before the m-r equality, so ˝k₍eq₎˝ shifts to a later 
time and m-r equality shifts to an earlier time. (and v.v.)

Varying ˝n₍s₎˝:

{{< fig d5-ps.png 11 >}}
- __figure 346.__
{.caption}

The effect of varying ˝n₍s₎˝ is evident from equation ˝(4)˝. Increasing ˝n₍s₎˝ 
effectively "scales up" power spectrum, and v.v. But the general form/shape of 
the spectrum is preserved.

Varying the neutrino masses:

{{< fig d6.png 6 >}}
- __figure 347.__
{.caption}

Varying the neutrino masses means the transfer function is now dependent on 
neutrino mass. Increasing neutrino masses suppresses small scale modes because 
they can freestream out of regions with high matter density. The larger the 
mass, the stronger the effect. The transfer function for ˝m₍ν₎ = 0.1eV˝ is 
larger than that for ˝m₍ν₎ = 0.03eV˝, and likewise for ˝0.01eV˝.
