---
time: 202303031656
title: "le super-wimp"
type: problem
source: csmp23
draft: true

what: |
  Consider the super-WIMP scenario, where a heavy particle, mass ˝m₍H₎˝, is 
  produced thermally by dropping out of equilibrium. This particle then decays 
  with τ ≈ 3 months into a lighter (stable) particle of mass ˝m₍L₎˝ and an 
  effectively massless particle (which you can forget about). If ˝⟨σv⟩ = 
  2⋄10⁽-40⁾⦃cm⁽2⁾⦄˝ and ˝m₍H₎/m₍L₎ = 10˝, what is ˝Ω₍L₎˝? At what velocity are 
  light particles produced immediately after decay? Assume that ˝x₍f₎ = 
  m₍H₎/T₍f₎ = 10˝ (where ˝T₍f₎˝ is the temperature when the heavy particle 
  freezes out) and ˝g₍*₎(T₍f₎) = 100˝.
---

The relic density for the heavy particle at 100 GeV is given by
```
  Ω₍X₎h⁽2⁾ = 0.3 ⋄ (÷{x₍f₎}{10})(÷{g₍❄₎(m)}{100})⁽1/2⁾
              ( ÷{10^{-39}ͺcm⁽2⁾}{⟨σv⟩} )
```
[your lecture notes, L09 p4]. (some of my working for this is in Appendix 23).

To calculate the density of the light particle, which is 10x lighter than the 
heavy particle and has cross section ˝⟨σv⟩ = 2⋄10⁽-40⁾ͺcm⁽2⁾˝, you can 
substitute the values: 
- ˝x₍f₎ = 1˝ (since the light particle is a factor of 10 lighter)
- ˝g₍❄₎ = 100˝
- ˝⟨σv⟩ = 2⋄10⁽-40⁾˝
- ˝h = 0.6770˝

and you get ˝Ω₍L₎ ≈ 0.327˝.

To calculate the velocity of the light particle, you can use the energy-momentum 
relation. Let ˝E₍L₎˝ be the energy of the light particle, and ˝m₍L₎˝ be the rest 
mass of the light particle, then ˝E₍L₎⁽2⁾ = p⁽2⁾ + m₍L₎⁽2⁾˝.  (I'll be honest -- 
I spent hours scouring the pages of DS and KT for how to do this, to little 
avail. I suspect there's some relation from basic GR that I'm missing. My 
"intuitive" guess is that the velocity of the light particles should be some 
(quite large, since 10x is not a huge difference) fraction of the speed of 
light. Idk).

---

APPENDIX 23

[blackboard]

{{< fig a23.jpg 12 >}}
