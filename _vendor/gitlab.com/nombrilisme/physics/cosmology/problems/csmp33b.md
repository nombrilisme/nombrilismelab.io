---
time: 202304151539
title: "3-3b"
type: example
source: csmp33b
draft: true
what: |
  Prove
  ```
    P₍Φ₎(k) = ÷49 P₍ζ₎(k)
  ```
---

Define

```
  ζ = ÷{ ik⟦i⟧}{k⁽2⁾} ÷{δT⟦0'i⟧H}{k⁽2⁾(ρ+p)} - Ψ
```

Insert the definitions of ˝ρ˝ and ˝p˝ from before, you get
```
  ρ + p = (÷{φ'₍0₎}a)⁽2⁾
```

At the time of the horizon crossing, ˝ζ = -aH δφ / φ'₍0₎˝. _After_ inflation, ˝ζ 
= ik₍i₎ δT⟦i'0⟧ = -4kρ₍r₎Θ₍1₎/a˝ (DS 7.58), and the pressure of radiation is a 
third of the density, so
```
  ζ = -÷{3aH}{k}Θ₍1₎ - Ψ
```
```
  = -÷3k aH (-÷{k}{6aH} Φ) - Ψ
  = ÷12Φ - Ψ
```
(DS eq 7.59).

You can assume the anisotropic stresses are small, so the approximation ˝Ψ ≈ -Φ˝ 
holds, so ˝ζ ≈ -÷32 Φ˝. Rearranging, you get ˝Φ = -÷23ζ˝.

Thus, in terms of the power spectrum of ζ, 
```
  P₍Φ₎ = ÷49 P₍ζ₎ = ÷89 ÷{πGH⁽2⁾}{k⁽3⁾t}
```

{{< fig 33-4.JPG 10 >}}
