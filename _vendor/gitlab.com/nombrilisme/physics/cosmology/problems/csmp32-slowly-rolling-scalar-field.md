---
time: 202304021828
title: "slowly rolling scalar field"
type: example2
source: csmp32
draft: true
what: |
  Show that for a universe dominated by a slowly rolling scalar field with an 
  exponential potential
  ```
    V(φ) ∝ exp(-√{÷2p} √{8πG} φ)
  ```
  the scale factor grows like ˝a(t) ∝ t⁽p⁾˝ and ˝1 - n₍s₎ = 2/p˝. Current limits 
  on ˝n₍s₎˝ rule out ˝p ≤ 60˝.
---

Start with some background (from DS p165): 

The lagrangian density for a minimally coupled scalar field:
```
  ℒ = ÷12 𝝏₍x⟦i⟧₎(φ)ͺ𝝏₍x⟦i'⟧₎(φ)ͺ - V(φ)
```
The first term is the kinetic energy of the field and the second term is the 
potential. The energy-momentum tensor for ˝φ˝ is
```
  T⟦i'j⟧ = g⟦i'k'⟧ ͺ 𝝏₍x⟦k'⟧₎(φ)ͺ𝝏₍x⟦l'⟧₎(φ) -
            δ⟦i'l⟧(÷12 g⟦m'k'⟧ͺ 𝝏₍x⟦m'⟧₎(φ)ͺ𝝏₍x⟦k'⟧₎(φ) + V(φ))
```
This is a general expression. If you assume a _homogeneous_ scalar field, only 
the time derivatives of ˝φ˝ matter. You can eliminate the spatial derivatives 
from these equations, which gives you
```
  ℒ = ÷12˙φ⁽2⁾ - V(φ)
```
and
```
  T⟦i'j⟧ = -δ⟦i'0⟧ δ⟦0'j⟧ ˙φ⁽2⁾ + δ⟦i'j⟧ (÷12 ˙φ⁽2⁾ - V(φ))
```
The time component of ˝T˝ gives the energy density:
```
  -T⟦0'0⟧ = ρ = ÷12 ˙φ⁽2⁾ + V(φ)
```
and the spatial components give the pressure:
```
  T⟦i'i⟧ = p = ÷12 ˙φ⁽2⁾ - V(φ)
```

Substitute these equations for ˝ρ˝ and ˝p˝ into the friedmann equation, you get
```
  H⁽2⁾ = (÷{˙a}a)⁽2⁾ = ÷13 8πG (÷12 ˙φ⁽2⁾ + V(φ))
```
(assume a flat universe so ˝k = 0˝).

To determine the time evolution of ˝φ˝, use the energy-momentum fluid 
conservation law: 
```
  𝝏₍t₎ρ + 3H(ρ+p) = 0
```
and substitute in the expressions for ˝ρ˝ and ˝p˝, you get
```
  𝝏₍t₎(÷12˙φ⁽2⁾ + V(φ)) + 3H((÷12˙φ⁽2⁾ + V(φ)) + (÷12˙φ⁽2⁾ - V(φ))) = 0
```
which reduces to
```
  ˙φ¨φ + 𝝏₍φ₎V(φ)˙φ + 3H˙φ⁽2⁾ = 0
```
and, cancelling the factor of ˝˙φ˝,
```
  ¨φ + 𝝏₍φ₎V(φ) + 3H˙φ = 0
```

If you slightly rearrange this evolution equation for ˝φ˝, you can see that it 
has the form of a damped harmonic oscillator:
```
  ¨φ + 3H˙φ = -𝝏₍φ₎V(φ)
```
The ˝˙φ˝ term on the LHS is like a "classical friction" term, where ˝H˝ 
represents the damping coefficient. On the RHS,˝-𝝏₍φ₎V(φ)˝ represents the force.

For a slowly rolling field, you can make two important approximations: first, 
the energy density is dominated by the potential term, i.e. potential energy ˝>˝ 
kinetic energy, ˝V » ˙φ⁽2⁾˝. This means you can neglect the kinetic ˝˙φ˝ term in 
the friedmann equation. Second, if ˝V » ˙φ⁽2⁾˝ then it follows that ˝𝝏₍φ₎V(φ) » 
¨φ˝, so you can assume ˝¨φ ≈ 0˝ and neglect the ˝¨φ˝ term in the evolution 
equation. 

_Anyway:_ what this problem asks you to do is prove that ˝a ∝ t⁽p⁾˝ for a slowly 
rolling scalar field model with a potential
```
  V(φ) ∝ exp(-√{÷2p}√{8πG}φ)
```

I'm going to prove this "backwards", i.e. show that for a slowly rolling field 
model where you know that ˝a ∝ t⁽p⁾˝, the potential must be of form ˝(14)˝. 

First, note that if ˝a ∝ t⁽p⁾˝, then
```
  H = ÷{˙a}{a} ∝ ÷{÷d{dt} t⁽p⁾}{t⁽p⁾} = ÷{pt⁽p-1⁾}{t⁽p⁾} = ÷pt
```

So ˝H ∝ ÷pt˝. 

Also, 
```m
  ˙H = ÷d{dt}H 
  = ÷d{dt}(˙a ÷1a) 
  = ÷d{dt}(˙a) ÷1a + ˙a ÷d{dt} a⁽-1⁾
  = ÷{¨a}{a} - ÷{˙a⁽2⁾}{a⁽2⁾}
```
```
  = ÷{¨a}{a} - H⁽2⁾
```
The first term in ˝(16)˝, ˝÷{¨a}a˝, is the usual acceleration equation, equal to
```
  ÷{¨a}a = -÷13 4πG(ρ + 3p)
```
Substitute the expressions for ˝ρ˝ and ˝p˝, you get
```
  ÷{¨a}a = -÷13 4πG(÷12 ˙φ⁽2⁾ + V + ÷32 ˙φ⁽2⁾ - 3V) = -÷13 8πG(˙φ⁽2⁾ - V)
```
The second term in ˝(16)˝ is
```
  H⁽2⁾ = ÷13 8πGρ = ÷13 8πG(÷12 ˙φ⁽2⁾ + V)
```
Putting ˝(17)˝ and ˝(19)˝ together,
```
  ˙H = -÷13 8πG(˙φ⁽2⁾ - V) - ÷13 8πG(÷12 ˙φ⁽2⁾ + V) = -4πG ˙φ⁽2⁾
```

Also, since ˝H ∝ ÷pt˝, then ˝˙H ∝ -÷{p}{t⁽2⁾}˝.  Equate this with ˝(20)˝, 
you get
```
  -÷p{t⁽2⁾} ∝ -4πG ˙φ⁽2⁾
```
∴
```
  ˙φ ∝ ÷1t √{÷p{4πG}}
```
where ˝C˝ is some constant. Integrate this, you get
```
  φ ∝ √{÷p{4πG}} ln(÷t{t₍0₎})
```
and rearrange for ˝t˝,
```
  t ∝ exp(√{÷1{p}}√{4πG} φ)
```

Now, go back to the friedmann equation ˝(8)˝, and rearrange it for ˝V˝, gives
```
  V = ÷3{8πG} H⁽2⁾ - ÷12 ˙φ⁽2⁾
```
and substitute in our earlier expressions for ˝H⁽2⁾˝ and ˝˙φ˝ (from ˝(15)˝ and 
˝(22)˝), both of which are ˝∝ t⁽-2⁾˝, you get the proportionality relation
```
  V ∝ t⁽-2⁾
```
Substitute the expression for ˝t˝ from ˝(24)˝, you get
```
  V ∝ exp(-√{÷2p} √{8πG} φ)
```
This is the form of the potential given in the problem. So you can conclude that 
for a slowly rolling scalar field with potential of form ˝(27)˝, the solution 
for scale factor evolves as ˝a ∝ t⁽p⁾˝.

Next: the scalar spectral index ˝n₍s₎˝ is defined
```m
  n₍s₎ - 1 = ÷{dͺln(P₍Φ₎)}{dͺln(k)} 
  = ÷{d}{dͺln(k)} ÷{8πG}{9k⁽3⁾} ÷{H⁽2⁾}{ε} \bigg|₍aH=k₎
  = ÷{d}{dͺln(k)} (ln(H⁽2⁾) - ln(ε))
```
```m
  = ÷{dͺln(H⁽2⁾)}{dͺln(k)} -  ÷{dͺln(ε)}{dͺln(k)}
  = -2ε - 2(ε + δ)
  = -4ε - 2δ
```
where 
```
  ε = ÷d{dt} ÷1H = ÷1p
```
and
```
  δ = ÷1H ÷{¨φ}{˙φ}
```
```m
  = -÷1{8πG} ÷1V 𝝏₍φ₎⁽2⁾V + ε
  = -(÷1{8πG} ͺCͺexp(√{2/p}√{8πG}φ) C⁽-1⁾ ÷2pͺ8πGͺexp(√{2/p}√{8πG}φ)) + ÷1p
  = -÷2p + ÷1p
  = -÷1p
```
Substitute the expressions from earlier, you get
```
  n₍s₎ - 1 = -÷4p + ÷2p = -÷2p
```
∴ conclude that ˝1 - n₍s₎ = 2/p˝.
