---
time: 202302170703
title: "shape of the earth"
type: example
source: csmp14
draft: true
what: |
  You are a 2D being living on the surface of a sphere of radius R. Show that if 
  you draw a circle of radius r, the circumference will be C = 2πR·sin(r/R). 
  Idealize the earth as a perfect sphere of radius R = 6731 km. If you could 
  measure distances with an error of ±1 m, how large a circle would you have to 
  draw on the earth's surface to convince yourself the earth was not flat?
---

First, define what it means to _draw a circle of radius r:_ it's the circle you 
end up with when you start at some origin point (drive a stake into the ground, 
or anchor a boat in the sea, or whatever), tether yourself to that point with an 
(ideally) unstretchable string whose length is r, and then trace out the (only) 
path that's equidistant to the origin point (while keeping the string taut) until 
you arrive where you started. This keeps you firmly planted on the manifold 
(i.e. surface) that you're _actually_ on. 

It's obvious that if you're on a sphere, the circumference you draw will be 
smaller than what you'd get on a flat surface:

{{< fig csmp14f1.png 6 >}}

On a sphere, the "radius" r is not the radius of the circle you drew. It's an 
arclength, r = Rθ. The circle's path traces out points of constant colatitude θ 
on the sphere. Let 'a' be the real radius of the circle (indicated in the 
figure). Using the right triangle, a = R·sin(θ), and since θ = r/R, then a = 
R·sin(r/R). So the circumference of the circle is C = 2πR·sin(r/R). Done.

But that was fairly banal. A much less banal way to reach the same conclusion: 
define the metric for a sphere. In fact, here it is (assuming coordinates 
r,θ,φ):
```
  dl⁽2⁾ = dr⁽2⁾ + r⁽2⁾·dθ⁽2⁾ + r⁽2⁾·sin⁽2⁾θ·dφ⁽2⁾
```
where dl is the line element that connects two points. Since we're only allowed 
on the surface of the sphere, r = R = constant, and dr = 0, so the metric for 
the surface of a sphere reduces to
```
  dl⁽2⁾ = R⁽2⁾·dθ⁽2⁾ + R⁽2⁾·sin⁽2⁾θ·dφ⁽2⁾
```
and the components of the metric tensor (in coordinates θ,φ) are
```
  ⬈g⟦ij⟧ → %%<
    R⁽2⁾ & 0 ,,
    0 & R⁽2⁾·sin⁽2⁾θ
  >%%
```
Belle. Now, on a sphere, drawing a circle means tracing a path of constant 
colatitude (wrt to a perpendicular axis that runs thru the centre of the 
sphere). So dθ = 0 and φ sweeps the values 0:2π. So
```
  C = ∫{0}{C} dl = ∫{0}{2π} R·sinθ·dφ = [ Rͺsinθͺφ ]₍0₎⁽2π⁾ = 2πR·sinθ
```
with θ = r/R. This is the same result. 

To convince yourself that the earth is not flat, first you would need to believe 
that it _is actually possible_ for the earth to not be flat.

Next: the problem says you can measure distances with an accuracy of ±1 metres.
But, I wonder, which distances exactly? All of them? The apparent radius r, the 
observed circumference you trace out? Both? What about the earth's radius R?

I'm going to assume that what this question is really asking is: at what point 
does the predicted circumference when using C = 2πr become sufficiently 
different from the "correct" formula, C = 2πR.sin(r/R)? Or, at what radius does 
the small angle approximation become invalid? 

Here's a plot of ˝C₍diff₎˝ vs radius r, where ˝C₍diff₎ = 2π·r - 2π·R·sin(r/R)˝:

{{< fig cdiff.png 6 >}}

You can see that the difference between the two formulas becomes >1 m around 
when r ≈ 35 km. [If your value for radius r also has an error margin of ±1 m, 
then you can compute an upper and lower bound using the radii (r+1) and (r-1).
Having said that, this is fairly unnecessary, because you'd still be using the 
same radius in both formulas, and at this scale it makes a negligible 
difference]. So, for all intents and purposes (in this problem), you should make 
your circle bigger than ∼35 km. In reality, though, it won't be quite so simple 
(i.e. if you actually set out to draw a massive circle on earth with the 
_specific_ goal of determining that it's not flat. There's more to consider).
Also, this analysis only compares the two alternatives where the earth is EITHER 
flat OR a sphere. If you were open to the possibility that the earth might be a 
cylinder, or some kind of oval 3d ellipse, you'd need a stronger condition.

# APPENDIX 14

[the blackboard. feat. some unfinished musings on the metric for a potentially 
"cylindrical earth"]
{{< fig L-shapeofearth.jpg 10 >}}
