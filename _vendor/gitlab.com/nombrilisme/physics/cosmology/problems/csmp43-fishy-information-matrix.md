---
time: 202304021829
title: "fishy information matrix"
title: "43"
type: example2
source: csmp43
draft: true
what: |
  Using the following data,
  - `fiducial.dat`: matter power spectrum from CAMB with fiducial parameters 
    ˝Ω₍m₎ = 0.3˝ and ˝Ω₍Λ₎ = 0.7˝. The other parameters aren't important for 
    this problem.
  - `omega_m_plus_epsilon.dat`: matter power spectrum from CAMB where ˝Ω₍m₎˝ has 
    been increased by ˝ε = 0.0001˝, i.e. ˝Ω₍m₎ = 0.3001˝. ˝Ω₍Λ₎˝ is still equal 
    to the fiducial value of ˝0.7˝.
  - `omega_lambda_plus_epsilon.dat`: matter power spectrum from CAMB where 
    ˝Ω₍Λ₎˝ has been increased by ˝ε = 0.0001˝, i.e. ˝Ω₍Λ₎ = 0.7001˝. ˝Ω₍m₎˝ is 
    still equal to the fiducial value of ˝0.3˝.

  calculate the fishy information matrix for ˝Ω₍m₎˝ and ˝Ω₍Λ₎˝. Assume each 
  ˝k˝ mode of the power spectrum can be measured with a ˝1σ˝ uncertainty of 
  ˝5⋄10⁽4⁾ͺ(h⁽-1⁾Mpc)⁽3⁾˝. Plot ˝1σ˝ and ˝2σ˝ error contours around the 
  fiducial values for ˝Ω₍m₎˝ and ˝Ω₍Λ₎˝. This last bit is tricky. You'll need to 
  consult a ˝χ⁽2⁾˝ distribution table to determine the right spacing of the 
  contours in 2D.

  Note also that this problem is unrealistic for several reasons. No experiment 
  can measure with the ˝k˝-space resolution of the CAMB outputs (especially on 
  the largest scales. We're ignoring this fact for convenience. We're also 
  assuming that all other cosmological parameters are known perfectly. To 
  properly calculate the fisher matrix, you'd need to include all the parameters 
  and marginalize over all but the ones of interest. This can degrade the 
  predicted constraints considerably.
---

The _fisher information:_ describes the extent to which a set of experimental 
_given_ an underlying theory that gives (or asserts) the values of the 
parameters.

The larger the fisher information for a certain parameter, the more sharply 
peaked ("curved") the likelihood function is, and the more the observed data 
"constrains" the true value of that parameter.

In this example, the "given" underlying theory is the ΛCDM model. The observable 
of this theory is the matter power spectrum ˝P(k)˝. The parameters we're 
considering are ˝Ω₍m₎˝ and ˝Ω₍Λ₎˝. Their fiducial values are ˝0.3˝ and ˝0.7˝. 
Let ˝[Ω₍fu₎]˝ be the array of fiducial parameters, and ˝[Ω]˝ be the array of 
observed parameters.

The ˝χ⁽2⁾˝ distribution that relates the observed data to the theory is
```
  χ⁽2⁾([Ω]) = ∑{k} ÷{(P(k,[Ω₍fu₎]) - P₍obs₎(k))⁽2⁾}{δ(P(k))⁽2⁾}
```
where ˝P(k,[Ω₍fu₎])˝ is the theoretical power spectrum for the true set of 
parameters ˝[Ω₍fu₎]˝, and ˝P₍obs₎(k)˝ is the hypothetical observed power 
spectrum. ˝δ(P(k))˝ is the uncertainty in the spectrum (sum of squared 
residuals). The ˝2×2˝ fisher matrix components are:

```
  F⟦ij⟧ = ∑{k} ÷1{(δP(k, [Ω₍fu₎]))⁽2⁾} 
               ÷{ 𝝏P(k, [Ω₍fu₎]) }{ 𝝏Ω₍fu₎⟦i⟧ } 
               ÷{ 𝝏P(k, [Ω₍fu₎]) }{ 𝝏Ω₍fu₎⟦j⟧ }
```

You can compute the partial derivatives numerically, using the definition of a 
derivative
```
  ÷{ 𝝏P }{ 𝝏Ω₍m,fu₎ } = (lim₍ε→0₎ ÷{ P(Ω₍m,fu₎+ε) - P(Ω₍m,fu₎) }{ε} )
```
where ˝P(Ω₍m,fu₎)˝ is the matter power spectrum with ˝Ω₍m₎ = 0.3˝ (fiducial) and 
˝P(Ω₍m,fu₎ + ε)˝ is the matter power spectrum under a small variation in the 
value of ˝Ω₍m₎˝.

The sum of squared residuals is just
```
  ∑{k} (P(k) - ⟨P(k)⟩)^2
```

Using these definitions, I got the fisher matrix
```
  F = 
  %%<
    4.70e7 & -1.77e7 \\
    -1.77e7 & 1.34e7
  >%%
```

Plot of the ˝1σ˝ and ˝2σ˝ error contours (after adjusting for the fact that each 
˝k˝ mode can be measured with ˝1σ˝ uncertainty of ˝5⋄10⁴ (h⁻¹Mpc)³˝):

{{< fig fishercontours.png 8 >}}

Code: https://github.com/knbe/fishy

# APPENDIX 43

{{< fig a7s-FisherMatrix.jpg 12 >}}
- (blackboard)
{.caption}
