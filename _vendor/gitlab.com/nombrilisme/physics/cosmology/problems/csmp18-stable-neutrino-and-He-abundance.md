---
time: 202302170714
title: "effect of a massive, stable tau neutrino on primordial he abundance"
type: example
source: csmp 18
draft: true

what: |
  Consider a massive stable tau neutrino ˝ν₍τ₎˝ that's slightly nonrelativistic 
  at weak interaction freezeout, at the time of ˝⁽4⁾He˝ synthesis. Does the 
  ˝⁽4⁾He˝ abundance increase or decrease? What happens to ˝D˝?
---

The expansion rate of the universe is proportional to the relativistic degrees 
of freedom, ˝H ∝ g₍❄₎⁽1/2⁾ T⁽2⁾˝. During the production of light elements, the 
DoFs were ˝e⁽±⁾,γ˝, and three neutrino species (assumed to be light). Each of 
these has ˝g = 2˝, and using the formula for relativistic species in thermal 
equilibrium with photons,
```
  g₍❄₎ = ∑{i,bosons} g₍i₎ + ÷78 ∑{i, fermions} g₍i₎
```
you get ˝g₍❄₎ = 10.75˝. 

If there were an additional massive tau neutrino (i.e. ˝» MeV˝), then ˝g₍*₎ = 9˝. 
[KT p93] (honestly, I don't really get this part -- KT says on p70 that for a 
species that decouples when it's semirelativistic, it doesn't maintain an 
equilibrium distribution. I'm not sure how that actually affects ˝g₍❄₎˝. Alas).

Decreasing ˝g₍*₎˝ leads to a slower expansion rate. Freeze-out occurs when ˝H ∼ 
Γ˝ (where ˝Γ˝ is the rate of the weak interactions that maintain the 
proton-neutron balance). So the neutron/proton ratio will freeze out later, at a 
lower value of ˝T₍f₎˝, and the n/p ratio will be smaller. Since the ˝⁽4⁾He˝ 
abundance depends on the n/p freezeout ratio, this heavy tau neutrino will 
decrease the ˝⁽4⁾He˝ abundance, and likewise deuterium. I think.
