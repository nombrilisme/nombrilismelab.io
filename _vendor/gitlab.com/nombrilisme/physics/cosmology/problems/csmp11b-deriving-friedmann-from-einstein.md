---
time: 202302220221
title: "deriving friedmann from einstein"
type: problem
source: csmp11b

what: |
  Derive the friedmann and acceleration equations from the einstein equations. 
  State the assumptions you make.
---

This is the einstein field equation (EFE):
```
  ⬈G + Λ·⬈g = 8πG·⬈T
```
[note: this a "geometrized" tensor form of the EFE; bold typeface indicates a 
tensor]. ˝⬈G˝ is the einstein tensor, ˝⬈g˝ is the metric tensor, 
˝⬈T˝ is the stress-energy tensor. They're all rank 2 tensors. ˝G˝ is the 
gravitational constant, and ˝Λ˝ is the cosmological constant.

The einstein tensor ˝⬈G˝ is defined:
```
  ⬈G = ⬈R - ÷12·⬈g·R
```
where ˝⬈R˝ is the [ricci tensor][rt] and ˝R˝ is the [ricci scalar][rs]. Both 
these "ricci" thingies characterize the curvature of the manifold, i.e. the 
extent that it differs from a euclidean space. In component form this equation 
reads
```
  G⟦αβ⟧ = R⟦αβ⟧ - ÷12·g⟦αβ⟧·R
```

[rt]: https://en.wikipedia.org/wiki/Ricci_curvature
[rs]: https://en.wikipedia.org/wiki/Scalar_curvature

and the ricci scalar ˝R˝ is
```
  R = g⟦α'β'⟧·R⟦αβ⟧
```
[admittedtly, I use unconventional notation for components and co/contravariant 
indices -- there's an explanation in Appendix 11B].

_Assumption:_ the universe is a perfect fluid (homogeneous, isotropic) at rest 
in the "proper" reference frame. The stress-energy tensor for a perfect fluid is 
defined as
```
  ⬈T = (ρ + P)↗u⊗↗u + P⬈g
```
where ˝↗u˝ is the fluid's macroscopic velocity. In spacetime we can assume the 
components are ˝u⟦i⟧ = [1, 0, 0, 0]˝. So the components of the stress tensor are
```
  T⟦ab⟧ =
  %%<
    ρ ,,
    & [ P·g⟦ii⟧ ]
  >%%
  ͺͺforͺi=1:3
```

To evaluate the components of these tensors, you need a metric. For spacetime 
(assume isotropic, homogeneous), the FLRW metric:
```
  ds⁽2⁾ = -dt⁽2⁾ + a⁽2⁾(t) (÷1{1-kr⁽2⁾}dr⁽2⁾ + r⁽2⁾dθ⁽2⁾ + r⁽2⁾sin⁽2⁾θdφ⁽2⁾)
```
where the value of ˝k˝ determines the geometry. If ˝k = 0˝, it's euclidean, else 
it's curved. I will assume a flat geometry, so set ˝k = 0˝.

The components of the ricci tensor are:
```
  R⟦αβ⟧ = Γ⟦αβγ';γ⟧ - Γ⟦γαγ';β⟧ + Γ⟦γεγ'⟧ Γ⟦αβε'⟧ - Γ⟦αεγ'⟧ Γ⟦γβε'⟧
```
where the christoffel symbols are defined:
```
  Γ⟦abc'⟧ = ÷12 g⟦c'd'⟧ (g⟦bd;a⟧ + g⟦ad;b⟧ - g⟦ab;d⟧)
```

Now since these equations have 60 components, I'm not going to typeset out all 
the brute calculations (in fact, I solved for the components of the tensor(s) 
using [a tensor library I wrote][tns]) -- but you can see [the relics of] my 
brute force scribblings in Appendix 11A.

[tns]: https://github.com/knbe/tensor

The nonzero components of the ricci tensor are:
```m
  R₍tt₎ = -3÷{¨a}{a}
  R₍rr₎ = ÷{a¨a + 2˙a⁽2⁾ + 2k}{1-kr⁽2⁾}
  R₍θθ₎ = r⁽2⁾a¨a + 2r⁽2⁾˙a⁽2⁾ + 2k⁽2⁾
  R₍φφ₎ = r⁽2⁾sin⁽2⁾θͺa¨a + 2r⁽2⁾˙a⁽2⁾·sin⁽2⁾θ + 2k sin⁽2⁾θ
```
which can be summarized as
```m
  R⟦00⟧ = -3÷{¨a}a
  R⟦ii⟧ =  g⟦ii⟧ ÷{a¨a + 2˙a⁽2⁾ + 2k}{a⁽2⁾} ͺͺforͺiͺinͺ1:3
```

The ricci scalar is:
```
  R = g⟦a'b'⟧R⟦ab⟧ = ÷{6a¨a}{a} + 6(÷{˙a}a)⁽2⁾ + ÷{6k}{a⁽2⁾}
```

Now, put it all together. First evaluate the time component of the EFE:
```
  G₍tt₎ = 8πG·T₍tt₎ - Λ·g₍tt₎
```
or
```
  R₍tt₎ - ÷12·g₍tt₎·R = 8πGT₍tt₎ - Λg₍tt₎
```
Substitute the relevant components in, you get
```
  -(3÷{¨a}a) - ÷12 (-1) (6÷{¨a}a + 6÷{˙a⁽2⁾}{a⁽2⁾} + 6÷{k}{a⁽2⁾})
  =  8πG(ρ) - Λ(-1)
```
which simplifies to
```
  (÷{˙a}a)⁽2⁾ + ÷k{a⁽2⁾} = ÷13·8πGρ + ÷13·Λ
```
This is the friedmann equation. [I probably should have stated earlier -- I've 
assumed reduced coordinates where ˝c = 1˝].

Next evaluate the spatial components of the EFE:
```
  G⟦ii⟧ = 8πG·T⟦ii⟧ - Λ·g⟦ii⟧ ⦃for i=1:3⦄
```
Substitute the relevant components,
```
  g⟦ii⟧ ÷1{a⁽2⁾} (a¨a + 2˙a⁽2⁾ + 2k) = 8πG(g⟦ii⟧) + Λ(g⟦ii⟧)
```
and cancel the metric factors, you get
```
  (÷{¨a}{a} + 2(÷{˙a}a)⁽2⁾ + 2÷k{a⁽2⁾}) 
  - (÷12 (6÷{¨a}a + 6÷{˙a⁽2⁾}{a⁽2⁾} + 6÷{k}{a⁽2⁾}))
  = 8πGP - Λ
```
Simplify a wee bit,
```
  -2÷{¨a}a - (÷{˙a}a)⁽2⁾ - ÷k{a⁽2⁾} = 8πGP - Λ
```
and now subtract the friedmann equation from this, you get
```
  -2÷{¨a}a = 8πGP + ÷13·8πGρ - Λ - ÷13Λ
```
Simplifying once more,
```
  ÷{¨a}a = -÷13·4πG(ρ + 3P) + ÷13·Λ
```
This is the acceleration equation.

--------------------------------------------------------------------------------

# APPENDIX 11A

[blackboard]
{{< fig L-efe.jpg 10 >}}

# APPENDIX 11B {.11b}

[notation]

When I typeset math, I put square braces around any subscripts that denote an 
_index_ of an array, e.g. ˝x⟦i⟧˝ means the ith component of the array ˝x˝.

Subscripts without square braces are just named variables, so if I write ˝x₍i₎˝ 
=, this is _not_ the ith component of ˝x˝; rather, ˝x₍i₎˝ is a variable in itself. 

The reason I do this is because I always seem to get confused reading people's 
(and my own) math where descriptive subscripts and indexing subscripts are both 
present in a single equation. This way a variable like critical temperature 
˝T₍c₎˝ cannot be confused to mean the ˝c˝th elements of a matrix ˝T˝. If ˝T₍c₎˝ 
were a matrix then I would express its components as ˝T₍c₎⟦ij⟧˝.

When doing tensor algebra, I denote contravariant (up) indices with a tick 
marker, e.g. ˝g⟦a'b⟧˝. Normally this would be written ˝g⁽a⁾₍b₎˝.
