---
time: 202304021828
title: "33"
type: example
source: csmp33
draft: true
what: |
  Prove

  (a)
  ```
    (÷{aH}{˙φ₍0₎})⁽2⁾ = ÷{4πG}{ε}
  ```
  (b)
  ```
    P₍Φ₎(k) = ÷{4P₍ζ₎(k)}{9}
  ```
---

