---
time: 202302180254
title: "olber's paradox"
type: lexicon

what:
  0: |
    why the night sky is dark. best answer: the universe has a finite age. it's 
    13.8 billion years old.
    - {{< 𝖗 JPN l1 p3 >}}

---
