---
title: "big bang"
time: 202305100041
type: lexicon
domain: cosmology
aka: 

alsoread: 

what:
  0: |
    a silly little theory (though currently the main one) that accounts the 
    expansion of the universe from an initial state of (infinitely) high 
    temperature and density.
---
