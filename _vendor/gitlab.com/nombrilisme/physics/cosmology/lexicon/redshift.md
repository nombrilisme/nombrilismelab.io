---
title: "redshift"
time: 202305121724
type: lexicon
domain: cosmology

aka: 

alsoread: 

what:
  0: |
    an observed increase in the wavelength of electromagnetic radiation (e.g. 
    light). and the accompanying decrease in frequency.

---
