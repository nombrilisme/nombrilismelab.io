---
title: "blueshift"
time: 202305121726
type: lexicon
domain: cosmology

aka: 

alsoread: 

what:
  0: |
    the opposite of {{< 词典 redshift >}}.

    the red/blue terminology comes from the visible light colour spectrum. 
    an increase in wavelength = more "red".

---
