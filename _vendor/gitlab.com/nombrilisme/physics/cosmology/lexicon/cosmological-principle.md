---
title: "cosmological principle"
time: 202305100044
type: lexicon
domain: cosmology
aka: 

alsoread: 

what:
  0: |
    the spatial distribution of matter in the universe is HOMOGENEOUS and 
    ISOTROPIC, when viewed at a large enough scale.


    HOMOGENEOUS means it has the same properties at every point. that it's 
    uniform and doesn't have irregularities. e.g. a uniform electric field. 
    homogeneity implies a kind of invariance. all components of an equation have 
    the same _degree_ of a value, regardless of whether the components are 
    scaled up or down to different values.

---
