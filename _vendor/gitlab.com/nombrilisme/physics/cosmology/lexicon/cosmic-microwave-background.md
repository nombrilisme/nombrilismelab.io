---
title: "cosmic microwave background"
time: 202305090914
type: lexicon
domain: cosmology

aka: 

alsoread: 

what:
  0: |
    a remnant of microwave radiation that pervades the entire observable 
    universe. the CMB is an important source of information on the primordial 
    universe.

    the CMB can be observed with a sufficiently powerful radio telescope. it 
    manifests as a faint uniform background "glow" not associated with any known 
    "object" in space (like galaxies, stars, etc). generally, for less powerful 
    optical telescopes, the space between "objects" in the universe (galaxies, 
    stars, etc) is observed to be completely empty. 
---
