---
title: "metric"
time: 202305100047
type: lexicon
domain: cosmology
aka: 

alsoread: 

what:
  0: |
    something that returns the physical value of points in spacetime. e.g. the 
    {{< 词典 FLRW metric >}}.
---
