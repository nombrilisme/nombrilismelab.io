---
time: 202303011823
title: "ionisation energy"
type: lexicon
domain: cosmology

what:
  0: |
    the energy needed to remove the most loosely bound outer electron in a 
    neutral atom.

---

