---
time: 202304190400
title: "galaxy luminosity function"
type: lexicon
domain: cosmology

what:
  0: |
    gives the number density of galaxies of a given luminosity.

alsoread:
- UIO AST4320, _Press Schechter Mass Function_, Undervisningsmateriale Høst 2012
---
