---
title: "ising model"
type: long
---

# WHAT

a model of a ferromagnet or antiferromagnet on a lattice. 

in 1925 lenz and ising showed that in 1d the model doesn't have a phase 
transition for £ T > 0 £
- they wrongly concluded that the model also doesn't have a phase transition for 
  d > 1, and they concluded it couldn't describe real magnetic systems

# SETUP

a lattice in £ d £ dimensions of sites £ i, i = 1..N £ (assume hypercubic)

the dofs are classical spin vars £ S_i £ that reside on the vertices of the 
lattice, only take two values, £ S_i = \pm 1 £

the total num of states of the system is £ 2^N £

the spins interact with an ext magnetic field £ H_i £ (in the most general setup 
the field can vary from site to site)

the spins also interact with each other thru exchange interactions £ J_{ij}, 
K_{ijk}, ... £ (couples two spins, three spins...)

# HAMILTONIAN

general form for the hamiltonian

{{< M >}}
  -Ham = ∑_{i=1..N} H_i S_i + ∑_{ij} J_{ij} S_i S_j + ∑_{ijk} K_{ijk} S_i S_j 
  S_k + ...
{{< /M >}}

- although we can just neglect 3 and higher spin interactions

# SUMMATION/TRACE NOTATION

Tr means:

{{< M >}}
  Tr = ∑_{S_1 = -1, 1} ∑_{S_2 = -1, 1} ... ∑_{S_N = -1, 1} = ∑_{S_i = \pm 1}
{{< /M >}}

# FREE ENERGY

the free energy:

{{< M >}}
  F(T, [H_i], [J_{ij}]...) = -k_B T \log Tr 𝓮^{-β Ham}
{{< /M >}}

- can get td properties by differentiating this

avg value of magnetisation at site £ i £. differentiate £ F £ wrt the ext field 
£ H_i £

{{< M >}}
  ÷{∂F}{∂H_i}
  == -k_B T ÷{1}{Tr 𝓮^{-β Ham}} Tr ÷{S_i}{k_B T} 𝓮^{-β Ham}
  == -\expval{S_i}
{{< /M >}}

# THERMODYNAMIC LIMIT

for a finite system £ F £ is an analytic function of each of its args *at least 
in some strip incl the real axis*, pk for each of the £ 2^N £ states of the system, the energy of the nth state £ E_n £ is just a linear combination of the coupling constants

{{< M >}}
  Z = ∑_{n = 1..2^N} \exp(-β E_n[K])
{{< /M >}}

- this shows that £ Z[K] £ is analytic

phase transitions only arise in the TD limit. 

for the TD limit to exist it's necessary that the two spin interaction £ J_{ij} £ ssfies

{{< M >}}
  ∑_{j \neq i} | J_{ij} | < ∞
{{< /M >}}

the range of the interaction £ J_{ij} £ and the dimensionality determine whether 
or not the TD limit exists.
- e.g. suppose for two spins £ S_i £ and £ S_j £ at lattice sites with positions 
  £ ↗r_i, ↗r_j £, 

  {{< M >}}
    J_{ij} = A | ↗r_i - ↗r_j |^{-σ}
  {{< /M >}}
 
- in this case, we require £ σ > d £ for the limit to exist.
- in £ d = 3 £ this condition excludes magnetic dipole dipole interactions. they 
  fall off with distance £ r £ as £ r^{-3} £
- but as long as the dipoles aren't aligned, you can show that the TD limit does 
  exist and is independent of the shape of the region

# NEAREST NEIGHBOUR ISING MODEL

hamiltonian

{{< M >}}
  -Ham = H ∑_{i=1..N} S_i + J ∑_{<ij>} S_i S_j
{{< /M >}}

- assumed the ext field £ H £ is uniform in space
- and that the only interaction between spins is between neighbouring spins, denoted £ J £

magnetisation or magnetic moment per site, £ M £

{{< M >}}
  M 
  == - ÷1N ÷{∂F}{∂H}
  ==÷1N ∑_{i=1..N} \expval{S_i}
{{< /M >}}

# ANALYTIC PROPERTIES OF £ f £

it's important to examine the analytic properties of the bulk free energy density £ f_b[K] £, pk we want to know exactly when and how nonanalytic behaviour arises. 

the main analytic properties of £ f £
- £ f < 0 £
- £ f(H, T, T, ...) £ is continuous
- £ ∂f / ∂T, ∂f / ∂H £ exist almost everywhere
- the entropy per site £ S = -∂f/∂T \geq 0 £ almost everywhere
- £ ∂f/∂T £ is monotonically nonincreasing with £ T £, so
  {{< M >}}
    \ppp fT \leq 0
  {{< /M >}}
  implying the specific heat at constant magnetic field £ C_H \geq 0 £
  {{< M >}}
    C_H = T \pp ST |_H = -T \ppp FT |_H \geq 0
  {{< /M >}}
- £ ∂f/∂H £ is monotonically nonincreasing with £ H £, so
  {{< M >}}
    \ppp fH \leq 0
  {{< /M >}}
  implying the isothermal susceptibility £ χ_T \geq 0 £, 
  {{< M >}}
    χ_T = \pp MH |_T = -\ppp fH
  {{< /M >}}

# SYMMETRIES OF THE ISING MODEL

the symmetries of the ising model show that a phase transition isn't possible in a finite system

lemma: for any function £ φ £ that depends on the spin configuration £ [S_i] £, 

{{< M >}}
  ∑_{S_i = \pm 1} φ([S_i]) = ∑_{S_i = \pm 1} φ([-S_i])
{{< /M >}}

# TIME REVERSAL SYMMETRY

up down symmetry, time reversal symmetry, £ Z_2 £ symmetry

can prove that £ Z(-H, J, T) = Z(H, J, T) £ (gf 2.51)

free energy density is even in H: £ f(H, J, T) = f(-H, J, T) £

# SUB LATTICE SYMMETRY

occurs when £ H = 0 £

can prove that £ f(0, J, T) = f(0, -J, T) £ (gf 2.57)

in zero field the ferromagnetic and anti ising models on a hypercubic lattice have the same thermodynamics
