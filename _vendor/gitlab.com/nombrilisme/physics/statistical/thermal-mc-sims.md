---
title: "thermal mc sims"
type: long
---

# ESTIMATOR



# MARKOV PROCESSES
--------------------------------------------------------------------------------

generating an appropriate set of random states according to the boltzmann 
distribution is not trivial.

{{< 𝖉𝖊𝖋 MARKOV PROCESS >}}
  a process that takes a system in one state, a, and generates a new state, b.
{{< /𝖉𝖊𝖋 >}}

{{< 𝖉𝖊𝖋 TRANSITION PROBABILITY >}}
  the probability of getting state b from a: P(a→b).
{{< /𝖉𝖊𝖋 >}}

properties:
  1. shouldn't vary with time
  2. should only depend on the properties of the current states a and b, not 
  other states the system passed through.
  - ie the probability the process generates state b when it's given state a is 
    the same every time it's fed the state a
  3. transition probabilities satisfy
  ```
    ∑{b} P(a→b) = 1
  ```
  - ie the markov process must generate some state b when it's given state a
  4. the transition probability £ P(a→a) £ doesn't need to be zero
  - ie the there's some probability the markov process stays in state a

¶ MC sims use markov processes repeatedly to generate a markov chain of states
  - start with state a, use the process to generate a new state b, then feed 
    that into the process to generate state c, etc

¶ you choose the markov process in such a way that when you run it for long 
  enough, it produces a series of states that have probabilities given by the 
  boltzmann distribution

¶ the process of reaching the boltzmann distro: "coming to equilibrium"
  - real systems also go through this process
  - in order to achieve this effect, you have to put two further conditions on 
    the markov process: ergodicity and detailed balance

# ERGODICITY
--------------------------------------------------------------------------------

{{< 𝖉𝖊𝖋 ERGODICITY CONDITION >}}
  requires that the markov process should be able to reach any state of the 
  system from any other state, if you run it for long enough 
{{< /𝖉𝖊𝖋 >}}

¶ this basically means that even though you can make some of the transition 
  probabilities in the markov process zero, there still has to be @least 1 path 
  of nonzero transition probabilities between two arbitrary states
  - MC algs generally set most transition probabilities to zero. you have to be 
    careful to do this in a way that doesn't violate ergodicity
  - whenever you construct a MC alg, explicitly prove it doesn't violate 
    ergodicity

# DETAILED BALANCE
--------------------------------------------------------------------------------

{{< 𝖉𝖊𝖋 DETAILED BALANCE CONDITION >}}
  the rate the system makes transitions in and out of an arbitrary state a must 
  be equal
  ```
    ∑{a} p_b P(b → a) = ∑{b} p_a P(a → b)
  ```
  or
  ```
    p_a = ∑{b} p_b P(b → a)
  ```
{{< /𝖉𝖊𝖋 >}}

¶ this condition is what ensures that after the system comes to equilibrium, the 
  distribution of states is the boltzmann distribution

¶ another condition for the transition probabilities:
  ```
    p_a P(a → b) = p_b P(b → a)
  ```

¶ the detailed balance condition says the transition probabilities should 
  satisfy
  ```
    ÷{P(a→b)}{P(b→a)} = ÷{p_b}{p_a} = exp{-β(E_b - E_a)}
  ```

# THE TRANSITION PROBABILITIES SHOULD SATISFY
--------------------------------------------------------------------------------

  1. 
  ```
    ∑{b} P(a→b) = 1
  ```

  2. detailed balance:
  ```
    ÷{P(a→b)}{P(b→a)} = ÷{p_b}{p_a} = exp{-β(E_b - E_a)}
  ```

  3. ergodicity

¶ if you satisfy these, and ergodicity, then the equilibrium distribution of 
  states in the markov process will be the boltzmann distro

¶ basic algorithm: given a set of transition probabilities, implement the markov 
  process (i.e. generate a chain of states) for these transition probabilities

# ACCEPTANCE RATIOS
--------------------------------------------------------------------------------

¶ finding the right markov process for a given set of transition probabilities 
  that satisfies the two conditions above is not trivial.
  - as I fucking suspected
  - fuck you newman

¶ gamechanger: it turns out you can choose any algorithm you want for generating 
  new states, and still have the desired set of transition probabilities
  - you have to introduce an acceptance ratio

¶ decompose the transition probability into two parts:
  ```
    P(a → b) = g(a → b) A(a → b)
  ```
  - g(a → b) is the  selection probability. given an initial state a, the 
    probability that the alg with generate a new target state b
  - A(a → b) is the acceptance ratio. if you start in state a and the alg 
    generates new state b, should only accept the new state a fraction of the 
    time. else stay in state a.

¶ the acceptance ratio can be anything between 0 and 1.
  - if you choose A = 0, this is equivalent to P(a → a), i.e. remain in state a.

¶ basic MC alg:
  - find an alg that generates random new states b from old ones a, with some 
    set of probabilities g(a → b)
  - accept or reject those states with acceptance ratio A(a → b)
  - this satisfies the reqs for the transition probabilities. it produces a 
    string of states that appear with their correct boltzmann probability when 
    the alg reaches equilibrium

¶ tips about the acceptance ratio
  - make it close to 1 as possible. pq you want the system to actually move 
    around state space, not just amble about
  - {{< 𝖗 newman-barkema p42 >}}
  - the best way to keep acceptance ratios large: try and make the selection 
    probabilities g(a → b) resemble the dependence of P(a → b) on the 
    characteristics of a and b (i.e. rely as little as possible on the 
    acceptance ratio)
  - the ideal alg: one where the new states are selected with exactly the 
    correct transition probabilities all the time, ∴ the acceptance ratio can 
    always be set to 1.
