---
title: "mc extrapolation methods"
type: long
---

# SINGLE HISTOGRAM METHOD

¶ a method where you can do a single MC sim at some T and extrapolate the 
  results to other nearby temperatures
  - saves cpu time

¶ also the multiple histogram method. can estimate the observable over a larger 
  range than that allowed by the single histogram method
