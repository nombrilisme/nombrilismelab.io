---
time: 202303230143
title: "ising mft"
type: long
---

# WHAT

A model of a ferromagnet or antiferromagnet on a lattice. "The ising model is the 
_drosophila_ of statistical mechanics" ∼ Nigel Goldenfeld.

In 1925 Lenz and Ising showed that in 1D, the model doesn't have a phase 
transition for £ T > 0 £. They wrongly concluded that the model also doesn't 
have a phase transition for ˝d > 1˝.

# SETUP

Consider a lattice of sites in £ d £ dimensions, £ i = 1..N £ (assume hypercubic 
in the most general sense). Attach a classical spin variable ˝S₍i₎ = ±1˝ to each 
site.

The #total of states of the system is £ 2^N £.

The spins may interact with an external magnetic field £ H_i £ (in the most 
general setup, the field can vary from site to site, but normally can just 
assume a uniform field).

The spins interact with each other thru exchange interactions £ J_{ij}, 
K_{ijk}, ... £ (couples two spins, three spins...)

# HAMILTONIAN

A very general form for the hamiltonian:
```
  -βĦ = ∑_{i=1..N} H_i S_i + ∑_{ij} J_{ij} S_i S_j + ∑_{ijk} K_{ijk} S_i S_j + ...
```
although normally we neglect the ≥3 spin couplings.

# FREE ENERGY

```
  F(T, [H_i], [J_{ij}]...) = -k_B T \log Tr e^{-β Ħ}
```

Various derivatives of this give the thermodynamic properties. E.g. the average 
magnetisation at site £ i £: Differentiate £ F £ wrt the external field £ H_i £
```
  ÷{∂F}{∂H_i} = -k_B T ÷{1}{Tr e^{-β Ħ}} Tr ÷{S_i}{k_B T} e^{-β Ħ} = -\expval{S_i}
```

# NEAREST NEIGHBOUR ISING MODEL

Hamiltonian:
```
  -βĦ = H ∑_{i=1..N} S_i + J ∑_{⟨ij⟩} S_i S_j
```

here we're assuming the ext field £ H £ is uniform, and that the only 
interaction between spins is between neighbouring spins, denoted £ J £.

The magnetisation per site, £ M £
```
  M = - ÷1N ÷{∂F}{∂H} =÷1N ∑_{i=1..N} \expval{S_i}
```

# MEAN FIELD THEORY -- HIGH LEVEL STUFF

MFT is a simple way to treat interacting systems (the only simpler method is to 
just ignore the interactions completely). In MFT you neglect correlations 
between fluctuations of different particles. This approximation becomes less and 
less accurate as you approach the critical point (the fluctuations contain the 
most important physics near a critical point).

Basically, MFT involves taking the interacting hamiltonian, removing the 
correlations between fluctuations of different particles, and what you end up 
with is a non-interacting hamiltonian with an "effective field". This effective 
field is due to the neighbouring spins. So essentially, MFT replaces the 
many-interaction reality with one "effective" interaction.

# WEISS MFT -- FERROMAGNETIC ISING MODEL

The nearest neighbour ising model in d dimensions (to keep it general - there's 
an implementation of the 2D ising model at the end):
```
  Ham = -J ∑_{⟨ij⟩} S_i S_j - H ∑_i S_i
```
- i,j are points on a d-dimensional square lattice
- each point has £ z = 2d £ nearest neighbours. £ z £ is the coordination number.
- £ J > 0 £ for ferromagnetism
- £ H £ is proportional to the external magnetic field ˝B˝ (this is basically a 
  stand-in variable)

In MFT, we assume the spins behave uniformly, i.e. that they behave the same way 
at each site (on average). Means that in the low T phase, we expect the spins to 
have the same nonzero average, and in the high T phase, we expect the spins to 
average to zero.

Next: express the spin variable as the sum of an average and a fluctuation
```
  S_i = \expval S + δS_i
```
- £ \expval S £ is the average value of spin, the same for all sites
- £ δS_i £ is the fluctuation
- £ \expval{δS_i} = 0 £

Rewrite £ Ħ £ in terms of £ \expval{S} £ and £ δS_i £:
```
  Ħ == -J ∑_{ij} (\expval S + δS_i)(\expval S + δ S_j) - H ∑_i S_i
  == -J ∑_{⟨ij⟩} ( \expval S^2 + \expval S (δS_i + δS_j) + δS_i δS_j) - H ∑_i S_i
```
- the third term in the parens is the interaction between neighbouring spins
- its presence means £ S_i £ and £ S_j £ aren't independent random variables, so £ 
  \expval{S_i S_j} \neq \expval{S_i} \expval{S_j} £ or £ \expval{δ S_i δS_j} 
  \neq 0 £
- the second term in the parens represents the effect of the field produced by 
  the other nearby spins
- a given spin feels both the external field and the effective field due to the 
  other spins

In the MF approximation, we ignore the correlations of fluctuations of 
neighbouring particles. This means setting the term £ δS_i δS_j £ to zero. This 
approximation allows you to rewrite £ Ħ £ as a linear function of the £ S_i £, 
as if the spins aren't interacting.
```
  \expval S (δS_i + δS_j) = \expval S (S_i + S_j - 2 \expval S)
```
∴
```
  Ħ_{MF} = -J ∑_{ij} (  \expval{S}^2 + \expval S (S_i + S_j - 2 \expval S)) - H ∑_i S_i
```

- note, I've used the fact that £ ∑_{ij} \expval S^2 = \expval S^2 ∑_{ij} 1 = 
  \expval S^2 ∑_i ÷z2 = ÷{Nz}2 \expval S^2 £ (you have to divide by two 
  otherwise you'll count each site twice)
- also note that £ \expval S ∑_{ij} S_i = \expval S ∑_i ÷z2 S_i £

∴

```
  
  Ħ_{MF} == ÷12 JNz \expval S^2 - ∑_i (J z \expval S + H) S_i
  == ÷12 JNz \expval S^2 - H_{eff} ∑_i S_i
```

This is the mean field hamiltonian. What we've done here is we've replaced the 
interacting spin problem with a noninteracting problem with one unknown 
parameter, £ \expval S £.

Next, let £ M = \expval S £. You can do this since you're assuming the spins 
behave uniformly, so you can assume the magnetisation per site is the same for 
all, i.e. £ M_i = M £.
- recall, magnetisation per site £ M = ÷1N ∑_{i=1..N} \expval{S_i} = \expval S £

The partition function in the MF approximation:
```
  Z_{MF} == ∑_{[S_i]} e^{-β Ham_{MF}}
  ==  ...
  == \exp(-÷12 JNz β M^2) (2 \cosh(β(H + JzM)))^N
```
- (missing steps: look@ Oxford C6 theory handbook p78 eq548)

You can derive a self-consistency equation for £ M £:
```
  M = \expval{S_i}
  == ÷{Tr \; S_i e^{β H_{eff} S_i}}{Tr e^{β H_{eff} S_i}}
  == \tanh(β H_{eff})
  == \tanh(β(H + JzM))
  == \tanh(β(H + 2dJM))
```

Solve the self-consistency equation graphically:
- let £ x = β(H + JzM) £
- then £ M = ÷{k_B T x}{Jz} - ÷{H}{Jz} = \tanh(x) £

With regards to what you can compute using the mean field approximation: 
here are a some things---

__Case:__ £ H = 0 £, zero external field: 

The equation to solve: ˝÷{k_B T x}{Jz} = \tanh(x)˝. Here's a sketch of the 
self-consistency equation:
  
{{< fig sceq.jpg 8 >}}

For £ (k_B T / Jz) > 1 £:
- £ \tanh(x) \simeq x £ for small x, so the curves intersect once, at £ x = 0 £.

For £ (k_B T / Jz) < 1 £:
- there are two additional roots, equal magnitude, opposite sign.

At high T, £ k_B T > Jz £, and there's only one root, corresponding to £ M = 0 
£, no magnetisation.

At zero T, the system is in the lowest energy state, £ M = \pm 1 £. The system 
will choose one of the two nonzero roots.

As you increase T, the system will have £ M £ equal to the nonzero root until £ 
T = T_c = Jz / k_B £
- basically, when £ T < T_c £ the nonzero root(s) are favourable
- you can prove this rigorously: the helmholtz free energy is lower for the 
  nonzero roots when £ T < T_c £
- and when £ T > T_c £, £ M = 0 £ (for £ H = 0 £)

__Case:__ £ H \neq 0 £, nonzero external field:
- ˝M = \tanh(β(JzM + H))˝

__Case:__ high temperature, £ T > T_c £, weak field (the Curie weiss law)
- under small £ H £ (weak field limit), £ M £ will be small, and so will £ β(JzM 
  + H) £
- ∴
  {{< M >}}
    M \simeq ÷{JzM + H}{k_B T}
  {{< /M >}}
  ∴
  {{< M >}}
    M(1 - ÷{Jz}{k_B T}) == ÷{H}{k_B T}
    == ÷{H}{k_B T - Jz}
    == ÷{H}{k_B (T - T_c)}
  {{< /M >}}

This gives the zero field susceptibility, £ H = 0 £:
{{< M >}}
  χ = ÷{𝝏M}{𝝏H} = ÷1{k_B(T - T_c)}
{{< /M >}}
- for £ T > T_c £
- because of interactions, £ χ £ diverges at the MF transition temp £ T_c £
- if there are no interactions, £ T_c = 0 £, and you get **curie's law**, £ χ = 
  1 / 4 k_B T £, or £ M = H / 4 k_B T £
- the interactions lead to the divergence at £ T = T_c £

Experimentally, it turns out that £ χ = ÷{𝝏M}{𝝏H} \~ a (T - T_c)^{-γ} £ near the 
critical point. MFT predicts £ γ = 1 £, but experimental values tend to be 
larger than 1

{{< fig bhfig81.png 6 >}}

        Source: Berlinsky & Harris. 

- the solid curve is the curie weiss result
- the dashed curve is the susceptibility when you account for fluctuations that 
  aren't captured in MFT.
- the discrepancy is the result of ignoring the correlations of fluctuations.

__Case:__ T just below £ T_c £, £ H = 0 £

- £ M £ is nonzero but small
- expand £ M = \tanh(βJzM) £ as
  ```
    M = βJzM - ÷13(βJzM)^3 + ...
  ```
- since £ M \neq 0 £ divide by £ M £ to get
  ```
    M = \pm \sqrt{÷{3(βJz - 1)}{(βJz)^3}} = \pm \sqrt 3 ÷{k_B T}{Jz} \sqrt{1 - ÷{k_B T}{Jz}}
  ```
- take £ H = 0^{+} £ so £ M > 0 £. since £ T_c = Jz / k_B £,
  ```
    M = \sqrt 3 ÷{T}{T_c} \sqrt{1 - T / T_c}
  ```
- but if £ T £ is close to £ T_c £, set £ T = T_c £ in the first factor (the 
  factor between root 3 and the other root), then
  ```
    M \simeq \sqrt 3 (1 - T / T_c)^β
  ```
- with £ β = 1/2 £

Susceptibility for £ T < T_c £:
```
  χ = ÷{1}{2 k_B (T_c - T)} \hspace{1cm} T < T_c
  χ = ÷{1}{k_B (T - T_c)} \hspace{1cm} T > T_c
```

In general, 
  ```
  χ \sim A_{\pm} | T - T_c |^{-γ}
  ```
- there's a different prefactor above and below the transition
- same exponent above and below the transition
- thus, £ χ £ diverges at the critical point.

The relationship between £ M £ and £ H £ at the critical point:
```
  M = (÷{3H}{k_B T_c})^{1/3}
```
- at £ T_c £, magnetisation obeys
  ```
    M \sim |H|^{1/δ}
  ```
- with £ δ = 3 £ for MFT

Heat capacity in MFT:
- specific heat at constant field, £ H = 0 £
  ```
    c = ÷32 k_B ÷T{T_c} (÷{3T - 2 T_c}{T_c})
  ```
- for £ T > T_c £, £ M = 0 £, so £ U/N = 0 £ and £ C = 0 £
  ```
    c(T_c^-) = ÷32 k_B
    c(T_c^+) = 0
  ```
- specific heat has a discontinuity at the transition
- relate this to the definition of the critical exponent £ α £, where specific 
  heat is given by
  ```
    C \sim a_{\pm} |T-T_c|^{-𝛼}
  ```
- the discontinuity corresponds to £ α = 0 £, with amplitudes £ a_+ = 3/2 k_B £ 
  and £ a_- = 0 £
- you see a jump like this in the specific heat of low temperature 
  superconductors. But most other phase transitions are not mean-field like.

# TAKEAWAYS

The MF hamiltonian is a sum of single-site hamiltonians. You get a 
self-consistent equation for ˝M˝ which you can solve graphically. You can 
compute parameters like susceptibility, heat capacity, and other thermodynamic 
quantities. MFT accounts for the ferromagnetic phase transition, but it loses 
accuracy near the critical transition. 

Specifically, MFT can't describe correlations _between_ fluctuations of £ S_i £ 
and £ S_j £, and the approximation breaks down when the £ S_i S_j £ correlation 
length becomes large (which it does in the vicinity of the critical 
temperature). The MFT predictions of critical exponents are generally incorrect.

Near the phase transition you'll see data collapse: the equation of state will 
go from being a a relation between 3 variables to a relation between 2 _scaled_ 
variables. You can model this using the renormalisation group.


{{< fig isingscreenshot.png 8 >}}

(A quick little MC implementation of the 2D Ising model, written by yours truly. 
https://github.com/knbe/ising)

# REFERENCES

N. Goldenfeld, _Lectures on Phase Transitions and the Renormalisation Group_.

A. J. Berlinsky, A. B. Harris, _Statistical Mechanics_.

F. H. L. Essler, _Lecture Notes for the Oxford C6 Theory Option_.
