---
title: "ising - metropolis"
type: long
---

# THE PROBLEM
--------------------------------------------------------------------------------

¶ consider a 2d ising model on a 10x10 grid
  - there are ˝N=100˝ spins 
  - the system has ˝2⁽100⁾˝ possible states (i.e. sets of spin alignments)

¶ a typical MC summation involves taking a random sampling of the states. 
  - take a random sample of as many states as possible
  - compute the boltzmann factors for each of these states
  - compute avg energy, magnetisation, etc
    
¶ but in this case, even that would be idiotic, as a random sampling of ˝2⁽100⁾˝
  would still be a massive fucking number

¶ another issue is that at low T, when the system wants to magnetise, the 
  important states (ie the ones that point in the same direction, the ones that 
  contribute to the onset of magnetisation) constitute a very small proportion 
  of the total possible states. so a random sample might well miss them. and 
  that is bad.

# METROPOLIS
--------------------------------------------------------------------------------

¶ use the boltzmann factors to dictate which subset of states to sample. you 
  want to sample from states that are likely.

¶ scheme:
  - start with some state
  - choose a dipole randomly to decide whether to flip
  - compute the energy difference ΔE the flip would cause
  - if ΔE ≤ 0, the system's energy decreases or stays the same. flip this spin.
  - if ΔE > 0, the system's energy increases, so decide at random whether to 
    flip the dipole, with probability of the flip ˝exp{-ΔE/ƙT}˝
  - if the spin isn't flipped, the system's state will be the same as before
  - then choose another spin at random and repeat, and over and over until every 
    spin has had many opportunities to be flipped
  - >> hmm, isn't it a problem we're flipping the spins one at a time? like, 
    irl, this would be happening simultaneously? <<

¶ the metropolis alg generates a subset of states where lower energy states 
  occur more frequently than higher ones.
  - consider two states, 1 and 2, with energies ˝E₍1₎, E₍2₎˝, that differ by one 
    spin flip
  - let £ E₍1₎ ≤ E₍2₎ £
  - if the system is in state 2, the probability of making a transition to state 
    1 (high energy state → low energy state) is £ 1/N £.
    - think about it. there are N spins in total, each one could be flipped 
      next. but we're looking at one specific spin. 1 out of N. so the 
      probability is 1/N
      ```
        hhh
      ```
  - if the system is initially in state 1, the probability of making a 
    transition to state 2 (low E → high E) is ˝÷1N exp{-(E₍2₎ - E₍1₎)/ƙT}˝
  - the ratio of these transition probabilities:
    ```
      ÷{P(1→2)}{P(2→1)}
      == ÷{(1/N) exp{-(E_2 - E_1)/ƙT}}{(1/N)}
      == ÷{exp{-E_2 / ƙT}}{exp{-E_1 / ƙT}}
    ```
  - this is the detailed balance condition!
    - means the rate of transitions into and out of an arbitrary state is the 
      same
    - ensures that after the system equilibriates, the distribution of states is 
      the boltzmann distribution
  - consider another two states, 3 and 4. suppose they differ from 1 and 2 by 
    flipping some spin. note the system can go between 1 and 2 through 3 and 4
    ```
      ÷{ P(1→3→4→2) }{ P(2→4→3→1) }
      == ÷{exp{-E_3/ƙT} exp{-E_4/ƙT} exp{-E_2/ƙT}}{exp{-E_1/ƙT} exp{-E_3/ƙT} exp{-E_4/ƙT}}
      == ÷{ exp{-E_2/ƙT} }{ exp{-E_1/ƙT} }
    ```
  - this is the ergodicity condition! 
    - means the system can reach any state from any other state if you run it 
      for long enough

¶ thus, the metropolis alg generates states with the correct boltzmann 
  statistics.

¶ BUT. you can only really say this after you run the alg for a very very long 
  time, so every state has been generated many times
  - if you run the alg for a short time, most states aren't generated
  - ∴ there's no guarantee the subset of generated states fairly represents the 
    full set of states

¶ at low T, the metropolis alg pushes the system into a metastable state where 
  nearly all the spins are parallel to their neighbours. 
  - boltzmann statistics says that such a state is quite probable
  - but it might take the alg a long time to generate other probable states that 
    differ significantly (eg the state where every spin is flipped)
  - but this is what happens irl. a large system doesn't explore all possible 
    microstates. the relaxation time to achieve thermodynamic equilibrium can be 
    very long
  - >> schroeder ch8 says a large system "doesn't have time" to explore all 
    possible microstates. I think this is a dubious use of the word time. <<

# MEASUREMENT
--------------------------------------------------------------------------------

¶ total energy:
  - calculate total energy once, at start of sim
  - then at each spin flip increment it by dE

¶ magnetisation:
  - calculate total magnetisation ˝M˝ once, at start of sim
  - at each spin flip increment ˝M˝ by ˝2S₍new₎˝ or ˝-2S₍current₎˝

¶ specific heat:
  ```
    c = ÷{β^2}{N} (⟨E⁽2⁾⟩ - ⟨E⟩⁽2⁾)
  ```

¶ susceptibility:
  ```
    χ = βN(⟨m⁽2⁾⟩ - ⟨m⟩⁽2⁾)
  ```

¶ correlation time, ˝τ˝
  - if you want to get averages like ˝⟨E⟩, ⟨M⟩˝, you need to know how long you 
    have to average them over to get a good estimate. 
  - ie ideally you'd wait for them to reach their equilibrium value

{{< 𝖉𝖊𝖋 CORRELATION TIME >}}
  measure of how long it takes a system to get from one state to another state 
  that's sufficiently different from the first
{{< /𝖉𝖊𝖋 >}}

# FAILURE OF METROPOLIS IN THE CRITICAL REGION
--------------------------------------------------------------------------------

¶ statistical errors in the measured values of qties like m and E are propto the 
  size of the critical fluctuations
  - ∴ the errors grow as you approach ˝T₍c₎˝

¶ in a finite system the size of the fluctuations doesn't actually diverge. true 
  divergence can only happen in the TD limit. but still, the fluctuations can 
  become very large, and have correspondingly large errors.
  - the errors can be reduced by running the program for longer, ie getting more 
    measurements

¶ the other big problem: {{< ℑ critical slowing down >}}. the correlation time τ 
  also diverges at ˝T₍c₎˝. in finite systems it doesn't _really_ diverge, but 
  still, it can become very large. 
  - if τ is very large, it means the measurements won't be independent, and 
    they'll be less accurate (incraese the errors)

¶ the fact that critical fluctuations increase the errors of measurements is an 
  innate feature of the ising model. 

¶ BUT the increase in correlation time (critical slowing down) is only a feature 
  of the MC algorithm, not of the ising model itself.
