---
title: monte carlo renormalisation group
type: long
---

# INFO
--------------------------------------------------------------------------------

¶ mcrg is more accurate than finite size scaling for a given computational 
  expense
  - you only have to do simulations at one system size (possibly two)

¶ downside: has some bad approximations that make it hard to accurately estimate 
  the errors in the critical exponents

# REAL SPACE RENORMALISATION
--------------------------------------------------------------------------------

¶ __blocking__: where the spins (or other dofs) on a lattice are grouped into 
  "blocks" to form a coarser-grained lattice
  - in the blocked system, represent each block by a bigger block spin
  - the value of the block spin is assigned by majority
  - if there are an even number of spins, and you get 2 up 2 dn, choose the 
    value at random
    - ?? uh, can you do that? won't that mess up the nn interactions?

¶ __rescaling factor__: the factor by which you're reducing the size of the 
  system (should apply in all directions). denote ˝l˝
  - the number of spins in the system decreases by ˝l⁽d⁾˝
  - choosing ˝l = 2˝ is common for square or cubic lattice
  - for a triangular lattice it's common to group the spins in threes, with a 
    rescaling factor ˝l = √3˝

¶ ways of blocking:
  - majority rule (as described above)
  - decimation: discard all but one spin from each block, set the new block spin 
    to that value

¶ RG assumption: that the blocked (coarse-grained) configuration is just a 
  typical configuration of another model on a lattice of size ˝L' = L/l˝

  {{< fig rgblocking.png 7 >}}

  - ie you're assuming the blocked (CG'd) states you generate will appear with 
    correct boltzmann probabilities if you calculate their energies using the 
    same hamiltonian for the original system
  - this assumption doesn't actually hold up (see goldenfeld)

¶ the CG'd states don't normally have the same boltzmann probabilities (for a 
  given T) as the original states
  - consider the correlation length ˝ξ˝ (the typical size of a cluster of spins 
    that are correlated, ie have the same value)
  - when you CG the system, the avg ˝ξ˝ between two arbitrary spins far away 
    must be roughly the same (large scale aspects of the system DON'T change 
    when you CG it, ie the _actual_ correlation length is the same no matter 
    what)
  - so, when you CG the system the correlation length should stay the same.
  - trouble is, when you CG the system you reduce the number of spins by a 
    factor of ˝l˝ in each direction
  - ∴ correlation length of the CG'd system, in units of lattice spacing, is
    ```
      ξ₍l₎ = ξ / l
    ```
    - the orig correlation length ˝ξ˝ is only defined ito the original lattice 
      spacing
    - when you CG the system, you change the lattice spacing between the block 
      spins from ˝a → la˝. since the corrlen must be constant, follows that you 
      have to normalise the corrlen for the CG'd system by a factor of ˝l˝
  - major implications of this:
    - you know that corrlen varies with temperature, ˝ξ = ξ(T)˝
    - what this means is, the CG'd corrlen ˝ξ₍l₎˝ must represent a model at a 
      _different_ temperature ˝T₍l₎˝ than the original
  - >> ∴ the CG'd states represent a model at a different temperature ˝T₍l₎˝, 
    for which the corrlen is a factor ˝1/l˝ of the original system <<
    - why? ok, lookie here. one more bloody time. the original system has some 
      value of corrlen, ˝ξ˝ at a given temperature ˝T˝. obviously, it has a 
      _different_ value of ˝ξ˝ for a different temperature ˝T˝, because ˝ξ˝ 
      depends on ˝T˝. so if you reverse the logic. if you know the corrlen of a 
      system, you know it's specific to some temperature. ∴ two diff corrlens 
      represent the system at two diff temperatures. not exactly rocket science, 
      is it

¶ effect of CGing on measurable properties like energy per spin ˝u˝ or 
  magnetisation per spin ˝m˝
  - these params are also functions of temperature
  - so makes sense that you'll get two different values ˝m˝ and ˝m₍l₎˝ for the 
    orig and CG'd system, resp
  - the CG'd states appear with the correct boltzmann probabilities for a system 
    at temperature ˝T₍l₎˝
  - ∴ follows that magnetisation per spin for the CG'd system, ˝m₍l₎˝, is the 
    correct value for a system at temperature ˝T₍l₎˝

>> at the critical tempereature, ˝T₍c₎˝, the correlation length of the orig 
  system is the SAME as the corrlen of the CG'd system <<
  - bugger me, how did you miss this fact? do you realise how important this is?

¶ at ˝T₍c₎˝ the corrlen diverges to ∞. the rescaled corrlen _also_ diverges to 
  ∞.
  - at ˝T₍c₎˝ the orig and CG'd systems the same corrlen, and thus the same 
    temperature ˝T = T₍l₎ = T₍c₎˝
  - also, at ˝T₍c₎˝, the orig and CG'd systems have the same values of all other 
    intensive observables, like ˝u˝ and ˝m˝

# SCHEME FOR CALCULATING CRITICAL TEMPERATURE
--------------------------------------------------------------------------------

1. do a MC sim at temperature ˝T˝ and calculate internal energy per spin ˝u˝

2. take the states generated by the simulation and CG them, using some blocking 
   rule (majority, decimation, whatev)

3. calculate the internal energy for the CG'd system, ˝u₍l₎˝, which is averaged 
   over the CG'd states

4. vary the temperature ˝T˝ until you find the point where ˝u₍l₎ = u˝
  - ooh, how nifty
  - sidenote: an efficient way to vary the temperature is the single/multiple 
    histogram method, faster than doing a separate sim for every T. 
    - we'll see about that

¶ note: to extrapolate the value of ˝u₍l₎˝, treat it as an observable quantity 
  in the original system, and reweight with the boltzmann factors for the 
  original system
  - wtf?

  {{< fig rgcalcu.png 6 >}}

  - newman: do sim of 32x32 system at single temperature, calculate values of 
    ˝u˝ and ˝u₍l₎˝ over a range of temperatures using the single histogram 
    method
  - the intersection gives an estimate of ˝T₍c₎˝
  - obviously, you don't know ˝T₍c₎˝ to begin with, so what you actually do is a 
    series of sims, each at a guessed value of ˝T₍c₎˝ estimated from the 
    previous sim, so you'll converge on the right answer. might have to do 3 or 
    4.

# PROBLEMS WITH RG BLOCKING
--------------------------------------------------------------------------------

¶ no method for estimating error

¶ big source of error: finite size effects
  - the sim is just, more accurate, when the lattice is bigger
  - can be problematic pk the orig and rescaled systems have different sizes
  - you can get a more accurate estimate if you compare the internal energies of 
    two systems of the same size
  - ie do an extra sim of a smaller system at the same size as the original 
    system, compare its internal energy with that of the 16x16 rescaled system
  - eg, for magnetisation:

  {{< fig mcrg-blocking-m.png 8 >}}

# RENORMALISATION GROUP TRANSFORMATION
--------------------------------------------------------------------------------

¶ consider;
  - 32x32 ising model
  - can calculate ˝u˝ as a fn of ˝T˝ using the single histogram method
  - can calculate ˝u₍l₎˝ (for the CG'd system which has ˝1/l⁽2⁾˝ as many spins)
  - the CG'd states are a reasonable approximation of the states for a 16x16 
    ising system at temperature ˝T₍l₎˝

¶ can calculate ˝T₍l₎˝ by looking at the two ˝u˝ curves:

  {{< fig rgcalcu.png 6 >}}

  - for a given ˝T˝, the temp ˝T₍l₎˝ is the temperature at which
    ```
      u₍l₎(T) = u(T₍l₎)
    ```
  - ∴ ˝T₍l₎˝ in terms of ˝T˝ is
    ```
      T₍l₎ = u⁽-1⁾(u₍l₎(T))
    ```
  - mapping ˝T˝ onto ˝T₍l₎˝ is an eg of a RG transformation

> the RG transformation for ˝T˝:
  ```
    T₍l₎ = u⁽-1⁾(u₍l₎(T))
  ```
  - it's just a mapping from ˝T˝ to ˝T₍l₎˝
  - this is just a simple case of one parameter.
  - this mapping is what allows you to calculate the critical exponents
  - graph of ˝T₍l₎˝ against ˝T˝ from 16x16 CG'd ising model, and a direct 
    simulation of a 16x16 model

  {{< fig graphCGTonT.png 6 >}}

  - dotted line is ˝T₍l₎˝, solid is ˝T˝
  - ˝T₍c₎˝ is a fixed point of the transformation, the **critical fixed point**.
  - above ˝T₍c₎˝, ˝T₍l₎ > T˝, and below ˝T₍c₎˝, ˝T₍l₎ < T˝
  - >> ie the RGT pushes the temperature in the direction AWAY from the fixed 
    point <<
  - the RGT gives a "flow" through parameter space of the model. here we're 
    looking at a 1d parameter space, the temperature 
  - temperature flows AWAY from the critical fixed point under the RGT
    - ie the CG'd system is further from criticality than the orig
  - why?
    - above ˝T₍c₎˝, the spins are in clusters of typical size ˝ξ˝, and ˝ξ˝ is 
      finite.
    - when you CG the system, the clusters of the block spins become a factor of 
      ˝l˝ smaller, so the configuration has a smaller ˝ξ˝. 
    - smaller values of ˝ξ˝ correspond to higher temperatures
    - below ˝T₍c₎˝, there is spontaneous magnetisation so most spins point in 
      the same direction, so there are many blocks with spins pointing in one 
      direction, so the majority rule means the blocked spins also point in this 
      direction, i.e. in the CG'd system the small number of opposing spins get 
      washed out (obviously - the small guy loses), and the majority prevails. 
      so there's a larger majority of spins pointing in one direction, ie higher 
      ˝ξ˝, corresponding to lower temperatures.
    - thus: the RGT pushes temperature away from the fixed point

# CALCULATING ν
--------------------------------------------------------------------------------

¶ ν is defined as
  ```
    ξ ∼ |t|⁽-ν⁾
  ```
  - >> this relation is only valid close to ˝T₍c₎˝ <<

¶ in the CG'd system, the rescaled corrlen ˝ξ₍l₎˝ is given by
  ```
    ξ₍l₎ ∼ |t₍l₎|⁽-ν⁾
  ```
  - ditto, this relation is only valid close to ˝T₍c₎˝

  - divide these two equations, use tft ˝ξ₍l₎ = ξ / l˝, you get
    ```
      (t / t₍l₎)⁽-ν⁾ = l    𝓮𝓺{1}
    ```
  - if you know the functional form of the transformation from ˝T˝ to ˝T₍l₎˝, 
    then you can just use this equation to calculate ˝ν˝

¶ __linearising the RGT__: you do this pq the scaling laws are only valid near 
  ˝T₍c₎˝, so you need to know the relationship between the CG'd parameters and 
  the original parameters (in this case there's only 1 parameter, ˝T˝ and its 
  CG'd version ˝T₍l₎˝)

¶ since the power scaling laws for ˝ξ˝ and ˝ξ₍l₎˝ are only valid near ˝T₍c₎˝, 
  you need to know the relationship between ˝T₍l₎˝ and ˝T˝ close to ˝T₍c₎˝. the 
  way to do this: **linearise** the RGT about the critical fixed point by doing 
  a taylor expansion to leading order:
  ```
    T₍l₎ - T₍c₎ = (T - T₍c₎) Ɗ{T}{T'}|₍T₍c₎₎
  ```
  - use tft ˝t = (T - T₍c₎) / T₍c₎˝, substitute this into 𝓮𝓻{1}, get
    ```
      ν = ÷{ ln{l} }{ ln{ Ɗ{T}{T'}|₍T₍c₎₎ } }
    ```
  - if you do a numerical derivative on the transformation in newman fig 8.12, 
    you get result ˝ν = 1.02˝ for the 2d ising model. if you use the 
    magnetisation data, you get ˝ν = 1.03˝. the correct value is ˝ν = 1˝

¶ since the gradient of the transformation function (newman f8.12) doesn't 
  change much near the critical point, means that even if you don't know the 
  position of the fixed point accurately, it doesn't much affect the estimate of 
  the critical exponent

¶ problems: can't estimate error on value of ν, pq there are no systematic 
  errors we haven't taken account of
  - can improve by including longer range interactions into the hamiltonian

# CALCULATING β, α, γ
--------------------------------------------------------------------------------

{{< fig calculatingabg.jpg 7 >}}

# CALCULATING δ, θ
--------------------------------------------------------------------------------

¶ scaling law:
  ```
    m ∼ J⁽1/δ⁾
  ```

¶ look at how correlation length ˝ξ˝ diverges as ˝J → 0˝ at ˝T₍c₎˝. define 
  exponent ˝θ˝ to describe this:
  ```
    ξ ∼ |J|⁽-θ⁾
  ```

¶ read: {{< 𝖗 newman p251 >}}
