---
title: renormalisation group
type: long
---

# SUMMARY OF PRE-RG SHIZ
--------------------------------------------------------------------------------

¶ MFT doesn't accurately predict critical exponents.

¶ trying to improve MFT by perturbation theory fails because there are 
  divergences (in the perturbation expansion parameter) at t → 0

¶ dimensional analysis implies that there are anomalous dimensions that enter 
  the problem
  - these ADs can only be accounted for by accounting for the existence of a 
    microscopic length scale
    - eg lattice spacing
  - this micro length scale is important even when correlation length is very 
    large

¶ now: we'll show how the scaling hypothesis follows from the presence of a 
  diverging correlation length.
  - kadanoff: suggested that a diverging correlation length implies there's a 
    relationship between the coupling constants of an effective hamiltonian AND 
    the length scale over which the order parameter is defined
  - kadanoff's idea was on the right track but ultimately too simplistic. and it 
    doesn't allow you to calculate critical exponents.
  - wilson completed kadanoff's argument. he showed how the relationship 
    between coupling constants at different length scales can actually be 
    computed. wilson's theory is the RG.

# HIGH LEVEL OVERVIEW OF RG
--------------------------------------------------------------------------------

¶ RG has 2 main steps:
  1. realise the coarse graining transformation
  2. identify the origin of singular behaviour

¶ what these two steps mean:
  - after you do a block spin transformation, the block spins are separated by 
    distance la
  - you then rescale lengths, so that in the new units, the block spins are 
    again separated by the original distance a
  - > the upshot of this is that the new system looks like the original system 
    (in terms of dofs), but it has a different hamiltonian. crucially, this new 
    system is further from criticality than the original.
  - repeat this sequence of steps. you end up with a sequence of hamiltonians.  
    each describes a system further and further from criticality 
  - although the block spin transformation is analytic, nonanalyticities can 
    arise after an infinite number of repetitions

¶ consider a system with hamiltonian
  ```
    Ħ = -βH_Ω = ∑{n} K⌞n⌟ Θ⌞n⌟([S…])
  ```
  - K⌞n⌟ are the coupling constants
  - Θ⌞n⌟([S…]) are the local operators, functionals of the dofs [S…]

# THE RG TRANSFORMATION £ R_l £
--------------------------------------------------------------------------------

¶ we're considering how Ħ changes under a transformation which coarse grains the 
  short wavelength dofs (ie leaves an effective Ħ for the long wavelength dofs)
  - simplest way to do this: just group together dofs in a block, which has 
    linear dimension la
  - irl there are more complex ways of doing this

¶ let R_l be transformation that coarse grains the short wavelength dofs
  - this transformation redefines the coupling constants under a change of scale 
    (the change of scale is defined by l), and rescales the dofs
  - R_l is called the renormalisation group transformation

¶ suppose under R_l the coupling constants [K...] become
  ```
    [K'...] = R_l·[K...]   l > 1
  ```
  - this is a recursion relation
  - I guess this is because you can keep applying R_l to each new set of 
    coupling constants. snazzy

¶ the transformations R_l for different l > 1 form a semi group. two 
  successive transformations with l = l_1 and l = l_2 should be equivalent to a 
  combined scale change of l_1·l_2
  ```
    [K'...] = R⌞l₁⌟·[K...]
    [K''...] = R⌞l₂⌟·[K'...]
    ∴
    R⌞l₁l₂⌟·[K…] = R⌞l₂⌟·R⌞l₁⌟·[K…]
  ```

# HOW TO CALCULATE £ R_l £
--------------------------------------------------------------------------------

¶ there is no unique RG transformation. there are many ways to define it.
  - the main thing is, you're coarse graining the dofs.

¶ write down the partition function:
  ```
    Z_N([K…]) = Tr e^Ħ
  ```

¶ define a qty called g
  ```
    g([K…]) = ÷1N log Z_N([K…])
  ```
  - related to the free energy per dof ("specific" free energy)

¶ a RG transformation reduces the #dofs by a factor l^d:
  - this leaves £ N' = N/l^d £ dofs 

¶ these N' dofs are described by block variables
  ```
    [S⌞I⌟'…]   for: I = 1…N'
  ```
  - and an effective hamiltonian £ Ħ⌞N'⌟' £

¶ to do this transformation, make a partial trace over the original dofs 
  [S⌞i⌟...], keeping the block dofs [S⌞I⌟']...] fixed:
  - partial trace over what? 
  - the partition function?
  - note the projection operator

  {{< fig blockpartialtrace.jpg 8 >}}

  {{< fig projectionoperatorising.png 8 >}}

¶ 3 requirements of the projection operator:

  1. £ P(S⌞i⌟, S⌞I⌟') ≥ 0 £

  2. £ P(S⌞i⌟, S⌞I⌟') £ reflects the symmetries of the system

  3. £ ∑{S⌞I⌟'} P(S⌞i⌟, S⌞I⌟') = 1 £

¶ condition 1 guarantees that £ exp(Ħ'⌞N'⌟([K'],[S⌞I⌟'])) ≥ 0 £, so we can say £  
 Ħ⌞N'⌟ £ is the effective hamiltonian for the dofs £ S⌞I⌟' £

¶ condition 2 implies £ H'⌞N'⌟ £ has the same symmetries as the original 
  hamiltonian £ H⌞N⌟ £
  - if £ H⌞N⌟ £ has form
  ```
    Ħ⌞N⌟ = N K_0 + h ∑{i} S⌞i⌟ + K⌞1⌟ ∑{ij} S⌞i⌟ S⌞j⌟ + ...
  ```
  - then £ Ħ'⌞N'⌟ £ has form
  ```
    Ħ'⌞N'⌟ = N' K_0' + h' ∑{I} S⌞I⌟' + K⌞1⌟' ∑{IJ} S⌞I⌟' S⌞J⌟' +...
  ```

¶ condition 3 implies the partition function is invariant under a RG 
  transformation
  ```
    Z⌞N'⌟([K']) = Z⌞N⌟([K])
  ```
  - {{< 𝖗 gf p239 >}}

¶ the "free energy" g:
  ```
    g[K] = l^{-d} g[K']
  ```
  - {{< 𝖗 gf p239 >}}

# ORIGINS OF SINGULAR BEHAVIOUR
--------------------------------------------------------------------------------

¶ you need to do an ∞ iterations of the RG transformations to eliminate ALL the 
  dofs of a TD system in the TD limit N → ∞.
  - "in this way, singular behaviour can occur"
  - >> wtf?
    {.q}

¶ GF p240 - gives an example of how an analytic function can mysteriously still 
  give rise to singular behaviour
  - particle in a 1d potential under damping
  - the potential function is smooth, analytic
  - the position as a function of time and initial position x(t,x₀) is a 
    continuous function for finite t, but discontinuous for t = ∞
  - the singular behaviour is not due to pathologies of V(x). 
  - the origin of singular behaviour is the amplification of the initial 
    condition due to the infinite time limit.
  - there are 3 fixed points for this system
  - if a particle is at a fixed point at time t', then it remains there for t > 
    t'
  - there are 2 kinds of fixed points: repulsive and attractive
    - if the particle starts at or near the fixed pt C, it will always end up at 
      A or B but never at C
    - if it starts at either A or B, it stays there

  {{< fig gf-fig91.png 8 >}}

¶ the set of ICs [x₀...] that flow to a given fixed point is the basin of 
  attraction of that fixed point
  - the basin of attraction for B is x > x_C

¶ this example suggests that singular behaviour can arise after an ∞ number of 
  RG transformations

¶ RG group flow:
  - after n RG iterations, the length scale is l^n, and the system is described 
    by coupling constants K⌞n,0⌟, K⌞n,1⌟...
  - as n varies the system is like a point moving in a space where the axes are 
    the coupling constants K⌞0⌟, K⌞1⌟, for various values of n
  - as you change n, the point traces out a path in coupling constant space
  - the set of all trajectories, generated by different initial sets of coupling 
    constants is the renormalisation group flow in coupling constant space.

¶ in practice we find that the trajectory tends to become attracted to fixed 
  points

# FIXED POINTS
--------------------------------------------------------------------------------

¶ suppose you know the RG transformation R_l·[K...]
  - the fixed point of the RG transformation is a point [K*] in coupling 
    constant space satisfying
  ```
    [K*] = R_l·[K*]
  ```
  - ie a point where the original ccs and the transformed ccs have the same 
    values

¶ under the RG transform R_l, length scales are reduced by a factor of l
  - for any particular values of coupling constants, you can compute the corrlen 
    ξ, which transforms under R_l via
  ```
    ξ([K']) = ξ([K]) / l
  ```
  - ie the system moves further from criticality after a RG transform

¶ at a fixed point, 
  ```
    ξ([K*]) = ξ([K*]) / l
  ```
  - but this is only possible if ξ([K*]) is 0 or ∞

{{< 𝖉𝖊𝖋 CRITICAL FIXED POINT >}}
  a fixed point with ξ = ∞
{{< /𝖉𝖊𝖋 >}}

{{< 𝖉𝖊𝖋 TRIVIAL FIXED POINT >}}
  a fixed point with ξ = 0
{{< /𝖉𝖊𝖋 >}}

¶ in general a RG transform can have many fixed points

¶ each fixed point has its own basin of attraction
  - all points in cc space that are in the basin of attraction of a given fixed 
    point flow towards the fixed point. they reach it after ∞ iterations

> theorem: all points in the basin of attraction of a critical fixed point have 
  infinite correlation length
  - {{< 𝖗 gf p242 >}}

{{< 𝖉𝖊𝖋 CRITICAL MANIFOLD >}}
  the set of points that make up the basin of attraction of a critical fixed point
{{< /𝖉𝖊𝖋 >}}

¶ all points on the critical manifold flow towards the same fixed point. this is 
  the basic mechanism for universality

¶ tldr: the critical fixed points desribe the singular critical behaviour; the 
  trivial fixed points describe the bulk phases of the system

¶ if you know the location and type of fixed points of an RG transformation, you 
  can construct the phase diagram. 
  - the behaviour of the RG flows near a critical fixed point determine the 
    critical exponents

# BEHAVIOUR OF RG FLOWS NEAR A FIXED POINT
--------------------------------------------------------------------------------

{{< fig rgflowsnearfps.jpg 10 >}}

> _conclusion:_ local behaviour of the RG flows near critical fixed points is what 
  determines the critical behaviour.

# GLOBAL PROPERTIES OF RG FLOWS
--------------------------------------------------------------------------------

¶ global behaviour of the RG flows is what determines the phase diagram of the 
  system.
  - start from any point in CC space (ie inside the phase diagram)
  - iterate the RG transformation and find the FPs the system flows towards
  - the system state described by the FP represents the phase at the original 
    point in the phase diagram

# CLASSIFICATION OF FIXED POINTS
--------------------------------------------------------------------------------

{{< table >}}
  | codimension | ξ | type of FP                | physical domain         |
  |-------------|---|---------------------------|-------------------------|
  | 0           | 0 | sink                      | bulk phase              |
  | 1           | 0 | discontinuity FP          | plane of coexistence    |
  | 1           | 0 | continuity FP             | bulk phase              |
  | 2           | 0 | triple point              | triple point            |
  | 2           | ∞ | critical FP               | critical manifold       |
  | >2          | ∞ | multicritical point       | multicritical point     |
  | >2          | 0 | multiple coexistence FP   | multiple coexistence    |
{{< /table >}}

¶ FPs with codimension 0:
- have no relevant directions, ∴ trajectories only flow into them, hence they're 
  sinks
- sinks correspond to stable bulk phases. the nature of the CC at the sink 
  characterises the phase
- eg a 3d ising magnet with nn ferromagnetic coupling in an ext field H has 
  sinks at H = ±∞, T = 0. corresponds to tft in a +ve/-ve field, there's a net 
  +ve/-ve magnetisation for all temperatures. if you start at any point in the 
  phase diagram (H,T), successive RG iterations will drive the system to the 
  sink corresponding to the appropriate sign of H.

¶ FPs with codimension 1:
  - two kinds:
    1. discontinuity FPs
      - points on a phase boundary
      - describes a 1st order phase transition where an order parameter has 
        discontinuous behaviour
      - eg the line H = 0, T < T_c in a ferromagnet. all pts on that line flow to 
        a discontinuity FP at zero T and field.
    2. continuity FPs
      - represents a phase of the system
      - nothing interesting happens in its vicinity
      - eg the paramagnetic FP at H = 0, T = ∞. it attracts points on the line H = 
        0, T > T_c. 
  - both these kinds of FPs are unstable towards the sinks
  - an infinitesimal ext field will cause the RG flows to approach the sinks, 
    not the codimension 1 FPs

¶ FPs with codimension ≥ 2:
  - depending on the value of ξ, these are either
    - points of multiple phase coexistence
    - multicritical points
  - for codim = 2: 
    - either a triple point (ξ = 0)
    - or a critical point (ξ = ∞)
  - two relevant directions represent two variables that must be tuned to place 
    the system at the appropriate point.
  - eg afinque hold a magnetic system at the critical point, you have to adjust 
    the ext field to be zero and the temp to be the critical temperature

# WHAT HAPPENS CLOSE TO CRITICALITY
--------------------------------------------------------------------------------

{{< fig gf-fig92.png 10 >}}
  - {{< 𝖗 gf fig 9.2 >}}

¶ trajectories of systems on the critical manifold stay on the manifold and flow 
  to the FP

¶ trajectories of systems that start slightly off the critical manifold 
  initially flow towards the critical FP, but ultimately they're repelled from 
  the critical manifold
  - pq the critical FP has 2 unstable directions (2 relevant directions)

¶ note it's the same eigenvalues that drive _all_ slightly off-critical systems 
  away from the FP
  - this is the origin of universality

¶ this means that the initial values of the CCs don't determine critical 
  behaviour. it's the flow behaviour near the FP that controls the critical 
  behaviour.

eg

{{< fig ising-flow-nn-nnn.jpg 10 >}}
  - {{< 𝖗 gf p248 >}}
