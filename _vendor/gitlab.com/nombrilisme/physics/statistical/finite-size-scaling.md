---
title: finite size scaling
type: long
---

# IMPORTANCE OF CRITICAL PHENOMENA
--------------------------------------------------------------------------------

¶ critical phenomena: phenomena when a system is close to a continuous phase 
  transition.

¶ phase transitions are interesting because of universality: when the values of 
  critical exponents for a system are independent of most parameters of the 
  model (except ˝d˝, the dimensionality of the system, and the dimensionality of 
  the order parameter)
  - the values of the exponents fall into universality classes
  - systems of the same universality class will have the same critical exponents

¶ finite size scaling is a method for getting the values of the critical 
  exponents

# ISING PHASE TRANSITION
--------------------------------------------------------------------------------

¶ what happens as you pass through the critical point:
  - at high T the spins are random, uncorrelated
  - as you lower T, the interactions between the spins encourage neighbours to 
    point in the same direction, and there are correlations in the system
    - >> correlations in the fluctuations, right? <<
  - groups of adjacent spins correlated are clusters
  - the size of these clusters is the correlation length ξ
  - as you approach ˝T₍c₎˝ the size of the clusters diverges
  - when you're exactly at ˝T₍c₎˝ you could find _arbitrarily_ large regions where 
    the spins point mostly up or down
  - then, as you pass below ˝T₍c₎˝ the system _spontaneously_ decides to have 
    most of its spins in the up or down direction
  - so the system acquires a nonzero magnetisation in that direction
  - the direction it chooses comes down to the random thermal fluctuations it 
    was going through as it passed the critical temperature

¶ {{< ℑ critical region >}}: the region close to ˝T₍c₎˝. processes that are 
  characteristic of this region are critical phenomena.

¶ as you approach ˝T₍c₎˝, the system forms large clusters of mostly up or down 
  spins. these clusters contribute substantially to the magnetisation and energy 
  of the system
  - as the _clusters_ flip from one orientation to another, they produce large 
    fluctuations in ˝m˝ and ˝E˝. these are {{< ℑ critical fluctuations >}}
  - as the size of the clusters, ˝ξ˝, diverges as ˝T → T₍c₎˝, the size of the 
    fluctuations also diverges
  - fluctuations in ˝m˝ and ˝E˝ are related to susceptibility and heat through
    ```
      c = ÷{β⁽2⁾}{N} (⟨E⁽2⁾⟩ - ⟨E⟩⁽2⁾)
      χ = βN(⟨m⁽2⁾⟩ - ⟨m⟩⁽2⁾)
    ```
  - ∴ c and χ diverge at ˝T₍c₎˝.

# DIRECTLY MEASURING CRITICAL EXPONENTS
--------------------------------------------------------------------------------

¶ some important quantities and critical exponents that describe singular 
  behaviour near ˝T₍c₎˝:
  - reduced temperature
    ```
      t = (T - T₍c₎) / T₍c₎
    ```
  - the behaviour of correlation length in the critical region is described by 
    the critical exponent ν:
    ```
      ξ ∼ |t|⁽-ν⁾   𝓮𝓺{@}
    ```
  - exponents γ and α describe singular behaviour in susceptibility and specific 
    heat:
    ```
      χ ∼ |t|⁽-γ⁾   𝓮𝓺{@#}
      c ∼ |t|⁽-α⁾
    ```
  - the ˝t < 0˝ exponent β describes the behaviour of the order parameter:
    ```
      m ∼ |t|⁽β⁾
    ```
  - the ˝t = 0˝ exponents δ and η describe the coupling constant and the 
    correlation (?)
    ```
      J ∝ m⁽δ⁾
      ⟨M(0)M(x)⟩ ∝ r⁽-d+2-η⁾
    ```

¶ it turns out that direct measurements of the critical exponents (ie running 
  the sys, calculating the qty of interest, and just fitting a curve to it), is 
  highly unreliable.
  - {{< 𝖗 newman-barkema p231 >}}

# FINITE SIZE SCALING METHOD
--------------------------------------------------------------------------------

¶ fss is a way of getting values for critical exponents by observing how 
  quantities vary as the _size_ of the system ˝L˝ changes
  - here: going to show the method for the susceptibility exponent γ. will use a 
    method that actually gives a value of ˝T₍c₎˝, in addition to the critical 
    exponents

¶ the susceptibility is described by the power law
  ```
    χ ∼ |t|⁽-γ⁾
  ```

¶ take equations 𝓮𝓻{@} and 𝓮𝓻{@#} and eliminate ˝|t|˝ from them, you get
  ```
    χ ∼ ξ⁽γ/ν⁾
  ```
  in the vicinity of a phase transition.

¶ in systems of finite size ˝L˝, the correlation length gets cut off as it 
  approaches the system size, i.e. as ˝ξ ∼ L˝

  {{< fig correlationlengthcutoff.png 6 >}}

  - {{< 𝖗 newman-barkema fig4.1 >}}
  - the solid line shows the way ˝ξ˝ diverges in the TD limit (infinitely large 
    system)
  - the dashed line shows how ˝ξ˝ behaves in a finite system of size ˝L˝ around 
    ˝ξ ∼ L˝

¶ this means the suscepbility χ will also be cut off, i.e. in finite systems the 
  susceptibility never _actually_ diverges

¶ as long as ˝ξ « L˝, the value of ˝χ˝ should be the same as for the infinite 
  system, i.e.
  ```
    χ = ξ⁽γ/ν⁾·χ₍0₎(L/ξ)  𝓮𝓺{@@}
  ```
  - ˝χ₍0₎˝ is a dimless function of a single variable with properties
    ```
      χ₍0₎ = constant     for: x » 1
      χ₍0₎ ∼ x⁽γ/ν⁾       x → 0
    ```
  - the form of 𝓮𝓻{@@} still contains ξ (the correlation length at temperature 
    ˝t˝ in the infinite system, which is unknown). need to rewrite it.
  - define a new dimless function ˝~χ˝:
    ```
      ~χ(x) = x⁽-γ⁾·χ₍0₎(x⁽ν⁾)
    ```
  - use the fact that ˝ξ ∼ |t|⁽-ν⁾˝ to write
    ```
      χ = L⁽γ/ν⁾ ~χ(L⁽1/ν⁾t)    𝓮𝓺{fsseq}
    ```
  - this is the equation for finite size behaviour of magnetic susceptibility. 
    it says how susceptibility should vary with system size ˝L˝ for finite 
    systems close to ˝T₍c₎˝.
    - see working in metropolis concepts leaf
    - note also this is for susceptibility per spin. for extensive 
      susceptibility, the leading power of L would be ˝L⁽γ/ν + d⁾˝

¶ ˝~χ(s)˝ is the **scaling function**. we don't know what it is exactly, but it 
  has properties we know.
  - from tft ˝χ₍0₎ ∼ x⁽γ/ν⁾˝, 
    ```
      ~χ(x) → x⁽-γ⁾(x⁽ν⁾)⁽γ/ν⁾ = constant     x → 0
    ```
  - ie ˝~χ˝ is finite at the origin (ie close to the critical T)
  - all the L-dependence of χ is displayed in the scaling function equation 
    (there's no hidden L dependence not accounted for)
  - means if you measure ˝~χ(s)˝ you should get the same result regardless of 
    system size
  - this property is what lets you use 𝓮𝓻{fsseq} to calculate the exponents γ and 
    ν and ˝T₍c₎˝
    
# USING FSS TO CALCULATE EXPONENTS
--------------------------------------------------------------------------------

¶ perform a set of MC sims for a bunch of different system sizes ˝L˝ and over a 
  range of temperatures _near_ the critical temperature (doesn't have to be 
  exact. but you usually know roughly where the crit T is anyway)
  - for each ˝L˝, measure the susceptibility ˝χ₍L₎(t)˝ for a range of 
    temperatures ˝t˝
  - rearrange the scaling function 𝓮𝓻{fsseq} as:
    ```
      ~χ(L⁽1/ν⁾t) = L⁽-γ/ν⁾χ₍L₎(t)    𝓮𝓺{fsseq2}
    ```
  - this gets you an estimate of the scaling function ˝~χ˝ for several values of 
    the **scaling variable**
    ```
      x = L⁽1/ν⁾t
    ```
    for each system size
  - the scaling function should be the same for all system sizes, so these 
    estimates should coincide, ie they should fall on the same curve if you plot 
    them together on a graph
  - obv, the scaling fns for different ˝L˝'s will only coincide _if_ you use the 
    correct values of γ and ν in 𝓮𝓻{fsseq2}, and the correct value of ˝T₍c₎˝

¶ so basically what you do is, you calculate ˝~χ(x)˝ for each different system 
  size, the vary ˝γ˝ and ˝ν˝ and ˝T₍c₎˝ until the curves all fall on top of each 
  other ("collapse")

  {{< fig fssdatacollapse.png 6 >}}

  - values agree with exact known values ˝γ = 7/4, ν = 1, T₍c₎ = 2.269J˝
  - can easily extend this method to other quantities other than susceptibility

¶ for the ising model, the scaling equations for specific heat and 
  magnetisation:
  ```
    c = L⁽α/ν⁾ ~c(L⁽1/ν⁾t)
    m = L⁽-β/ν⁾ ~m(L⁽1/ν⁾t)
  ```
  - doing the data collapse using these equations gives values for ˝α, β, T₍c₎˝
