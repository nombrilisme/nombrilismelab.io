---
title: "landau theory"
type: long
---

# RECAP OF WEISS MFT
--------------------------------------------------------------------------------

{{< 𝖉𝖊𝖋 WEISS MFT >}}
  that bullshit
  ```
    fab
  ```
  etc
{{< /𝖉𝖊𝖋 >}}

¶ nearest neighbour ising model hamiltonian:
  ```
    Ħ = -∑{⟨ij⟩} J¸ij·S¸i·S¸j - ∑{i} H¸i S¸i
  ```

¶ in weiss MFT you approximate the ith spin as a sum of an average and a 
  fluctuation term:
  ```
    S¸i = ⟨S⟩ + δS¸i
  ```
  where δS¸i is the fluctuation of the ith spin:
  ```
    ⟨δS¸i⟩ = 0
  ```

¶ but then you ignore the interaction of the fluctuations ¶
  - when you substitute eq(*) into the hamiltonian, you get a term proportional 
    to δS¸i δS¸j
  - note δS¸i δS¸j represents the covariance of the two spins
  - rearrange eq(*), you can see that
  ```
    δS¸i δS¸j = (S¸i - ⟨S⟩)(S¸j - ⟨S⟩)
  ```
  - which you know from stats class is a form that represents [covariance][1] 
    between two random variables
  - in weiss MFT you ignore this term
  - but these correlated fluctuations become very important near a critical 
    point

[1]: http://stats103.com/joint-variability/

# LANDAU THEORY BASIC GIST
--------------------------------------------------------------------------------

¶ landau's postulate: you can construct a function(al) L, called the landau free 
  energy, where the state of the system is given by minimising L wrt η 
  - L is a functional
  - depends on the coupling constants K[…] and the order parameter η

¶ L has dimensions of energy:
  - it'ss similar to the gibbs free energy
  - differentiating L gives the thermodynamic state functions

--------------------------------------------------------------------------------

# NECESSARY PROPERTIES OF THE LANDAU FREE ENERGY

1. L has to be consistent with the symmetries of the system

2. near T_c you can expand L in a power series in η, ie L is an analytic 
   function of η and K[…]
   - let 𝓛 be the landau free energy density, 𝓛 = L / V
   - then
   ```
     𝓛 = ∑{n=0…∞} a[n] ηⁿ
   ```

3. in an inhomogeneous system (as opposed to a uniform system) with a spatially 
   varying order parameter η(↗r), 𝓛 is a local function. 
   - it depends only on η(↗r) and a finite number of derivatives

4. in the disordered phase, η = 0. in the ordered phase η is nonzero. near the 
   transition η is small and nonzero. 
   - for T > T_c, setting η = 0 solves the minimisation equation
   - for T < T_c, setting η ≠ 0 solves the equation
   - for a homogeneous system, 
 
     𝓛 = ∑{n=0…4} a[n] ηⁿ
 
   - ie we've expanded 𝓛 to the 4th order in η, bc we expect η is small, and 
     all the essential physics near T_c appears at this order
   - the validity of this truncation depends on the dimensionality of the system 
     and the codimension of the singular point


# CONSTRUCTING THE LANDAU FUNCTION (ISING)
--------------------------------------------------------------------------------

¶ use these properties to construct 𝓛:
  - write out the expansion
  ```
    𝓛 = a[0] + a[1]·η + a[2]·η² + a[3]·η⁴
  ```
  - the minimisation equation:
  ```
    𝝏{𝓛}{η} = a[1] + 2·a[2]·η + 3·a[3]·η² + 4·a[4]·η³ = 0
  ```
  - for T > T_c, η = 0, ∴ a[1] = 0
  - next consider all the constraints

¶ the symmetry constraint:
  - for the ferromagnet, η = M
  - in a finite system with H = 0, the probability distribution for η is even, 
    so Prob(η) = Prob(-η)

  ʡ this just means the probability of the magnetisation being in one 
     direction is the same as in the other direction, right ʡ

  - since Prob ∽ exp{-β·L}, then
  ```
    𝓛(η) = 𝓛(-η)
  ```
  - this means 𝓛 must be an even function, so a¸3, a¸5 ... are zero
  - this leaves
  ```
    𝓛 = a[0] + a[2]·η² + a[4]·η⁴
    eq(**)
  ```
  - next consider the form of the coefficients a[n]

¶ consider a[0]:
  - a¸0 is just the value of 𝓛 in the high T phase, bc when T > T_c the 
    order parameter is zero so the a¸2 and a¸3 terms are zero
  - a¸0 represents the dofs of the system that aren't described by the order 
    parameter. it's kind of like a "background" on which the singular behaviour 
    is superimposed
  - a¸0 varies smoothly through T_c

  - usually set a¸0 to a constant (zero)

¶ consider a¸4:
  - taylor expand it in T near T_c:

    a¸4 = (a¸4)⁰ + (a¸4)¹(T - T_c) + ...

  - looking at this, you can just approximate a¸4 as a positive constant. 
  - this is fine bc, even though it depends on T, its T dependence near T_c is 
    small and won't dominate the leading behaviour of the thermodynamics (near 
    T_c)

¶ consider a¸2:
  - taylor expand in T near T_c:

    a¸2 = (a¸2)⁰ + (a¸2)¹(T - T_c)/T_c + Ớ((T - T_c)²)

    ʡ why suddenly divide by T_c in the middle termʡ

  - determine the form of this from the 4th property, that η = 0 for T > T_c and 
    η ≠ 0 for T < T_c
  - basically, solve the minimisation equation 𝝏{𝓛}{η} = 0 for η, using the 4th 
    property
  - we already set a¸1 to zero
  - for T > T_c you just get η = 0. 
  - for T < T_c, you get the quadratic:

    4 a¸4 η² + 3 a¸3 η + 2 a¸2 = 0

  - so the two solutions are:

    η = 0

    η = √{-a¸2 / 2 a¸4}
    eq(@)

    (OB22B p5-6)

  - now look at the taylor expansion in the first step. if η is nonzero when T < 
    T_c than (a¸2)⁰ = 0

    ?? why? oh, I see. no, I don't. (OB22B p7)

  - anyway, 

    a¸2 = (a¸2)¹ ((T - T_c)/T_c) + Ớ(((T - T_c)/T_c)²) 

  - but the higher order terms don't contribute to leading behaviour near T_c, 
    so

    a¸2 = (a¸2)^1 ((T - T_c)/T_c)

¶ so wtf did we just learn:
  - that a¸0 = const = 0
  - a¸2 = (a¸2)¹ t
  - a¸4 = +ve const

¶ extend for cases H ≠ 0:
  - for the ising ferromagnet, η = M, and the energy term given by the external 
    field is -H ∑{i} S¸i = -HNM

    ?? verify this

  - the final expression is (using the conclusions from above about the 
    coefficients)

    L = V𝓛 = N𝓛   (for lattice systems)

    and 

    𝓛 = atη² + ½bη⁴ - Hη

  - a and b are phenomological parameters. 
  - in theory a there should also be (by symmetry) a term proportional to Hη³,
    but it's not a leading term near the critical point so just ignore

¶ so that just about sums it up for the ising universality class:
  - in general you construct the landau function by writing down all possible 
    scalar terms that are powers and products of the order parameter components, 
    consistent with the symmetries

  {{< fig goldenfeld-fig51.png 8 >}}

# CONTINUOUS PHASE TRANSITIONS
--------------------------------------------------------------------------------

¶ the continuous transition occurs for H = 0:
  ?? exclusively?
  - middle row of graphs in fig 51
  - when T > T_c, the minimum of 𝓛 is at η = 0
  - when T = T_c, the minimum of 𝓛 is also at η = 0, and the landau function 
    has zero curvature
  - when T < T_c there are two degenerate minima at η = ±η_s

  ?? the s subscript indicates symmetry? this is the symmetry that's lost in the 
     transition, right? all the spontaneous symmetry breaking nonsense. right.

  - η_s is a fn of T, η_s = η_s(T)

# CRITICAL EXPONENTS
--------------------------------------------------------------------------------

## first treat the case H = 0. 

¶ get β by varying the order parameter η with t:
  - using eq(@), 

    η_s(T) = (-at/b)¹𝄍²

    for t < 0
  - ∴ β = 1/2

¶ for t > 0, 𝓛 = 0:

¶ for t < 0, 𝓛 = - ½(a²t²/b):

¶ heat capacity:
  
    C_V = -T 𝝏²{𝓛}{T} 
        = 0             T > T_c
        = a² / b T_c    T < T_c

  - the heat capacity has a discontinuity, ∴ α = 0

¶ get the remaining critical exponents by letting H ≠ 0:
  - diff 𝓛 wrt H. gives the magnetic equation of state for small η:
  ```
    atη + bη³ = ½H
    eq(@#)
  ```
  - on the critical isotherm t = 0 and H ∝ η³
  - ∴ δ = 3
  - for susceptibility, diff eq(@#) wrt H
  ```
    χ_T(H) 
      = 𝝏{η}{H}|T 
      = 1 / 2(at + 3bη²)
  ```
  - where η(H) is a solution of eq(@#)
  - we want the response function at zero ext field. 
  - for t > 0, η = 0, and χ_T = (2at)⁻¹
  - for t < 0, η² = -at/b, and χ_T = (-4at)⁻¹
  - ∴ γ = γ' = 1

¶ all the critical exponents agree with the weiss method


# 1st ORDER PHASE TRANSITIONS
--------------------------------------------------------------------------------

¶ yeah so landau theory is also pretty useful for this


# INHOMOGENEOUS SYSTEMS
--------------------------------------------------------------------------------

¶ now deal with systems where order parameter can spatially vary, η = η(↗r):
  - spatially varying order parameters can arise pq of inhomogenous external 
    fields, H(↗r)

¶ treating spatially varying order parameters will also clarify what the landau 
  free energy actually is:
  - pffft! as if it wasn't clear already!
  - this is one of those things where the simplest case (H = 0) is actually a 
    reduced special case of a more general thing, but the general thing is 
    what's really important
  - ugh, pedagogy
