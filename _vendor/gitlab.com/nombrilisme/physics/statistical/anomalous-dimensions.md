---
title: anomalous dimensions
type: long
---

# DIMENSIONAL ANALYSIS OF LANDAU THEORY
--------------------------------------------------------------------------------

## OVERVIEW
-------------

¶ what we learned so far: MFT is improved when you include gaussian fluctuations
  - transition T is shifted
  - heat capacity shows divergence rather than discontinuity

¶ but still, this method is inapplicable near T_c. it predicts its own regime of 
inapplicability. ginzburg criterion, etc. 

¶ question that remains: can you continue to improve the theory (MFT?), ie is it 
  possible to possibly extend into the critical region by going beyond gaussian 
  fluctuations?
  - tldr: no. such attmpts are doomed.
  - the expansion has an expansion parameter that diverges as you approach the 
    critical region.
  - but even if the expansion were valid, it couldn't generate critical 
    exponents with different values from the ones you already have
  - >> not to mention these "already derived" critical exponents are actually 
    incorrect anyway
    {.c}

¶ what we're going to show: that for d > 4, when you perturbatively include the 
  fluctuation interactions, there is NO new singular behaviour as t → 0
  - ie there's no singularity/divergence/blowup as t → 0, ie near the critical 
    temperature
    - there should be one though, right?
  - but for d < 4, the perturbation theory is divergent. the perturbing 
    parameter blows up as T → T_c.
  - >> when you say there's no new singular behaviour. is this a good thing? is 
    this what we're trying to show, the emergence of singular behaviour as t → 
    0?
    {.q}
    - it is, innit
    - yeah

¶ goal: identify the single parameter for which you can do a systematic 
  expansion

## SETUP - functional integral and rescaling η
----------------------------------------------

¶ the partition function written as a functional integral
  ```
    Z = ∫ D η exp{-βL}                  (*$)
  ```
  where 
  ```
    L = ∫{ɗᵈ↗r} (½γ(∇η)² + atη² + ½bη⁴ - Hη)
  ```

¶ it's conventional to rescale the order parameter η so the coefficient of the term 
  (∇η)² is just ½
  - >> why?
    {.q}
    - I suppose you'll find out...?
  - define
  ```
    φ = (βγ)¹𝄍²η;   ½r₀ = at/γ;    ¼u₀ = ½b/βγ²
  ```
  - substitute these into L, for the case H = 0 you have
  ```
    H⌌eff⌏([φ]) = βL = ∫{ɗᵈ↗r} (½(∇φ)² + ½r₀φ² + ¼u₀φ⁴)          (*)
  ```
  {{< fig rescale-eta.png 5 >}}

¶ next:
  1. identify the dimensions of the quantities in (*) 
  2. rewrite (*) ito dimensionless variables φ and ū₀, which are φ and u₀ 
  rescaled by appropriate powers of r₀

## STEP 1 - IDENTIFY DIMENSIONS
--------------------------------

¶ H_eff is dimensionless, ie dims{H_eff} = 1 

¶ each term in (*) must have dimension 1.
  
    dims{ ∫{ɗᵈ↗r} (∇φ)² } = 1

    ∴ Lᵈ L⁻² dims{φ}² = 1

  - ∴ dims{φ} = L^{1 - d/2}, where L is length

    dims{ ∫{ɗᵈ↗r} r₀ φ² } = dims{ ∫{ɗᵈ↗r} u₀ φ⁴ } = 1

  - ∴ 
  ```
    dims(r⌞0⌟) = L⁻²
    dims(u⌞0⌟) = L⌃d-4⌝
  ```

fig step1a.png
fig step1b.png

## STEP 2 - REWRITE HEFF ITO DIMLESS VARS
------------------------------------------

¶ the results from step 1 show that you can use r₀ to define a length scale 
  independent of dimension 
  - it's the only one of the qties that doesn't depend on d. note that the 
    dimensions of φ and u⌞0⌟ include d in them
  - since r⌞0⌟ ∝ (T - T_c), then in the gaussian approximation
  ```
    r⌞0⌟ ∝ 1 / ξ(T)²
  ```
  - this must be because of some kind of relationship like (T - T_c) ∝ 1/ξ(T)
    - oh, of course, you dingus. 
    - you know that ξ diverges at T_c. that's just an expression stating that
  - using £ r⌞0⌟⌃-1/2⌝ £ as the length scale is equivalent to measuring length in 
    units of correlation length ξ(T) 

¶ since gaussian functional integrals are easy to do, write the partition 
  function as a gaussian functional integral with a modification, which you then 
  treat with perturbation theory.

¶ define the dimless variables
  ```
    φ = ÷{φ}{L^{1-d/2}}
    ↗x = ÷{↗r}{L}
    ū⌌0⌏ = ÷{u⌌0⌏}{L^{d-4}}
    L = r⌌0⌏^{-1/2}
  ```
  
¶ then the partition function eq(*$) becomes 
  ```
    Z(ū⌞0⌟) = ∫ Dφ exp{ -H⌞0⌟([φ]) - H⌞int⌟([φ]) } 
  ```

