---
title: "band structure engineering"
type: note
back: "../"
---

# ALLOYING SEMICONDUCTORS - AlGaAs
--------------------------------------------------------------------------------

¶ aluminium-gallium-arsenide is an example of an alloyed semiconductor. it's a 
  mixture of the semiconductors GaAs (gallium arsenide) and AlAs (aluminium 
  arsenide).

  invividually, at ↗k = 0: 
  - E₍gap₎[GaAs] = 1.4 eV
  - E₍gap₎[AlAs] = 2.7 eV

  in the alloy, a fraction x of the mixture is AlAs, and the rest is GaAs. 
  denote the alloyed structure
  ```
    Al₍x₎Ga₍1-x₎As
  ```

  the band gap of the AlGaAs is pretty well approximated via direct interpolation 
  of the band gaps of its constituents
  ```
    E₍gap₎[AlGaAs(x)] = 2.7x  eV + 1.4(1-x)  eV
  ```
  (valid for x < 0.4).


¶ __gap energy__ or __lasing energy__: the lowest energy transition that 
  recombines a hole with an electron.

¶ by alloying two (or more) semiconductors, you can get a structure with any 
  band gap you want. tuning the composition of the alloy allows you to tune the 
  gap energy.
  - eg when building a laser, tuning the gap energy tunes the optical frequency 
    of the laser.

¶ __the virtual crystal approximation__: if you have an alloy with arbitrary x, 
  the system will be pretty much a random mixture, ie it _won't be an exactly 
  periodic crystal structure_. BUT if all you're interested in are the long 
  wavelength electron waves (states near the bottom of the conduction band, or 
  top of the valence band), it turns out the randomness "averages out", and you 
  can approximate the system as a periodic crystal of As atoms and some average 
  Al₍x₎Ga₍1-x₎ "atom". [simon p197]

# NONHOMOGENEOUS BAND GAPS
--------------------------------------------------------------------------------

¶ this is a structure where the material, or the alloyed "state" of the 
  material, is a function of position. eg

  here's an example of a __quantum well__:

  {{< fig simonfig181.png 4 >}}

  - there's a layer of GaAs inserted between two layers of AlGaAs.
  - E₍gap₎[GaAs] < E₍gap₎[AlGaAs], ∴ the electrons in the conduction band and 
    the holes in the valence band can be trapped in the GaAs region

  band diagram of the quantum well structure as a fn of vertical position z:

  {{< fig simonfig182.png 4 >}}

  - the difference in band gap energy between the GaAs region and the AlGaAs 
    region are like a potential that an electron (or hole) would feel.
  - an electron in the conduction band can have lower energy in the quantum well 
    region (ie the GaAs region) than in the AlGaAs region. thus, a low energy 
    electron in the conduction band will get trapped in this region.
  - the particle in a box system applies: there'll be discrete eigenstates of 
    the electron's motion in the z direction.
  - ditto for holes in the valence band.

¶ __semiconductor heterostructure__: a structure made of many semiconductors.

# MODULATION DOPING
--------------------------------------------------------------------------------

¶ to add electrons or holes to a quantum well, you have to include dopants to 
  the heterostructure. n-dopants for electrons, p-dopants for holes. 

  what you do is put the dopant atoms outside the quantum well. the potential 
  energy is lower in the well, so electrons released by n-donors will fall into 
  the well and will be trapped there.
  - in the above example, you put dopants in the AlGaAs region, not the GaAs 
    region.

  this is called __modulation doping__. the purpose of it is to allow the 
  carriers (charge carriers?) to move freely within the well region without 
  having to bump into dopant ions.

  carriers can have very long mean free paths, so modulation doping is useful 
  for designing devices with low dissipation.

¶ electrons that are trapped in a quantum well are free in 2 directions (x,y), but 
  constrained in the third direction (z). 

  at low T, if a confined electron doesn't have enouch energy to jump out of the 
  well, or to jump to a higher particle-in-box state, we say the electron motion 
  is strictly 2d.

  this study of 2d electrons is a new area of research, a "treasure trove" of 
  new physics. [simon p199]
