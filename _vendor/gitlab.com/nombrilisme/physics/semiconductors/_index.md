---
title: semiconductors
type: domain
---

## [../](../)

<br>

- {{< note mosfet transistor >}}
- {{< note pn junction >}}
- {{< note band structure engineering >}}
{.notelist} 
