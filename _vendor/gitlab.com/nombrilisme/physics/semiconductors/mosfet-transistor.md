---
title: "Mosfet Transistor"
type: note
font: concrete
domain: semiconductors
back: "../"
---

A MOSFET transistor, "Metal Oxide Semiconductor Field Effect Transistor", is a 
transistor that can change its electrical conductivity by varying an applied 
voltage. Physically it's comprised of a metal gate atop an oxide insulator atop 
a (usually) silicon semiconductor, and three electrical contacts (source, drain, 
gate). The semiconductor is split into n-doped and p-doped regions. For an 
n-MOSFET, the regions surrounding to the source and drain electrical contacts 
are n-doped, and the rest is p-doped (and vice versa for p-doped). This is shown 
in Figure 1.

{{< fig mossfet1.jpg 10 >}}

        Figure 1. n-MOSFET with V = 0 at gate. The depletion layers 
        between the n-doped and p-doped regions are highlighted in 
        orange.

The depletion layer prevents the flow of current between the n- and p-doped 
regions. When ˝V = 0˝ at the gate, (effectively) no current flows between the 
source and drain.

When you apply a +ve voltage to the gate, the metal gate and the semiconductor 
form a kind of capacitor, since the excess +ve charge above the oxide insulator 
will cause -ve charge to build up in the region of the semiconductor just below 
the oxide insulator.

To quickly recap some definitions regarding doping: an n-doped system has free 
electrons carriers (-ve) in a mostly empty conduction band, and a p-doped system 
has free hole carriers (+ve) in a mostly filled valence band. As you increase 
the applied voltage on the gate, the buildup of -ve charge in the region under 
the oxide insulator also increases, and eventually, this region will become 
"n-like" (with lots of -ve free electron carriers). At a certain threshold 
voltage ˝V₍th₎˝, a conduction channel will open up between the two formerly 
separated n-doped regions. This allows a current to flow between the source and 
the drain.

When I say the region becomes "n-like" (this is Simon's lingo), what I actually 
mean is: at sufficiently high gate voltage, the edge of the valence band is 
driven away from the fermi level, and consequently the holes are driven away 
from the gate. As you increase the applied voltage even more, the edge of the 
conduction band gets driven toward the fermi level. Thus the region becomes 
populated with electrons, which form a channel (called an n-channel or inversion 
layer). Hence, "n-like". When the gate voltage is much higher than the threshold 
voltage, there is a high electron density in the n-channel, and a large current 
flows between the source and drain. When the gate voltage is below the threshold 
value, the channel is only lightly populated, and only a small current flows 
between the source and drain (called the subthreshold leakage current).

{{< fig mossfet3.jpg 10 >}}

          Figure 2. n-MOSSFET when V₍gate₎ » V₍threshold₎. The n-channel 
          is highly populated with electrons, and a large current 
          flows between the source and drain.

{{< fig mossfet2.jpg 10 >}}

          Figure 3. n-MOSSFET when V₍gate₎ ∼ V₍threshold₎. The n-channel 
          is only lightly populated, and only a small current flows 
          between the source and drain.

In a p-MOSFET, the n- and p-doped regions are reversed. Conductance becomes 
nonzero when the gate voltage is suffciently _negative_. The region under the 
oxide layer attracts _holes_, which form a p-channel. 

MOSFET transistors are often used as amplifiers (a small gate voltage can result 
in a large current between source and drain) and as electronic switches (in this 
case they're tuned so the leakage current is as small as possible).
