---
title: "p-n junction"
type: note
back: "../"
---

¶ a p-n junction is a semiconductor structure where a p-doped semiconductor is 
  brought into contact with a n-doped semiconductor.

# n-DOPED & p-DOPED SEMICONDUCTORS
--------------------------------------------------------------------------------

¶ n-doped systems have free negatively charged electrons, p-doped systems have 
  free positively charged holes, but both systems are overall neutral because 
  charged ions even out for the charges of the charge carriers.

{{< fig simonfig183.png 8 >}}

  - the n-doped semiconductor has free electron e- carriers in the mostly empty 
    conduction band. but it's electrically neutral because of the positive ions.
  - the p-doped semiconductor has free hole h+ carriers in the mostly filled 
    valence band. it's electrically neutral because of the negative ions.
  - when they're brought together, the electrons "want" to fall down to the 
    lower chemical potential, filling (annihilating) the empty holes.

¶ for n-doped semiconductors, the chemical potential is near the top of the band 
  gap, and v.v. for p-doped.

  this means when they're brought together, electrons in the conduction band 
  fall into the valence band and fill the empty hole states. _pair-annihilation_ 
  of the electron and hole.

  ?? bringing together means, along the horizontal axis? hmph.

¶ if E₍gap₎ is the energy gap between the bottom of the conduction band and the 
  top of the valence band, then pair-annihilation process causes each pair to 
  gain energy E₍gap₎

# BRINGING TOGETHER n-DOPED and p-DOPED SEMICONDUCTORS
--------------------------------------------------------------------------------

¶ what happens when the n-doped and p-doped semiconductors are brought together: 

  {{< fig simonfig184.png 8 >}}

  - the electrons near the p-n interface fall into the holes, annihilating the 
    electron and the hole.
  - immediately after this annihilation occurs, there's a depletion region near 
    the interface where there are no free carriers. 
  - there's a net electric field pointing from the +ve charged ions to the -ve 
    charged ions, ie it points from the n-doped region to the p-doped region.
  - it's kinda like a capacitor, +ve charge spatially separated from -ve charge 
    with an E field in the middle.
  - the depletion region is electrically charged, since there are charged ions 
    but no carriers to neutralise them
  - this depletion region forms because the energy for an electron to cross the 
    depletion region (-eφ) is smaller than the energy it gains by annihilating a 
    hole (which is E₍gap₎), i.e. -eφ < E₍gap₎. so it chooses annihilation.
  - ie the annihilation process gives a gain of energy E₍gap₎, but moving across 
    the depletion region costs energy -eΔφ, where φ is the electrostatic 
    potential.
  - note, when we say electrons crossing the depletion region, we're talking 
    horizontally, right?
  - in this depletion region, the charged ions cause an electric field, which 
    has electrostatic potential -eφ (the lower graph)
  - the depletion region continues to grow until the energy for another electron 
    to cross the depletion region (this is the size of the step in -eφ, ie the 
    magnitude of -eφ) is larger than the gap energy that it would gain by the 
    electron annihilating a hole.
  - ie when the depletion region is sufficiently large, and thus Δφ is large, 
    it's no longer favourable for electrons to annihilate. 
  - so, the depletion region grows to a width where the two energy scales are 
    the same.

¶ band diagram of the p-n junction: 

  {{< fig simonfig185.png 8 >}}

  - this is a less biased depiction than before
  - the electrostatic potential is added to the band energy
  - equality of μ on the L and R mean there's no force for electrons to flow 
    left or right
  - but, inside the depletion region there's a net electric field, so if 
    electron hole pairs are created in this region by absorbing a photon, the 
    electron will flow left and the hole will flow right, creating a net 
    current.
  - the total potential voltage drop over the depletion region is equal to the 
    band gap.
  - so this figure better represents the system in equilibrium after the 
    depletion region has stopped growing.

# SOLAR CELLS
--------------------------------------------------------------------------------

¶ if you apply light to a semiconductor, ehole pairs can get excited if the 
  energy of a photon > energy of the bandgap.

  if you expose the p-n junction in the previous fig to light. in most regions 
  of the semiconductor, the created electrons and holes will quickly annihilate. 
  
  BUT in the depletion region, since there's an electric field, electrons 
  created will tend to flow to the right (towards the p-doped region) and holes 
  will tend to flow left, to the n-doped region. (note, these both describe 
  current moving to the right, i.e. -ve flowing left and +ve flowing right both 
  constitute a current flowing right)

  thus a p-n junction spontaneously creates a current, thus voltage, thus power, 
  just by being exposed to light.

# DIODES
--------------------------------------------------------------------------------

¶ one property of the p-n junction: rectification. it allows current to flow 
  through the junction easily in one direction, but not easily (with high 
  resistance) in the other.
