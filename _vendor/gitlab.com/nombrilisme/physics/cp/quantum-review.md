---
time: 202304200947
title: "quantum review"
type: note
back: "../"
domain: computational
---

there are only a handful of qm problems that have exact analytic solutions:
  - harmonic oscillator
  - particle in a box
  - hydrogen atom

# TIME INDEPENDENT SCHRODINGER EQUATION
--------------------------------------------------------------------------------

TISE for a particle in 3d:
```
  -⌠h²⧸2m⌡∇²ψ + V(⬀r)ψ = Eψ 
```

the wavefunction ˝ψ = ψ(⬀r)˝: is a complex-valued function of position. if it's 
normalised properly, the product
```
  ψ(⬀r)⁽❄⁾ ψ(⬀r) = Pr(⬀r)
```
is the _probability per unit volume_ that the particle will be found at position 
˝⬀r˝.

the TISE is a pde. the only difference between this pde and those of waves on a 
string is that here, the energy E is also unknown.

so to solve the TISE, you have to determine both ψ and the corresponding E.
  - aka the eigenfunction(s) and corresponding eigenvalue(s)

eigenvalue problems in general: in this case solutions exist only for certain 
values of eigenvalues, the energy values. 
  - mathematically this is where discrete energy levels of qm theory come from.

# e.g. FREE PARTICLE 1D (analytic)
--------------------------------------------------------------------------------

free particle in space means a region where potential is constant. problems are 
invariant to potential shifts, so assume ˝V = 0˝.

1d means ψ = ψ(x) and ∇² → ⌠d²⧸dx²⌡

under these conditions the tise becomes
```
  -⌠ħ²⧸2m⌡ d²₍x₎ψ = Eψ
```

general solution:
```
  ψ = Ae⁽ikx⁾
```

when you sub this solution into the equation of motion, you get
```
  E = ⌠ħ²k²⧸2m⌡
```

the wave function ψ(x) has the form of a plane wave with wave vector k = 2π/λ. λ 
is the wavelength of the particle.

relation between k and particle's momentum:
```
  p = ħk
```

there is a solution to the tise for _any value of k_. this means nonnegative 
values of E aren't allowed. we haven't determined the constant A yet. our 
solution satisfies the tise for any value of A, since it's a linear equation.

A sets the magnitude of the wave function. you determine it by physically 
interpreting ψ and its relation to probability density.
  - the prob of finding the particle in a certain region of space is ∝ ψ⁽❄⁾ψ
  - in 1d space ψ(x)⁽❄⁾ψ(x)dx is the probability of finding the particle in a 
    region of length dx, in the space around x.

the probability must satisfy normalisation
```
  ∫ dxͺψ⁽❄⁾ψ = 1
```
just ensures the total probability of finding the particle somewhere must be 1

problem: for our wavefunction ψ⁽❄⁾ψ = A², so ∫dx ψ⁽❄⁾ψ = A² ∫ dx. so if the 
particle's allowed to move freely across an infinitely large region, A has to be 
infinitely small to preserve normalisation.

so: confine the particle to a box from x = -L to x = L. constrain the solution 
so the plane wave defined by ψ = Ae⁽ikx⁾ must fit in the box.

to do this, the potential is V = 0 in the box and V = ∞ outside. so V(x) is 
discontinuous in x.

so what you do is solve ψ separately for the regions |x| ≤ L (inside) and |x| ≥ 
(outside).

inside, V = 0, so ψ(x) is still given by Ae⁽ikx⁾.

outside, V = ∞, but the energy of the particle isn't ∞, so the only way to 
satisfy the schrodinger equation is to set ψ₍outside₎ = 0, which means 
d²ψ₍outside₎/dx² = 0, and both sides of the tise are zero. physically: a 
particle with finite energy has NO chance of being found in a region where V = 
∞. ∴ its probability amplitude has to vanish in this region.

there's one more constraint on ψ: it must be continuous in x. 
- since ψ = 0 for x > ±L, the solution for x inside the box must vanish at x = 
  ±L. 
- satisfy this constraint by using the linearity of the schrodinger equation: 
  the sum of two solutions is also a solution. 

so you can construct wavefunctions of form
```m
  ψ₊ = ½A(e⁽ik₊x⁾ + e⁽-ik₊x⁾) = Acos(k₊x) 
  ψ₋ = ½A(e⁽ik₋x⁾ - e⁽-ik₋x⁾) = Asin(k₋x)
```

these are standing waves, like you get with a vibrating string. the condition 
ψ(±L) = 0 requires
```m
  k₊ = ⌠π⧸2L⌡, ⌠3π⧸2L⌡, ... = ⌠(2n-1)π⧸2L⌡
  k₋ = ⌠π⧸L⌡, ⌠2π⧸L⌡, ... = ⌠nπ⧸L⌡
```

constraining k like this gives you discrete energy levels. because only certain 
values of the wave vector are allowed: the ones that give standing waves which 
satisfy the BCs on ψ at the walls.
  - in general, these kinds of constraints lead to quantised energy levels

use the normalisation condition to find the value of A. you get A = L⁽-½⁾

# FEATURES OF THE WAVEFUNCTIONS
--------------------------------------------------------------------------------

the wavefunctions

    k₊ = ⌠π⧸2L⌡, ⌠3π⧸2L⌡, ... = ⌠(2n-1)π⧸2L⌡

    k₋ = ⌠π⧸L⌡, ⌠2π⧸L⌡, ... = ⌠nπ⧸L⌡

these wavefunctions form a complete set of orthogonal functions over the 
interval |x| ≤ L.
  - orthogonal means ∫dx ψ₁⁽❄⁾ψ₂ = 0 UNLESS ψ₁ and ψ₂ refer to the same 
    eigenstate
  - "complete set" means that any function in the interval that vanishes at x = 
    ± L can be written as a SUM of these wavefunctions with the right 
    coefficients.
  - this collection of functions is called a BASIS

similar to fourier transforms: the basis set is ALSO made up of sines and 
cosines.
