---
time: 202303012308
title: "molecular dynamics"
type: note
domain: computational
back: "../"
---

# WHAT
--------------------------------------------------------------------------------

MOLECULAR DYNAMICS means modelling the trajectory of each particle in a many 
particle system.

assumptions about the system:
  - classical 
  - particles are spherical
  - particles are chemically inert
  - the interaction between two given particles depends ONLY on the distance 
    between them.

# POTENTIAL
--------------------------------------------------------------------------------

total PE is a sum of two-particle interactions:
```mm
  U₍total₎ 
  &= U(r₍12₎) + U(r₍13₎) + ... + U(r₍23₎) + ...
  &= ∑{i}ͺ∑{j} U(r₍ij₎)    FOR: i=1:(N-1),ͺͺj=(i+1):N
```

required properties of U(r):
  - needs to have strong repulsion for small r, weak attraction for  big r
  - small r repulsion is due to the pauli exclusion principle: the wave 
    functions of two molecules have to distort to avoid overlapping. this causes 
    some of the electrons to be in different quantum states. this causes the KE 
    to increase, and there is an effective repulsion interaction between the 
    electrons, called CORE REPULSION.
  - the weak attraction at r is due to the mutual polarisation of each molecule. 
    the attractive potential is called the van der waals potential.

common form for U(r): the lennard jones potential
```
  U(r) = 4ε·((÷σr)⁽12⁾ - (÷σr)⁽6⁾)
```
  - two parameters for the LJ potential: ε and σ.
  - σ is the length. σ is the point where U(r) = 0 (at r = σ). U(r) is close to 
    zero for r > 2.5σ.
  - ε is the depth of the potential at the minimum of U(r). the minimum occurs 
    at separation 2⁽1/6⁾σ.

# UNITS
--------------------------------------------------------------------------------

let the LJ parameters σ and ε be the units of distance and energy.

let the unit of mass be the mass of one atom m.

then, you can express all other qties in terms of σ, ε, m.

e.g. velocity is in units of (ε/m)⁽1/2⁾. time in units σ(m/ε)⁽1/2⁾.

if you run the MD program for 2000 time steps with Δt = 0.01, the total time of 
the run is 2000·0.01 = 20 in reduced units. or, for argon, 4.34·10⁽11⁾⦃s⦄. 

the longest run you can do practically is in the range 10-10⁽4⁾ in reduced 
units, or 10⁽-11⁾-10⁽-8⁾⦃s⦄.

# ALGORITHM
--------------------------------------------------------------------------------

criteria for a good numerical algorithm:
  - preserves phase space volume
  - consistent with known conservation laws
  - time reversible
  - accurate for large time steps, to reduce cpu time needed for the total time 
    of the simulation.

should use a sympletcic algorithm.
```m
  x₍n+1₎ = x₍n₎ + v₍n₎·Δt + ÷12·a₍n₎·(Δt)⁽2⁾ 
  v₍n+1₎ = v₍n₎ + ÷12·(a₍n+1₎ + a₍n₎)·Δt
```
  - this is for only one component of the particle's motion.
  - you use the new position to find the new acceleration, a₍n+1₎.
  - then use a₍n+1₎ along with a₍n₎ to find the new velocity v₍n+1₎.
  - this is velocity verlet.

# PERIODIC BOUNDARY CONDITIONS
--------------------------------------------------------------------------------

in reality the effects of walls is fairly negligible, since the fraction of 
particles interacting with walls is small when the number of particles is 
∼10⁽23⁾. on computers you can only do about 10⁽3⁾ to 10⁽5⁾, so it's not 
negligible. 

to minimise, you use periodic BCs. if a particle leaves a box in a certain 
direction, you modify its position by adding/subtracting the length of the box 
in that direction.

also, something about minimum distance in a certain direction between two 
particles.

# MD PROGRAM
--------------------------------------------------------------------------------

use one "class" to represent all N particles.

store the x,y comps of pos and vel in the state array. store accelerations of 
pcls in separate array.

there are a few different ways to initialise the particles. 

the easiest way to get a config with a desired density is to place them on a 
regular lattice.

the most time consuming part of the program is computing the accelerations of 
the particles.
