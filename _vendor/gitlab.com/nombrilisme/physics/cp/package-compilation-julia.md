---
title: package compilation in julia
type: term
domain: computational
---

sysimage
: basically a saved julia session. can include packages, code, global variables.

to make the current directory a project:

    using PackageCompiler
    pkg> activate .

add packages to the project:

    pkg> add <PackageName>

create a sysimage with the package losded:

    create_sysimage(["<PackageName>"]; sysimage_path="path.so")

if you want to also precompile certain functions, start julia with:

    julia --trace-compile=<precompilefile.jl>

then run all the functions you need to:

    using <Package>
    <Package>.function1()
    <Package>.function2()

quit the session. check the file <precompilefile.jl> to make sure it has all 
the precompilation functions are printed there.

then in a julia terminal:

    using PackageCompiler
    create_sysimage(
      ["<PackageName>"]; 
      sysimage_path="path.so", 
      precompile_execution=file="<precompilefile.jl>"
    )

and that should just about do it.
