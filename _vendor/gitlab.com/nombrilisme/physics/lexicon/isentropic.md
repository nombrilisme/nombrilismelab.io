---
time: 202304181718
title: "isentropic"
type: lexicon
---

an isentropic process: is both adiabatic and reversible.
