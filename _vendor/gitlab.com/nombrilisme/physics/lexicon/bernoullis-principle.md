---
time: 202304181716
title: "bernoullis principle"
type: lexicon
---

increasing the speed of a fluid decreases the static pressure or potential 
energy. 

only applies for isentropic flows (no irreversible processes like turbulence or 
thermal radiation). fluids and gases at low mach number.

you can derive the principle from energy conservation.

most flows (liquids and gases) with low mach number have a reasonably constant 
density. so you assume the fluid is incompressible.

bernoulli's equation:
```
  ÷12 v⁽2⁾ + gz + ÷1ρ p = const.
```
- applies in any region of a flow where energy per unit mass is uniform.
- this accurately describes fluid flow only in regions where viscous forces 
  don't exist (and diminish the energy per unit mass)

another common statement of bernoulli's principle:
```
  ÷12·v² + ∫dPͺ÷1ρ = const
```

assumption:
- steady flow (parameters like ˝v, p, ρ˝ are constant in time)
- incompressible
- no friction by viscous forces
