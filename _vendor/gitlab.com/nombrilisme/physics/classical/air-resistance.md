---
time: 202302130415
title: "air resistance"
type: long
---

¶ a general expression for the force due to air resistance (drag):
  ```
    F₍drag₎ ≈ -B₍1₎·v - B₍2₎·v⁽2⁾
  ```
  this looks kinda like a taylor expansion. {{< 𝖗 GN p21 >}}. 

  at VERY LOW VELOCITIES the first term dominates. you can calculate the 
  coefficient ˝B₍1₎˝ for simple objects (supposedly). look into {{< ℑ STOKES LAW 
  >}}. 

  at NORMAL VELOCITIES, the ˝v⁽2⁾˝ term is what dominates (for most objects).
  however, you can't really calculate ˝B₍2₎˝ exactly, even for simple objects 
  (e.g. ball).

¶ how to approximate the coefficient ˝B₍2₎˝: 

  as an object moves the air, it pushes air out of the way. the mass of air 
  moved in a time ˝dt˝:
  ```
    m ∼ ρAvdt
  ```
  ˝ρ˝ is the density. ˝A˝ is the front area of the object.

  the velocity of the chunk of air that gets moved is roughly the same as (on 
  the same order of) the object's velocity. so the kinetic E of the air chunk is
  ```
    E ∼ mv⁽2⁾/2
  ```
  this KE is also the work done by the drag force in time ˝dt˝.
  ```
    W₍drag₎ = dragforce × distance = F₍drag₎·v·dt = E
  ```
  so
  ```
    F₍drag₎ ≈ -÷12·CρA·v⁽2⁾
  ```
  ˝C˝ is the drag coefficient. 

¶ the drag coefficient ˝C˝ depends on the specific aerodynamic properties of the 
  object.  the best way to determine is to use an air tunnel.
