---
time: 202302161037
title: "nonlinear pendulum"
type: long
---

¶ we're talking about a pendulum when you INCLUDE dissipation, an external 
  driving force, and nonlinearity.
  - aka the "nonlinear, damped, driven pendulum", or "physical pendulum".
  - >>> the kind of pendulum I'm talking about, you don't kill people! <<<
    {.methinks}

¶ key things:
  - don't use the small angle approximation.
  - add a friction term that's linear to velocity, form ˝-b·Dθ˝.
  - optional anharmonic term, ˝-c·θ⁽3⁾˝
  - add a sinusoidal driving force, form ˝f₍d₎·sin(Ω₍d₎·t)˝.

¶ equation of motion:
```
   D²θ = -÷gl·sin(θ) - b·Dθ - f₍d₎·sin(Ω₍d₎·t)
```
  - solve numerically.


