---
time: 202302130509
title: "cannon ball"
type: long
---

# EQUATIONS
--------------------------------------------------------------------------------

a cannonball. motion in a plane so 2d. simplest case (no air resistance), there 
are 2 diffeqs, for x and for y.
```m
  ḍ₍t₎⁽2⁾x = 0
  ḍ₍t₎⁽2⁾y = -g
```
or as 4 first order equations:
```m
  Ɗ₍t₎x = v₍x₎
  Ɗ₍t₎v₍x₎ = 0
  Ɗ₍t₎y = v₍y₎
  Ɗ₍t₎v₍y₎ = -g
```

can use a regular old integrator to solve this shiz. evolve the integrator until 
y₍t+1₎ is negative (collision with ground). at which point either end the 
simulation or model bounceback.

# ADD AIR RESISTANCE
--------------------------------------------------------------------------------

now what happens when you add air resistance. the drag force on the cannon is
```
  F₍drag₎ = -B₍2₎·v⁽2⁾
```
compute x and y components of the drag force separately. ˝F₍drag,x₎ = 
F₍drag₎·cos(θ), F₍drag,y₎ = F₍drag₎·sin(θ)˝. so
```
  F₍drag,x₎ = -B₍2₎·v·v₍x₎
  F₍drag,y₎ = -B₍2₎·v·v₍y₎
```

# EFFECT OF CHANGING AIR DENSITY WITH ALTITUDE
--------------------------------------------------------------------------------

you could use the isothermal approximation (constant temperature). pressure 
varies with altitude via
```
  p(y) = p(y=0)·e⁽-mgy/ƙT⁾
```
for an ideal gas pressure is proportional to density so
```
  ρ = ρ₍0₎·e⁽-y/y₍0₎⁾
```
but this isn't a great approximation because temperature actually can change.

instead, the adiabatic approximation:
```
  ρ = ρ₍0₎·(1 - ay/T₍0₎)⁽α⁾
```

the drag force due to air resistance is proportional to density so
```
  F₍drag*₎ = ÷{ρ}{ρ₍0₎}·F₍drag₎(y=0)
```
