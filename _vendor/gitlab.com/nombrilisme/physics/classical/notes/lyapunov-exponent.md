---
time: 202302260258
title: "lyapunov exponent"
type: note
---

define {{< ℑ LYAPUNOV EXPONENT >}}: characterises the rate of separation of two 
trajectories that are infinitesimally close together (initially).
```
  Δθ ≈ e⁽λt⁾
```
  - let θ₍1₎ and θ₍2₎ be two different trajectories. initially their separation 
    is very (infinitesimally) small.
  - define Δθ = θ₍1₎ - θ₍2₎, the separation
  - λ is the lyapunov exponent.

the lyapunov exponent characterises whether a trajectory is chaotic/not.
  - λ > 0: chaotic
  - λ < 0: nonchaotic
  - the transition to chaos occurs at λ = 0.
