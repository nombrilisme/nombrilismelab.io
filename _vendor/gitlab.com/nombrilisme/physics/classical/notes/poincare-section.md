---
time: 202302260324
title: "poincare section"
type: note
---

¶ more specifically, a POINCARE MAP. consider a periodic orbit. the initial 
  conditions of this orbit are (necessarily) in a _section_ of the space (i.e. 
  the ICs are at a _specific_ point in space, and you can think of the general 
  "ballpark" area where the ICs are as a _section_ of the space). so, consider a 
  periodic orbit, which starts in some section of the space, and leaves that 
  section. but since it's periodic, it must necessarily return to the original 
  "section". observe the point that the orbit returns to the original section. 
  you can then create a MAP that "sends" the first point to the second point.
  - >> what do you mean "sends"? <<
    {.question}
  - a poincare map is aka a FIRST RECURRENCE MAP
  - significance: things look simpler when you observe them at a rate that 
    matches a characteristic frequency of the system.

¶ a more technical definition: a poincare map is the intersection of a periodic 
  orbit with a specific lower-dimensional SUBSPACE, called the POINCARE SECTION, 
  which is transversal to the flow of the system.

¶ e.g. a chaotic driven pendulum:
  - a phase space plot Ḍθ,θ where you only display the point when Ω₍d₎·t = 2nπ, 
    where n is an integer.
  - the driving frequency is a good choice of characteristic frequency for you 
    to observe the system.

¶ if you draw a poincare map for a nonchaotic system, it'll only have a single 
  point (because each "period" the system returns to the same point). for a 
  nonchaotic system, each "period" (i.e. when the system returns to the poincare 
  section, the subspace), will have a slightly different point.
  - the point (or set of points, in the case of chaotic systems) is called a 
    STRANGE ATTRACTOR.
  - strange attractors are usually fuzzy.
  - strange attractors have a fractal structure.

references:
  - GN p63-64
