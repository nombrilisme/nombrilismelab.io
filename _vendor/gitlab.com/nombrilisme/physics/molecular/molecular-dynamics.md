---
time: 202303012308
title: "molecular dynamics"
type: longue
---

MOLECULAR DYNAMICS (MD)
================================================================================
  is where you model the trajectory of _each particle_ in a many-particle 
  system. 

  you model the particles' interaction with distance-dependent interatomic 
  potentials. then numerically solve newton's equations of motion for the system 
  of particles.

  is useful for modelling dense gases, liquids, solids.

  assumptions (about the particles)
  --------------------
  - classical 
  - spherical
  - chemically inert
  - interactions between them depend only on distance.


THE INTERMOLECULAR POTENTIAL
================================================================================

  general form of U: should be a sum of two-particle interaction potentials 
  between all the particles.
  --------------------
  ```
    U₍total₎ = ∑₍i₎∑₍j₎ U(r₍ij₎)    ͺͺͺforͺi=1:(N-1),ͺj=(i+1):N
  ```


  two properties that U(r) must satisfy
  --------------------
  1. strong repulsion for small r.
  - [because the pauli exclusion principle... there's a core repulsion as the 
    wave functions of the two particles distort to avoid overlapping. causes 
    some of the electrons to "be" in different quantum states. ∴ an effective 
    repulsion].
  2. weak attraction for big r. 
  - [pq the vdw potential. mutual polarisation of each molecule].


LENNARD JONES POTENTIAL
================================================================================
  is
  ```
    U(r) = 4ε·((÷σr)⁽12⁾ - (÷σr)⁽6⁾)
  ```

  two parameters: ε and σ.
  - σ = "length". this is the point where U(r) = 0 (at r = σ). U(r) is close to 
    zero for r > 2.5σ.
  - ε = the depth of the potential at the minimum of U(r). the minimum occurs at 
    separation 2⁽1/6⁾σ.

  note if you look at a plot of the lennard jones potential U(r) it's easy to 
  see why σ is a length and ε is an energy. 

  the force is just F(r) = -∇U(r)
  ```
    F(r) = ÷εr (48(÷σr)⁽12⁾ - 24(÷σr)⁽6⁾)
  ```


REDUCED UNITS
================================================================================

  you can express all units in terms of σ,ε,m.
  --------------------
  - since σ is a distance, define it to be the distance unit for the problem.
  - and since ε is an energy, define it to be the energy unit.
  - also define the mass of one atom/molecule/particle/whatever to be the mass unit.

  in terms of these base units:
  
  velocity: ˝(ε/m)⁽1/2⁾˝
  --------------------
  - >> where the fuck did this come from? <<
    {.question}

  time: ˝σ(ε/m)⁽-1/2⁾ = σ(m/ε)⁽1/2⁾˝
  --------------------
  - >>> ok I get this one, d = vt ∴ t = d/v <<<
    {.methinks}

  force: ˝ε/σ˝
  --------------------
  - f units are m·v/t = ˝(ε/m)⁽1/2⁾·m·÷1σ·(ε/m)⁽1/2⁾ = ε/σ˝

  pressure: ˝ε/σ⁽2⁾˝
  --------------------

  temperature: ˝ε/ƙ˝
  --------------------


INTEGRATOR
================================================================================

  need a symplectic integrator because:
  - preserves phase space volume
  - preserves conservation laws
  - time reversible
  - accurate for large time steps, to reduce cpu time needed for the total time 
    of the simulation.

  velocity verlet
  --------------------
  ```m
    x₍n+1₎ = x₍n₎ + v₍n₎·Δt + ÷12·a₍n₎·(Δt)⁽2⁾
    v₍n+1₎ = v₍n₎ + ÷12·(a₍n+1₎ + a₍n₎)·Δt
  ```
  - this is only for one component of a particle's motion.
  - you use the new position, x₍n+1₎, to find the new acceleration, a₍n+1₎.
  - then you use both the new acceleration a₍n+1₎ and the current acceleration 
    a₍n₎ to find the new velocity v₍n+1₎.


PERIODIC BOUNDARY CONDITIONS
================================================================================
  
  if a particle leaves the box it should "reenter" from the opposite edge/wall.

  note, irl, the effects of collisions with walls is negligible. the fraction of 
  particles interacting with walls is small compared to the total number of 
  particles. in the simulation, we have a finite size problem.

  minimum image approximation
  --------------------


MD PROGRAM
================================================================================

  use one "class" to represent all N particles.

  store the x,y comps of pos and vel in the state array. store accelerations of 
  pcls in separate array.

  you need to figure out a scheme for initialising the positions and velocities.
  - for velocities, use the equipartition theorem
  - for positions, there are a few ways. the easiest way to get a config with a 
    desired density is to place them on a regular lattice.

  the most time consuming part of the program is computing the accelerations of 
  the particles.

INITIAL VELOCITIES
================================================================================

  equipartition theorem: the average KE of a particle per degree of freedom, 
  ˝K₍pp,pdof₎˝ is:
  ```
    K₍pp,pdof₎ = ÷12 ƙT
  ```

  also, mind that:
  ```mm
    K₍pp,pdof₎ &= K₍total₎ / ⦃numDoFs⦄ 
    &= K₍pp₎ / d
    &= K₍total₎ / Nd
  ```
  - N is the number of particles, d is number of spatial dimensions.
  - and ˝K₍total₎˝ is the average _total_ KE of the system, i.e. ˝∑{i} 
    ÷12m⟦i⟧v⟦i⟧⁽2⁾˝ for all the particles in the system

  ∴ define temperature at time t:
  ```
    ƙT(t) = ÷{2K₍total₎(t)}{Nd} = ÷1{Nd} ∑{i=1:N} m⟦i⟧ (⬀v⟦i⟧(t)⦁⬀v⟦i⟧(t))
  ```
