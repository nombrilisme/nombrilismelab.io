---
time: 202302140436
title: "levi civita tensor"
type: note
---

¶ *define* {{< ℑ LEVI CIVITA TENSOR >}} : in 3d (in a euclidean coordinate 
  system) it takes the three basis vectors
  ```
    ε = ε(↗e₍1₎, ↗e₍2₎, ↗e₍3₎) = ↗e₍1₎ ⊗ ↗e₍2₎ ⊗ ↗e₍3₎
  ```
  and returns (components)
  ```
    ε⟦ijk⟧ = 
      +1  (even  permutations)
      -1  (odd  permutations)
      0  (not  all  different)
  ```

¶ you can also define a 2d LC tensor, ˝ε = ↗e₍1₎ ⊗ ↗e₍2₎˝, where ˝ε⟦12⟧ = 1˝, 
  ˝ε⟦21⟧ = -1˝, ˝ε⟦11⟧ = ε⟦22⟧ = 0˝.
