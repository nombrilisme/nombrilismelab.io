---
time: 202302081853
title: "definition of a rank-n tensor"
type: note
---

¶ *define* {{< ℑ VECTOR >}} : a geometric entity that "transports" you from one 
  point in space P₍a₎ to another point P₍b₎.

  "transport" is key. a vector goes FROM one point to another. it has 
  directionality.

  two points in space have a distance between them. so a vector must have a 
  length or magnitude.

  so a vector has two pieces of information, or constituent parts, or whatever: 
  a length and direction.

  you can move a vector to any point in space and its definition doesn't change. 
  it's INVARIANT TO TRANSLATIONS in space. so a vector doesn't depend on a 
  specific point in space. instead, _given_ a point in space, a vector will 
  transport you to another point, prescribed by the direction and length 
  information that it contains.

¶ *define* {{< ℑ TENSOR >}} a rank-n tensor is a linear function of n vectors. the n 
  vectors are the inputs. it returns number. 
  - {{< 𝖗 TB p9 >}}

  for a tensor T, the expression T(v₍1₎, v₍2₎, v₍3₎) will output a number.

¶ a tensor is a {{< ℑ LINEAR OPERATOR >}}
  ```
    T(a·v₍1₎ + b·v₍2₎, v₍3₎) = a·T(v₍1₎, v₍3₎) + b·T(v₍2₎, v₍3₎)
  ```

¶ the {{< ℑ INNER PRODUCT >}} of two vectors:
  ```
    ↗A⦁↗B = ÷14 ((↗A + ↗B)⁽2⁾ - (↗A - ↗B)⁽2⁾)
  ```
  > uh, wow. <

¶ the inner product is a rank-2 tensor: it's a linear function of two vectors. 
  it takes two vectors as input and returns a number. 

  writing the inner product as a function, 
  ```
    g(↗A, ↗B) = ↗A⦁↗B
  ```
  g is called the {{< ℑ METRIC TENSOR >}}.
  > mais pourquoi "g"? <

¶ the metric tensor is SYMMETRIC. this is because A⦁B = B⦁A.

¶ a vector is a rank-1 tensor. A(C) = A⦁C.

  >> hang on, doesn't that mean a vector and the metric tensor are the same?
  {.question}
  - hmm. maybe not. it's not saying they're the same. but they produce the same 
    thing. by "they" I mean the two distinct operations:
    ```
      g(A,B) = A⦁B
      A(B) = A⦁B
    ```
    if you insert vector B into vector A, you get the dot product of A and B. 
    but also, if you insert both vectors A and B into the metric tensor, you 
    also get the dot product.
    > ok, ok. jesus. <

¶ it's not mandatory to specify ALL the arguments of a tensor for it to produce 
  an output.

  the inertia tensor I is a rank 2 tensor. it's a function with two arguments, 
  I(  ,  ). yet, if you insert the angular velocity vector Ω into the 
  second "slot" and leave the first empty, you get the angular momentum vector.  
  J = I(  ,Ω). 

  > clearly, if you specify all the arguments of a tensor, it outputs a scalar. 
    because it has to, right? that's part of the definition. but if you leave 
    some of the args blank, it'll output a higher rank tensor. a scalar is a 
    rank 0 tensor, remember. <

¶ a scalar is a rank 0 tensor.
  > right, I mean, a scalar is not a function. right? it doesn't take arguments.
    <

