---
time: 202302082314
title: "contraction"
type: note
---

¶ given any two vectors, A and B, applying the {{< ℑ INNER PRODUCT >}} A⦁B will 
  return a scalar (rank-0 tensor), and applying the {{< ℑ TENSOR PRODUCT >}} A⊗B 
  will return a rank-2 tensor. 
  - {{< 𝖗 TB p11 >}}

---
¶ *define* {{< ℑ CONTRACTION >}}: the process of constructing the scalar A⦁B 
  from the tensor A⊗B.
  ```
    contraction(A⊗B) = A⦁B
  ```

  > oh, I get it now. I think. by "constructing", I think what's happening is 
    they're literally taking the form A⊗B and turning into A⦁B. when you do 
    that, it's called contraction. <

  e.g. if you construct a rank-2 tensor T as "the sum of tensor products of two 
  vectors" (ugh), as follows:
  ```
    T = v₍1₎⊗v₍2₎ + v₍3₎⊗v₍4₎ + ...
  ```
  then the contraction of T is
  ```
    contraction(T) = v₍1₎⦁v₍2₎ + v₍3₎⦁v₍4₎ + ...
  ```

¶ contraction lowers the rank of a tensor by two. for a rank-2 tensor, it 
  produces a scalar (as expressed above).

¶ for higher rank tensors, you have to specify which arguments the contraction 
  acts on. for a rank-3 tensor T = A⊗B⊗C + E⊗F⊗G, contracting T on arguments 1 
  and 3 gives
  ```
    contraction₍1,3₎(T) = (A⦁C)·B + (E⦁G)·F
  ```
