---
time: 202302110256
title: "slot naming index notation"
type: note
---

¶ this is a pretty confusing/annoying convention. thorne admits so himself {{< 𝖗 
 TB p18 >}}.

¶ demonstrate by example: you have a tensor G = G(  ,  ). you define a new 
  tensor F but with the slots in reverse order. so G(A,B) = F(B,A). thorne's 
  "radical" notation is to use sub indices to show which slot. so G₍ab₎ = F₍ba₎.

  the idiocy comes from the fact that now it's harder to distinguish between 
  slot references and indexing. thorne says to just do a quick mental mindflip 
  whenever you encounter this. 

¶ e.g. rank-3 tensor T. if you do a 1,3 contraction, i.e. you want to scalar 
  multiply the vectors in the 1st and 3rd slots, 
  ```
    contraction₍1,3₎(T) = T₍aba₎
  ```
  the 1st and 3rd slots "annihilate" each other, so the only remaining free slot 
  is the second one, b. thus what you get is a rank-1 tensor (a vector).
  - {{< 𝖗 TB p18 >}}
