---
time: 202302081854
title: "what"
type: note
domain: tensors
back: "../"
---

{{< P >}}
  THE POINT OF TENSOR NOTATION, is to write equations/laws/etc in a COORDINATE 
  INVARIANT way.

  THIS IS USEFUL BECAUSE when we talk about physical laws, we're describing 
  relationships between things, real things, physical, GEOMETRIC things. 

  ∴ a notation that can express relationships between geometric objects should 
  be better. and more intuitive than (e.g.) defining a vector as just a tuple of 
  3 numbers. you want the vector to be a geometric object. something that 
  actually exists.

{{< P >}}
  {{< 𝖗 TB p6 >}} gives an example of coordinate independent physics. the 
  moment of inertia tensor I. he says "insert the body's angular velocity vector 
  ˝Ω˝ into the inertia tensor ˝I˝ and you get the body's angular momentum 
  vector, ˝J = I(Ω)˝." goes on to say "no coordinates or basis vectors are 
  needed for this law of physics, nor is any description of I as a matrix-like 
  entity with components ˝I₍ij₎˝ required."
  > see, this is what I have a problem with. it's just, that there ARE 
    coordinates and basis vectors here, right? they're implicitly there, by 
    definition, right? just because the equation doesn't physically contain them 
    doesn't mean they don't exist. for me, this equation couldn't exist without 
    there simultaneosuly existing coordinate systems and bases. you have to have 
    them, otherwise the equation is just letters on a page.
  {.question}

  {{< 𝖗 TB p6 >}}: "components are an impediment to a clear and deep 
  understanding of the laws of classical physics"
  > YES, YES. exactly. I agree. but don't you still flipping use them?
  {.methinks}
