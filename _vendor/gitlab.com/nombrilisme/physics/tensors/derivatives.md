---
time: 202302140458
title: "derivatives"
type: note
---

¶ the gradient of a tensor (field) is itself a tensor that has one higher rank. 
  if T is rank 3, then the gradient ∇T is a rank 4 tensor.
  ```
    ∇T = T₍abc;d₎ = ÷{𝝏T₍ijk₎}{𝝏x₍d₎}
  ```

¶ gradient of a vector field is a rank 2 tensor
  ```
    ∇↗A = (arg1,arg2) 
    = ↗∇ ⊗ ↗A 
    = A₍i;j₎ 
    = ÷{𝝏A₍i₎}{𝝏x₍j₎}
  ```

¶ divergence of a vector field: contract the gradient tensor
  ```
    divergence(↗A) = contract(∇↗A)
    = ↗∇ ⦁ ↗A
    = A₍i;i₎ 
    = ÷{𝝏A₍i₎}{𝝏x₍i₎}
  ```

¶ divergence of a tensor. contract wrt one of the args. ˝T₍abc;b₎˝ is the 
  divergence on the 2nd slot.

¶ laplacian:
  ```
    ∇⁽2⁾T = (↗∇ ⦁ ↗∇)T = T₍abc;dd₎
  ```

¶ the {{< ℑ LEVI CIVITA TENSOR >}} defines the cross product and curl. cross 
  product:
  ```
    ↗A × ↗B = ε(  , ↗A, ↗B) = ε₍ijk₎A₍j₎B₍k₎
  ```

  the curl:
  ```
    ↗∇ × ↗A = ε₍ijk₎A₍k;j₎
  ```
  where ˝A₍k;j₎ = 𝝏A₍k₎/𝝏x₍j₎˝.
