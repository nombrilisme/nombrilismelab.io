---
time: 202302190148
title: "notation"
type: note
---

let T be a tensor. the components of the tensor:
```
  T⟦i'j⟧ = T(e⟦j⟧,e⟦i'⟧)
```
  - the prime ' marker denotes a contravariant index. "normally" this would be 
    expressed ˝T⁽i⁾₍j₎˝.
  - define e = [e⟦1⟧, e⟦2⟧, e⟦3⟧] to be an array of basis vectors.
