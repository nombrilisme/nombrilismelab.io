---
time: 202302090349
title: "components"
type: note
---

¶ up/down index notation. {{< 𝖗 TB >}} initially says "sometimes we write the 
  indices up, sometimes down, it doesn't matter." but then he says "in CCs a 
  quantity is the same whether its index is down or up". {{< 𝖗 TB p16 >}}
  > tbd, I guess <

¶ component representations of a vector:
  ```
    ↗A = A₍i₎·↑e₍i₎
  ```
  ```
    A₍i₎ = ↗A⦁↑e₍i₎
  ```

  > you know, I'm increasingly becoming annoyed with this notation. what it 
    seems to say is: ˝↗A˝ is a vector, a geometric object, and all that. it has 
    two parts, ˝A˝ and ˝↑e˝. ˝A˝ is a 3-tuple. ˝↑e˝ is a tuple of three vectors, 
    and the index refers to whihc one. jesus. <  
  > I think you have to stop thinking of ˝A₍i₎˝ as an _index_. it can't be.  
    isn't ˝A₍i₎˝ just the ith element of the vector ˝↗A˝? ah, ok, but that's in 
    the tuple-centric view of vectors, which thorne said we're not doing. a 
    vector is a geometric object, remember. <

  ok, here's what I've concluded. in this notation, 
  - ˝↗A˝ is a vector
  - ˝A˝ is a tuple of n scalars (coordinate values)
  - ˝↑e˝ is a tuple of n basis vectors


  computationally, if you want to represent it like how it's written, I feel 
  like the only way that makes sense is:

    A = ∑{i=1:3} A.coordinate[i] · A.basis[i]

  right, so basically the "vector" A is an object containing two tuples
  - a 3-tuple of scalars (coordinate values)
  - a 3-tuple of basis vectors
  - maybe: an attribute defining the dimensionality

  struct Vector {  
    coordinate::Vector{Float64}  
    basis::Vector{BasisVector}  
  }

  > or something <

¶ component representations of a tensor T (rank 3):
  ```
    ⬀T = T₍ijk₎ ↑e₍i₎ ⊗ ↑e₍j₎ ⊗ ↑e₍k₎
  ```
  ```
    T₍ijk₎ = ⬀T(↑e₍i₎,↑e₍j₎,↑e₍k₎)
  ```

  so here, 
  - ˝⬀T˝ is a rank-n tensor
  - ˝T˝ is a n⁽n⁾ array of scalars
  - ˝↑e˝ is a tuple of dim basis vectors

  e.g. for the metric tensor, the ij'th component
  ```
    g₍ij₎ = ⬀g(↑e₍i₎,↑e₍j₎) = ↑e₍i₎⦁↑e₍j₎ = δ₍ij₎
  ```

¶ components of a tensor product: e.g. ˝⬀T(  ,  ,  ) ⊗ ⬀S(  ,  )˝. what you do 
  is you insert the basis vectors into the arguments. 
  ```
    ⬀T(↑e₍i₎,↑e₍j₎,↑e₍k₎) ⊗ ⬀S(↑e₍l₎,↑e₍m₎) = T₍ijk₎S₍lm₎
  ```

  the components of a tensor product equal the product of the components of the 
  individual tensors.

¶ consider a vector, ˝↗A˝. is a rank-1 tensor. so it can accept one argument. 
  when you insert another vector ˝↗B˝ into its argument, you get the dot 
  product. in component notation:
  ```
    ↗A(↗B) = ↗A⦁↗B = ∑{i} A₍i₎B₍i₎
  ```

  and for a rank-3 tensor, it can accept 3 arguments. when you insert these 
  arguments, you get
  ```
    ⬀T(↗A,↗B,↗C) = T₍ijk₎·A₍i₎·B₍j₎·C₍k₎
  ```

¶ contracting a tensor. suppose you have a 4th rank tensor, ˝⬀T(  ,  ,  ,  )˝. 
  you contract it on args 1 and 3. the components of the 1,3 contraction on T 
  are
  ```
    components  of  (contraction₍1,3₎(⬀T)) = T₍ijik₎
  ```
  you sum over the index i. j and k are free indices. so T₍ijik₎ is the 
  component of a rank-2 tensor.

