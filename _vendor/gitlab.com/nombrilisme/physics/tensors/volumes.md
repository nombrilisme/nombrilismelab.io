---
time: 202302140449
title: "volumes"
type: note
---

¶ a 2-volume. the area of a parallelogram with edge vectors "legs" ˝↗A, ↗B˝
  ```
    2volume = ε(↗A,↗B) = ε⟦ij⟧·A⟦i⟧·B⟦j⟧ = A⟦1⟧·B⟦2⟧ - A⟦2⟧·B⟦1⟧
  ```

¶ for a 3-volume. the volume of a parallelepiped
  ```
    3volume = ε(↗A,↗B,↗C) = ε⟦ijk⟧·A⟦i⟧·B⟦j⟧·C⟦k⟧ = ↗A⦁(↗B×↗C)
  ```
