---
time: 202302081855
title: "tensor product"
type: note
---

¶ *define* {{< ℑ TENSOR PRODUCT >}} : given 3 vectors, A, B, C, you can form a 
  rank-3 tensor, T, using the tensor product.
  ```
    T = A ⊗ B ⊗ C
  ```
  T takes 3 vectors as inputs and returns a number (like any other rank 3 
  tensor).
  ```mm
    T(v₍1₎,v₍2₎,v₍3₎)
    == A⊗B⊗C(v₍1₎,v₍2₎,v₍3₎)
    == A(v₍1₎)·B(v₍2₎)·C(v₍3₎)
    == (A⦁v₍1₎)·(B⦁v₍2₎)·(C⦁v₍3₎)
  ```

¶ the tensor product is aka the {{< ℑ OUTER PRODUCT >}}.

¶ generalises to higher dimensions. given n vectors, applying the tensor product 
  will give a rank-n tensor. 

¶ you can apply the tensor product between any tensors of arbitrary rank.

  e.g. if F is a rank 2 tensor and G is rank 3, then ˝F⊗G = H˝ is a rank 
  5 tensor.
  ```
    F⊗G(v₍1₎,v₍2₎,v₍3₎,v₍4₎,v₍5₎) = F(v₍1₎,v₍2₎)·G(v₍3₎,v₍4₎,v₍5₎)
  ```
