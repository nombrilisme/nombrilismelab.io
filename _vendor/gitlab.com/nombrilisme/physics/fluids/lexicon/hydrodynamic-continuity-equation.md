---
time: 202304041732
title: "hydrodynamic continuity equation"
type: lexicon
domain: fluids

what:
  0: |
    is essentially a statement of mass conservation:
    ```
      𝝏₍t₎ρ = -∇⦁(ρ⬀v)
    ```
---

