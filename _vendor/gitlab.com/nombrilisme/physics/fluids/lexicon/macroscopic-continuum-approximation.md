---
title: "macroscopic continuum approximation"
time: 202305181029
type: lexicon
domain: fluids

aka: 

alsoread: 

what:
  0: |
    in a large, many-particle system, when the {{< 词典 mean free path >}} of 
    particles is much smaller than typical {{< 词典 macroscopic length scales 
    >}}, you can describe the system using mean local field variables (defines a 
    mean _local_ value of a quantity, that can vary spatially and temporally).

---
