---
time: 202305020951
title: "couette flow"
type: lexicon
domain: fluids

what:
  0: |
    a viscous flow between two surfaces which are moving tangentially. the flow 
    is induced in the fluid because the relative movement of the surfaces 
    creates a shear stress on the fluid.
---
