---
time: 202304041716
title: "equation of hydrostatic equilibrium"
type: lexicon
domain: fluids

what:
  0: |
    for a fluid at rest in a gravitational field ˝⬀g˝:
    ££
      ∇⦁⬈T = ρ⬀g
    ££
    where ˝⬈T˝ is the {{< 词典 stress tensor >}}, and in this case ˝⬈T = p⬈g˝, 
    ˝ρ˝ is the {{< 词典 density >}}, and ˝⬀g˝ is the gravitational acceleration.

    if you express ˝⬀g = -∇φ˝ where ˝φ˝ is the gravitational potential, and 
    since ˝⬈T = p⬈g˝ and thus ˝∇⦁⬈T = ∇p˝, then another form of this is:
    ££
      -∇p - ρ∇φ = 0
    ££
---

