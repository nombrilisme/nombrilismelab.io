---
title: "d'arcys law"
time: 202305161501
type: lexicon
domain: fluids

aka: 

alsoread: 

what:
  0: |
    a proportionality relationship between the instantaneous flux through a 
    porous medium and the permeability.

    {{< figure DarcysLaw.jpg >}}
---
