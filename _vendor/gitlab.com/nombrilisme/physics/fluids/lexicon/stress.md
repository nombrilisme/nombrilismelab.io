---
title: "stress"
time: 202305181124
type: lexicon
domain: fluids

aka: 

alsoread: 
  - https://en.wikipedia.org/wiki/Stress_(mechanics)

dimensions: |
  ˝[σ] = L⁻¹MT⁻²˝

units: |
  (SI) ˝Pa = kg m⁽-1⁾ s⁽-2⁾˝

what:
  0: |
    a quantity that describes _all the internal forces within a body that cause 
    it to deform_. mathematically, has the same units as pressure: a force per 
    unit area.

    not to be confused with {{< 词典 strain >}}, which describes the deformation 
    itself.

  1: |
    describes the internal forces that neighbouring particles exert on each 
    other in a continuous material that (may) result in deformation.


---
