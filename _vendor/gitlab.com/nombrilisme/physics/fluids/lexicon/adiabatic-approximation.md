---
title: "adiabatic approximation"
time: 202305120455
type: lexicon
domain: fluids

aka: 

alsoread: 

what:
  0: |
    an _approximately_ adiabatic process, e.g. a chemical reaction (or whatever) 
    where the process itself happens so fast that no (i.e. a negligible amount 
    of) heat is exchanged with the environment.
---
