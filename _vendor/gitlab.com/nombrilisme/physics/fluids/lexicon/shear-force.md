---
title: "shear force"
time: 202305060423
type: lexicon
domain: fluids

aka: 

alsoread: 

what:
  0: |
    a force that acts on one part of a body in a certain direction, and another 
    part in the opposite direction.

    (as opposed to the case where forces are aligned with each other 
    (collinear): then the're just "ordinary" tension or compression forces.
---
