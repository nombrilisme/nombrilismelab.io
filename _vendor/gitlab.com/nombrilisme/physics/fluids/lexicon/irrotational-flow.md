---
title: "irrotational flow"
time: 202305161513
type: lexicon
domain: fluids

aka: 

alsoread: 

what:
  0: |
    a flow with zero vorticity everywhere.
---
