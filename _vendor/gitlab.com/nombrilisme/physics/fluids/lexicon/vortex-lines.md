---
title: "vortex lines"
time: 202305151133
type: lexicon
domain: fluids

aka: 

alsoread: 

what:
  0: |
    the set of curves (_integral curves_, tbs) defined by solving the following 
    equation for the parameter ˝λ˝:
    ££
      ⌠d⬀x⧸dλ⌡ = ⬀ω
    ££
    where ˝⬀ω = ⬀ω(⬀x,t)˝ is the {{< 词典 vorticity >}} field.

    analagous to magnetic field lines.
---
