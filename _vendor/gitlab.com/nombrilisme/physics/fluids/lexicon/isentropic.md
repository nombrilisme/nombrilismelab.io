---
time: 202304181718
title: "isentropic"
type: lexicon
domain: fluids

what:
  0: |
    an isentropic process: one that's both {{< 词典 adiabatic >}} and 
    {{< 词典 reversible >}}.
---

