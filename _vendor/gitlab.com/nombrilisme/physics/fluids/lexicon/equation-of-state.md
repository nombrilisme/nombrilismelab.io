---
time: 202304041731
title: "equation of state"
type: lexicon
domain: fluids

what:
  0: |
    an equation that relates changes in ρ to changes in p.

    in a {{< 词典 perfect fluid >}}, the EOS is ρ = const.

    in a {{< 词典 barotropic fluid >}}, ρ is a function of p.
---


