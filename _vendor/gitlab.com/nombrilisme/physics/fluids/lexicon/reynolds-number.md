---
title: "reynolds number"
type: lexicon
domain: fluids


what:
  0: |
    a dimensionless number used to quantify/compare different flows, defined:
    ```
      Re = ⌠LV⧸ν⌡
    ```
    where ν is the {{< 词典 viscosity >}}, V is the flow's {{< 词典 
    characteristic velocity >}} and L is the {{< 词典 characteristic length 
    scale >}}.

    flows with Re < 1 are dominated by viscosity.

    flows with Re > 1 can be _influenced_ by viscosity, especially when it acts 
    near boundaries, BUT the viscous stresses are negligible over most of the 
    flow's volume

alsoread:
- (numberphile) https://inv.riverside.rocks/watch?v=wtIhVwPruwY
---
