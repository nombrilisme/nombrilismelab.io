---
title: "bulk modulus"
time: 202305171421
type: lexicon
domain: fluids

aka: 

alsoread: 

what:
  0: |
    quantifies the compressive resistance of a material:
    ££
      K = -V ⌠dp⧸dV⌡
    ££
    where ˝p˝ is pressure and ˝V˝ is volume.
---
