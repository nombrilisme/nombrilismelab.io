---
title: "euler equation"
time: 202305120525
type: lexicon
domain: fluids

aka: 

alsoread: 

what:
  0: |
    an equation of motion for the fluid that you can derive from conservation 
    laws:  

    ££ D₍t₎↗v = 𝝏₍t₎↗v + (↗v⦁∇)↗v = -÷1ρ·∇p + ↗g ££
---
