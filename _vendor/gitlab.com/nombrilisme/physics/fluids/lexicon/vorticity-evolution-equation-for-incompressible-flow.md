---
title: "vorticity evolution equation for incompressible flow"
time: 202305120900
type: lexicon
domain: fluids

aka: 

alsoread: 

what:
  0: |
    is:

    ££ ₯₍t₎⬀ω = -⌠∇p × ∇ρ⧸ρ²⌡ + ν∇²⬀ω ££

    where ˝⬀ω = ∇×⬀v˝, and the flow is assumed incompressible, ˝∇⦁⬀v = 0˝, and 
    ˝₯˝ is the {{< 词典 fluid derivative >}}.
---
