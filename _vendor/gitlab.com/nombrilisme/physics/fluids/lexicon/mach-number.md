---
time: 202304181354
title: "mach number"
type: lexicon
domain: fluids

dimensions: dimensionless

what:
  0: |
    mach number is:
    ```
      M = ⌠v₍upstream₎⧸v₍sound₎⌡
    ```

    the mach number is dimensionless. this is a useful quantity in flows where 
    the sound speed is high enough that the incompressibility approximation is 
    invalid.

---
