---
title: "navier stokes equation"
time: 202305121156
type: lexicon
domain: fluids

aka: 

alsoread: 

what:
  0: |
    ££
      ρ Ð₍t₎⬀v = -∇p + μ∇⁽2⁾⬀v + ρ⬀F
    ££

  1: |
    ££
      ρ Ð₍t₎⬀v = -∇p + ∇⦁⬈T + ρ⬀F
    ££
---
