---
title: "laplacian"
time: 202305161443
type: lexicon
domain: fluids

aka: 

alsoread: 
- https://en.wikipedia.org/wiki/Laplace_operator

what:
  0: |
    denoted ˝Δ˝, the laplacian is the divergence of the gradient of a function 
    ˝f˝:
    ££
      Δf = ∇²f = ∇⦁∇f  
    ££

    is a twice-differentiable, real-valued function.
---
