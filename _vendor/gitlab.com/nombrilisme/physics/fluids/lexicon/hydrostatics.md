---
time: 202304041659
title: "hydrostatics"
type: lexicon
domain: fluids

what:
  0: |
    the theory of fluids at rest, i.e. when the {{< 词典 flow velocity >}} 
    v(x,t) is zero everywhere.  

---
