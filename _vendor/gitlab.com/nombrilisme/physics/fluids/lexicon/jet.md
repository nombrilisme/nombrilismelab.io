---
title: "jet"
time: 202305181214
type: lexicon
domain: fluids

aka: 

alsoread: 

what:
  0: |
    a stream of high-momentum fluid flow that's projected (from some kind of 
    source) into a surrounding medium. _high momentum_ means higher than the 
    surrounding medium, in order to form a distinct flow.
---
