---
time: 
title: hydrostatics
type: note
domain: fluids
---

# HYDROSTATICS: FLUIDS AT REST
--------------------------------------------------------------------------------

{{< 𝖉𝖊𝖋 HYDROSTATICS >}}
  the theory of liquids at rest.
{{< /𝖉𝖊𝖋 >}}

¶ in hydrostatics, ignore viscous effects.

¶ when liquids are at rest, there are no shear forces.

¶ basic law of hydrostatics: stresses are always normal to any surface inside 
  the fluid.

¶ pressure is the same in all directions.
  - for a given (arbitrary) surface in the fluid, the normal force on that 
    surface per area is the pressure. 
  - the pressure stress must be the same in all directions because there is no 
    shear on any plane in a static fluid (should prove this)

# PRESSURE VARIES WITH HEIGHT
--------------------------------------------------------------------------------

¶ in a static fluid on the earth's surface, pressure varies with height, because 
  of the weight of the fluid.
  - assume density ˝ρ˝ is constant
  - let the pressure at some arbitrary reference level be ˝p₍0₎˝
  - the pressure at height ˝h˝ above the reference point is:
    ```
      p = p₍0₎ - ρgh
    ```

¶ the quantity ˝p + ρgh˝ is a constant in a static fluid.

# PRESSURE ON A SMALL CUBE OF WATER
--------------------------------------------------------------------------------

link: {{< ex pressure on an infinitesimal cube of liquid >}}

# EQUATION OF HYDROSTATIC EQUILIBRIUM
--------------------------------------------------------------------------------

¶ here:
  ```
    -∇p - ρ∇φ = 0
  ```
  - in general it has no solution
  - if the density varies arbitrarily, there's no way for the forces to balance, 
    and the fluid can't be in static equilibrium. convection currents will 
    start.
  - lookie here: the pressure term is a "pure" gradient. the other term has the 
    ˝ρ˝ prefactor. if it's a variable, this term _isn't_ a "pure" gradient. 
  - only when ˝ρ˝ is constant will the second term be a pure gradient.
  - then the equation has a solution
  ```
    p + ρφ = const
  ```
  - alternatively, ˝ρ˝ could also be a function of ˝p˝. this scenario also gives 
    hydrostatic equilibrium, and the equation will have a solution.

¶ another (thorne's) formulation of this: for a fluid at rest in a gravitational 
  field ˝↗g˝, the equation of hydrostatic equilibrium is the same as that for 
  (elastostatic) equilibrium with a vanishing shear stress, so
  ```
    T = P↗g
  ```
  and
  ```
    ∇⦁T = ∇P = ρ↗g
  ```
  
  you can express ˝↗g˝ as a potential: ˝↗g = -∇φ˝. and then you get feynman's 
  form.
