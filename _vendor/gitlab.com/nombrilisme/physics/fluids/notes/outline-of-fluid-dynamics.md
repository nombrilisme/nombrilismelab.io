---
time: 202302060328
title: "Quick Outline of Fluid Dynamics"
type: note
domain: fluids
font: concrete
showsummary: T
---

The configuration of a fluid is characterised by three continuous fields: 
density ˝ρ(⬀x,t)˝, pressure ˝p(⬀x,t)˝, and velocity ˝⬀v(⬀x,t)˝.

To describe the dynamics of a a fluid, what you need to do establish the what 
relationship is between changes in the fluid's density and changes in its 
pressure.

The "usual" place to start is {{< 词典 hydrostatics >}} -- the theory of fluids 
at rest. By considering the forces at work in a fluid (e.g. {{< 词典 shear >}}, 
{{< 词典 stress >}}, external fields, and related mathematical objects that help describe 
them, like the {{< 词典 stress tensor >}}) you can derive the conditions for 
{{< 词典 hydrostatic equilibrium >}}.

To "transition" to {{< 词典 hydrodynamics >}} -- fluids in motion -- you start 
by defining a flow field, and by considering the flux of the flow field, you can 
derive some {{< 词典 conservation laws >}} for energy, momentum, and mass.

And, thence, two important "simplifications" emerge. You motivate these 
simplifications by decomposing the stress tensor. For "many" fluids you will get 
two terms:
- an isotropic pressure term
- A viscous term linear in the rate of shear (i.e. the velocity gradient).

(1) the {{< 词典 perfect fluid >}}. IF the viscous stress is negligble, AND heat 
conductivity in the fluid is negligible, then you can call it a perfect fluid.
Deriving the conservation laws under these assumptions leads to the {{< 词典 
"euler equation" >}} (an equation of motion) and {{< 词典 "bernoulli's principle" 
>}} for steady flow in a pipe.

(2) the {{< 词典 incompressible approximation >}}. IF the fluid's changes in 
density are small, AND the velocites are « the speed of sound, then you can say 
the fluid is incompressible. However modelling a fluid as incompressible doesn't 
imply that it actually is -- just that the relative changes in density are small 
in the scale or reference frame you're observing the fluid (e.g. it's often true 
that gravity is too _weak_ to compress a fluid noticeably). The incompressible 
approximation is generally fine for liquids with large {{< 词典 bulk modulus >}} 
and gases.

In general, though, viscous stresses are _not_ negligible. When the perfect 
fluid assumptions _don't_ hold, you need modify the euler equation to account 
for viscous stresses and heat conduction. This leads to the {{< 词典 navier 
stokes equation >}} (a more general equation of motion for a fluid).

Next, introduce {{< 词典 "vorticity" >}}, the curl of the flow field. This opens 
up treatment of rotating fluids. Using the definition of vorticity you can 
mathematically transforming the navier stokes equation to the {{< 词典 vorticity 
evolution equation for incompressible flow >}}. 

⋮

[tbc]
