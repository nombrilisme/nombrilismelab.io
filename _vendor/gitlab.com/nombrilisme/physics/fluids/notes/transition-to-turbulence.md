---
time: 202305021336
title: "transition to turbulence"
type: note
---

concerning: how a viscous flow becomes turbulent.

# rotating couette flow between two concentric cylinders
--------------------------------------------------------------------------------

the inner cylinder rotates the the outer one stays still.

you decrease the viscosity of the liquid gradually by heating it (causing Re to 
increase).

at low Re numbers, the equilibrium flow is stable, stationary, and azimuthal, 
i.e. strictly in the φ direction (in the direction of the rotating cylinder).

at very hi Re numbers, the flow is unstable.

there's a critical value of reynolds number at which the flow becomes unstable 
because of the growth of small perturbations.

these perturbations cause a transition to a new stationary equilibrium that 
involves poloidal circulation (quasi circular motions in the r and z directions: 
TAYLOR ROLLS)

so what happened? there WAS an equilibrium with a high degree of symmetry, and 
that became unstable, and a new lower symmetry stable equilibrium was 
established. in the transition, you lost translational invariance along the 
cylinder axis. an example of the BIFURCATION OF EQUILIBRIA.

when you increase the reynolds number more, you see this happen again. there's a 
second critical reynolds number, and a second bifurcation of equilibrium. now, 
the azimuthally smooth taylor rolls become unstable, and get replaced by new 
azimuthally wavy taylor rolls. the new equilibrium has even lower symmetry (lost 
rotational invariance).

there's a fundamental frequency ˝f₍1₎˝ in the fluid's velocity ˝⬀v(⬀x,t)˝ as the 
wavy taylor rolls circulate around the central cylinder.

when you increase Re more, there's a third critical reynolds number, and another 
bifurcation. the taylor rolls get a second set of waves, superimposed on the 
first, with a corresponding new fundamental frequency ˝f₍2₎˝ that's got nothing 
to do with ˝f₍1₎˝. 

COOL THING: in the energy spectrum you see various harmonics of ˝f₍1₎˝ and 
˝f₍2₎˝ and sums and differences of the two. KT p827

it becomes harder and harder to actually SHOW what happens next as the reynolds 
number increases. finite size effects, etc. 

but we all know what will happen: the sequence of bifurcations continue, with 
ever-decreasing intervals of reynolds number ΔRe between them. they form a maze 
more and more complex of frequencies, harmonics, sums, differences, so we just 
see it as turbulence.

# route to turbulence in rotating couette flows: 
--------------------------------------------------------------------------------

as you increase the Re number (or a control parameter), first you get 
oscillations with one fundamental frequency AND its harmonics, 

THEN you get a second frequency and ITS harmonics, AND sums and differences of 
the first and second frequencies.

THEN, SUDDENLY, chaos starts. 

BUT the chaos is not being produced by a complicated superposition of 
frequencies: it's CLEARLY FUNDAMENTALLY DIFFERENT.


# the feigenbaum sequence / logistic equation
--------------------------------------------------------------------------------

is
```
  x₍n+1₎ = 4ax₍n₎(1-x₍n₎)
```
where ˝a˝ is a fixed control parameter.

you can compute feigenbaum sequences, i.e. different ˝x₍1₎, x₍2₎, ...˝ for 
different values of ˝a˝. 

what you find is there are critical parameters ˝a₍1₎, a₍2₎, ...˝ where the 
character of the sequence changes substantially. 

when ˝a < a₍1₎˝, the sequences asymptotes to a stable fixed point. 

for ˝a₍1₎ < a < a₍2₎˝ the sequence asymptotes to stable periodic oscillations 
between two fixed points. 

if you increase ˝a˝ more, so ˝a₍2₎ < a < a₍3₎˝, the sequence is a periodic 
oscillations between 4 fixed points. i.e. the period of oscillation has doubled.

PERIOD DOUBLING. it happens again, and again.

period doubling increases with shorter and shorter intervals of ˝a˝ until at 
some value ˝a₍∞₎˝ the period becomes infinite, and the sequence doesn't appear. 
THIS IS CHAOS.

period doubling is an alternative route to chaos, different from the one 
frequency, two frequencies, etc route to chaos for the rotating couette flow.

FD chaos can be done with convection. section 18.4.
