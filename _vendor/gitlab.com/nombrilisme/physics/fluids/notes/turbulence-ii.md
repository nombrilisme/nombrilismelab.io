---
time: 202304251117
title: "turbulence ii"
type: note
---

# STRONG vs WEAK TURBULENCE
--------------------------------------------------------------------------------

on the so-called "strength" of turbulence: we're describing turbulence in terms 
of interacting eddies.

define: τ₍❄₎, the timescale for a large eddy to feed most of its energy to a 
small eddy.

define: τ, the turnover time (rotation period) of the large eddy.

define: WEAK TURBULENCE, when τ₍❄₎ is long compared to τ.

this weak turbulence formalism is essentially an expansion in τ/τ₍❄₎.

in most fluids the turbulence you see is strong, i.e. has τ/τ₍❄₎ ∼ 1, so the 
eddy loses its identity in about ONE turnover time. the following 
semiquantitative analysis of weak turbulence is only partially accurate in 
strong turbulence.

# WEAK TURBULENCE FORMALISM
--------------------------------------------------------------------------------

## ASSUMPTIONS

- gravity negligible
- highly subsonic flow, so incompressible

## EQUATIONS

start with the usual equations for incompressible flow sans gravity.

incompressibility:
```
  ∇⦁⬀v = 0
```

time-dependent navier stokes:
```
  ρ 𝝏₍t₎(⬀v) + ∇⦁(ρ ⬀v⊗⬀v) = -∇p + ρν∇²⬀v
```
or
```
  𝝏₍t₎(⬀v) + (⬀v⦁∇)⬀v = -÷1ρ ∇p + ν∇²⬀v
```

these are four scalar equations for four unknowns: p(⬀x,t) and three components 
of the velocity field ⬀v(⬀x,t). ρ and ν are constants.

you have to transform these to the weak-turbulence versions

## TIME-AVERAGING THE EQUATIONS

split the velocity field ⬀v(⬀x,t) and pressure field p(⬀x,t) into steady parts 
and fluctuating parts:
```m
  ⬀v = ⟨⬀v⟩ + δ⬀v
  p = ⟨p⟩ + δp
```
where ⟨⬀v⟩ and ⟨p⟩ are the time averages of ⬀v and p, and δ⬀v and δp are the 
differences between the exact values and the average values.

the time averaged variables ⟨⬀v⟩ and ⟨p⟩ are governed by time-averaged versions 
of the incompressibility and NS equations.

the incompressibility equation is linear so there is no correlation/coupling 
between the averaged ⬀v and the fluctuation term
```
  ∇⦁⟨⬀v⟩ = 0
```
(time averaged incompressibility)

the NS equation has a nonlinear inertial term, ˝∇⦁(ρ ⬀v⊗⬀v) = ρ(⬀v⦁∇)⬀v˝. this 
nonlinear term _does_ have a coupling between the time-averaged quantities and 
the fluctuations 
```
  ρ(⟨⬀v⟩⦁∇)⟨⬀v⟩ = -∇⟨p⟩ + ρν∇²⟨⬀v⟩ - ∇⦁⬈T₍R₎
```
(time-averaged navier stokes equation)

where ˝⬈T₍R₎˝ is the REYNOLDS STRESS TENSOR:
```
  ⬈T₍R₎ = ρ ⟨δ⬀v ⊗ δ⬀v⟩
```

the reynolds stress tensor is a driving term in the t-a NS equation. it couples 
turbulence to the time-averaged flow. it's the term that allows the fluctuating 
part of the flow to _act on_ the time-averaged flow.

# REYNOLDS STRESS TENSOR
--------------------------------------------------------------------------------

you can think of it as an extra part of the total stress tensor. similar to the 
gas pressure in kinetic theory P = ÷13 ρ⟨v²⟩ where v is the molecular speed.

˝⬈T₍R₎˝ will be dominated by the largest eddies present.

˝⬈T₍R₎˝ can be anisotropic, esp. when the largest scale turbulent velocity 
fluctuations are distorted by interaction with an averaged shear flow.


# REYNOLDS STRESS FOR STATIONARY HOMOGENEOUS TURBULENCE
--------------------------------------------------------------------------------

if turbulence is both stationary AND homogeneous (as is the case for the kolmogorov 
spectrum): then the reynolds stress tensor is
```
  ⬈T₍R₎ = p₍R₎⬈g
```
p₍R₎ is the reynolds pressure (independent of position) and ˝⬈g˝ is the metric, 
˝g⟦ij⟧ = δ⟦ij⟧˝. 

in this kind of flow, the turbulence exerts no force density on the mean flow, 
so the term ˝∇⦁⬈T₍R₎ = ∇p₍R₎˝ will vanish in the t-a NS equation.

near the edge of a turbulent region (edge of a turbulent wake, jet, or boundary 
layer) the turbulence is inhomogeneous. in this case it has a noticeable 
influence on the time-independent averaged flow.

# CORRELATION FUNCTION
--------------------------------------------------------------------------------

the reynolds stress tensor is the tensorial correlation function (aka the 
autocorrelation function) of the velocity fluctuation field at zero time delay, 
multiplied by density ρ.

the RST also involves the temporal cross-correlation function of the components 
of the velocity fluctuation, 
```
  ⟨δv₍x₎(⬀x,t) δv₍y₎(⬀x,t)⟩
```

it's possible to extend the weak turbulence formalism so that it probes the 
statistical properties of turbulence more deeply. you can do this with 
correlation functions with finite time delays and correlation functions of 
velocity components at two different points in space simultaneously - e.g. in 
cosmology, solution to the perturbation equations.

discussed in 15.4.4: the fourier transforms of these correlation functions give 
the spatial and temporal spectral densities of the fluctuating quantities.

so: the structure of time-averaged flow is governed by the time-averaged 
incompressibility and NS equations. 

tout la meme, the fluctuating part of the flow is governed by fluctuating 
incompressibility and navier stokes equations. see: exercise 15.5. 
- it exhibits the weak turbulence formalism.
- and shows how it can be applied to spatial energy flow in a 2d turbulent wake, 
  fig 15.7
