---
time: 202302170247
title: "the Meaning of the Advective Derivative"
type: problem
source:
font: concrete
thumb: advective-derivative.svg
what: |
  In a fluid, ˝⬀a ≠ ⌠𝝏⧸𝝏t⌡⬀v˝, i.e. you can't write the acceleration of the flow 
  as a partial derivative of velocity with respect to time. Instead, the 
  acceleration is given by the {{< ℑ advective derivative >}} of velocity:
  ```
    ⬀a = ⌠𝝏⧸𝝏t⌡⬀v + (∇⦁⬀v)⬀v
  ```
  Why?
---

First establish what you're actually trying to find: _an expression for the 
acceleration of the flow_. Acceleration is defined the rate of change of 
velocity wrt time.

As a reminder, in fluid dynamics the velocity ˝⬀v˝ is a _flow field_. It 
describes a vector field defined over space _and_ time, ˝⬀v = ⬀v(⬀x,t)˝. This is 
significant. ˝⬀v˝ has both spatial and temporal components.

Physically speaking, the _partial_ derivative of velocity wrt time, ˝⌠𝝏⧸𝝏t⌡⬀v˝, 
calculates the rate of change of velocity in _fixed space:_
```
  ⌠𝝏⧸𝝏t⌡⬀v(⬀x,t) = 𝓵{Δt→0} ⌠⬀v(⬀x,t+Δt) - ⬀v(⬀x,t) ⧸ Δt⌡ \bigg|₍⬀x=const₎
```
(a _partial_ derivative ˝⌠𝝏⧸𝝏t⌡˝ means the derivative is wrt ˝t˝ only, holding 
the other variables constant).

Now, go back to your fluid. Suppose you follow the movement of a _single 
particle_ in the fluid over a time interval ˝Δt˝. You'll find that the 
individual velocity vector ˝⬀v˝ of this particle is:
```m
  @timeͺt:
  ͺͺͺ⬀v(x,y,z,t) = %%< v₍x₎ ,, v₍y₎ ,, v₍z₎ >%%

  @timeͺt+Δt:
  ͺͺͺ⬀v(x+Δx,y+Δy,z+Δz,t+Δt) = %%< v₍x₎+Δv₍x₎,,v₍y₎+Δv₍y₎,,v₍z₎+Δv₍z₎>%%
```
where ˝Δx = v₍x₎Δt˝, ˝Δy = v₍y₎Δt˝, ˝Δz = v₍z₎Δt˝. It should be obvious looking 
at this why the rate of change of velocity can't just be equal to ˝⌠𝝏⧸𝝏t⌡⬀v˝.

{{< figure advective-derivative.svg 10 >}}

When you compare the particle at times ˝t˝ and ˝t+Δt˝, both the temporal _and 
spatial_ inputs to its velocity function have changed. This is a general 
property of _flow_ fields (as opposed to ordinary "garden-variety" vector 
fields). The particle moves with the flow. _And the flow moves in time_. The 
"embedding space" of the particle moves with time --- this is what it means for 
the particle to be in a flow.

And thence, the problem: when you compute the partial time derivative of a 
variable that's also defined spatially, what you're really doing is: choosing a 
fixed reference frame in space, then observing the value of the variable at a 
particular time, then waiting a small time interval, then observing the variable 
again _in the same spatial reference frame_. The system may move, but space 
itself does not. 

But this isn't true in a fluid. The flow field ˝⬀v(⬀x,t)˝ _doesn't_ describe a 
set of spatially fixed vectors in the system. In the sense that the velocity 
vectors are "attached" to something, you should think of them as attached to the 
individual particles/molecules that comprise the flow. So in order to properly 
compute the rate of change of velocity, you have to account for the variation of 
space as well as time.

Another way to think about this: ˝⬀v˝ is a function, of the spatial variables 
˝⬀x˝ and time ˝t˝. In this system you'd say that space and time are _correlated_ 
variables. You know what happens next: when the dependent variables of a 
function are correlated, the derivative of the function wrt one of the variables 
must include the effect of its correlation with other variables (see 
[stats103](http://stats103.com/joint-variability/)).

Consider the total derivative of ˝⬀v = ⬀v(x,y,z,t)˝:
```
  d⬀v = ⌠𝝏⬀v⧸𝝏x⌡dx + ⌠𝝏⬀v⧸𝝏y⌡dy + ⌠𝝏⬀v⧸𝝏z⌡dz + ⌠𝝏⬀v⧸𝝏t⌡dt
```

So the total time derivative is
```mm
  ⌠d⬀v⧸dt⌡ 
  &= ⌠𝝏⬀v⧸𝝏x⌡⌠dx⧸dt⌡ + ⌠𝝏⬀v⧸𝝏y⌡⌠dy⧸dt⌡ + ⌠𝝏⬀v⧸𝝏z⌡⌠dz⧸dt⌡ + ⌠𝝏⬀v⧸𝝏t⌡
  &= ⌠𝝏⬀v⧸𝝏x⌡v₍x₎ + ⌠𝝏⬀v⧸𝝏y⌡v₍y₎ + ⌠𝝏⬀v⧸𝝏z⌡v₍z₎ + ⌠𝝏⬀v⧸𝝏t⌡
  &= (∇⦁⬀v)⬀v + ⌠𝝏⬀v⧸𝝏t⌡
```

So
```
  ⬀a = d₍t₎⬀v = 𝝏₍t₎⬀v + (∇⦁⬀v)⬀v
```

The partial derivative in time describes the "intrinsic" variation in ˝⬀v˝ in 
the absence of any flow, and the nonlinear advection term describes the 
transport of the velocity field by the flow.
