---
time: 202305021107
title: "the structure of a turbulent wake behind a cylinder"
type: problem
colour: overcast
source: 
thumb: TurbulentWakeBehindACylinder.svg
wide: true
---

{{< figure TurbulentWakeBehindACylinder.svg >}}
