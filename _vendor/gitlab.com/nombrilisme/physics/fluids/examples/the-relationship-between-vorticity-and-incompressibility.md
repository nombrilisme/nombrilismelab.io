---
time: 202304100245
title: "The relationship between Vorticity and Incompressibility"
type: problem
#font: concrete
source: TB142
what: |
  {{< 𝖗 TB ex14.2 >}} Sketch the streamlines for these stationary 2d flows, 
  determine whether the flow is compressible, and evaluate its vorticity.

  (1) - cartesian
  ```
    v₍x₎ = 2xy ͺͺͺ v₍y₎ = x²
  ```

  (2) - cartesian
  ```
    v₍x₎ = x² ͺͺͺ v₍y₎ = -2xy
  ```

  (3) - polar
  ```
    v₍ω₎ = 0 ͺͺͺ v₍φ₎ = ω
  ```

  (4) - polar
  ```
    v₍ω₎ = 0 ͺͺͺ v₍φ₎ = ω⁻¹
  ```
---
