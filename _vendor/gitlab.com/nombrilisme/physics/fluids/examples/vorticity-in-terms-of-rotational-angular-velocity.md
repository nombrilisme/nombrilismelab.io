---
time: 202304161509
title: "vorticity in terms of rotational angular velocity"
type: problem
domain: fluids
source: 
---

but just what is vorticity? 

take a small fluid element. think about how this fluid element moves over a 
short period of time ˝dt˝.

each "bit" of fluid in the fluid element has a small displacement in this 
interval. let the displacement of this bit be ˝⬀ε = ⬀v dt˝.

the gradient of the displacement ˝⬀ε˝ will be comprised of an expansion, 
rotation, and shear.

the angle of rotation (a vector quantity) is
```
  ⬀φ = ÷12·∇×⬀ε
```

the time derivative of the angle of rotation is
```
  Ð₍t₎↗φ = ÷12·Ð₍t₎↗ε = ÷12·∇×↗v
```
note this is just the fluid element's rotational angular velocity. so basically, 
you can conclude that the vorticity is just twice the angular velocity of 
rotation of a fluid element.

the equation of motion becomes
```
  𝝏₍t₎{⬀v} + ↗ω×↗v + ÷12 ∇v⁽2⁾ = -÷1ρ ∇p - ∇φ
```

if vorticity in the flow is zero everywhere, you say it's an {{< 词典 
irrotational flow >}}. why?  recall: the circulation of a vector field around 
any closed loop. the {{< 词典 circulation >}} around a closed loop in a fluid is 
the line integral of the fluid velocity at a particular instant of time around 
that loop
```
  (circulation) = ∮ ⬀v⦁d⬀s
```

