---
time: 202304181227
title: "blasius profile for a laminar boundary layer"
type: problem
source: 
---

tldr: 

the thickness of the boundary layer, as measured distance a ˝x˝ downstream from 
the start of the boundary layer, is
```
  3δ = 3(⌠νx⧸v₍ups₎⌡)¹𝄍²
```

when ˝Re˝ is sufficiently large, i.e. ˝Re = ⌠v₍ups₎d⧸ν⌡ ∼ 3⋄10⁵˝ or ˝Re₍δ₎ ∼ 
√{Re₍d₎} ∼ 500˝ for flow past a cylinder, the boundary layer becomes turbulent.

[todo: typeset derivation]
