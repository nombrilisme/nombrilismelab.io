---
title: 语言学
type: term
domain: 中文
---

# morphemes
--------------------------------------------------------------------------------

* __free morpheme:__ an atomic unit, can stand alone
	+ "factual", fact-u-al, "fact" is a free morpheme, "u" and "al" are bound morphemes
	+ "中国人" | Zhōng-guó-rén, composed of entirely free morphemes [which is typical of chinese. see: analytic languages]

* __bound morpheme:__ an atomic unit, only appears as part of larger structures
	+ _derivational morpheme:_ modifies the semantic meaning or part of speech of the word, e.g. "sad-_ness_", where the bound morpheme "-ness" changes word from an adjective to a noun
	+ _inflectional morpheme:_ modifies the tense/aspect/person/number of a verb or number/gender/case of a noun/adjective/pronoun, e.g. in english adding "-s" to the end of words, or all the verb conjugations in french and other european languages

# analytic vs synthetic languages
--------------------------------------------------------------------------------

* __analytic language:__ relies on word order and helper words [particles/prepositions/...] to convey relationships between words. has few/no inflectional morphemes 
	+ since chinese has mostly unbound morphemes [separate words], it is very analytic (few inflectional morphemes) 
	+ english is also analytic (but less so than chinese). it also relies on helper words/grammatical markers to convey relations between words. but it also uses inflections, so it has synthetic character too. 

* __isolating language:__ language with a low morpheme-to-word ratio (any kind of morpheme). an isolating language is by definition an analytic language, since it must have few/no inflectional morphemes. but not all analytic languages are necessarily isolating languages
	+ chinese has a high morpheme-to-word ratio since it has many compound words. but since it uses mostly unbound morphemes and has few inflectional morphemes, it's analytic, very analytic
	+ analytic = good?

* __synthetic language:__ rely on inflections [aka agglutinations] to convey relationships between words
	+ _derivational synthesis:_ where morphemes of different types [nouns, verbs, affixes] are joined to create new words. e.g. german [those massive words] 
	+ _relational synthesis:_  where root words are joined to bound morphemes. e.g. italian, "communicandovele", communic-ando-ve-le, communicate + gerund suffix + you pl. + those fem. pl.¸ or hungarian, "szeretlek", szeret-lek, love + I [reflexive] you 

# logograph(y?)
--------------------------------------------------------------------------------

* __logogram:__ a single written character representing a morpheme. 
* chinese has a logographic writing system: there are thousands of logograms to represent all the words in the language
