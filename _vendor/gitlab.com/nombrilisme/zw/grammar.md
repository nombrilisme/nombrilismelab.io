---
title: 语法
type: term
domain: 中文
---

语法 / yufa / grammar

# parts of speech
--------------------------------------------------------------------------------

## noun 

inflected (if at all) for: case

## verb 
--------------------------------------------------------------------------------

inflected (if at all) for: tense/aspect/person/mood

## participle 
--------------------------------------------------------------------------------

a verb form. an example of inflecting a verb [afaik these don't explicitly exist 
in 中文]
  - _present participle:_ indicates an ongoing action
    - english: infinitive + "ing"; "drinking"
    - français: drop the "-ons" of the nous conjugation of the verb and add 
      "-ant"; "ayant", "voulant"
    - 中文: 莫! 
  - _past participle:_ indicates a wholly completed action in the past action
    - english: "ate"
    - français: "allé"
    - 中文: 莫! 

## auxiliary verb 
--------------------------------------------------------------------------------

a helper verb to express aspect/tense/voice 
	- "I _have_ taken the three cups"
	- "J'_ai_ prendu les tasses"
	- don't exist in 中文?

## article 
--------------------------------------------------------------------------------

marker of definiteness
	+ "the", "a", 
	+ "le/la/les", "un/une/des"
	+ status in 中文?

## pronoun 
--------------------------------------------------------------------------------

sub for noun/noun phrase

## preposition 
--------------------------------------------------------------------------------

relates words to each other
	+ "in", "of", "at"
	+ "dans", "dedans", "de"
	+ 在 

## adverb 
--------------------------------------------------------------------------------

verb/adverb/adjective modifier
	+ "quickly", 
	+ "rapidement"

## conjunction 
--------------------------------------------------------------------------------

syntactic connection between clauses
	+ "and", "but" + "et", "mais"

## adverbial 
--------------------------------------------------------------------------------

dependent clause that acts like an adverb
	+ "in order to"
	+ "afin que/de"

## clitic 
--------------------------------------------------------------------------------

there's a very technical definition of what this is, that I can't [yet] really 
fathom. it's more nuanced than just contractions
  + in english there are _enclitics:_ "I'm", "it's", "we've"
  + in french there are proclitics: "je _me_ lève", "je _t_'aime", representing 
    the reflexive verb aspect 

# coverbs
--------------------------------------------------------------------------------

verbs that have a "kind of" prepositional function. don't really exist in 
anglais et français. but they crop up in languages with __serial verb 
construction__ or __verb stacking__ [stringing together two verbs/verb 
phrase]---such as 中文!
  + coverbs are followed by nouns (objects) to form a coverb clause, which can 
    express relationships that would in anglais/français be represented by 
    prepositions
  + 我从家去东京 -> [literal] I start home go tokyo -> [actual] "I go to tokyo 
    from home"
  + where the verb 从[cōng] means "to start", but in this case acts like a kind 
    of preposition-hybrid and means something more like "I [start _from_] home 
    go tokyo"
  + they get more complex: this webpage is pretty comprehensive: 
    www.ctcfl.ox.ac.uk/Grammar exercises/Co-verbs.htm

# determiner 
--------------------------------------------------------------------------------

appears with a noun/noun phrase to express some property it has (e.g. quantity, 
possession)
  + _articles_
  + _demonstratives:_
    + "this", "that"
    + "ce", "celle", "ci", "celle-ci", "cela"
    + 这, 那
  + _possessive determiner:_
    + "my", "her"
    + "mon", "sa"; the gender agrees with the thing owned, not the owner, "elle 
      a tuée son [masc] petit-ami"
    + 中文 I think just uses possessive particles instead
  + _quantifier:_
    + "all", "some", "few"
    + "tout", "plusieurs"
    + 都
  + _distributive determiner:_
    + "each", "any", "either"
    + "chaque", "n'importe lequel"
  + _interrogative:_ to ask a question
    + "which", "what", "who", "whose":
    + "quel", "qui", "qu'est-ce que" 

# particle 
--------------------------------------------------------------------------------

a function word. doesn't really exist in anglais/français. a few in 中文. more 
on the wikipedia page for chinese particles
  + 的: possession indicator (我的朋友), topic marker 
  + 等: used at end of list, means "for example", "such as"
  + 个: measure word/counter
  + 还: "also", "even", "still"
  + 可: "could", "XXX-able"
  + 了: indicates a completed action
  + 吗: question 
  + 是: copula "to be"
  + 也: "also"
  + and a few more...
	
# noun cases
--------------------------------------------------------------------------------

common cases in modern languages:

* __nominative:__ subject of a finite verb
	+ "_you_ went to the pool"
	+ "_tu_ es allé(e) à la piscine"

* __accusative:__ direct object of a transitive verb
	+ "you looked at _me_"
	+ "tu vis _à moi_" | "tu _m_'as vis" 

* __dative:__ indirect object of a transitive verb
	+ "you gave _me_ a dog"
	+ "tu _m_'as donnée un chien"| "tu as donnée un chien _à moi_" 

* __ablative:__ movement away from 
	+ "you went _from me_ to them"

* __genitive:__ possessor of another noun
	+ "_her_ book" | "_Quasar's_ book"
	+ "_sa_ livre" | "le livre _de Jean-Vinzo_"
	+ 中文 doesn't have this. uses the possessive particle

# verbs
--------------------------------------------------------------------------------

* __transitive verb:__ a verb that takes one/more object, expressed or implied
* __intransitive verb:__ a verb with no object

# verb tense
--------------------------------------------------------------------------------

* 中文 doesn't have tense markers [as this would be just silly]. instead it has 
  aspect markers [see: verb aspect]. english and français do mark tense
* français conjugates every tense and aspect of a verb. english doesn't but has 
  helper verbs to indicate tenses. 

* present: recurring/habitual events
* past:
  + past perfective / passé simple: past events in a perfective aspect [wholly 
    completed]
  + past imperfective / passé imparfait: past events in an imperfective aspect 
* [simple] future / futur simple: future events
* conditional / conditionnel: aka future-in-past. "I _would_ do X"; "je 
  _voudrais_ coucher avec toi"

# verb aspect
--------------------------------------------------------------------------------

* __perfective aspect:__ indicates a wholly completed action in the past
	+ "I _have_ eaten", "I _ate_"
	+ "J'ai mangé"
	+ 我了吃 -> I [perfective aspect particle] eat -> I ate

* __imperfective aspect:__ indicates a not wholly completed action in the past
	+ "I _was eating_"
	+ "Je manjais" [use the present participle conjugation] 
	+ 中文 has two imperfective aspect markers: 在 and 着
	+ 在 precedes the verb, used for ongoing events:
	+ 我在吃 -> I [imperfective aspect particle] eat -> I am eating 
	+ 着 follows the verb, used for static events:
	+ 雪正下着呢 -> snow rectifies[?] down/below [imperfective aspect particle] [auxiliary particle] -> it's snowing

* __experiential aspect:__ indicates the subject "has experienced" the thing. doesn't exist in french [well, not as an aspect anyway. this would be expressed as a verb mood I think]
	+ the experiential particle: 过, guò
	+ 我过吃 -> I [imperfective aspect particle] eat -> I have once eaten / I once ate

## possessive
--------------------------------------------------------------------------------

* broadly: possessions can be formed from pronouns or nouns

## clauses
--------------------------------------------------------------------------------

* __main clause/independent clause:__ it could stand alone. usually there's a subordinate/dependent clause, or a relative pronoun connecting it to a subordinate clause
	+ _I was eating_ when I saw it
	+ _Je mangais_ quand je l'ai vu

* __subordinate clause/dependent clause:__ can't stand alone. connected to a main clause
	+ I was eating _when I saw it_
	+ Je mangais _quand je l'ai vu_

# relative pronouns
--------------------------------------------------------------------------------

* __english:__ uses relative pronouns "who", "whom", "which", "that"
	+ the relative pronoun goes at the beginning of the relative clause:
	+ "the person _which_ I was interacting with"
	+ unless: preceded by a __fronted preposition:__
	+ "the person _with_ whom I was interacting"
	+ whom is used instead of who when its antecedent is the object of the relative clause
	+ __relative pronouns in english are sometimes optional__, e.g. could say "the person I was interacting with"

* __français:__ uses relative pronouns "qui", "que", "lequel", "dont", "où" 
	+ relative pronouns are mandatory in français
	+ _qui:_ refers to subject or indirect object (if person)
		+ la personne _qui_ a gagné le match
	+ _que:_ refers to direct object
		+ le gâteau _que_ j'ai mangé 
	+ _dont:_ object of "de", indicates possession, means like "of which", "that which", etc
		+ l'homme dont je parle habite ici
	+ _lequel:_ refers to indirect object
		+ la ville à lequel je..., le livre dans lequel je... 
		+ can also be used as an interrogative pronoun to replace "quel + noun":
		+ "quel livre veux-tu?" / "lequel veux-tu?" -> which [book] do you want?
	+ _où:_ indicates place or time, means "when/where/which/that"
		+ la ville où j'ai appris la chinois
	+ also: __indefinite relative pronouns:__ don't have a specific antecedent, "ce que/que/qu'/dont/quoi"
		+ "c'est ce que je détèste" -> that's what I hate
		+ "ce que je veux, c'est être trilingue" -> what I want is to be...
		+ "c'est ce qui me dérange" -> that's what annoys me
	
* __中文:__ precedes the noun that it modifies, ends with particle 的 [similar to other adjectival phrases]
	+ if the relative clause has an object but no subject:
	+ 

# morphemes
--------------------------------------------------------------------------------

* __free morpheme:__ an atomic unit, can stand alone
	+ "factual", fact-u-al, "fact" is a free morpheme, "u" and "al" are bound morphemes
	+ "中国人" | Zhōng-guó-rén, composed of entirely free morphemes [which is typical of chinese. see: analytic languages]

* __bound morpheme:__ an atomic unit, only appears as part of larger structures
	+ _derivational morpheme:_ modifies the semantic meaning or part of speech of the word, e.g. "sad-_ness_", where the bound morpheme "-ness" changes word from an adjective to a noun
	+ _inflectional morpheme:_ modifies the tense/aspect/person/number of a verb or number/gender/case of a noun/adjective/pronoun, e.g. in english adding "-s" to the end of words, or all the verb conjugations in french and other european languages

# analytic vs synthetic languages
--------------------------------------------------------------------------------

* __analytic language:__ relies on word order and helper words [particles/prepositions/...] to convey relationships between words. has few/no inflectional morphemes 
	+ since chinese has mostly unbound morphemes [separate words], it is very analytic (few inflectional morphemes) 
	+ english is also analytic (but less so than chinese). it also relies on helper words/grammatical markers to convey relations between words. but it also uses inflections, so it has synthetic character too. 

* __isolating language:__ language with a low morpheme-to-word ratio (any kind of morpheme). an isolating language is by definition an analytic language, since it must have few/no inflectional morphemes. but not all analytic languages are necessarily isolating languages
	+ chinese has a high morpheme-to-word ratio since it has many compound words. but since it uses mostly unbound morphemes and has few inflectional morphemes, it's analytic, very analytic
	+ analytic = good?

* __synthetic language:__ rely on inflections [aka agglutinations] to convey relationships between words
	+ _derivational synthesis:_ where morphemes of different types [nouns, verbs, affixes] are joined to create new words. e.g. german [those massive words] 
	+ _relational synthesis:_  where root words are joined to bound morphemes. e.g. italian, "communicandovele", communic-ando-ve-le, communicate + gerund suffix + you pl. + those fem. pl.¸ or hungarian, "szeretlek", szeret-lek, love + I [reflexive] you 

# logograph(y?)
--------------------------------------------------------------------------------

* __logogram:__ a single written character representing a morpheme. 
* chinese has a logographic writing system: there are thousands of logograms to represent all the words in the language
