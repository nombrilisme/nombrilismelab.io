---
title: 注音
type: term
domain: 中文
---

注音 / zhūyīn / bopomofo

# ONSETS
--------------------------------------------------------------------------------

    ㄅ	b
    ㄆ	p
    ㄇ	m
    ㄈ	f
    ㄉ	d
    ㄊ	t
    ㄋ	n
    ㄌ	l
    ㄍ	g
    ㄎ	k
    ㄏ	h
    ㄐ	j
    ㄑ	q
    ㄒ	x
    ㄓ	zh, zhi
    ㄔ	ch, chi
    ㄕ	sh, shi
    ㄖ	r, ri
    ㄗ	z, zi
    ㄘ	c, ci
    ㄙ	s, si


# RHYMES
--------------------------------------------------------------------------------

    ㄧ	-i, yi
    ㄨ	-u, w, wu
    ㄩ	-ü, yu
    ㄚ	a
    ㄛ	o
    ㄜ	e
    ㄝ	ê, ye
    ㄞ	ai
    ㄟ	ei
    ㄠ	ao
    ㄡ	ou
    ㄢ	an
    ㄣ	en, -n
    ㄤ	ang
    ㄥ	eng, -ng
    ㄦ	er, -r


# diph/triphthongs
--------------------------------------------------------------------------------

    ing 	ㄧㄥ [i-eng]
    ong 	ㄨㄥ [u-eng]
    iong 	ㄩㄥ [ü-eng]
    ie	ㄧㄝ [i-ê]
    ue	ㄩㄝ [ü-ê]
    ui 	ㄨㄟ [u-ei]
    iu	ㄧㄡ [i-ou]
    -er	ㄜㄦ [e-er]
    ye	ㄧㄝ [i-e]


# using the fcitx keyboard
--------------------------------------------------------------------------------

the qwerty keyboard mapping preserves the "natural" order of zhūyīn symbols, 
e.g.  the symbols for b,p,m,f,d,t,n,l map to 1,q,a,z,2,w,s,x and so on (for a ≥ 
60% keyboard).

there's a slightly strange input order in fcitx-libpinyin: to type a morpheme 
you have to enter the rhyme first, _then_ its preceding onset, e.g. to type 
"mao" you first press the key for "ao" and then the key for "m". it doesn't let 
you do it the other (more logical?) way around. 

BUT you don't have to do this for onsets that aren't followed by rhymes, namely 
zh,ch,sh,r,q,c,x,s (i.e. it only imposes the restriction to onsets that can't 
exist singularly).

