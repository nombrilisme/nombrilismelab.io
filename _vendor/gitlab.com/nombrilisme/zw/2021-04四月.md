---
title: 2021 四月
time: 202104
type: term
domain: 中文
---

# 2021年4月1日

- 4月 fools!
- 2011 audi s1

# 2021年4月2日

- grammar day
- down to 4 cards left in the review

# 2021年4月3日

- we finished the deck!

# 2021年4月4日

- grammar point: always with 总是

vocab:
- 忘记 -> wàngjì -> forget
- 一个人 -> in addition to meaning "one person", also means "alone"
- 有意思 -> "interesting" (adjectival), "has meaning"
- 特别 -> tèbié -> particularly
- 干净 -> gānjìng -> clean
- 开心 -> kāixīn -> happy

# 2021年4月9日

- reading day: flavoured potato chips

# 2021年4月11日

- grammar day: expressing and also with 还

card to make:

predicate 1 + and also + predicate 2

predicate 1 + 还 + predicate 2

# 2021年4月13日

- grammar day: expressing "just" with 刚

card to make:

subject + 刚【刚】+ verb + （duration）

subject "just" did something (a specified duration ago)  

# 2021年4月26日

- grammar day: expressing only with 只
